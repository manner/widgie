// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "widgie/application.h"
#include "widgie/button.h"
#include "widgie/label.h"
#include "widgie/uidefs.h"
#include "widgie/window.h"
#include "widgie/log.h"
#include "widgie/scrollarea.h"
#include "widgie/linearlayout.h"
#include "widgie/gridlayout.h"
#include "widgie/configstore.h"
#include "widgie/settings.h"

#include <stddef.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

static const char *BTN_TEXT_START = " Start";
static const char *BTN_TEXT_PAUSE = " Pause";
static const char *BTN_TEXT_MARK = "\uF097 Mark";
static const char *BTN_TEXT_RESET = " Reset";

static struct WGStopper
{
    struct WGLabel *label;
    struct WGGridLayout *result_layout;
    struct WGLabel *button1_label;
    struct WGLabel *button2_label;
    int64_t begin_time;
    int64_t position;
    int round;
} stopper = { NULL };

static void stopper_update(void *userdata);

static void
schedule_update(void)
{
    wg_application_add_event_timer(30, stopper_update, NULL);
}

static void
stopper_set_position_on_label(struct WGLabel *label, int show_round)
{
    long msecs = stopper.position;
    long secs = msecs / 1000;
    msecs -= secs * 1000;
    long mins = secs / 60;
    secs -= mins * 60;

    static char st[80];  // TODO: asprintf wrapper api for label? or "take memory", or just overwrite after sanity checks
    char *p = st;
    size_t max_len = sizeof(st);
    if (show_round) {
        int len = snprintf(p, max_len, "%d.: ", stopper.round);
        max_len -= len;
        p += len;
    }

    snprintf(p, max_len, "%ld:%02ld.%03ld", mins, (long)secs, msecs);
    st[79] = '\0';
    wg_label_set_text(label, st);
}

static void
stopper_update(void *userdata)
{
    if (stopper.begin_time <= 0) {
        return;
    }

    stopper.position = wg_application_time_ms() - stopper.begin_time;
    stopper_set_position_on_label(stopper.label, 0);

    schedule_update();
}

static void
stopper_playpause(void)
{
    if (stopper.begin_time <= 0) {
        // start the stopper
        stopper.begin_time = wg_application_time_ms() - stopper.position;
        schedule_update();
        wg_label_set_text(stopper.button1_label, BTN_TEXT_PAUSE);
        wg_label_set_text(stopper.button2_label, BTN_TEXT_MARK);
    } else {
        // pause the stopper
        stopper.position = wg_application_time_ms() - stopper.begin_time;
        stopper.begin_time = 0;
        stopper_set_position_on_label(stopper.label, 0);
        wg_label_set_text(stopper.button1_label, BTN_TEXT_START);
        wg_label_set_text(stopper.button2_label, BTN_TEXT_RESET);
    }
}

static void
stopper_reset(void)
{
    stopper.round = stopper.begin_time = stopper.position = 0;
    stopper_set_position_on_label(stopper.label, 0);
    wg_label_set_text(stopper.button1_label, BTN_TEXT_START);
    wg_label_set_text(stopper.button2_label, BTN_TEXT_RESET);

    wg_grid_layout_reset_children(stopper.result_layout, 1, 0, 1);
    WG_WIDGET(stopper.result_layout)->height = 0;
}

static int
stopper_clicked(struct WGWidget *widget)
{
    (void) widget;
    stopper_playpause();
    return 1;
}

static int
stopper_longclicked(struct WGWidget *widget)
{
    (void) widget;
    stopper_reset();
    return 1;
}

static void
stopper_mark_position(void)
{
    stopper.position = wg_application_time_ms() - stopper.begin_time;
    ++stopper.round;

    struct WGWidget *mark_label = wg_label_new("");
    stopper_set_position_on_label(WG_LABEL(mark_label), 1);
    wg_label_resize_oneline(WG_LABEL(mark_label));

    ++stopper.result_layout->rows;
    wg_grid_layout_add(stopper.result_layout, mark_label);
    WG_WIDGET(stopper.result_layout)->height += mark_label->height;

    wg_scrollarea_scroll_to_end(WG_SCROLLAREA(WG_WIDGET(stopper.result_layout)->parent));
}

static int
button_clicked(struct WGWidget *button)
{
    struct WGLabel *label = WG_LABEL(wg_button_get_content(WG_BUTTON(button)));
    if (label == stopper.button1_label) {
        stopper_playpause();

    } else if (strcmp(label->text, BTN_TEXT_MARK) == 0) {
        stopper_mark_position();

    } else if (strcmp(label->text, BTN_TEXT_RESET) == 0) {
        stopper_reset();
    }
    return 1;
}

static int
stopper_mouse_press(struct WGWidget *widget, int x, int y)
{
    return 1; // grab mouse for a potential click
}

static void
create_window(void)
{
    const int row_height = 80;

    // container for paused results
    stopper.result_layout = WG_GRID_LAYOUT(wg_grid_layout_new(1, 0));
    struct WGWidget *scroll = wg_scrollarea_new(WG_WIDGET(stopper.result_layout));
    WG_SCROLLAREA(scroll)->snap_alignment = WGSnapAlignEnd;

    struct WGWidget *label = wg_label_new("0:00.000");
    stopper.label = WG_LABEL(label);
    stopper.label->text_height = wg_settings()->text.height * 3;  // TODO: configurable?
    label->on_mouse_press = stopper_mouse_press;
    label->on_mouse_click = stopper_clicked;
    label->on_mouse_longclick = stopper_longclicked;

    struct WGGridLayout *button_layout = WG_GRID_LAYOUT(wg_grid_layout_new(2, 1));
    struct WGWidget *button1 = wg_button_new_with_label(BTN_TEXT_START);
    button1->on_mouse_click = button_clicked;
    wg_grid_layout_add(button_layout, button1);
    struct WGWidget *button2 = wg_button_new_with_label(BTN_TEXT_RESET);
    button2->on_mouse_click = button_clicked;
    wg_grid_layout_add(button_layout, button2);
    stopper.button1_label = WG_LABEL(wg_button_get_content(WG_BUTTON(button1)));
    stopper.button2_label = WG_LABEL(wg_button_get_content(WG_BUTTON(button2)));

    struct WGLinearLayout *app_layout = WG_LINEAR_LAYOUT(wg_linear_layout_new(WG_Vertical));
    wg_linear_layout_add(app_layout, scroll, 1);
    label->height = row_height;
    wg_linear_layout_add(app_layout, label, 0);
    WG_WIDGET(button_layout)->height = row_height;
    wg_linear_layout_add(app_layout, WG_WIDGET(button_layout), 0);

    wg_app->window = wg_window_new("Widgie stopper", WG_PreferredSize);
    wg_window_set_size_hint(wg_app->window, 800 * wg_settings()->scale, 600 * wg_settings()->scale);
    wg_window_set_content(wg_app->window, WG_WIDGET(app_layout), 0);
}

int
main(int argc, char **argv)
{
    wg_application_new();
    if (wg_app == NULL)
        return 1;

    wg_config_load(argc, argv, NULL);

    create_window();

    int rc = wg_application_run();

    wg_application_free();
    return rc;
}
