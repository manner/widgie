#! /bin/bash

set -euo pipefail

root="$(dirname "$0")"
bin="$root/../../build/widgie-counter"
slowness=${1:-}

slowdown() {
  if [ "$slowness" != "" ]; then
    sleep "$slowness"
  fi
}

testinput() {
    echo -n "tw"
    slowdown
    echo o
    slowdown
    echo "one"
    slowdown
    echo "two"
    slowdown
    echo -e "five\nfive\nfive\nfive\nfive"
}

testinput | "$bin"
