// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "widgie/application.h"
#include "widgie/button.h"
#include "widgie/inputfield.h"
#include "widgie/uidefs.h"
#include "widgie/window.h"
#include "widgie/log.h"
#include "widgie/settings.h"
#include "widgie/scrollarea.h"
#include "widgie/linearlayout.h"
#include "widgie/gridlayout.h"
#include "widgie/configstore.h"
#include "widgie/utf8.h"

#include <xkbcommon/xkbcommon-keysyms.h>

#include <stddef.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

static struct WGGridLayout *item_layout = NULL;
static struct WGInputField *add_input = NULL;
static struct WGWidget *scroll = NULL;
static struct WGWidget *money_line = NULL;
static struct WGWidget *money_input = NULL;
static struct WGWidget *sum_label = NULL;
static long long sum = 0;

struct CounterButton {
    struct WGButton button;
    char *text;
    int count;
    long long money;
};

static void
counter_button_free(struct WGWidget *self)
{
    free(((struct CounterButton *)self)->text);
    wg__button_free(self);
}

static void
counter_button_update_text(struct CounterButton *self)
{
    char *new_text = NULL;
    struct WGLabel *label = WG_LABEL(wg_button_get_content(WG_BUTTON(self)));
    int rc = 0;
    if (self->money == 0) {
        rc = asprintf(&new_text, "%s: %d", self->text, self->count);
    } else {
        rc = asprintf(&new_text, "%s: %d (x $%lld)", self->text, self->count, self->money);
    }
    if (rc  >= 0) {
        wg_label_set_text(label, new_text);
        free(new_text);
    }
}

static void
counter_button_inc_count(struct CounterButton *self)
{
    ++self->count;
    counter_button_update_text(self);

    // update sum
    sum += self->money;
    if (sum != 0 || self->money != 0) {
        char *new_text = NULL;
        int rc = asprintf(&new_text, "Sum: $%lld", sum);
        if (rc  >= 0) {
            wg_label_set_text(WG_LABEL(sum_label), new_text);
            free(new_text);
        }
    }
}

static int
counter_button_clicked(struct WGWidget *self)
{
    counter_button_inc_count((struct CounterButton *)self);
    return 1;
}

static struct CounterButton *
counter_button_new(const char *text, long long money)
{
    struct CounterButton *btn = malloc(sizeof(struct CounterButton));
    wg__button_init(WG_BUTTON(btn));
    wg_button_set_content(WG_BUTTON(btn), WG_WIDGET(wg_label_new("")), 1);
    WG_WIDGET(btn)->free = counter_button_free;
    WG_WIDGET(btn)->height = wg_settings()->text.height * 6;  // TODO: configurable
    WG_WIDGET(btn)->on_mouse_click = counter_button_clicked;
    btn->text = strdup(text);
    btn->count = 0;
    btn->money = money;
    counter_button_update_text(btn);
    return btn;
}

static struct CounterButton *
add_new_button(const char *text, int money)
{
    struct CounterButton *cbutton = counter_button_new(text, money);
    wg_grid_layout_add(item_layout, WG_WIDGET(cbutton));
    item_layout->rows = WG_LAYOUT(item_layout)->children_count;
    WG_WIDGET(item_layout)->height = item_layout->rows * WG_WIDGET(cbutton)->height;
    wg_widget_request_rerender(scroll);
    return cbutton;
}

static struct CounterButton *
find_button(const char *text)
{
    int count = WG_LAYOUT(item_layout)->children_count;
    struct WGWidget **pwidget = WG_LAYOUT(item_layout)->children;
    for (int i = 0; i < count; ++i, ++pwidget) {
        if (wg_widget_is_hidden(*pwidget))
            continue;
        struct CounterButton *button = (struct CounterButton *)*pwidget;
        if (strcmp(button->text, text) == 0) {
            return button;
        }
    }
    return NULL;
}

static int
add_button_clicked(struct WGWidget *sender)
{
    const char *text = NULL;
    if (add_input == NULL || (text = wg_inputfield_get_text(add_input)) == NULL || (*text) == '\0')
        return 1;

    char *num_endptr = NULL;
    const char *money_str = wg_label_get_text(WG_LABEL(money_input));
    long long money = strtoll(money_str, &num_endptr, 10);
    if (*money_str != '\0' && *num_endptr != '\0') {
        // the number is invalid
        // TODO: some visual error
        return 0;
    }

    add_new_button(text, money);
    wg_scrollarea_scroll_to_end(WG_SCROLLAREA(scroll));

    wg_inputfield_set_text(add_input, "");
    wg_inputfield_set_text(WG_INPUTFIELD(money_input), "");
    return 1;
}

static int
money_open_button_clicked(struct WGWidget *sender)
{
    int money_line_disappears = wg_widget_is_visible(money_line);
    wg_widget_set_hidden(money_line, money_line_disappears);
    wg_window_set_focus(wg_app->window, money_line_disappears ? WG_WIDGET(add_input) : money_input);

    wg_layout_request_relayout(WG_LAYOUT(money_line->parent));
    return 1;
}

static void
on_keypress(void *userdata, const char *utf8, uint32_t keysym)
{
    (void) userdata;
    (void) utf8;
    // TODO: maybe button group instead so we can move with keys (move key handling to common code)
    switch (keysym) {
        case XKB_KEY_Tab:  // TODO: should be handled by the window
            wg_window_set_focus(wg_app->window, add_input->is_focused && wg_widget_is_visible(money_line) ? money_input : WG_WIDGET(add_input));
            break;
        case XKB_KEY_Return:
            add_button_clicked(NULL);
            break;
    }
}

static void
create_window(void)
{
    // container for paused results
    item_layout = WG_GRID_LAYOUT(wg_grid_layout_new(1, 0));
    scroll = wg_scrollarea_new(WG_WIDGET(item_layout));

    struct WGWidget *add_button = wg_button_new_with_label("+");
    add_button->on_mouse_click = add_button_clicked;
    add_button->width = wg_settings()->text.height * 4; // TODO: configurable

    struct WGWidget *money_open_button = wg_button_new_with_label("$");
    money_open_button->on_mouse_click = money_open_button_clicked;
    money_open_button->width = add_button->width;

    struct WGWidget *space = wg_widget_new();
    space->width = wg_settings()->text.height * 2; // TODO: configurable

    add_input = WG_INPUTFIELD(wg_inputfield_new());

    struct WGLinearLayout *header_line = WG_LINEAR_LAYOUT(wg_linear_layout_new(WG_Horizontal));
    WG_WIDGET(header_line)->height = WG_WIDGET(add_button)->width; // TODO: configurable
    wg_linear_layout_add(header_line, space, 0);
    wg_linear_layout_add(header_line, WG_WIDGET(add_input), 1);
    wg_linear_layout_add(header_line, money_open_button, 0);
    wg_linear_layout_add(header_line, add_button, 0);

    money_line = wg_linear_layout_new(WG_Horizontal);
    money_line->height = WG_WIDGET(header_line)->height;
    space = wg_widget_new();
    space->width = wg_settings()->text.height * 2; // TODO: configurable
    wg_linear_layout_add(WG_LINEAR_LAYOUT(money_line), space, 0);
    struct WGWidget *money_text = wg_label_new("$ ");
    wg_label_resize_oneline(WG_LABEL(money_text));
    wg_linear_layout_add(WG_LINEAR_LAYOUT(money_line), money_text, 0);
    money_input = wg_inputfield_new();
    // TODO wg_inputfield_set_filter(WGInputFilterNumber);
    wg_linear_layout_add(WG_LINEAR_LAYOUT(money_line), money_input, 1);
    wg_widget_set_hidden(money_line, 1);

    sum_label = wg_label_new("");
    sum_label->height = WG_WIDGET(header_line)->height;

    struct WGLinearLayout *app_layout = WG_LINEAR_LAYOUT(wg_linear_layout_new(WG_Vertical));
    wg_linear_layout_add(app_layout, WG_WIDGET(header_line), 0);
    wg_linear_layout_add(app_layout, WG_WIDGET(money_line), 0);
    wg_linear_layout_add(app_layout, scroll, 1);
    wg_linear_layout_add(app_layout, sum_label, 0);

    wg_app->window = wg_window_new("Widgie counter", WG_PreferredSize);
    wg_window_set_size_hint(wg_app->window, 800 * wg_settings()->scale, 600 * wg_settings()->scale);
    wg_window_set_content(wg_app->window, WG_WIDGET(app_layout), 0);
    wg_window_set_focus(wg_app->window, WG_WIDGET(add_input));

    wg_application_watch_keyboard(on_keypress, NULL);
}

void
stdin_new_input(void *)
{
    static char line[128] = {'\0'};
    static int pos = 0;

    const int buffer_max = sizeof(line) / sizeof(*line) - 1;
    int tmp = 0;

    // read to buffer:
    ssize_t size = read(0, line + pos, buffer_max - pos);  // one read is garanteed to not block
    wg_log_debug("Read %ld bytes from stdin, buffer content: >>%s<<\n", size, line);
    if (size < 0) {
        wg_log_error("Read error: %s\n", strerror(errno));
        return;
    }
    line[size + pos] = '\0';

    // process all lines in buffer:
    char *line_begin = line;
    char *line_end = NULL;
    while ((line_end = wg_u8_strchr(line_begin, '\n', &tmp)) != NULL)
    {
        *line_end = '\0';

        // and or increase the item
        struct CounterButton *button = find_button(line_begin);
        if (button == NULL) {
            button = add_new_button(line_begin, 0);
        }
        if (button != NULL) {
            counter_button_inc_count(button);
        }

        line_begin = line_end + 1;
    }

    // store remaining content (after the last new line)
    char *lpos;
    for (lpos = line; *line_begin != '\0'; ++line_begin, ++lpos) {
        *lpos = *line_begin;
    }
    pos = lpos - line;
    *lpos = '\0';

    if (size > 0) {  // size == 0 means end of file
        wg_application_add_event_fd(0, stdin_new_input, NULL);
    }
}

int
main(int argc, char **argv)
{
    wg_application_new();
    if (wg_app == NULL)
        return 1;

    wg_config_load(argc, argv, NULL);

    wg_application_add_event_fd(0, stdin_new_input, NULL);

    create_window();

    int rc = wg_application_run();

    wg_application_free();
    return rc;
}
