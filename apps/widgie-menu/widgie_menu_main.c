// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "widgie/application.h"
#include "widgie/inputfield.h"
#include "widgie/uidefs.h"
#include "widgie/window.h"
#include "widgie/button.h"
#include "widgie/button_group.h"
#include "widgie/iconlabel.h"
#include "widgie/label.h"
#include "widgie/gridlayout.h"
#include "widgie/linearlayout.h"
#include "widgie/scrollarea.h"
#include "widgie/log.h"
#include "widgie/utf8.h"
#include "menu_config.h"

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <xkbcommon/xkbcommon-keysyms.h>

static enum WGOrientation orientation;
#define SIZE(name) (orientation == WG_Portrait ? menu_settings.portrait_##name : menu_settings.landscape_##name)

static struct WGButtonGroup *button_group = NULL;
struct WGWidget *apps_scrollarea = NULL;
static struct WGWidget *input = NULL;
struct WGWidget *title_line = NULL;
static struct WGLinearLayout *grids = NULL;


static int
on_button_clicked(struct WGWidget *button)
{
    struct WGIconLabel* iconlabel = WG_ICON_LABEL(wg_button_get_content(WG_BUTTON(button)));
    char *text = wg_icon_label_to_text_line(iconlabel);
    if (text == NULL)
        return 1;

    printf("%s", text);
    free(text);
    wg_window_close(wg_app->window);
    return 1;
}

int
strarray_any_matches(char **list, const char *str)
{
    if (str == NULL || list == NULL)
        return 0;

    for (char **pitem = list; *pitem != NULL; ++pitem) {
        if (strstr(str, *pitem) != NULL) {
            return 1;
        }
    }
    return 0;
}

static inline int
is_special_menu(const char *menu)
{
    return strarray_any_matches(menu_settings.special_menus, menu);
}

static inline int
is_separator_menu(const char *menu)
{
    return strarray_any_matches(menu_settings.separator_menus, menu);
}

static const char *
button_text(struct WGButton *button)
{
    struct WGIconLabel *icon_label = WG_ICON_LABEL(wg_button_get_content(button));
    return icon_label->label->text;
}

static void
update_grid_columns(int change_column_count)
{
    int max_columns = SIZE(grid_columns);
    int button_width = SIZE(button_width);
    int button_height = SIZE(button_height);

    int max_width = 0;
    for (int i = 1; i < WG_LAYOUT(grids)->children_count; i+=2)
    {
        struct WGGridLayout *apps_list = WG_GRID_LAYOUT(WG_LAYOUT(grids)->children[i]);
        int cell_count = WG_LAYOUT(apps_list)->children_count;
        if (change_column_count) {
            apps_list->columns = wg_min(cell_count, max_columns);
        } else {
            cell_count = wg_layout_visible_children_count(WG_LAYOUT(apps_list));
        }

        apps_list->rows = wg_grid_layout_rows_for_columns(cell_count, apps_list->columns);
        WG_WIDGET(apps_list)->height = apps_list->rows * button_height;
        WG_WIDGET(apps_list)->width = apps_list->columns * button_width;
        if (WG_WIDGET(apps_list)->width > max_width)
            max_width = WG_WIDGET(apps_list)->width;
        wg_layout_request_relayout(WG_LAYOUT(apps_list));
        wg_log_debug("Computed table: %dx%d, size: %dx%d\n", apps_list->columns, apps_list->rows,
                  WG_WIDGET(apps_list)->width, WG_WIDGET(apps_list)->height);
    }
    wg_layout_request_relayout(WG_LAYOUT(title_line));

    wg_linear_layout_fit_to_content(WG_LINEAR_LAYOUT(grids));
    WG_WIDGET(grids)->width = max_width;
}

static void
on_filter_change(struct WGLabel *label)
{
    const char *filter = wg_label_get_text(label);
    wg_log_debug("Filter changed: %s\n", filter);

    struct WGButtonGroupItem *button_to_select = NULL;

    for (struct WGButtonGroupItem *item = button_group->first; item != NULL; item = item->next)
    {
        const char *btext = button_text(item->button);
        if (btext != NULL) {
            const char *found_pos = wg_u8_strcasestr(btext, filter);
            int hidden = (found_pos == NULL);
            if (button_to_select == NULL && btext == found_pos) {
                button_to_select = item;
            }
            wg_log_debug("Matching filter %s with %s? %d\n", filter, btext, hidden);
            wg_widget_set_hidden(WG_WIDGET(item->button), hidden);
        }
    }

    update_grid_columns(0);

    if (button_to_select != NULL)
        wg_button_group_select(button_group, button_to_select);
    else
        wg_button_group_select_first(button_group);
}

static void
resize_window_for_items(void)
{
    int window_width = wg_max(WG_WIDGET(grids)->width, SIZE(button_width));
    wg_log_debug("window_width=%d\n", window_width);
    int first_label_height = (WG_LAYOUT(grids)->children_count > 0 &&
                              wg_widget_is_visible(WG_LAYOUT(grids)->children[0])) ?
        WG_LAYOUT(grids)->children[0]->height : 0;

    int max_grid_height = SIZE(grid_rows) * SIZE(button_height) + first_label_height;
    int grid_height = max_grid_height < WG_WIDGET(grids)->height ? max_grid_height : WG_WIDGET(grids)->height;
    wg_log_debug("grid_height=%d max_grid_height=%d grids->height=%d\n", grid_height, max_grid_height, WG_WIDGET(grids)->height);
    int window_height = menu_settings.title_line_height + grid_height;

    wg_window_set_size_hint(wg_app->window, window_width, window_height);
}

static void
orientation_change(enum WGOrientation new_orientation)
{
    wg_log_debug("Orientation change: %s\n", orientation == WG_Portrait ? "WG_Portrait" : "WG_Landscape");
    orientation = new_orientation;

    update_grid_columns(1);
    resize_window_for_items();
    if (*wg_label_get_text(WG_LABEL(input)) != '\0') {
        update_grid_columns(0);
    }

    wg_window_invalidate_layouts(wg_app->window);
    struct WGWidget *selected_button = button_group->selected != NULL ? WG_WIDGET(button_group->selected->button) : NULL;
    if (selected_button != NULL && wg_widget_is_visible(selected_button)) {
        wg_widget_request_showme(selected_button);
    } else {
        wg_scrollarea_scroll_to_start(WG_SCROLLAREA(apps_scrollarea));
    }
}

static void
button_group_keypress(struct WGButtonGroup *button_group, const char *utf8, uint32_t keysym)
{
    wg_log_debug("Got key: %s %u\n", utf8, keysym);

    switch (keysym) {
        case XKB_KEY_Return:
        case XKB_KEY_XF86PowerOff:  // TODO: maybe these should be phone only :)
            if (!wg_button_group_click_selected(button_group) && input != NULL) {
                const char *text = wg_label_get_text(WG_LABEL(input));
                if (text != NULL && text[0] != '\0') {
                    printf("%s", text);
                    wg_window_close(wg_app->window);
                }
            }
            break;

        case XKB_KEY_Escape:
            wg_window_close(wg_app->window);
            wg_application_request_exit(1);
            break;

        case XKB_KEY_F2:
        {
            enum WGOrientation new_ori = orientation == WG_Portrait ? WG_Landscape : WG_Portrait;
            orientation_change(new_ori);
            break;
        }

        default:
            wg_button_group_keypress(button_group, utf8, keysym);
            break;
    }
}

static const char *
easy_getline(void)
{
    static char *line = NULL;
    static size_t line_size = 0;

    int length = getline(&line, &line_size, stdin);
    if (length >= 0) {
        if (line[length - 1] == '\n')
            line[length - 1] = '\0';
        return line;
    }

    return NULL;
}

static void
load_buttons(struct WGLinearLayout *grids, struct WGLinearLayout *title_line)
{
    char *line = NULL;
    const char *line_next = NULL;
    char *last_separator = NULL;
    const char *app_name = NULL;
    struct WGGridLayout *apps_list = NULL;

    line_next = easy_getline();
    while (1)
    {
        free(line);
        line = line_next == NULL ? NULL : strdup(line_next);
        line_next = easy_getline();
        if (line == NULL)
            break;

        app_name = line;

        if (app_name[0] == ' ')
        {
            while (*app_name != '\0' && *app_name == ' ')
                ++app_name;
        }

        if (is_separator_menu(app_name))
        {
            wg_log_debug("separator: '%s'\n", app_name);
            // we have got a separator for a new section
            free(last_separator);
            last_separator = strdup(app_name);
            apps_list = NULL;  // start a new section
            continue;
        }

        struct WGWidget *icon = wg_icon_label_from_text_line(app_name);
        struct WGWidget *button = wg_button_new_with_content(icon);
        button->on_mouse_click = on_button_clicked;

        if (WG_ICON_LABEL(icon)->label && is_special_menu(WG_ICON_LABEL(icon)->label->text)) {
            button->width = menu_settings.title_line_height;  // TODO maybe configurable
            wg_label_set_text_align(WG_ICON_LABEL(icon)->icon, WG_AlignCenter);
            wg_widget_set_hidden(WG_WIDGET(WG_ICON_LABEL(icon)->label), 1);
            wg_linear_layout_add(title_line, button, 0);
        } else {
            if (apps_list == NULL) {
                struct WGWidget *separator_label = wg_label_new(last_separator ? last_separator : "");
                if (last_separator != NULL)
                    wg_label_resize_oneline(WG_LABEL(separator_label));
                else
                    wg_widget_set_hidden(separator_label, 1);
                wg_linear_layout_add(grids, separator_label, 0);

                apps_list = WG_GRID_LAYOUT(wg_grid_layout_new(2, 5));
                wg_linear_layout_add(grids, WG_WIDGET(apps_list), 0);
            }

            wg_grid_layout_add(apps_list, button);
        }

        wg_button_group_append(button_group, WG_BUTTON(button));
    }

    free(last_separator);
    free(line);
}

static void
init_window(void)
{
    wg_log_debug("\n");

    // create the layouts
    struct WGWidget *prompt_widget = wg_label_new(args.prompt);
    wg_label_resize_oneline(WG_LABEL(prompt_widget));
    prompt_widget->width += 20;

    input = wg_inputfield_new();
    WG_LABEL(input)->on_text_change = on_filter_change;

    title_line = wg_linear_layout_new(WG_Horizontal);
    wg_linear_layout_add(WG_LINEAR_LAYOUT(title_line), prompt_widget, 0);
    wg_linear_layout_add(WG_LINEAR_LAYOUT(title_line), input, 1);
    title_line->height = menu_settings.title_line_height;

    grids = WG_LINEAR_LAYOUT(wg_linear_layout_new(WG_Vertical));
    button_group = wg_button_group_new();
    load_buttons(grids, WG_LINEAR_LAYOUT(title_line));

    apps_scrollarea = wg_scrollarea_new(WG_WIDGET(grids));
    struct WGWidget *main_widget = wg_linear_layout_new(WG_Vertical);
    wg_linear_layout_add(WG_LINEAR_LAYOUT(main_widget), title_line, 0);
    wg_linear_layout_add(WG_LINEAR_LAYOUT(main_widget), apps_scrollarea, 1);

    wg_app->window = wg_window_new("Widgie menu", args.resizable ? WG_PreferredSize : WG_FixedSize);
    wg_window_set_content(wg_app->window, main_widget, 1);
    wg_window_set_focus(wg_app->window, input);  // just receives keyboard if focused
    wg_application_watch_keyboard((KeyboardHandlerFunc)button_group_keypress, button_group);  // always receives keys
}

int
main(int argc, char *argv[])
{
    wg_application_new();
    if (wg_app == NULL)
        return 1;

    int rc = process_args(argc, argv);
    if (rc != 0 && wg_app->backend == NULL && *menu_settings.terminal_app != '\0') {
        wg_log_info("Falling back to '%s' (specified by config item 'terminal_app')\n", menu_settings.terminal_app);
        rc = execl("/bin/sh", "sh", "-c", menu_settings.terminal_app, (char *) NULL);
        wg_log_error("Failed to execute /bin/sh: '%s'\n", strerror(errno));
        wg_application_free();
        return rc;
    }

    init_window();

    wg_app->orientation_change = &orientation_change;
    if (wg_app->desktop_orientation != WG_UnknownOrientation)
        orientation_change(wg_app->desktop_orientation);

    wg_application_set_keyboard_grab(1);

    rc = wg_application_run();

    wg_button_group_free(button_group);
    wg_application_free();
    return rc;
}
