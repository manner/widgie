// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "menu_config.h"

#include "widgie/configstore.h"
#include "widgie/iniparser.h"
#include "widgie/utf8.h"
#include "widgie/log.h"

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>

struct MenuSettings menu_settings = {
    .base = {
        .fname = "menu.conf",
    },
    .title_line_height = 50,
    .portrait_button_height = 100,
    .landscape_button_height = 100,
    .portrait_button_width = 175,
    .landscape_button_width = 175,
    .portrait_grid_columns = 2,
    .landscape_grid_columns = 4,
    .portrait_grid_rows = 4,
    .landscape_grid_rows = 2,
    .special_menus = NULL,
    .separator_menus = NULL,
    .terminal_app = NULL,
};

struct Args args = {};

static int
_menu_config_set(struct WGConfigStore *store, const char *key, const char *value)
{
    struct MenuSettings *settings = (struct MenuSettings *)store;

    WG_SET_CONFIG_INT(settings, title_line_height)
    else WG_SET_CONFIG_INT(settings, portrait_button_height)
    else WG_SET_CONFIG_INT(settings, landscape_button_height)
    else WG_SET_CONFIG_INT(settings, portrait_button_width)
    else WG_SET_CONFIG_INT(settings, landscape_button_width)
    else WG_SET_CONFIG_INT(settings, portrait_grid_columns)
    else WG_SET_CONFIG_INT(settings, landscape_grid_columns)
    else WG_SET_CONFIG_INT(settings, portrait_grid_rows)
    else WG_SET_CONFIG_INT(settings, landscape_grid_rows)
    else WG_SET_CONFIG_STRLIST(settings, separator_menus)
    else WG_SET_CONFIG_STRLIST(settings, special_menus)
    else WG_SET_CONFIG_STR(settings, terminal_app)
    else return EINVAL;

    return 0;
}

static int
_menu_args_set(struct WGConfigStore *store, const char *key, const char *value)
{
    #define IS_ARG_SINGLE(arg) \
        (strcmp(key, arg) == 0 && value == NULL)

    #define IS_ARG_VALUE(arg) \
        (strcmp(key, arg) == 0 && value != NULL)

    struct Args *dest = (struct Args *)store;
    wg_log_debug("Processing argument '%s' with value '%s'\n", key, value ? value : "-");
    struct WGConfigStore *settings = WG_CONFIG_STORE(&menu_settings);

    if IS_ARG_VALUE("-p") {
        dest->prompt = strdup(value);
    } else if IS_ARG_SINGLE("--resizable") {
        // TODO => shorthand for --config "[widgie] window.resize=Free"
        dest->resizable = 1;

    } else if IS_ARG_VALUE("--separator-menus") {
        _menu_config_set(settings, "separator_menus", value);

    } else if IS_ARG_VALUE("--special-menus") {
        _menu_config_set(settings, "special_menus", value);

    } else {
        return EINVAL;
    }

    return 0;
}

static void
_menu_config_free(struct WGConfigStore *store)
{
    struct MenuSettings *settings = (struct MenuSettings *)(store);
    wg_strarray_free(settings->separator_menus);
    wg_strarray_free(settings->special_menus);
    free(settings->terminal_app);
}

static int
_menu_config_print_example(struct WGConfigStore *store, int fd)
{
    struct MenuSettings *settings = (struct MenuSettings *)store;
    int rc = 0;

    #define WRITE_SETTING(element, type, description) \
        if (*description != '\0' && dprintf(fd, "\n; " description " [" #type "]\n") < 0) { \
            rc = errno; \
            goto finish; \
        } \
        if ((rc = wg_ini_parser_write_##type(fd, #element, settings->element)) != 0) \
            goto finish

    WRITE_SETTING(title_line_height, number, "Height of the title line containing the title, input field and special buttons");
    WRITE_SETTING(portrait_button_height, number, "Height of the buttons in portrait/landscape mode");
    WRITE_SETTING(landscape_button_height, number, "");
    WRITE_SETTING(portrait_button_width, number, "Width of the buttons in portrait/landscape mode");
    WRITE_SETTING(landscape_button_width, number, "");
    WRITE_SETTING(portrait_grid_columns, number, "The count of buttons shown in one row for portrait/landscape mode");
    WRITE_SETTING(landscape_grid_columns, number, "");
    WRITE_SETTING(portrait_grid_rows, number, "Specifies how many button rows will be visible on the screen at once");
    WRITE_SETTING(landscape_grid_rows, number, "");
    WRITE_SETTING(special_menus, stringlist, "If the text of a selection contains one of these strings, it will be considered a 'special button' and shown on the title bar");
    WRITE_SETTING(separator_menus, stringlist, "If the text of a selection contains one of these strings, it will be considered a separator: the title of a button group");
    WRITE_SETTING(terminal_app, string, "If widgie-menu is unable to initialize a graphical engine, it will replace itself to this application instead");

finish:
    return rc;
}

static void
_menu_config_scale_sizes(struct WGConfigStore *store, double scale)
{
    struct MenuSettings *settings = (struct MenuSettings *)store;
    settings->landscape_button_height *= scale;
    settings->landscape_button_width *= scale;
    settings->portrait_button_height *= scale;
    settings->portrait_button_width *= scale;
    settings->title_line_height *= scale;
}

static int
_menu_args_print_example(struct WGConfigStore *store, int fd)
{
    return dprintf(fd,
        "  -p title                Add a title for the window. (The title will be shown before\n"
        "                          the input field in the title bar.)\n"
        "\n"
        "  --resizable             Instead of a fix sized \"dialog\", open as a \"normal\" resizable window.\n"
        "                          The button-width config option will not have an effect, and buttons\n"
        "                          will get as much space as they can according to size of the window.\n"
        "\n"
        "  --special-menus         Overrides special menus specified in the config file.\n"
        "  --separator-menus       Overrides separator menus specified in the config file.\n"
        "\n") == -1 ? errno : 0;
}

static void
menu_config_register()
{
    struct WGConfigStore *cstore = WG_CONFIG_STORE(&menu_settings);
    cstore->set = _menu_config_set;
    cstore->print_example = _menu_config_print_example;
    cstore->scale_sizes = _menu_config_scale_sizes;
    cstore->free = _menu_config_free;
    wg_configstore_register("", cstore);

    cstore = WG_CONFIG_STORE(&args);
    cstore->set = _menu_args_set;
    cstore->print_example = _menu_args_print_example;
    cstore->free = NULL;
    wg_configstore_register("args", cstore);
}

static void
fill_defaults()
{
    if (menu_settings.separator_menus == NULL) {
        char * separator_menus[] = {
            "Call Options:", "Input:", "Output:", NULL,
        };
        menu_settings.separator_menus = wg_strarray_dup(separator_menus);
    }

    if (menu_settings.special_menus == NULL) {
        char *special_menus[] = {
            "Close Menu", "System Menu", NULL,
        };
        menu_settings.special_menus = wg_strarray_dup(special_menus);
    }

    if (menu_settings.terminal_app == NULL) {
        menu_settings.terminal_app = strdup("fzf --layout=reverse");
    }
}

int
process_args(int argc, char **argv)
{
    fill_defaults();
    menu_config_register();
    return wg_config_load(argc, argv, "menu.conf", NULL);
}
