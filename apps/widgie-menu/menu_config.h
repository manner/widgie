// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_WIDGIE_MENU_CONFIG_H
#define WG_WIDGIE_MENU_CONFIG_H

#include <widgie/configstore.h>

extern struct MenuSettings
{
    struct WGConfigStore base;
    int title_line_height;
    int portrait_button_height;
    int landscape_button_height;
    int portrait_button_width;
    int landscape_button_width;
    int portrait_grid_columns;
    int landscape_grid_columns;
    int portrait_grid_rows;
    int landscape_grid_rows;
    char **special_menus;
    char **separator_menus;
    char *terminal_app;
} menu_settings;

extern struct Args
{
    struct WGConfigStore base;
    char *prompt;
    int resizable;
} args;

int process_args(int argc, char **argv);

#endif // WIDGIE_MENU_CONFIG_H
