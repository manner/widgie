# widgie

Widgie aims to be a lightweight, minimal widget library that can be used to
create applications not needing a splash screen.

[See details.](doc/widgie.md).

## widgie apps

Until widgie matures, I keep here the applications as well which I have created
using it.

### widgie-menu

A dmenu/bemenu like menu application designed to be friendly for the finger (touch screens).

![widgie-menu-landscape](doc/widgie-menu-landscape.png)

[See details.](doc/widgie-menu.md).

### widgie-stopper

A simple stopwatch that measures how time passes: minutes, seconds, miliseconds.

![widgie-stopper](doc/widgie-stopper.png)

[See details.](doc/widgie-stopper.md).

### widgie-counter

The enterprise application that is able to count anything, anywhere. :)

You can add items as buttons, clicking on them increases their count. You can also
attach an amount to each of them (for example the money they cost) which is expressed
with the dollar sign, and the application will display their sum.

It can also be scripted: items can be added through standard input, each line adding
or increasing an entry.

![widgie-counter](doc/widgie-counter.png)

## INSTALL

Install the build dependencies (including headers, so the "dev" package if separate):

    make gcc pkgconf fontconfig freetype2 xkbcommon
    wayland (and wayland-protocols)      # if you need the wayland backend
    xkbcommon-x11 xcb xcb-image xcb-xkb  # if you need the X11 backend
    libinput                             # if you need the framebuffer backend

For running widgie-menu with nice icons you also need some nerd fonts:
SXMO by default uses dejavu-sans-mono-nerd.

For building a package, you might need some distro specific extra packages.

[See here for distro specific details.](doc/dependencies.md)

Once you have dependencies set up, clone the repository. I do some releases,
but master can be considered quite stable. (I use that myself.) Experimental
stuff is on feature branches.

Once you have the sources, build a package (only archlinux, debian and alpine
derivatives are supported currently):

    make package

The above command tries to guess from the available packaging tools which type
of package is needed. But you can also be explicit by running one of the following:

    make package-alpine  # for alpine derivatives
    make package-artix   # for artix/archlinux derivatives
    make package-debian  # for debian derivatives

You can find the resulting packages under "build/pkg" directory. Just install
them with your package manager.

Alternatively, you can compile:

    make

To only build widgie without the apps:

    make widgie

To customize the build, run "./configure" first! For example you can disable
backends, set custom paths or crosscompile. See ./configure --help for details.

It is also good idea to run "./configure" if you want to first verify which
backends will be available in the final build, and you do not miss some of
the dependencies for some of them.

    ./configure --prefix=/opt --disable-wayland
    make

The built binaries, shared objects will be placed under the "build" directory.
Copy them wherever you want. Currently apps are linked statically, so you
do not need widgie's shared object for them to work.

    # some Makefile targets can help you with install:
    make install         # installs both apps and widgie library
    make install-apps    # just installs the apps
    make install-widgie  # just installs the library

## News

[See here on whats new](NEWS.md)
