BUILDDIR=$(shell mkdir -p build/priv; echo $$PWD/build)

VERSION?=$(shell git describe --tags 2>/dev/null || sed -n 's,^## \([0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\).*,\1,p' <NEWS.md | head -1)

ifeq (,$(findstring clean,$(MAKECMDGOALS)))
include $(BUILDDIR)/config.mk
endif

export BUILDDIR
export VERSION
export DESTDIR
export PREFIX
export LIBDIR
export INCLUDEDIR
export BINDIR
export FEATURE_X11
export FEATURE_WAYLAND
export FEATURE_FRAMEBUFFER
export CC

.PHONY: all widgie apps compile_commands.json

all: widgie apps tests

$(BUILDDIR)/config.mk:
	./configure

install: install-widgie install-apps

widgie:
	$(MAKE) -C widgie

install-widgie:
	$(MAKE) -C widgie install

apps: widgie
	$(MAKE) -C apps

install-apps:
	$(MAKE) -C apps install

tests: widgie
	$(MAKE) -C tests

tests-perf: apps
	@for test in tests/perf-*.sh; do \
		echo "** Running $$test **"; \
		$$test || exit $?; \
	done

compile_commands.json:
	$(MAKE) clean
	bear -- $(MAKE)

preamble_check:
	@scripts/preamble.sh || \
	( echo "Some of the files does not contain a preamble/license notice. You can fix that by running 'scripts/preamble.sh update'."; exit 1; )
	@echo "Preamble check succeeded."

unit: widgie
	$(MAKE) -C tests unit

check: preamble_check unit

package-debian:
	ln -sf pkg/debian .; unset PREFIX DESTDIR LIBDIR INCLUDEDIR BINDIR; dpkg-buildpackage -rfakeroot -b && mkdir -p "$(BUILDDIR)/pkg" && mv ../*widgie*.deb ../*widgie_*.buildinfo ../*widgie_*.changes "$(BUILDDIR)/pkg"; rm -f debian

package-artix:
	cd "pkg/artix"; unset PREFIX DESTDIR LIBDIR INCLUDEDIR BINDIR; for recipe in PKGBUILD*; do makepkg -f -p "$$recipe" || exit 1; done && mv *.pkg.* "$(BUILDDIR)/pkg"

package-alpine:
	cd pkg/alpine; unset PREFIX DESTDIR LIBDIR INCLUDEDIR BINDIR; abuild -P "$(BUILDDIR)"

package:
	rm -rf "$(BUILDDIR)/pkg" && mkdir -p "$(BUILDDIR)/pkg"
	! command -v abuild || $(MAKE) package-alpine
	! command -v makepkg || $(MAKE) package-artix
	! command -v dpkg-buildpackage || $(MAKE) package-debian
	@printf "\nThe following packages were built:\n"
	@find "$(BUILDDIR)/pkg" -type f

clean:
	find "$(BUILDDIR)" -maxdepth 1 -mindepth 1 -not -name 'config.mk' -and -not -name 'Makefile' | xargs rm -rf

distclean:
	rm -rf "$(BUILDDIR)/"*
