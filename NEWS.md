# Widgie News

This is just a summary. For more details, see git log.

## 0.3.4
    * widgie/wayland: fixed window initializion error on wayland "buffer size is not divisible by scale".
      This has happened if the application (widgie-menu) set a fixed sizehint which is not conforming.
    * widgie-menu-run is now starting the selected command in the background, similarly to
      bemenu-run / dmenu_run. (Thanks to Sicelo A. Mhlongo)
    * widgie/scrollarea: fixed a bug where scrollarea optimized out rerender requests for big widgets
      which does not fit on the visible area.
    * widgie: some small stuff: helper to remove last item from a layout, helper macro to allocate type,
      rerender can now be requested during paint event handling, label can compute height for width (instead
      of resizing itself)

## 0.3.3

    * widgie-menu: show the title bar even if the item list is empty.

    * widgie: various small fixes. Fixed applying scale factor on window width/height, so it does not start
      up half size. Windows size hints now need to be specified scaled, like any other widget sizes. Scale
      factor of the active backend can be queried through wg_settings.
      Ini parser SET_CONFIG macros now supports different in and out variable names.

## 0.3.2

    * wayland: The wayland backend now renders bigger window if wayland display scale >= 1 avoiding
      the need for the compositor to upscale. This fixes that especially text output has looked blurry.
    * widgie: added support for events that triggers if a file descriptor receives input traffic.
      This change is not API compatible, previous wg_application_add_event functionality can be reached
      through wg_application_add_event_timer function.
    * widgie: button_group now got support for basic keypress handling.
    * widgie-counter: it can now count lines coming from standard input (same line content increases the
      same button) to be more suitable for scripting.
    * widgie/settings: new things to configure: log level, path and format; window sizehints (eg. fixed
      window with a size can be requested from config/argument which will override the application's
      preference).
    * widgie: some minors. Added helper for creating filepath under xdg cache dir. ConfigStore can now
      call a callback after loading finished. Added API to paint text directly without position cache.
      The values of enum WgSizeConstraint are now prefixed with WG_, and moved to uidefs.h.

## 0.3.1

    * widgie/config: ini parser and configstore now supports dynamic subgroups in the form of "[[groupname]]",
      or [parentgroup/subgroup]. Ini parser also has some helper defines for storing variables in
      settings struct.
    * widgie/text_rendering: improvement on fallback font lookup mechanism (we now use fontconfig's defaults
      by default, and fallback font loading became automatic up to maximum 10 fonts). Note that first of them
      is still configurable.
    * widgie: Small fixes. The layout will schedule a relayout if its children gets reset. The current locale
      now gets initialized according to environment variables. The widget_free function can be called with
      NULL argument. Fixed a memory leak in stringlist ini parsing. Default plugin search paths can
      now be queried with a separate function and widgie settings will only contain the empty list.
      Direction/orientation enum values are now prefixed with WG just like other enums.

## 0.3.0

    * widgie: added support for linux framebuffer. The backend is disabled by default, can be
      activated by "[widgie] backend.framebuffer = yes" config option. It also might need some manual
      configuration to work (orientation, keyboard layout, permissions), see widgie backend docs
      for details.

    * build: added a basic configure to avoid the need to specify variables for each make
      invocations. Simple "make" command runs it automatically, and the backend config defaults
      are changed now so all backends default to "shared" if their pkg-config dependencies
      are found, otherwise defaults to off. You can verify the result of the detection in the output.
      We also have separate "clean" and "distclean" targets now.

## 0.2.3

    * widgie/button group: added API for button group to select a button item explicitly, and to 
      select the first visible. The "Widget::showme" event now also works better.
    * widgie: application now traps on TERM/INT/HUP to to close gracefully with exit code
      of the signal to ensure backends get deinitialized. You can disable this default behaviour
      by trapping again after the application creation.
    * widgie: backend plugins (x11, wayland) are now only get loaded if the DISPLAY/WAYLAND_DISPLAY
      variable is set. This is to not waste linking time.
    * widgie-menu: it is now able to replace itself with another menu app (default is "fzf")
      in case no rendering engine (X11, wayland) could get initialized.
    * widgie-menu: filtering now also moves button selection: it selects the first button who's
      text begins with the filter (if any), otherwise selects the first match.

## 0.2.2

    * widgie/wayland: fix regression that orientation is not reported correctly when wayland reports
      rotated screen (eg. pinephone landscape mode).
      This has caused widgie-menu's window has a size of zero when opened in landscape mode.

## 0.2.1

    * apps: widgie-counter can also count money.

    * widgie/inputfield: Gains focus on click. Cursor is only shown if focused.

    * widgie: fixed some text rendering issues (characters partially disappearing) around the border

    * widgie/wayland: The wayland backend now also supports key press repeat similarly to X11.

    * build: "make package" can be used to create packages from git. Supported distros can be explicitly
      specified using "package-debian", "package-artix", "package-alpine" for debian / archlinux / alpine
      derivatives.

## 0.2.0

    * apps: Initial release of widgie-stopper.
      Start / pause / reset timer.
      Mark timer position, marked positions are scrollable.

    * widgie/mainloop: now supports time based user events.

    * widgie/scrollarea: helper functions to scroll to beginning / end.
      Better handling of small content: fixed that it can now be dragged
      freely within the overscroll limits which became configurable. Alignment
      can be specified which gets applied when scrolling finished.

    * widgie/widget: support for long click detection.

    * widgie: Added ConfigStore API for handling arguments and configs
      easily. Handles some arguments commonly: configs can be overridden
      with the "--config" option, alternative config file can be specified,
      see "--help".

    * widgie: lots of useless API changes to make it look like a lib, eg
      now functions / defines / enums has a wg prefix.

    * build: added an "install" target

    * widgie: added X11 backend. Unlike on wayland, the use of shared memory can be disabled
      though config. Support for the shared memory extension is optional, but xkeyboard extension
      is required for now. Building the X11 / wayland backends can be disabled with specifying
      FEATURE_X11=no or FEATURE_WAYLAND=no make variables during build.

    * both backends can be compiled into a separate shared object (or "plugin"), which
      widgie will load dynamically if needed. Use FEATURE_X11=shared or FEATURE_WAYLAND=shared
      to enable this. The default is wayland compiled in and x11 as shared.

    * widgie: added a scaling config option to scale up/down all sizes, which can be
      defined per backend.

    * widgie/iniparser: for lists, the final empty item is not considered any more, so user
      can express an empty list. Eg. "" means no list items, "," means one item which is empty.

    * apps: Initial release of widgie-counter. It can count anything, anywhere :)

## 0.1.0

    * widgie: Initial release of widgie widget library.
      Wayland only. Basic support for:
        - layouts (grid, linear layout),
        - scrollarea
        - and some widgets (Button, Label, InputField).
      Look and feel can be customized through configuration which can also
      get saved.

    * Initial release of widgie-menu: a dmenu like program designed for the
      finger (touch screens).
      Scrollable.
      Buttons can be filtered with the keyboard.
      Buttons can be pinned to the top avoiding scrolling.
      Items can be marked as separators instead of buttons.
      Basic configuration support.
      Default config file is designed for Simple X Mobile in mind.
