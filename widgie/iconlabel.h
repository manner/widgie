// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_ICONLABEL
#define WG_HEADER_WIDGIE_ICONLABEL

#include <widgie/linearlayout.h>

// Shows an icon and an underlying text
// Icon can currently only be an utf-8 character
struct WGIconLabel {
    struct WGLinearLayout base;

    struct WGLabel *icon;  // pointers into widgets inside the grid!
    struct WGLabel *label;
};

struct WGWidget *wg_icon_label_new(const char *icon, const char *text);
struct WGWidget *wg_icon_label_from_text_line(const char *icon_text);
char *wg_icon_label_to_text_line(struct WGIconLabel *label);

#define WG_ICON_LABEL(x) ((struct WGIconLabel *)x)

void wg_icon_label_set_text(struct WGIconLabel *label, const char *text);
void wg_icon_label_set_icon(struct WGIconLabel *label, const char *icon);

void wg_icon_label_set_text_height(struct WGIconLabel *label, int new_height);
void wg_icon_label_set_icon_text_height(struct WGIconLabel *label, int new_height);

// protected API, should only called by derived classes
void wg__icon_label_init(struct WGIconLabel *self);
void wg__icon_label_do_layout(struct WGLayout *self);

#endif // FB_HEADER_WIDGIE_ICONLABEL
