// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "button_group.h"

#include "settings.h"
#include "button.h"
#include "log.h"

#include <stdlib.h>
#include <xkbcommon/xkbcommon-keysyms.h>

#define BUTTON_GROUP_ITEM(item) ((struct WGButtonGroupItem *)(item))
#define NEXT_GROUP_ITEM(item, is_forward) (is_forward == 1 ? item->next : item->prev)

struct WGButtonGroup *
wg_button_group_new(void)
{
    struct WGButtonGroup *group = WG_BUTTON_GROUP(calloc(1, sizeof(struct WGButtonGroup)));
    group->select_rotates = wg_settings()->button.group.select_rotates;
    return group;
}

void
wg_button_group_append(struct WGButtonGroup *self, struct WGButton *button)
{
    struct WGButtonGroupItem *item = BUTTON_GROUP_ITEM(calloc(1, sizeof(struct WGButtonGroupItem)));
    item->button = button;

    if (self->last != NULL) {
        self->last->next = item;
        item->prev = self->last;
    }

    if (self->first == NULL) {
        self->first = item;
    }

    self->last = item;
}

// TODO refactor to use wg_button_group_select instead
static void
button_group_select_prev_next(struct WGButtonGroup *self,
                    struct WGButtonGroupItem *fallback,
                    int is_forward)
{
    struct WGButtonGroupItem *original_selected = self->selected;
    if (self->selected == NULL) {
        // if there is no current item, we select the first/last even if select does not rotate
        self->selected = fallback;
    } else {
        wg_button_set_selected(self->selected->button, 0);
        self->selected = NEXT_GROUP_ITEM(self->selected, is_forward);
    }

    struct WGButtonGroupItem *cycle_start = NULL;
    while (1) {
        if (self->selected == NULL) {
            if (self->select_rotates) {
                self->selected = fallback;
            } else {
                self->selected = original_selected;
                break;
            }
        }

        if (self->selected == NULL)
            break; // no more buttons to check

        if (cycle_start == NULL)
            cycle_start = self->selected;
        else if (cycle_start == self->selected) {
            self->selected = NULL;
            break; // end of list, all widgets are hidden
        }

        if (wg_widget_is_visible(WG_WIDGET(self->selected->button)))
            break; // we have found a visible widget!

        // our widget is hidden, move to the next
        self->selected = NEXT_GROUP_ITEM(self->selected, is_forward);
    }

    if (self->selected) {
        wg_button_set_selected(self->selected->button, 1);
    }
}

void
wg_button_group_select_next(struct WGButtonGroup *self)
{
    button_group_select_prev_next(self, self->first, 1);
}

void
wg_button_group_select_prev(struct WGButtonGroup *self)
{
    button_group_select_prev_next(self, self->last, 0);
}

void
wg_button_group_select(struct WGButtonGroup *self, struct WGButtonGroupItem *item)
{
    if (self->selected == item) {
        if (item != NULL) {
            wg_widget_request_showme(WG_WIDGET(item->button));
        }
        return;
    }

    if (self->selected != NULL)
        wg_button_set_selected(self->selected->button, 0);

    self->selected = item;

    if (self->selected != NULL)
        wg_button_set_selected(self->selected->button, 1);
}

void
wg_button_group_free(struct WGButtonGroup *self)
{
    if (self == NULL)
        return;

    struct WGButtonGroupItem *item = self->first;
    while (item != NULL) {
        struct WGButtonGroupItem *last = item;
        item = item->next;
        free(last);
    }
    free(self);
}

int
wg_button_group_click_selected(struct WGButtonGroup *self)
{
    if (self->selected == NULL || wg_widget_is_hidden(WG_WIDGET(self->selected->button))) {
        wg_button_group_select_next(self);
    }
    if (self->selected != NULL) {
        wg_widget_mouse_click(WG_WIDGET(self->selected->button));
        return 1;
    }
    return 0;
}

void
wg_button_group_select_first(struct WGButtonGroup *self)
{
    struct WGButtonGroupItem *item = self->first;
    while (item != NULL && wg_widget_is_hidden(WG_WIDGET(item->button))) {
        item = item->next;
    }
    wg_button_group_select(self, item);
}

void
wg_button_group_keypress(struct WGButtonGroup *self, const char *utf8, uint32_t keysym)
{
    switch (keysym) {
        case XKB_KEY_Up:
        case XKB_KEY_XF86AudioRaiseVolume:
            wg_button_group_select_prev(self);
            break;

        case XKB_KEY_Down:
        case XKB_KEY_XF86AudioLowerVolume:
            wg_button_group_select_next(self);
            break;

        case XKB_KEY_Return:
        case XKB_KEY_XF86PowerOff:  // TODO: maybe these should be phone only :)
            wg_button_group_click_selected(self);
            break;
    };
}

int
wg_button_group_select_button(struct WGButtonGroup *self, struct WGButton *widget)
{
    if (widget == NULL) {
        wg_button_group_select(self, NULL);
        return -1;
    }

    int i = -1;
    for (struct WGButtonGroupItem *item = self->first; item != NULL; item = item->next)
    {
        ++i;
        if (item->button == widget) {
            wg_button_group_select(self, item);
            break;
        }
    }
    return i;
}
