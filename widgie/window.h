// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_WINDOW
#define WG_HEADER_WIDGIE_WINDOW

#include <widgie/contentlayout.h>

#include <stdint.h>

struct WGWindow
{
    struct WGContentLayout base;

    char *title;

    struct WGMouseEventState *mouse_events;

    struct WGWindowBackend *backend;

    void (*close)(struct WGWindow *window);

    struct WGWidget *focused_widget;

    enum WGSizeConstraint size_constraint;
    int width_hint;
    int height_hint;

    int rerender_requested;
};

#define WG_WINDOW(x) ((struct WGWindow *)x)

struct WGWindow *wg_window_new(const char *title, enum WGSizeConstraint constraint);

void wg_window_free(struct WGWindow *self);

void wg_window_set_content(struct WGWindow *self, struct WGWidget *content, int destroy);

int wg_window_set_focus(struct WGWindow *self, struct WGWidget *new_focused_widget);

void wg_window_event_mouse(struct WGWindow *self, enum WGMouseEvent type, int event_id, int x, int y);
void wg_window_event_mouse_press(struct WGWindow *self, int event_id, int x, int y);
void wg_window_event_mouse_release(struct WGWindow *self, int event_id, int x, int y);
void wg_window_event_mouse_move(struct WGWindow *self, int event_id, int x, int y);
void wg_window_event_keypress(struct WGWindow *self, const char *utf8, uint32_t keysym);
void wg_window_set_size_hint(struct WGWindow *window, int width, int height);
void wg_window_invalidate_layouts(struct WGWindow *window);

void wg_window_dispatch(struct WGWindow *self);  // a round of event queue

void wg_window_close(struct WGWindow *window);

// protected API, should only called by derived classes
void wg__window_init(struct WGWindow *self, const char *title,
                  enum WGSizeConstraint constraint, int width, int height);
void wg__window_init_backend(struct WGWindow *self);
void wg__window_request_rerender(struct WGWidget *self, struct WGWidget *originator);
void wg__window_free(struct WGWidget *w);
int wg__window_violates_constraints(struct WGWindow *window);

#endif // FB_HEADER_WIDGIE_WINDOW