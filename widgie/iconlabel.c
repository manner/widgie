// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "iconlabel.h"

#include "settings.h"
#include "label.h"
#include "log.h"

#include "utf8.h"

#include <stdlib.h>
#include <string.h>


struct WGWidget *
wg_icon_label_new(const char *icon, const char *text)
{
    struct WGIconLabel *self = malloc(sizeof(struct WGIconLabel));
    wg__icon_label_init(self);
    wg_icon_label_set_icon(self, icon);
    wg_icon_label_set_text(self, text);
    return WG_WIDGET(self);
}

struct WGWidget *
wg_icon_label_from_text_line(const char *icon_text)
{
    int i = 0;
    wg_u8_nextchar(icon_text, &i);  // the icon
    int has_icon = wg_u8_nextchar(icon_text, &i) == ' ';
    if (has_icon && i <= 5) {
        char icon_str[5];
        strncpy(icon_str, icon_text, i - 1);
        icon_str[i - 1] = 0;
        icon_text += i;
        return wg_icon_label_new(icon_str, icon_text);
    } else {
        return wg_icon_label_new("", icon_text);
    }
}

void
wg_icon_label_set_text(struct WGIconLabel *self, const char *text)
{
    wg_label_set_text(self->label, text);
}

void
wg_icon_label_set_icon(struct WGIconLabel *self, const char *icon)
{
    wg_label_set_text(self->icon, icon);
}

static inline int
text_is_empty(const char *text)
{
    return text == NULL || *text == '\0';
}

void
wg_icon_label_set_text_height(struct WGIconLabel *self, int new_height)
{
    wg_label_set_text_height(self->label, new_height);
}

void
wg_icon_label_set_icon_text_height(struct WGIconLabel *self, int new_height)
{
    wg_label_set_text_height(self->icon, new_height);
}

void
wg__icon_label_init(struct WGIconLabel *self)
{
    struct WGLinearLayout *llayout = WG_LINEAR_LAYOUT(self);
    wg__linear_layout_init(llayout, WG_Vertical);
    WG_LAYOUT(self)->do_layout = wg__icon_label_do_layout;

    self->icon = WG_LABEL(wg_label_new(""));
    wg_label_set_text_align(self->icon, WG_AlignBottom | WG_AlignHCenter);
    wg_label_set_text_height(self->icon, wg_settings()->text.icon_height);

    self->label = WG_LABEL(wg_label_new(""));
    wg_label_set_text_align(self->label, WG_AlignTop | WG_AlignHCenter);

    wg_linear_layout_add(llayout, wg_widget_new(), 1);
    wg_linear_layout_add(llayout, WG_WIDGET(self->icon), 0);
    wg_linear_layout_add(llayout, wg_widget_new(), 1);
    wg_linear_layout_add(llayout, WG_WIDGET(self->label), 0);
    wg_linear_layout_add(llayout, wg_widget_new(), 1);
}

void
wg__icon_label_do_layout(struct WGLayout *self)
{
    struct WGIconLabel *iconlabel = WG_ICON_LABEL(self);

    int width = WG_WIDGET(self)->width;
    if (iconlabel->icon && wg_widget_is_visible(WG_WIDGET(iconlabel->icon)))
        wg_label_resize_multiline(iconlabel->icon, width);
    if (iconlabel->label && wg_widget_is_visible(WG_WIDGET(iconlabel->label)))
        wg_label_resize_multiline(iconlabel->label, width);

    wg__linear_layout_do_layout(self);
}

char *
wg_icon_label_to_text_line(struct WGIconLabel *label)
{
    char *result = NULL;
    const char *icon = label->icon ? label->icon->text : "";
    const char *text = label->label ? label->label->text : "";
    asprintf(&result, "%s%s%s", icon, *icon && *text ? " " : "", text);
    return result;
}
