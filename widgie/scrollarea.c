// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "scrollarea.h"
#include "contentlayout.h"
#include "log.h"
#include "widget.h"
#include "settings.h"

#include <stddef.h>
#include <stdlib.h>

#define FROM_CONTENT_X(self, value) ((value) - self->dx + WG_WIDGET(self)->x)
#define FROM_CONTENT_Y(self, value) ((value) - self->dy + WG_WIDGET(self)->y)
#define TO_CONTENT_X(self, value) ((value) + self->dx - WG_WIDGET(self)->x)
#define TO_CONTENT_Y(self, value) ((value) + self->dy - WG_WIDGET(self)->y)

#ifdef DEBUG_WIDGIE_SCROLLAREA
    #define sc_debug wg_log_debug
#else
    #define sc_debug WG_DEBUG_HIDE
#endif

static void
snap_scroll_position_with_overscroll(struct WGScrollArea *scroll, double overscroll)
{
    struct WGWidget *self = WG_WIDGET(scroll);
    int overscroll_height = self->height * overscroll;
    int overscroll_width = 0;  // TODO make it depend on if vertical or horizontal

    struct WGWidget *content = WG_CONTENT_WIDGET(scroll);

    int max = wg_max(content ? content->width : 0, self->width) - self->width + overscroll_width;
    if (scroll->dx > max)
        scroll->dx = max;

    max = wg_max(content ? content->height : 0, self->height) - self->height + overscroll_height;
    if (scroll->dy > max)
        scroll->dy = max;

    int min = wg_min(content ? content->width : 0, self->width) - self->width - overscroll_width;
    if (scroll->dx < min)
        scroll->dx = min;

    min = wg_min(content ? content->height : 0, self->height) - self->height - overscroll_height;
    if (scroll->dy < min)
        scroll->dy = min;
}

static void
snap_scroll_position(struct WGScrollArea *scroll)
{
    struct WGWidget *self = WG_WIDGET(scroll);
    struct WGWidget *content = WG_CONTENT_WIDGET(scroll);
    if (content != NULL && content->height < self->height) { // TODO: width if direction is so!
        switch (scroll->snap_alignment) {
            case WGSnapAlignBeginning:
                scroll->dx = scroll->dy = 0;
                return;
            case WGSnapAlignEnd:
                scroll->dx = content->width - self->width;
                scroll->dy = content->height - self->height;
                return;
            default:
                break;
        }
    }

    snap_scroll_position_with_overscroll(scroll, 0.0);
}

struct WGWidget *
wg_scrollarea_new(struct WGWidget *content)
{
    struct WGScrollArea *w = WG_SCROLLAREA(malloc(sizeof(struct WGScrollArea)));
    wg__scrollarea_init(w);
    wg_scrollarea_set_content(w, content, 1);
    return WG_WIDGET(w);
}

void
wg_scrollarea_scroll_to_start(struct WGScrollArea *self)
{
    self->dy = self->dx = 0;
    snap_scroll_position(self);
    wg_widget_request_rerender(WG_WIDGET(self));
}

void
wg_scrollarea_scroll_to_end(struct WGScrollArea *self)
{
    struct WGWidget *content = wg_contentlayout_get_content(WG_CONTENT_LAYOUT(self));
    self->dy = content->height - WG_WIDGET(self)->height;
    self->dx = content->width - WG_WIDGET(self)->width;
    snap_scroll_position(self);
    wg_widget_request_rerender(WG_WIDGET(self));
}

void
wg__scrollarea_do_layout(struct WGLayout *self)
{
    struct WGWidget *content = WG_CONTENT_WIDGET(self);
    if (content) {
        content->width = WG_WIDGET(self)->width;  // TODO if vertical scrolling
        sc_debug("width became: %d\n", content->width);
        snap_scroll_position(WG_SCROLLAREA(self));
    } else {
        sc_debug("no content widget");
    }
}

void
wg__scrollarea_init(struct WGScrollArea *self)
{
    wg__contentlayout_init(&self->base);

    WG_WIDGET(self)->render = wg__scrollarea_render;

    // Note: we do not override base.widget_at, but return ourselves as the point for the events,
    // and we'll forward (if it is not a scroll)
    WG_WIDGET(self)->widget_at = wg__widget_at;

    WG_WIDGET(self)->on_mouse_click = wg__scrollarea_on_mouse_click;
    WG_WIDGET(self)->on_mouse_longclick = wg__scrollarea_on_mouse_longclick;
    WG_WIDGET(self)->on_mouse_move = wg__scrollarea_on_mouse_move;
    WG_WIDGET(self)->on_mouse_press = wg__scrollarea_on_mouse_press;
    WG_WIDGET(self)->on_mouse_release = wg__scrollarea_on_mouse_release;
    WG_WIDGET(self)->request_rerender = wg__scrollarea_request_rerender;
    WG_WIDGET(self)->request_showme = wg__scrollarea_request_showme;
    WG_LAYOUT(self)->do_layout = wg__scrollarea_do_layout;
    self->overscroll = wg_settings()->scroll.overscroll;
    self->snap_alignment = WGSnapAlignBeginning;
    self->dx = 0;
    self->dy = 0;
    self->last_mouse_x = 0;
    self->last_mouse_y = 0;
    self->pressed_widget = NULL;
}

void
wg__scrollarea_render(struct WGWidget *self, struct WGRenderFrame *dest)
{
    struct WGScrollArea *scroll = WG_SCROLLAREA(self);

    int child_dx = -(scroll->dx);
    int child_dy = -(scroll->dy);
    dest->dx += child_dx;
    dest->dy += child_dy;
    dest->data += dest->width * self->y - self->x;
    dest->height -= self->y;
    dest->width -= self->x;
    // TODO: only visible
    wg__contentlayout_render(self, dest);

    dest->data -= dest->width * self->y - self->x;
    dest->height += self->y;
    dest->width += self->x;
    dest->dx -= child_dx;
    dest->dy -= child_dy;
}

struct WGWidget *
wg_scrollarea_content_at(struct WGScrollArea *self, int x, int y)
{
    struct WGWidget *result = wg__widget_at(WG_WIDGET(self), x, y);
    if (result == NULL)
        return NULL;  // it is not me, sorry

    struct WGWidget *content = WG_CONTENT_WIDGET(self);
    return content ? content->widget_at(content, TO_CONTENT_X(self, x), TO_CONTENT_Y(self, y)): NULL;
}

int
wg__scrollarea_on_mouse_click(struct WGWidget *self)
{
    struct WGScrollArea *scroll = WG_SCROLLAREA(self);
    if (scroll->pressed_widget) {
        return scroll->pressed_widget->on_mouse_click(scroll->pressed_widget);
    }
    return 0;
}

int
wg__scrollarea_on_mouse_longclick(struct WGWidget *self)
{
    struct WGScrollArea *scroll = WG_SCROLLAREA(self);
    if (scroll->pressed_widget) {
        return scroll->pressed_widget->on_mouse_longclick(scroll->pressed_widget);
    }
    return 0;
}

int
wg__scrollarea_on_mouse_move(struct WGWidget *self, int x, int y)
{
    // TODO at least do not crash if there is no content widget (or have an empty one by default)
    struct WGScrollArea *scroll = WG_SCROLLAREA(self);

    int old_dx = scroll->dx;
    int old_dy = scroll->dy;

    int ddx = (scroll->last_mouse_x - x);
    int ddy = (scroll->last_mouse_y - y);
    scroll->last_mouse_x = x;
    scroll->last_mouse_y = y;

    // TODO extract this logic to a separate abstraction (scoll area physic?)
    // so one could choose other physics. Note this depends on timer/animation support
    scroll->dx += ddx;
    scroll->dy += ddy;
    snap_scroll_position_with_overscroll(scroll, scroll->overscroll);

    sc_debug("scroll by %d x %d -> %d x %d\n", ddx, ddy, scroll->dx, scroll->dy);

    if (old_dx != scroll->dx || old_dy != scroll->dy)
        wg_widget_request_rerender(self);

    return 1;
}

int
wg__scrollarea_on_mouse_press(struct WGWidget *self, int x, int y)
{
    // maybe moves the scroller
    struct WGScrollArea *scroll = WG_SCROLLAREA(self);
    scroll->last_mouse_x = x;
    scroll->last_mouse_y = y;

    // maybe propagate event
    sc_debug("content press at %d x %d\n", TO_CONTENT_X(scroll, x), TO_CONTENT_Y(scroll, y));
    struct WGWidget *content = wg_scrollarea_content_at(scroll, x, y);
    if (content != NULL) {
        // TODO duplicated logic, also present in main
        while (content != NULL && content != self) {
            // TODO duplicated compatation
            if (content->on_mouse_press(content, TO_CONTENT_X(scroll, x), TO_CONTENT_Y(scroll, y))) {
                scroll->pressed_widget = content;
                sc_debug("  -> pressed widget: %p\n", content);
                break;
            } else {
                sc_debug("  -> press was not handled by: %p\n", content);
            }
            content = content->parent;
        }
    }

    return 1;
}

int
wg__scrollarea_on_mouse_release(struct WGWidget *self)
{
    sc_debug("content release\n");

    // propagate first if needed
    struct WGScrollArea *scroll = WG_SCROLLAREA(self);
    if (scroll->pressed_widget) {
        sc_debug("  -> released widget %p\n", scroll->pressed_widget);
        scroll->pressed_widget->on_mouse_release(scroll->pressed_widget);
    }

    // release the scroller
    int old_dx = scroll->dx;
    int old_dy = scroll->dy;
    snap_scroll_position(scroll);
    if (old_dx != scroll->dx || old_dy != scroll->dy)
        wg_widget_request_rerender(self);

    scroll->pressed_widget = NULL;
    return 1;
}

void
wg__scrollarea_request_rerender(struct WGWidget *self, struct WGWidget *originator)
{
    sc_debug("Scrollarea rerender request (self: %p, originator: %p)\n", self, originator);
    if (self == originator) {
        wg__layout_request_rerender(self, originator);  // the scroll view needs to be rerendered
        return;
    }

    // One of the widgets inside needs to be rerendered.
    struct WGScrollArea *scroll = WG_SCROLLAREA(self);

    // if it is at least partially visible, we need to get rerendered as well (in a separate event):
    int my_x = FROM_CONTENT_X(scroll, originator->x);
    int my_y = FROM_CONTENT_Y(scroll, originator->y);
    int inside = 0;
    if (wg_widget_intersects_rect(self, my_x, my_y, originator->width, originator->height))
    {
        wg_widget_request_rerender(self);
        inside = 1;
    }
    (void) inside;
    sc_debug("Is originator inside the visible area: %s (%d,%d %dx%d) -> translated: (%d,%d)\n",
        inside ? "yes" : "no", originator->x, originator->y, originator->width, originator->height, my_x, my_y);
}

void
wg__scrollarea_request_showme(struct WGWidget *self, struct WGWidget *originator)
{
    struct WGScrollArea *scroll = WG_SCROLLAREA(self);
    int x = FROM_CONTENT_X(scroll, originator->x);
    int y = FROM_CONTENT_Y(scroll, originator->y);

    // ensure left edge is at least at our left edge
    if (x < self->x)
        scroll->dx += (x - self->x);
    if (y < self->y)
        scroll->dy += (y - self->y);

    x = FROM_CONTENT_X(scroll, originator->x);
    y = FROM_CONTENT_Y(scroll, originator->y);

    if (x + originator->width > self->x + self->width)
        scroll->dx += (x + originator->width - self->x - self->width);

    if (y + originator->height > self->y + self->height)
        scroll->dy += (y + originator->height - self->y - self->height);

    snap_scroll_position(scroll);
}
