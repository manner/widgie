// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

// Originates from "Basic UTF-8 manipulation routines by Jeff Bezanson"
// placed in the public domain Fall 2005

#include "utf8.h"

#include "log.h"

#include <string.h>
#include <stdlib.h>

/* number of characters */
int wg_u8_strlen(const char *s)
{
    int count = 0;
    int i = 0;

    while (wg_u8_nextchar(s, &i) != 0)
        count++;

    return count;
}

/* reads the next utf-8 sequence out of a string, updating an index */
uint32_t wg_u8_nextchar(const char *s, int *i)
{
    uint32_t ch = 0;
    int sz = 0;
    static const uint32_t offsetsFromUTF8[6] = {
        0x00000000UL, 0x00003080UL, 0x000E2080UL,
        0x03C82080UL, 0xFA082080UL, 0x82082080UL
    };

    do {
        ch <<= 6;
        ch += (unsigned char)s[(*i)++];
        sz++;
    } while (ch && !wg_isutf(s[*i]));
    ch -= offsetsFromUTF8[sz-1];

    return ch;
}

void wg_u8_inc(const char *s, int *i)
{
    (void)(wg_isutf(s[++(*i)]) || wg_isutf(s[++(*i)]) ||
           wg_isutf(s[++(*i)]) || ++(*i));
}

void wg_u8_dec(const char *s, int *i)
{
    (void)(wg_isutf(s[--(*i)]) || wg_isutf(s[--(*i)]) ||
           wg_isutf(s[--(*i)]) || --(*i));
}

char *wg_u8_strchr(char *s, uint32_t ch, int *charn)
{
    int i = 0, lasti=0;
    uint32_t c;

    *charn = 0;
    while (s[i]) {
        c = wg_u8_nextchar(s, &i);
        if (c == ch) {
            return &s[lasti];
        }
        lasti = i;
        (*charn)++;
    }
    return NULL;
}

uint32_t wg_u8_lower(uint32_t cp)
{
  if (((0x0041 <= cp) && (0x005a >= cp)) ||
      ((0x00c0 <= cp) && (0x00d6 >= cp)) ||
      ((0x00d8 <= cp) && (0x00de >= cp)) ||
      ((0x0391 <= cp) && (0x03a1 >= cp)) ||
      ((0x03a3 <= cp) && (0x03ab >= cp)) ||
      ((0x0410 <= cp) && (0x042f >= cp))) {
    cp += 32;
  } else if ((0x0400 <= cp) && (0x040f >= cp)) {
    cp += 80;
  } else if (((0x0100 <= cp) && (0x012f >= cp)) ||
             ((0x0132 <= cp) && (0x0137 >= cp)) ||
             ((0x014a <= cp) && (0x0177 >= cp)) ||
             ((0x0182 <= cp) && (0x0185 >= cp)) ||
             ((0x01a0 <= cp) && (0x01a5 >= cp)) ||
             ((0x01de <= cp) && (0x01ef >= cp)) ||
             ((0x01f8 <= cp) && (0x021f >= cp)) ||
             ((0x0222 <= cp) && (0x0233 >= cp)) ||
             ((0x0246 <= cp) && (0x024f >= cp)) ||
             ((0x03d8 <= cp) && (0x03ef >= cp)) ||
             ((0x0460 <= cp) && (0x0481 >= cp)) ||
             ((0x048a <= cp) && (0x04ff >= cp))) {
    cp |= 0x1;
  } else if (((0x0139 <= cp) && (0x0148 >= cp)) ||
             ((0x0179 <= cp) && (0x017e >= cp)) ||
             ((0x01af <= cp) && (0x01b0 >= cp)) ||
             ((0x01b3 <= cp) && (0x01b6 >= cp)) ||
             ((0x01cd <= cp) && (0x01dc >= cp))) {
    cp += 1;
    cp &= ~0x1;
  } else {
    switch (cp) {
    default:
      break;
    case 0x0178:
      cp = 0x00ff;
      break;
    case 0x0243:
      cp = 0x0180;
      break;
    case 0x018e:
      cp = 0x01dd;
      break;
    case 0x023d:
      cp = 0x019a;
      break;
    case 0x0220:
      cp = 0x019e;
      break;
    case 0x01b7:
      cp = 0x0292;
      break;
    case 0x01c4:
      cp = 0x01c6;
      break;
    case 0x01c7:
      cp = 0x01c9;
      break;
    case 0x01ca:
      cp = 0x01cc;
      break;
    case 0x01f1:
      cp = 0x01f3;
      break;
    case 0x01f7:
      cp = 0x01bf;
      break;
    case 0x0187:
      cp = 0x0188;
      break;
    case 0x018b:
      cp = 0x018c;
      break;
    case 0x0191:
      cp = 0x0192;
      break;
    case 0x0198:
      cp = 0x0199;
      break;
    case 0x01a7:
      cp = 0x01a8;
      break;
    case 0x01ac:
      cp = 0x01ad;
      break;
    case 0x01af:
      cp = 0x01b0;
      break;
    case 0x01b8:
      cp = 0x01b9;
      break;
    case 0x01bc:
      cp = 0x01bd;
      break;
    case 0x01f4:
      cp = 0x01f5;
      break;
    case 0x023b:
      cp = 0x023c;
      break;
    case 0x0241:
      cp = 0x0242;
      break;
    case 0x03fd:
      cp = 0x037b;
      break;
    case 0x03fe:
      cp = 0x037c;
      break;
    case 0x03ff:
      cp = 0x037d;
      break;
    case 0x037f:
      cp = 0x03f3;
      break;
    case 0x0386:
      cp = 0x03ac;
      break;
    case 0x0388:
      cp = 0x03ad;
      break;
    case 0x0389:
      cp = 0x03ae;
      break;
    case 0x038a:
      cp = 0x03af;
      break;
    case 0x038c:
      cp = 0x03cc;
      break;
    case 0x038e:
      cp = 0x03cd;
      break;
    case 0x038f:
      cp = 0x03ce;
      break;
    case 0x0370:
      cp = 0x0371;
      break;
    case 0x0372:
      cp = 0x0373;
      break;
    case 0x0376:
      cp = 0x0377;
      break;
    case 0x03f4:
      cp = 0x03b8;
      break;
    case 0x03cf:
      cp = 0x03d7;
      break;
    case 0x03f9:
      cp = 0x03f2;
      break;
    case 0x03f7:
      cp = 0x03f8;
      break;
    case 0x03fa:
      cp = 0x03fb;
      break;
    }
  }

  return cp;
}

const char *
wg_u8_strcasestr(const char *haystack, const char *needle)
{
    int hi = 0;
    while (haystack[hi] != '\0')
    {
        int ni = 0;
        int hi2 = hi;
        uint32_t hch;
        do
        {
            hch = wg_u8_lower(wg_u8_nextchar(haystack, &hi2));
            uint32_t nch = wg_u8_lower(wg_u8_nextchar(needle, &ni));

            if (nch == '\0') { // we have a match!
                return &haystack[hi];
            }

            if (nch != hch)
                break;
        } while(hch != '\0');
        wg_u8_inc(haystack, &hi);
    }

    // we have failed to find it
    return needle[0] == '\0' ? haystack : NULL;
}


char *
wg_u8_strinsert(const char *text, int char_pos, const char *part)
{
    int text_bytes_len = strlen(text);
    int part_bytes_len = strlen(part);
    if (char_pos > text_bytes_len)
      char_pos = text_bytes_len;

    char *result = malloc(text_bytes_len + part_bytes_len + 1);
    if (result == NULL) {
        wg_log_error("Failed to allocate memory\n");
        return result;
    }

    int byte_pos = 0;
    for (; char_pos > 0 && text[byte_pos] != '\0'; --char_pos)
        wg_u8_inc(text, &byte_pos);
    memcpy(result, text, byte_pos);
    result[byte_pos] = '\0';
    strcat(result, part);
    strcat(result, text + byte_pos);
    return result;
}

char *
wg_u8_strdel(const char *text, int char_pos, int len)
{
    int text_bytes_len = strlen(text);

    char *result = malloc(text_bytes_len + 1);
    if (result == NULL) {
        wg_log_error("Failed to allocate memory\n");
        return result;
    }

    int byte_pos = 0;
    for (; char_pos > 0 && text[byte_pos] != '\0'; --char_pos)
        wg_u8_inc(text, &byte_pos);
    memcpy(result, text, byte_pos);
    result[byte_pos] = '\0';

    for (; len > 0 && text[byte_pos] != '\0'; --len)
        wg_u8_inc(text, &byte_pos);

    strcat(result, text + byte_pos);
    return result;
}

char **
wg_strarray_ndup(char **list, int count)
{
    char **result = malloc((count + 1) * sizeof(char *));
    if (result) {
        for (int i = 0; i < count; ++i) {
            result[i] = strdup(list[i]);  // TODO handle malloc failure
        }
        result[count] = NULL;
    }

    return result;
}

int
wg_strarray_len(char **list)
{
    int count = 0;
    if (list != NULL)
        for (char **pitem = list; *pitem != NULL; ++pitem)
            ++count;
    return count;
}

char **
wg_strarray_dup(char **list)
{
    if (list == NULL) {
        return list;
    }

    return wg_strarray_ndup(list, wg_strarray_len(list));
}

void
wg_strarray_free(char **list)
{
    if (list == NULL)
        return;

    for (char **pitem = list; *pitem != NULL; ++pitem)
    {
        free(*pitem);
        *pitem = NULL;
    }
    free(list);
}

int
wg_strarray_cmp(char **list1, char **list2)
{
    static char *empty[] = { NULL };
    if (list1 == NULL)
        list1 = empty;
    if (list2 == NULL)
        list2 = empty;

    char **pitem1 = list1, **pitem2 = list2;
    while (1) {
        if (*pitem1 == NULL)
            return *pitem2 == NULL ? 0 : -1;
        if (*pitem2 == NULL)
            return 1;

        int rc = strcmp(*pitem1, *pitem2);
        if (rc != 0)
            return rc;

        ++pitem1;
        ++pitem2;
    }
}
