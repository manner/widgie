// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "iniparser.h"

#include "log.h"
#include "utf8.h"

#include <asm-generic/errno-base.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>

#define DEBUG_INIPARSER  WG_DEBUG_HIDE


#define SEP_KEYVALUE " = "
#define SEP_ESCAPE "\\"
#define SEP_LIST ", "
#define GROUP_BEGIN "["
#define GROUP_END "]"

#define SEP_KEYVALUE_CH '='
#define SEP_ESCAPE_CH *(SEP_ESCAPE)
#define SEP_LIST_CH *(SEP_LIST)
#define SEP_COMMENT1_CH '#'
#define SEP_COMMENT2_CH ';'
#define GROUP_BEGIN_CH *(GROUP_BEGIN)
#define GROUP_END_CH *(GROUP_END)


// TODO maybe missing some unicode support here

// finds the character unless it is escaped (~strchr)
static const char *
find_char(const char *line, char ch)
{
    const char *pos = line;
    if (*pos == '\0')
        return NULL;

    while ((pos = strchr(pos, ch)) != NULL)
    {
        if (pos == line || *(pos - 1) != SEP_ESCAPE_CH)
            break; // not escaped, so found!
        ++pos;
    }

    return pos;
}

// removes '\' escaping from a string
static void
unescape(char *line)
{
    char *write = line;

    for (const char *read = line; *read != '\0'; ++read, ++write) {
        if (*read == SEP_ESCAPE_CH)
            ++read;
        *write = *read;
    }
    *write = '\0';
}

// Remove whitespace from the beginning and end of string,
// by writing '\0' at the end and returning the start position. (Destructive!)
static char *
trimmed(char *st)
{
    // skip spaces from start
    char *begin = st;
    while (*begin && isspace(*begin))
        ++begin;

    // chop spaces from the end
    char *end = begin + strlen(begin);
    while (isspace(*(end - 1)))
        --end;
    *end = '\0';
    return begin;
}

// Writes '\0' into the next occurrence of ch. Returns pointer to the next pos
// if found, otherwise returns NULL
// Similar to strsep, but handles escaping. (Destructive!)
static char *
chop_at_char(char *line, char ch)
{
    char *pos = (char *)find_char(line, ch);
    if (pos != NULL) {
        *pos = '\0';
        return pos + 1;
    }
    return NULL;
}

int
wg_ini_parser_process_line(char *line, char **pcurrent_group, const char *path, int line_num,
                        ConfigReceiverFunc receiver, void *userdata)
{
    char *current_group = pcurrent_group == NULL ? NULL : *pcurrent_group;
    if (path == NULL)
        path = "";

    // remove comments
    chop_at_char(line, SEP_COMMENT1_CH);
    chop_at_char(line, SEP_COMMENT2_CH);

    char *key = line;
    if (line[0] == GROUP_BEGIN_CH)
    {   // parse a group header
        if ((key = chop_at_char(line, GROUP_END_CH)) == NULL) {
            wg_log_warning("Failed to understand line in %s:%d >%s<\n", path, line_num, line);
            return EINVAL;
        } else if (current_group != NULL && line[1] == GROUP_BEGIN_CH) {
            // we have a subgroup!
            char *subgroup_name = line + 2;
            char *subgroup = NULL;
            chop_at_char(current_group, '/');  // remove child name if any (no problem if not)
            if (asprintf(&subgroup, "%s/%s", current_group, subgroup_name) < 0) {
                return ENOMEM;
            }
            free(*pcurrent_group);
            *pcurrent_group = subgroup;
            current_group = subgroup;
            unescape(current_group);
            ++key;
        } else {
            current_group = line + 1;
            if (pcurrent_group) {
                free(*pcurrent_group);
                *pcurrent_group = strdup(current_group);
                current_group = *pcurrent_group;
                if (current_group == NULL) {
                    return ENOMEM;
                }
            }
            unescape(current_group);
        }
    }

    // parse a key=value
    // skip spaces
    while (*key && isspace(*key))
        ++key;

    if (*key == '\0')
        return 0;  // skip empty lines

    char *value = chop_at_char(key, '=');
    if (value == NULL) {
        wg_log_warning("Skipping line in %s:%d >%s<\n", path, line_num, line);
        return EINVAL;
    }

    key = trimmed(key);
    unescape(key);
    value = trimmed(value);
    int rc = receiver(current_group, key, value, userdata);
    if (rc != 0) {
        wg_log_warning("Skipping unknown config option '%s' at %s:%d\n", key, path, line_num);
        return EINVAL;
    }
    return 0;
}

int
wg_ini_parser_load(const char *path, ConfigReceiverFunc receiver, void *userdata)
{
    char *line = NULL;
    size_t line_size = 0;
    int length = 0;
    int res = 0;
    int line_num = 0;
    char *current_group = strdup("");

    FILE *file = fopen(path, "r");
    if (file == NULL) {
        res = errno;
        goto end;
    }

    while ((length = getline(&line, &line_size, file)) > 0)
    {
        ++line_num;
        if (line[length - 1] == '\n')
            line[length - 1] = '\0';

        int rc = wg_ini_parser_process_line(line, &current_group, path, line_num, receiver, userdata);
        if (rc != 0)
            res = rc;
    }

end:
    free(line);
    free(current_group);
    if (file)
        fclose(file);
    return res;
}

int
wg_ini_parser_parse_bool(const char *value)
{
    if (strcmp(value, "1") == 0 ||
        strcmp(value, "yes") == 0 ||
        strcmp(value, "true") == 0)
        return 1;

    if (strcmp(value, "0") == 0 ||
        strcmp(value, "no") == 0 ||
        strcmp(value, "false") == 0)
        return 0;

    wg_log_warning("Expected boolean config option "
                "('true'/'false', 'yes'/'no', '0'/'1'), got: '%s'. "
                "Assuming 'false'.\n", value);
    return 0;
}

char **
wg_ini_parser_parse_stringlist(const char *value)
{
    // count the number of items
    int list_count = 1;
    for (const char *p = value; ; ++list_count) {
        p = find_char(p, SEP_LIST_CH);
        if (p == NULL)
            break;
        ++p;
    }

    char **result = malloc((list_count + 1) * sizeof(char *));
    char *value_dup = strdup(value);
    if (result == NULL || value_dup == NULL) {
        wg_log_warning("Failed to allocate memory.\n");
        return NULL;
    }

    int i = 0;
    for (char *part = value_dup; part != NULL && i < list_count; ++i)
    {
        char *next = chop_at_char(part, SEP_LIST_CH);
        while (*part && isspace(*part))
            ++part;
        unescape(part);
        result[i] = strdup(part);
        if (result[i] == NULL) {
            wg_log_warning("Failed to allocate memory.\n");
            wg_strarray_free(result);
            free(value_dup);
            return NULL;
        }
        part = next;
    }

    // chop last empty string, so user can configure an empty list as well
    if (i > 0 && *(result[i-1]) == '\0') {
        --i;
        free(result[i]);
    }

    result[i] = NULL;  // end with NULL
    free(value_dup);
    return result;
}

static long
_parse_long(const char *value, long min, long max)
{
    char *endp = NULL;
    errno = 0;
    long result = strtol(value, &endp, 0);

    if (result > max) {  // truncate it even further if needed
        result = max;
        errno = ERANGE;
    }
    if (result < min) {
        result = min;
        errno = ERANGE;
    }
    if (errno == ERANGE) {
        wg_log_warning("Config value '%s' is too long, it "
                    "will be truncated to %ld.\n", value, result);
    }

    if (*endp != '\0') {
        wg_log_warning("Failed to parse '%s' as number. "
            "Example of valid formats include: '0xfed' (hexa), "
            "'987' (decimal), '0765' (octal)\n", value);
    }
    return result;
}

uint32_t
wg_ini_parser_parse_color(const char *value)
{
    // TODO could handle some predefined color names, eg. 'red'
    return _parse_long(value, 0, UINT32_MAX);
}

int
wg_ini_parser_parse_int(const char *value)
{
    return _parse_long(value, INT_MIN, INT_MAX);
}

long
wg_ini_parser_parse_long(const char *value)
{
    return _parse_long(value, LONG_MIN, LONG_MAX);
}

double
wg_ini_parser_parse_double(const char *value)
{
    char *endp = NULL;
    errno = 0;
    double result = strtod(value, &endp);
    if (endp == NULL || *endp != '\0') {
        wg_log_warning("Failed to parse '%s' as floating point.\n", value);
    }
    return result;
}


char *
wg_ini_parser_parse_string(const char *value)
{
    char *result = wg_strdupnull(value);
    if (result)
        unescape(result);
    return result;
}

enum WGAlignment
wg_ini_parser_parse_alignment(const char *value)
{
    if (strcmp(value, "center") == 0) {
        return WG_AlignCenter;

    } else if (strcmp(value, "left") == 0) {
        return WG_AlignLeft | WG_AlignVCenter;

    } else if (strcmp(value, "right") == 0) {
        return WG_AlignRight | WG_AlignVCenter;

    } else if (strcmp(value, "top") == 0) {
        return WG_AlignTop | WG_AlignHCenter;

    } else if (strcmp(value, "bottom") == 0) {
        return WG_AlignBottom | WG_AlignHCenter;
    }

    return WG_AlignLeft;
}


#define SWRITE(fd, data, len) \
    do { \
        if (write(fd, data, len) < 0) { \
            int err = errno; \
            wg_log_warning("Write failed: %d - %s.\n", err, strerror(err)); \
            return err; \
        } \
    } while(0)

static int
ini_parser_write_escaped(int fd, const char *data, char escape)
{
    char sep[] = {escape, SEP_ESCAPE_CH, SEP_COMMENT1_CH, SEP_COMMENT2_CH, '\0'};
    while (*data != '\0') {
        size_t count = strcspn(data, sep);
        DEBUG_INIPARSER("Writing next token: '%.*s'\n", (int)count, data);
        while (count > 0) {
            int res = write(fd, data, count);
            if (res < 0) {
                wg_log_warning("Write failed\n");
                return errno;
            } else {
                count -= res;
                data += res;
            }
        }

        if (*data == '\0')
            break;

        // we have to escape the next char:
        SWRITE(fd, SEP_ESCAPE, 1);
        SWRITE(fd, data, 1);
        ++data;
    }

    return 0;
}

static int
ini_parser_write_key(int fd, const char *key)
{
    int err = ini_parser_write_escaped(fd, key, '=');
    if (err)
        return err;

    SWRITE(fd, " = ", 3);
    return 0;
}

int
wg_ini_parser_write_section(int fd, const char *section)
{
    SWRITE(fd, GROUP_BEGIN, 1);
    int err = ini_parser_write_escaped(fd, section, GROUP_END_CH);
    if (err)
        return err;
    SWRITE(fd, GROUP_END "\n", 2);
    return 0;
}

int
wg_ini_parser_write_subsection(int fd, const char *section)
{
    SWRITE(fd, GROUP_BEGIN GROUP_BEGIN, 2);
    int err = ini_parser_write_escaped(fd, section, GROUP_END_CH);
    if (err)
        return err;
    SWRITE(fd, GROUP_END GROUP_END "\n", 3);
    return 0;
}

int
wg_ini_parser_write_bool(int fd, const char *key, int value)
{
    return wg_ini_parser_write_string(fd, key, value == 0 ? "no" : "yes");
}

int
wg_ini_parser_write_string(int fd, const char *key, const char *value)
{
    if (value == NULL)
        value = "";

    DEBUG_INIPARSER("writing '%s' = '%s'\n", key, value);
    int rc = ini_parser_write_key(fd, key);
    if (rc)
        return rc;

    if ((rc = ini_parser_write_escaped(fd, value, SEP_ESCAPE_CH)) != 0)
        return rc;

    SWRITE(fd, "\n", 1);
    return 0;
}

int
wg_ini_parser_write_alignment(int fd, const char *key, enum WGAlignment alignment)
{
    const char *value = "free";
    switch ((int)alignment) {
        case WG_AlignCenter:
            value = "center";
            break;
        case WG_AlignLeft | WG_AlignVCenter:
            value = "left";
            break;
        case WG_AlignRight | WG_AlignVCenter:
            value = "right";
            break;
        case WG_AlignTop | WG_AlignHCenter:
            value = "top";
            break;
        case WG_AlignBottom | WG_AlignHCenter:
            value = "bottom";
            break;
        default:
            value = "free";
    };

    return wg_ini_parser_write_string(fd, key, value);
}


int
wg_ini_parser_write_stringlist(int fd, const char *key, char **values)
{
    int rc = ini_parser_write_key(fd, key);
    if (rc)
        return rc;

    if (values) {
        int is_empty = 0;
        for (char **valuep = values; *valuep != NULL; ++valuep) {
            if (valuep != values)
                SWRITE(fd, SEP_LIST, 2);
            int err = ini_parser_write_escaped(fd, *valuep, SEP_LIST_CH);
            if (err)
                return err;
            is_empty = (**valuep == '\0');
        }
        // add an extra "," if the last item is empty
        if (is_empty)
            SWRITE(fd, SEP_LIST, 2);
    }

    SWRITE(fd, "\n", 1);
    return 0;
}

int
wg_ini_parser_write_color(int fd, const char *key, uint32_t value)
{
    int rc = ini_parser_write_key(fd, key);
    if (rc)
        return rc;

    rc = dprintf(fd, "%#08x\n", value);
    if (rc < 0) {
        wg_log_warning("Write failed.\n");
        return errno;
    }

    return 0;
}

int
wg_ini_parser_write_number(int fd, const char *key, long long value)
{
    int rc = ini_parser_write_key(fd, key);
    if (rc)
        return rc;

    rc = dprintf(fd, "%lld\n", value);
    if (rc < 0) {
        wg_log_warning("Write failed.\n");
        return errno;
    }
    return 0;
}

int
wg_ini_parser_write_double_with_precision(int fd, const char *key, double value, int precision)
{
    int rc = ini_parser_write_key(fd, key);
    if (rc)
        return rc;

    rc = dprintf(fd, "%.*f\n", precision, value);
    if (rc < 0) {
        wg_log_warning("Write failed.\n");
        return errno;
    }
    return 0;
}

int
wg_ini_parser_write_comment(int fd, const char *comment)
{
    int rc = dprintf(fd, "; %s\n", comment);
    if (rc < 0) {
        wg_log_warning("Write failed.\n");
        return errno;
    }
    return 0;
}
