// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_INPUTFIELD
#define WG_HEADER_WIDGIE_INPUTFIELD

#include <widgie/label.h>

struct WGInputField
{
    struct WGLabel base;

    int cursor_position;
    char *cursor_shape;
    int is_focused;
};

struct WGWidget *wg_inputfield_new(void);

#define WG_INPUTFIELD(x) ((struct WGInputField *)x)

// sets the text and adjusts cursor's position if needed
void wg_inputfield_set_text(struct WGInputField *self, const char *text);

void wg_inputfield_set_text_and_cursor(struct WGInputField *self, const char *text, int cursor);
void wg_inputfield_add_text_at_cursor(struct WGInputField *self, const char *text);
void wg_input_field_del(struct WGInputField *self);  // deletes a character at cursor position (like pressing DEL)

// syntax sugar so users do not need to expect the InputField to be a label
static inline const char *wg_inputfield_get_text(struct WGInputField *self)
{ return wg_label_get_text(WG_LABEL(self)); }

static inline void wg_inputfield_set_text_height(struct WGInputField *self, int new_height)
{ wg_label_set_text_height(WG_LABEL(self), new_height); }

static inline void wg_inputfield_set_text_align(struct WGInputField *self, enum WGAlignment new_align)
{ wg_label_set_text_align(WG_LABEL(self), new_align); }

void wg_inputfield_set_cursor_position(struct WGInputField *self, int position);

// protected API, should only called by derived classes
void wg__inputfield_init(struct WGInputField *self);
void wg__inputfield_free(struct WGWidget *w);
void wg__inputfield_render(struct WGWidget *w, struct WGRenderFrame *dest);
int wg__inputfield_focus_event(struct WGWidget *self, int is_focused);
int wg__inputfield_keypress(struct WGWidget *self, const char *utf8, uint32_t keysym);
int wg__inputfield_mouse_press(struct WGWidget *self, int x, int y);
int wg__inputfield_mouse_click(struct WGWidget *self);

#endif // FB_HEADER_WIDGIE_INPUTFIELD