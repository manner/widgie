// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "settings.h"
#include "configstore.h"
#include "utf8.h"
#include "log.h"
#include "system.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <libgen.h>

static const char * const font_paths[] = {
    // these might avoid the fontconfig lookup (if successful, saves around ~20% startup perf)
    NULL
};

static const char * const fallback_font_paths[] = {
    NULL
};

static struct WGWidgieSettings default_settings = {
    .base = {
        .fname = "widgie.conf",
    },
    .backend = {
        .wayland = 1,
        .x11 = 1,
        .framebuffer = 0,
        .search_paths = NULL,
        .x11_config = {
            .shm = 1,
            .scale = 1.8,
        },
        .wayland_config = {
            .scale = 1.0,
        },
        .framebuffer_config = {
            .scale = 1.8,
            .device = "/dev/fb0",
            .tty = "/dev/tty",
            .xkb_config = {
                .rules = "",
                .model = "",
                .layout = "",
                .variant = "",
                .options = "",
                .repeat_delay = 600,
                .repeat_rate = 25,
            },
            .window = {
                .align = WG_AlignCenter,
                .margin_width = 0,
                .margin_height = 0,
                .rotate = 0,
            },
            .emulate_mouse = 1,
        },
    },
    .button = {
        .color = 0x303030,
        .pressed_color = 0x0077AA,
        .selected_color = 0x005577,
        .group = {
            .select_rotates = 1,
        }
    },
    .inputfield = {
        .cursor_shape = "_",
    },
    .text = {
        .font = "",          // the default font
        .fallback_font = "", // a fallback font in case the default font does not contain the character

        .font_paths = NULL,  // same, as path for avoiding the fontconfig lookup if exists
        .fallback_font_paths = NULL,
        .store_font_paths = 1,  // if true, identified paths will be saved to widgie.conf on settings deinit to avoid further fontconfig lookups

        .height = 12,        // the height of the default text font
        .icon_height = 20,   // height of default text icon
    },
    .scroll = {
        .overscroll = 0.3,
    },
    .window = {
        .constraint = WG_UnspecifiedConstraint,
        .width = 0,
        .height = 0,
    },
    .debug = {
        .show_widget_bounding_rects = 0,
        .log_level = DEBUG_LEVEL_INFO,
        .log_display_source = 1,
        .log_output = NULL,
    },
    .scale = 1.0,
};

static struct WGWidgieSettings *gb_settings = NULL;

static const char *constraint_names[WG_UnspecifiedConstraint] =
{
    "preferred", "minimum", "maximum", "fixed"
};

static inline int
wg_ini_parser_write_constraint(int fd, const char *key, enum WGSizeConstraint constraint)
{
    const char *str;
    if (constraint < WG_UnspecifiedConstraint && constraint >= 0)
        str = constraint_names[constraint];
    else
        str = "unspecified";
    return wg_ini_parser_write_string(fd, key, str);
}

static inline enum WGSizeConstraint
wg_ini_parser_parse_constraint(const char *constraint_str)
{
    for (int i = 0; i < WG_UnspecifiedConstraint; ++i) {
        if (strcmp(constraint_str, constraint_names[i]) == 0)
            return (enum WGSizeConstraint)i;
    }
    return WG_UnspecifiedConstraint;
}

static int
widgie_settings__set(struct WGConfigStore *store, const char *key, const char *value)
{
    struct WGWidgieSettings *settings = (struct WGWidgieSettings *)store;

    WG_SET_CONFIG(settings, backend.wayland, bool)
    else WG_SET_CONFIG(settings, backend.wayland_config.scale, double)
    else WG_SET_CONFIG(settings, backend.x11, bool)
    else WG_SET_CONFIG(settings, backend.x11_config.shm, bool)
    else WG_SET_CONFIG(settings, backend.x11_config.scale, double)
    else WG_SET_CONFIG(settings, backend.framebuffer, bool)
    else WG_SET_CONFIG(settings, backend.framebuffer_config.scale, double)
    else WG_SET_CONFIG_STR(settings, backend.framebuffer_config.device)
    else WG_SET_CONFIG_STR(settings, backend.framebuffer_config.tty)
    else WG_SET_CONFIG_STR(settings, backend.framebuffer_config.xkb_config.rules)
    else WG_SET_CONFIG_STR(settings, backend.framebuffer_config.xkb_config.model)
    else WG_SET_CONFIG_STR(settings, backend.framebuffer_config.xkb_config.layout)
    else WG_SET_CONFIG_STR(settings, backend.framebuffer_config.xkb_config.variant)
    else WG_SET_CONFIG_STR(settings, backend.framebuffer_config.xkb_config.options)
    else WG_SET_CONFIG(settings, backend.framebuffer_config.xkb_config.repeat_rate, int)
    else WG_SET_CONFIG(settings, backend.framebuffer_config.xkb_config.repeat_delay, int)
    else WG_SET_CONFIG(settings, backend.framebuffer_config.window.align, alignment)
    else WG_SET_CONFIG(settings, backend.framebuffer_config.window.margin_width, int)
    else WG_SET_CONFIG(settings, backend.framebuffer_config.window.margin_height, int)
    else WG_SET_CONFIG(settings, backend.framebuffer_config.window.rotate, int)
    else WG_SET_CONFIG(settings, backend.framebuffer_config.emulate_mouse, bool)
    else WG_SET_CONFIG_STRLIST(settings, backend.search_paths)
    else WG_SET_CONFIG(settings, button.color, color)
    else WG_SET_CONFIG(settings, button.pressed_color, color)
    else WG_SET_CONFIG(settings, button.selected_color, color)
    else WG_SET_CONFIG(settings, button.group.select_rotates, bool)
    else WG_SET_CONFIG_STR(settings, inputfield.cursor_shape)
    else WG_SET_CONFIG_STR(settings, text.font)
    else WG_SET_CONFIG_STR(settings, text.fallback_font)
    else if (strcmp(key, "text.font_paths") == 0) {
        wg_strarray_free(settings->text.font_paths);
        settings->text.font_paths = wg_ini_parser_parse_stringlist((char *)value);
        if (wg_strarray_len(settings->text.font_paths) != 0)
            settings->text.store_font_paths = 0;
    }
    else if (strcmp(key, "text.fallback_font_paths") == 0) {
        wg_strarray_free(settings->text.fallback_font_paths);
        settings->text.fallback_font_paths = wg_ini_parser_parse_stringlist((char *)value);
        if (wg_strarray_len(settings->text.fallback_font_paths) != 0)
            settings->text.store_font_paths = 0;
    }
    else WG_SET_CONFIG(settings, text.height, int)
    else WG_SET_CONFIG(settings, text.icon_height, int)
    else WG_SET_CONFIG(settings, scroll.overscroll, double)
    else WG_SET_CONFIG(settings, window.constraint, constraint)
    else WG_SET_CONFIG(settings, window.width, int)
    else WG_SET_CONFIG(settings, window.height, int)
    else WG_SET_CONFIG(settings, debug.show_widget_bounding_rects, bool)
    else WG_SET_CONFIG(settings, debug.log_level, int)
    else WG_SET_CONFIG(settings, debug.log_display_source, bool)
    else WG_SET_CONFIG_STR(settings, debug.log_output)
    else return EINVAL;
    return 0;
}

static void
widgie_settings_scale__sizes(struct WGConfigStore *store, double scale)
{
    struct WGWidgieSettings *settings = (struct WGWidgieSettings *)store;
    settings->text.height *= scale;
    settings->text.icon_height *= scale;

    settings->window.height *= scale;
    settings->window.width *= scale;
}

static void
widgie_settings__free_inner(struct WGWidgieSettings *settings)
{
    wg_strarray_free(settings->backend.search_paths);
    settings->backend.search_paths = NULL;

    free(settings->backend.framebuffer_config.device);
    settings->backend.framebuffer_config.device = NULL;
    free(settings->backend.framebuffer_config.tty);
    settings->backend.framebuffer_config.tty = NULL;

    free(settings->backend.framebuffer_config.xkb_config.rules);
    settings->backend.framebuffer_config.xkb_config.rules = NULL;
    free(settings->backend.framebuffer_config.xkb_config.model);
    settings->backend.framebuffer_config.xkb_config.model = NULL;
    free(settings->backend.framebuffer_config.xkb_config.layout);
    settings->backend.framebuffer_config.xkb_config.layout = NULL;
    free(settings->backend.framebuffer_config.xkb_config.variant);
    settings->backend.framebuffer_config.xkb_config.variant = NULL;
    free(settings->backend.framebuffer_config.xkb_config.options);
    settings->backend.framebuffer_config.xkb_config.options = NULL;

    free(settings->text.font);
    settings->text.font = NULL;

    free(settings->text.fallback_font);
    settings->text.fallback_font = NULL;

    wg_strarray_free(settings->text.font_paths);
    settings->text.font_paths = NULL;

    wg_strarray_free(settings->text.fallback_font_paths);
    settings->text.fallback_font_paths = NULL;

    free(settings->inputfield.cursor_shape);
    settings->inputfield.cursor_shape = NULL;
}

static void
_widgie_settings_store_font_paths(struct WGWidgieSettings *settings)
{
    int fd = -1;
    settings->text.store_font_paths = 0;
    char *path = wg_config_path(NULL, NULL);
    if (path == NULL)
        goto finish;

    wg_create_basedir_of(path);
    fd = open(path, O_WRONLY|O_CREAT|O_APPEND, 0600);
    if (fd < 0) {
        wg_log_error("Failed to write '%s': %s\n", path, strerror(errno));
        goto finish;
    }

    if (wg_ini_parser_write_comment(fd, "Font paths are generated by widgie to speed up startup time. "
        "Remove them to regenerate them automatically, or feel free to replace them with your own.") < 0)
        goto finish;
    wg_ini_parser_write_section(fd, "widgie");
    wg_ini_parser_write_stringlist(fd, "text.font_paths", settings->text.font_paths);
    wg_ini_parser_write_stringlist(fd, "text.fallback_font_paths", settings->text.fallback_font_paths);

finish:
    if (fd >= 0)
        close(fd);
    free(path);
}

static void
widgie_settings__free(struct WGConfigStore *settings)
{
    if (settings == NULL)
        return;

    if (WG_CONFIG_STORE(gb_settings) == settings) {
        if (gb_settings->text.store_font_paths) {
            _widgie_settings_store_font_paths(gb_settings);
        }
        gb_settings = NULL;
    }

    widgie_settings__free_inner((struct WGWidgieSettings *)settings);
    free(settings);
}

static void
widgie_settings__load_complete(struct WGConfigStore *store)
{
    struct WGWidgieSettings *settings = (struct WGWidgieSettings *)store;

    if (settings != gb_settings)  // only apply global config globally
        return;

    // apply logging configuration
    if (settings->debug.log_output == NULL || *settings->debug.log_output == '\0')
        wg_log_to_stderr();
    else
        wg_log_to_path(settings->debug.log_output, 0);

    wg_log_set_display_source(settings->debug.log_display_source);
    wg_log_set_level(settings->debug.log_level);
}

static int
widgie_settings__print_example(struct WGConfigStore *store, int fd)
{
    int rc = 0;
    struct WGWidgieSettings *settings = (struct WGWidgieSettings *)store;

    #define WRITE_SETTING(element, type, description) \
        dprintf(fd, "\n"); \
        if (*description != '\0' && dprintf(fd, "; " description " [" #type "]\n") < 0) { \
            rc = errno; \
            goto finish; \
        } \
        if ((rc = wg_ini_parser_write_##type(fd, #element, settings->element)) != 0) \
            goto finish

    WRITE_SETTING(backend.search_paths, stringlist, "The path where backend plugins will be searched (default is the path of the application and " LIBDIR ")");
    WRITE_SETTING(backend.wayland, bool, "Enable trying to use the wayland backend (default is true)");
    WRITE_SETTING(backend.wayland_config.scale, double, "Scale up/down every size related configuration values when using wayland engine (default is 1.0)");
    WRITE_SETTING(backend.x11, bool, "Enable trying to use the x11 backend (default is true)");
    WRITE_SETTING(backend.x11_config.shm, bool, "Enable trying to use the x11-shm extension if available (default is true)");
    WRITE_SETTING(backend.x11_config.scale, double, "Scale up/down every size related configuration values when using x11 engine (default is 1.0)");
    WRITE_SETTING(backend.framebuffer, bool, "Enable trying to use the framebuffer backend (default is no)");
    WRITE_SETTING(backend.framebuffer_config.scale, double, "Scale up/down every size related configuration values when using framebuffer engine (default is 1.0)");
    WRITE_SETTING(backend.framebuffer_config.device, string, "The device representing the framebuffer, usually /dev/fb0");
    WRITE_SETTING(backend.framebuffer_config.tty, string, "The framebuffer backend can alter a tty for example to hide cursor when it runs (default is /dev/tty, set it to empty to avoid doing so)");
    WRITE_SETTING(backend.framebuffer_config.xkb_config.rules, string, "Files of rules to be used for keyboard mapping composition, see xkeyboard (default is XKB_DEFAULT_RULES env var)");
    WRITE_SETTING(backend.framebuffer_config.xkb_config.model, string, "The model of the keyboard, see xkeyboard (default is XKB_DEFAULT_MODEL env var)");
    WRITE_SETTING(backend.framebuffer_config.xkb_config.layout, string, "The layout of the keyboard (example: 'us', 'hu', 'de' ... (default is XKB_DEFAULT_LAYOUT env var)");
    WRITE_SETTING(backend.framebuffer_config.xkb_config.variant, string, "Sets the variant of the keyboard like dvorak or colemak (default is XKB_DEFAULT_VARIANT env var)");
    WRITE_SETTING(backend.framebuffer_config.xkb_config.options, string, "Sets extra xkb configuration options for the keyboard (default is XKB_DEFAULT_OPTIONS env var)");
    WRITE_SETTING(backend.framebuffer_config.xkb_config.repeat_delay, number, "A delay in milliseconds after which a key long press be considered a repeat (default is 600)");
    WRITE_SETTING(backend.framebuffer_config.xkb_config.repeat_rate, number, "Sets the rate for the key repeat: how many times the key should be repeated per second (default is 25)");
    WRITE_SETTING(backend.framebuffer_config.window.align, alignment, "The alignment of the window if it is smaller than the framebuffer. Valid values: 'top', 'bottom', 'left', 'right', 'center' and 'free' (default is 'center')");
    WRITE_SETTING(backend.framebuffer_config.window.margin_width, number, "The number of columns the gap will contain between the window and the screen border.");
    WRITE_SETTING(backend.framebuffer_config.window.margin_height, number, "The number of rows the gap will contain between the window and the screen border.");
    WRITE_SETTING(backend.framebuffer_config.window.rotate, number, "The rotation count of 90 degrees to apply counterclockwise (default is 0)");
    WRITE_SETTING(backend.framebuffer_config.emulate_mouse, bool, "If yes, widgie will show a pointer arrow in case mouse moves (default is true)");
    WRITE_SETTING(button.color, color, "Default background color of buttons");
    WRITE_SETTING(button.pressed_color, color, "Default background color when a button is pressed");
    WRITE_SETTING(button.selected_color, color, "Default background color when a button is selected");
    WRITE_SETTING(button.group.select_rotates, bool, "Specifies whether button groups select to the first item after the last");
    WRITE_SETTING(inputfield.cursor_shape, string, "A character showing up at the cursor position in inputfields");
    WRITE_SETTING(text.font, string, "Default font of labels. Must be fontconfig lookup (multiple fonts can be separated by commas)");
    WRITE_SETTING(text.fallback_font, string, "Fallback font of labels used when a character is not present in the default font");
    WRITE_SETTING(text.font_paths, stringlist, "Optional direct paths where the default font might be found to avoid the fontconfig lookup");
    WRITE_SETTING(text.fallback_font_paths, stringlist, "Optional direct paths for the fallback font");
    WRITE_SETTING(text.height, number, "Default text height of labels");
    WRITE_SETTING(text.icon_height, number, "Default text height for icons");
    WRITE_SETTING(window.constraint, constraint, "Specifies how width/height affect the size of the window: 'unspecified', 'preferred', 'minimum', 'maximum', 'fixed'. Default is unspecified == controlled by the application.");
    WRITE_SETTING(window.width, number, "Override the sizehint (height) of the window (default is application specific)");
    WRITE_SETTING(window.height, number, "Override the sizehint (height) of the window (default is application specific)");
    WRITE_SETTING(scroll.overscroll, double, "How much overscrolling to permit relative to the scrollarea size (default is 0.3)");
    WRITE_SETTING(debug.show_widget_bounding_rects, bool, "");  // TODO make logging configurable, eg. to file
    WRITE_SETTING(debug.log_level, number, "0 = error, 1 = warning, 2 = info, 3 = debug, 4 = trace");
    WRITE_SETTING(debug.log_display_source, bool, "If true, the source code line where the log happens will be prepended to each log");
    WRITE_SETTING(debug.log_output, string, "If specified, logging will output into the specified file instead of stderr");
    settings->text.store_font_paths = 0;

finish:
    return rc;
}

static void
set_settings_defaults(struct WGWidgieSettings *settings)
{
    // init defaults if they are not
    if (default_settings.text.font_paths == NULL)
        default_settings.text.font_paths = (char **)font_paths;  // TODO widgie might store the result of the lookup in the config
    if (default_settings.text.fallback_font_paths == NULL)
        default_settings.text.fallback_font_paths = (char **)fallback_font_paths;

    memcpy(settings, &default_settings, sizeof(default_settings));

    struct WGConfigStore *store = WG_CONFIG_STORE(settings);
    store->free = widgie_settings__free;
    store->load_complete = widgie_settings__load_complete;
    store->print_example = widgie_settings__print_example;
    store->set = widgie_settings__set;
    store->scale_sizes = widgie_settings_scale__sizes;

    settings->text.font = wg_strdupnull(settings->text.font);
    settings->text.fallback_font = wg_strdupnull(settings->text.fallback_font);
    settings->text.font_paths = wg_strarray_dup(settings->text.font_paths);
    settings->text.fallback_font_paths = wg_strarray_dup(settings->text.fallback_font_paths);
    settings->inputfield.cursor_shape = wg_strdupnull(settings->inputfield.cursor_shape);
    settings->backend.framebuffer_config.device = wg_strdupnull(settings->backend.framebuffer_config.device);
    settings->backend.framebuffer_config.tty = wg_strdupnull(settings->backend.framebuffer_config.tty);
    settings->backend.framebuffer_config.xkb_config.rules = wg_strdupnull(settings->backend.framebuffer_config.xkb_config.rules);  // TODO write some helpers maybe instead?
    settings->backend.framebuffer_config.xkb_config.model = wg_strdupnull(settings->backend.framebuffer_config.xkb_config.model);
    settings->backend.framebuffer_config.xkb_config.layout = wg_strdupnull(settings->backend.framebuffer_config.xkb_config.layout);
    settings->backend.framebuffer_config.xkb_config.variant = wg_strdupnull(settings->backend.framebuffer_config.xkb_config.variant);
    settings->backend.framebuffer_config.xkb_config.options = wg_strdupnull(settings->backend.framebuffer_config.xkb_config.options);
}

static struct WGWidgieSettings *
widgie_settings_new()
{
    struct WGWidgieSettings *settings = (struct WGWidgieSettings *)malloc(sizeof(struct WGWidgieSettings));

    set_settings_defaults(settings);
    return settings;
}

void
wg_settings_register(void)
{
    if (gb_settings == NULL) {
        gb_settings = widgie_settings_new();
        wg_configstore_register("widgie", WG_CONFIG_STORE(gb_settings));
    }
}

char **
wg_settings_get_default_plugin_search_paths(void)
{
    char **search_paths = search_paths = malloc(3 * sizeof(char *));
    if (search_paths == NULL) {
        wg_log_error("Memory allocation error\n");
        return NULL;
    }

    // TODO maybe create our own wg_dirname, wg_basename
    int i = 0;
    char *app_path_dup = NULL;
    if (app_path != NULL) {
        app_path_dup = strdup(app_path); // unfortunately dirname might modify the string
        if (app_path_dup == NULL) {
            wg_log_error("Memory allocation error\n");
            return NULL;
        }

        search_paths[i++] = strdup(dirname(app_path_dup)); // unfortunately we can not free app_path_dup until its result is used, so we need to copy
    }
    search_paths[i++] = strdup(LIBDIR);
    search_paths[i] = NULL;

    free(app_path_dup);
    return search_paths;
}

struct WGWidgieSettings *
wg_settings(void)
{
    wg_settings_register();
    return gb_settings;
}

void
wg_settings_set_defaults(void)
{
    wg_settings_register();
    widgie_settings__free_inner(gb_settings);
    set_settings_defaults(gb_settings);
}
