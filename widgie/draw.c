// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "draw.h"

#include "frame.h"

void
wg_draw_rect(struct WGRenderFrame *dest, int x, int y, int width, int height, uint32_t color)
{
    wg_render_frame_init_position(dest, &x, &y);
    if (!wg_render_frame_fix_rect(dest, &x, &y, &width, &height))
        return;

    // do the actual paint
    uint32_t *dest_pos = dest->data + x + y *dest->width;
    for (int j = 0; j < height; ++j)
    {
        for (int i = 0; i < width; ++i, ++dest_pos)
        {
            *dest_pos = color;
        }
        dest_pos += dest->width - width;
    }
}

void
wg_draw_rect_border(struct WGRenderFrame *dest, int x, int y, int width, int height, uint32_t color)
{
    // TODO could avoid comparisions and checkings inside line draws by having draw_hline_unsafe, and precheck
    wg_draw_hline(dest, x, y, width, color);
    wg_draw_hline(dest, x, y + height, width, color);
    wg_draw_vline(dest, x, y, height, color);
    wg_draw_vline(dest, x + width, y, height, color);
}

void
wg_draw_hline(struct WGRenderFrame *dest, int x, int y, int width, uint32_t color)
{
    wg_render_frame_init_position(dest, &x, &y);

    // ensure we do not paint outside the frame
    if (x >= dest->width || y >= dest->height || y < 0)
        return;

    if (x < 0) {
        width += x;
        x = 0;
    }

    if (x + width > dest->width)
        width = dest->width - x;

    uint32_t *dest_pos = dest->data + x + y * dest->width;
    for (int i = 0; i < width; ++i, ++dest_pos)
    {
        *dest_pos = color;
    }
}

void
wg_draw_vline(struct WGRenderFrame *dest, int x, int y, int height, uint32_t color)
{
    wg_render_frame_init_position(dest, &x, &y);

    // ensure we do not paint outside the frame
    if (x > dest->width || y > dest->height || x < 0)
        return;

    if (y < 0) {
        height += y;
        y = 0;
    }

    if (y + height > dest->height)
        height = dest->height - y;

    uint32_t *dest_pos = dest->data + x + y * dest->width;
    for (int j = 0; j < height; ++j, dest_pos += dest->width)
    {
        *dest_pos = color;
    }
}
