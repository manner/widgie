// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "text_rendering.h"

#include "utf8.h"
#include "frame.h"
#include "log.h"
#include "settings.h"

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_ADVANCES_H
#include FT_GLYPH_H

#include <fontconfig/fontconfig.h>
#include <math.h>

#define DEBUG_TEXR  WG_DEBUG_HIDE
#define DEBUG_TEXLOOKUP  DEBUG_TEXR
//#define DEBUG_TEXLOOKUP  WG_DEBUG_SHOW

// TODO maybe make this configurable? (BLENDING_FAST, BLENDING_MEDIUM, BLENDING_PROPER)
#define BLENDING_MEDIUM

// uncomment this to enable debugging cache misses
// #define DEBUG_CACHE_MISS

#define MAX_CACHE_SIZE 1024   // has to be 2^n

#ifdef DEBUG_CACHE_MISS
static int cache_miss = 0;
static int cache_lookups = 0;
#endif

#ifdef BLENDING_MEDIUM
static uint8_t to_srgb_vec[256];
static uint8_t from_srgb_vec[256];
#endif

#define MAX_FACES 10
static FT_Face faces[MAX_FACES + 1] = {NULL};
static FT_Library library;

static inline int
file_exists(const char *filename)
{
    struct stat buffer;
    return (stat (filename, &buffer) == 0);
}

// TODO this still does not support a lot of stuff, maybe pango will be the only solution available?
static char *
fontconfig_lookup(const char *font_desc)
{
    char *path = NULL;

    FcPattern *pat = FcNameParse((FcChar8 *) font_desc);
    if (pat == NULL) {
        wg_log_warning("Unable to parse the pattern: '%s'\n", font_desc);
        return path;
    }

    FcConfigSubstitute(NULL, pat, FcMatchPattern);
    FcDefaultSubstitute(pat);

    FcResult result;
    FcPattern *match = FcFontMatch(0, pat, &result);
    FcPatternDestroy(pat);
    if (match == NULL) {
        wg_log_warning("No match for valid pattern: '%s'\n", font_desc);
        return path;
    }

    FcValue v;
    FcPatternGet(match, FC_FILE, 0, &v);
    const char *filepath = (const char *)v.u.f;
    if (filepath != NULL) {
        DEBUG_TEXLOOKUP("Matched font: '%s' for lookup '%s'\n", filepath, font_desc);
        path = strdup(filepath);
    } else {
        wg_log_error("Unable to get filepath from matched font, that is strange\n");
    }

    FcPatternDestroy(match);
    return path;
}

struct WGFontCacheItem
{
    int font_height;
    uint32_t ch;
    FT_Glyph glyph;
};

static struct WGFontCacheItem cache[MAX_CACHE_SIZE];

static inline int
glyph_cache_hash(int font_height, uint32_t ch)
{
    return (ch ^ (ch > 0xff ? (MAX_CACHE_SIZE - 1) : 0) ^ (font_height << 6)) & (MAX_CACHE_SIZE - 1);
}

static FT_Glyph
glyph_cache_lookup(int font_height, uint32_t ch)
{
    struct WGFontCacheItem *cache_item = &cache[glyph_cache_hash(font_height, ch)];
    FT_Glyph result = NULL;

    if (cache_item->font_height == font_height && cache_item->ch == ch) {
        result = cache_item->glyph;
    }

#ifdef DEBUG_CACHE_MISS
    else {
        ++cache_miss;
    }
    ++cache_lookups;
#endif

    return result;
}

static FT_Glyph
glyph_cache_insert(int font_height, uint32_t ch, FT_GlyphSlot slot)
{
    FT_Glyph glyph = NULL;
    FT_Error err = FT_Get_Glyph(slot, &glyph);
    if (err) {
        wg_log_error("Failed to get glyph, error='%d'\n", err);
        return NULL;
    }

    struct WGFontCacheItem *cache_item = &cache[glyph_cache_hash(font_height, ch)];

    if (cache_item->glyph != NULL) {
        FT_Done_Glyph(cache_item->glyph);
    }

    cache_item->font_height = font_height;
    cache_item->ch = ch;
    cache_item->glyph = glyph;
    return glyph;
}

static int
load_font(char **font_paths, const char *font_desc, FT_Face *face)
{
    char *path = NULL;
    if (font_paths != NULL) {
        for (char **ppath = font_paths; *ppath != NULL; ++ppath) {
            if (file_exists(*ppath)) {
                path = strdup(*ppath);
                DEBUG_TEXLOOKUP("Found font at path '%s' without fontconfig lookup\n", path);
                break;
            }
        }
    }

    if (path == NULL) {
        DEBUG_TEXLOOKUP("Running fontconfig lookup for font description '%s' (font %d)\n", font_desc, (int)(face - faces));
        path = fontconfig_lookup(font_desc);

        if (path == NULL) {
            wg_log_warning("Fontconfig lookup failed: '%s'\n", font_desc);
            return 1;
        }
    }

    FT_Error error = FT_New_Face(library, path, 0, face);
    if (error) {
        wg_log_error("Could not load font (%s), error='%d'\n", path, error);
        return 1;
    }

    if (wg_settings()->text.store_font_paths) {
        char *paths[] = { path, NULL };
        if (wg_strarray_len(wg_settings()->text.font_paths) == 0) {
            wg_strarray_free(wg_settings()->text.font_paths);
            wg_settings()->text.font_paths = wg_strarray_dup(paths);
        } else if (strcmp(wg_settings()->text.font_paths[0], path) == 0) {
            // already remembered
        } else if (wg_strarray_len(wg_settings()->text.fallback_font_paths) == 0) {
            wg_strarray_free(wg_settings()->text.fallback_font_paths);
            wg_settings()->text.fallback_font_paths = wg_strarray_dup(paths);
        }
    }

    free(path);

    FT_Select_Charmap(*face, FT_ENCODING_UNICODE);
    return 0;
}

static inline double
to_srgb_real(double c)
{
    return (pow(c / 256.0, 1 / 1.8) * 256.0);
}

static inline double
from_srgb_real(double c)
{
    return (pow(c / 256.0, 2.2) * 256.0);
}

static void
text_render_ensure_init(void)
{
    static int is_initialized = 0;

    if (!is_initialized) {
        FT_Error error = FT_Init_FreeType(&library);
        if (error) {
            wg_log_error("Could not initialized FreeType library, error='%d'\n", error);
            exit(1);
        }

        char **font_paths = wg_settings()->text.font_paths;
        const char *font = wg_settings()->text.font;
        char **ffont_paths = wg_settings()->text.fallback_font_paths;
        const char *ffont = wg_settings()->text.fallback_font;
        int is_different = (ffont != NULL && strcmp(ffont, font) != 0) ||
            wg_strarray_cmp(ffont_paths, font_paths) != 0;

        if (load_font(font_paths, font, &faces[0]) != 0) {
            wg_log_error("Failed to load font for the description: %s\n", font);
            exit(1);
        }

        if (is_different && load_font(ffont_paths, ffont, &faces[1]) != 0) {
            wg_log_warning("Failed to load fallback font for the description: %s\n", wg_settings()->text.fallback_font);
        }

        /* TODO cmap selection omitted;                                        */
        /* for simplicity we assume that the font contains a Unicode cmap */
        is_initialized = 1;

        #if defined(BLENDING_MEDIUM)
        for (int i = 0; i < 256; ++i) {
            to_srgb_vec[i] = (uint8_t)to_srgb_real(i);
            from_srgb_vec[i] = (uint8_t)from_srgb_real(i);
        }
        #endif
    }
}

#if defined(BLENDING_FAST)
    #define to_srgb(c) ((int)c)
    #define from_srgb(c) ((int)c)
#elif defined(BLENDING_MEDIUM)
    #define to_srgb(c) to_srgb_vec[c]
    #define from_srgb(c) from_srgb_vec[c]
#else // BLENDING_PROPER
    #define to_srgb(c) to_srgb_real(c)
    #define from_srgb(c) from_srgb_real(c)
#endif

static inline void
linear_blend(uint8_t *dest, uint8_t *rgb_color, uint8_t alpha)
{
    #define linear_blend_channel(rgb1, rgb2, alpha) \
        (uint8_t)to_srgb((rgb1 * alpha + rgb2 * (0x100 - alpha)) >> 8)

    // TODO: would need to step a byte for bigendian systems?
    dest[0] = linear_blend_channel(rgb_color[0], from_srgb(dest[0]), alpha);
    dest[1] = linear_blend_channel(rgb_color[1], from_srgb(dest[1]), alpha);
    dest[2] = linear_blend_channel(rgb_color[2], from_srgb(dest[2]), alpha);
}

static void
text_render_draw_bitmap(FT_Bitmap*  bitmap, struct WGRenderFrame *dest, FT_Int dest_x, FT_Int dest_y)
{
    const uint32_t text_rgb = 0xffffff;  // TODO specify this

    /* TODO for simplicity, we assume that `bitmap->pixel_mode' */
    /* is `FT_PIXEL_MODE_GRAY' (i.e., not a bitmap font)   */
    int x = dest_x, y = dest_y;
    int width = bitmap->width, height = bitmap->rows;
    DEBUG_TEXR("Character bitmap size: %dx%d rendering to %d,%d (%dx%d)\n",
                width, height, dest_x, dest_y, dest->width, dest->height);
    if (!wg_render_frame_fix_rect(dest, &x, &y, &width, &height))  // TODO optimization possibility (dont need to do this for every character)
        return;

    int i = y - dest_y, j = x - dest_x;
    width += j;
    height += i;
    DEBUG_TEXR("Upper box %d,%d (%dx%d) -> %d,%d (%dx%d)\n", j, i, width, height, x, y, width, height);
    unsigned char *src = bitmap->buffer + i * bitmap->pitch + j;
    uint32_t *destpos = dest->data + y * dest->width + x;

    for (; i < height; ++i)
    {
        for (j = 0; j < width; ++j, ++src, ++destpos)
        {
            unsigned long alpha = *src;
            // DEBUG_TEXR("%02lx ", alpha);
            if (alpha == 0xff) {
                *destpos = text_rgb;
            } else if (alpha != 0) {
                linear_blend((uint8_t *)destpos, (uint8_t *)&text_rgb, alpha);
            }
        }
        // DEBUG_TEXR("\n");
        destpos += dest->width - width;
        src += bitmap->pitch - width;
    }
    // DEBUG_TEXR("\n");
}

static void
set_char_size(FT_Face face, int font_height)
{
    FT_Error error = FT_Set_Char_Size(face, font_height << 6, 0, 100, 0);
    if (error) {
        wg_log_error("Failed to set char size for font\n");
        // TODO print the actual error
    }
}

static void
find_font_and_glyph(FT_ULong ch, FT_Face *pface, FT_UInt *pglyph_index)
{
    *pface = NULL;
    *pglyph_index = 0;

    FT_Face *pf = faces;
    for (; *pf != NULL; ++pf)
    {
        *pglyph_index = FT_Get_Char_Index(*pf, ch);
        if (*pglyph_index != 0) {
            *pface = *pf;  // found in an already known font face
            return;
        }
    }

    if ((pf - faces) < MAX_FACES) {
        DEBUG_TEXLOOKUP("Loading to empty slot: %d\n", (int)(pf - faces));
        char pattern[30];
        snprintf(pattern, sizeof(pattern), ":charset=%lx", ch);
        if (load_font(NULL, pattern, pf) != 0 ||
            (*pglyph_index = FT_Get_Char_Index(*pf, ch)) == 0)
        {
            wg_log_warning("Failed to find glyph for char %lx '%c'\n", ch, (char)ch);
        }
        else
        {
            *pface = *pf;
        }
    }
}

static struct WGTextRender *
text_render_construct(const char *text, int font_height)
{
    struct WGTextRender *cache = malloc(sizeof(struct WGTextRender));
    cache->char_count = wg_u8_strlen(text);
    cache->font_height = font_height;
    cache->full_text = strdup(text);
    cache->width = 0;

    cache->line_height = (faces[0]->size->metrics.height >> 6);  // TODO if fallback font is higher?
    cache->ascender_height = (faces[0]->size->metrics.ascender >> 6);
    cache->descender_height = (faces[0]->size->metrics.descender >> 6);

    cache->char_positions = malloc((cache->char_count + 1) * sizeof(int));
    cache->lines = calloc(2, sizeof(char *));
    cache->lines_width = calloc(2, sizeof(int));
    cache->lines[0] = cache->full_text;
    cache->line_count = 1;
    return cache;
}

struct WGTextRender *
wg_text_render_new(const char *text, int font_height)
{
    text_render_ensure_init();

    FT_Face face = faces[0];
    FT_Bool use_kerning = FT_HAS_KERNING(face);
    set_char_size(face, font_height);

    struct WGTextRender *result = text_render_construct(text, font_height);

    FT_UInt glyph_index = 0, prev_index = 0;
    FT_Vector kerning;

    int n = 0;
    int *px = result->char_positions;
    *px = 0;
    FT_ULong ch;
    while ((ch = wg_u8_nextchar(text, &n)) != 0)
    {
        // we have the current character position, we compute the next
        ++px;
        *px = *(px - 1);

        find_font_and_glyph(ch, &face, &glyph_index);
        /* TODO
        if (glyph_index == 0) {
            // failed to find font for the character
            continue;
        }
        */
        if (face != faces[0])  // for now we only do kerning for the main font
            prev_index = 0;

        FT_Fixed advance;
        FT_Error error = FT_Get_Advance(face, glyph_index, FT_LOAD_DEFAULT, &advance);
        if (error) {
            wg_log_error("Failed to compute advance for char %lu '%c'\n", ch, (char)ch);
            continue; // TODO
        }

        *px += (advance >> 16);

        if (use_kerning && prev_index && glyph_index)
        {
            FT_Get_Kerning(face, prev_index, glyph_index, FT_KERNING_DEFAULT, &kerning);

            *px += (kerning.x >> 6);  // TODO this can be in lot of formats, how to know which?
        }

        DEBUG_TEXR("computed pos char:%s pos:%d advance:%ld kerning:%ld -> %d\n",
                   (const char *)&ch, *(px-1), (advance >> 16), kerning.x >> 6, *px);
        if (face == faces[0]) {
            prev_index = glyph_index;
        } else {
            prev_index = 0;
        }
    }
    result->width = *px;
    result->lines_width[0] = result->width;

    return result;
}

void
wg_text_render_free(struct WGTextRender *self)
{
    if (self == NULL)
        return;

    free(self->char_positions);
    free(self->lines_width);
    free(self->lines);  // these point into full_text!
    free(self->full_text);
    free(self);
}

void
wg_text_render_wrap_lines(struct WGTextRender *self, int width, enum WGTextWrap wrap)
{
    DEBUG_TEXR("'%s' -  width:%d wrap mode:%d\n", self->full_text, width, wrap);
    self->width = width;
    switch (wrap)
    {
        case WG_TextWrapAtChar:
            break;
        default:
            return;
    }

    int line_x = 0;

    // TODO: for wrap at space, use the spaces count or the max of the two
    int lines_count = 100; // TODO self->char_positions[self->char_count] / width + 1;

    self->lines = realloc(self->lines, sizeof(*self->lines) * lines_count);
    self->lines_width = realloc(self->lines_width, sizeof(*self->lines_width) * lines_count);
        // TODO handle memory alloc errors
    int line_i = 0;
    self->lines[line_i] = self->full_text;
    int next_byte_pos = 0, ch_byte_pos = 0, char_pos = 0;
    FT_ULong ch;
    while ((ch = wg_u8_nextchar(self->full_text, &next_byte_pos)) != 0)
    {
        if (ch == ' ') {
            // TODO last line end could be remembered
        }

        DEBUG_TEXR("ch:%s char_pos:%d line_x:%d width:%d\n",
                   (const char *)&ch, self->char_positions[char_pos], line_x, width);

        if (self->char_positions[char_pos + 1] - line_x > width)
        {
            // we break
            self->lines_width[line_i] = self->char_positions[char_pos] - line_x;
            ++line_i;
            self->lines[line_i] = &(self->full_text[ch_byte_pos]);
            line_x = self->char_positions[char_pos];
            DEBUG_TEXR("we break!\n");
        }

        ++char_pos;
        ch_byte_pos = next_byte_pos;
    }

    self->lines_width[line_i] = self->char_positions[char_pos] - line_x;
    ++line_i;
    self->lines[line_i] = NULL;
    self->line_count = line_i;
    if (self->lines[line_i - 1][0] == '\0')
        --self->line_count; // the last line is empty
    DEBUG_TEXR("Line count:%d\n", self->line_count);
}

void
wg_text_render_elide(struct WGTextRender *self, int width, int height, enum WGTextElide elide)
{
    // TODO
}

static int
text_render_paint_char_internal(struct WGRenderFrame *dest,
                                int x, int y, uint32_t ch, int font_height)
{
    FT_Face face = NULL;
    FT_UInt glyph_index = 0;

    FT_Glyph glyph = glyph_cache_lookup(font_height, ch);
    if (glyph == NULL) {
        find_font_and_glyph(ch, &face, &glyph_index);
        if (face != NULL)
            set_char_size(face, font_height);

        FT_Error error = FT_Load_Glyph(face, glyph_index, FT_LOAD_RENDER);
        if (error) {
            wg_log_warning("Failed to load font face for char %x '%c'\n", (unsigned)ch, (char)ch);
            return 0; // TODO
        }

        glyph = glyph_cache_insert(font_height, ch, face->glyph);
    }

    if (glyph == NULL || glyph->format != FT_GLYPH_FORMAT_BITMAP) {
        wg_log_warning("Invalid glyph for char %x '%c'\n", (unsigned)ch, (char)ch);
        return 0;
    }

    FT_BitmapGlyph bglyph = ((FT_BitmapGlyph)glyph);
    text_render_draw_bitmap(&bglyph->bitmap, dest, x + bglyph->left,
        y - bglyph->top);

    return glyph->advance.x >> 16;
}

void
wg_text_render_paint_direct(struct WGRenderFrame *dest, int x, int y, char *st, int font_height)
{
    wg_render_frame_init_position(dest, &x, &y);
    if (dest->width <= x || dest->height <= y) {
        wg_log_debug("Text render would happen outside at %d,%d on a surface of size %dx%d\n",
                  x, y, dest->width, dest->height);
        return;
    }

    uint32_t ch;
    int next_byte_pos = 0;
    while ((ch = wg_u8_nextchar(st, &next_byte_pos)) != 0)
        x += text_render_paint_char_internal(dest, x, y, ch, font_height);
}

static void
text_render_paint_line(struct WGRenderFrame *dest, int x, int y,
                       const char *st_begin, const char *st_end,
                       int ** pchar_pos, int font_height)
{
    int *char_pos = *pchar_pos;
    FT_ULong ch;
    int ch_byte_pos = 0, next_byte_pos = 0;
    while ((ch = wg_u8_nextchar(st_begin, &next_byte_pos)) != 0 &&
           (st_end == NULL || st_begin + ch_byte_pos < st_end))
    {
        text_render_paint_char_internal(dest, x + *char_pos, y, ch, font_height);
        ++char_pos;
        ch_byte_pos = next_byte_pos;
    }
    *pchar_pos = char_pos;
}

void
wg_text_render_paint(struct WGTextRender *cache, struct WGRenderFrame *dest,
                  int x, int y, enum WGAlignment align)
{
    // TODO align center, right  -> adjust x for each line
    // TODO justify              -> adjust at spaces
    wg_render_frame_init_position(dest, &x, &y);
    if (dest->width <= x || dest->height + cache->line_height <= y) {
        DEBUG_TEXR("Text render would happen outside at %d,%d on a surface of size %dx%d\n",
                  x, y, dest->width, dest->height);
        return;
    }
    DEBUG_TEXR("rendering '%s' to %d,%d\n", cache->full_text, x, y);

    int *char_pos = cache->char_positions;

    for (int i = 0; i < cache->line_count; ++i) {
        int text_width = cache->lines_width[i];

        int delta_x = 0;
        if (align & WG_AlignHCenter)
            delta_x = cache->width / 2 - text_width / 2;
        else if (align & WG_AlignRight)
            delta_x = cache->width - text_width;

        text_render_paint_line(dest, x - *char_pos + delta_x, y,
            cache->lines[i], cache->lines[i+1], &char_pos, cache->font_height);
        y += cache->line_height;
    }
}

void
wg_text_render_deinit(void)
{
#ifdef DEBUG_CACHE_MISS
    wg_log_info("Cache miss: %d / %d\n", cache_miss, cache_lookups);
#endif

    FcFini();
    for (FT_Face *pface = faces; *pface != NULL; ++pface)
    {
        FT_Done_Face(*pface);
        *pface = NULL;
    }
    FT_Done_FreeType(library);
}
