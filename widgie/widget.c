// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "widget.h"

#include "settings.h"
#include "draw.h"
#include "log.h"

#include <stdlib.h>
#include <stdio.h>

#define SET_FLAG(flags, flag, enable)  flags = (enable ? flags | flag : ~flag & flags)

void
wg__widget_free(struct WGWidget *self)
{
    free(self);
}

void
wg__widget_render(struct WGWidget *self, struct WGRenderFrame *dest)
{
    // TODO: it could use a depth for a color and one pixel inner
    // TODO this would require rendering the opposite order: from derived class to base class
    if (wg_settings()->debug.show_widget_bounding_rects)
    {
        wg_draw_rect_border(dest, self->x, self->y, self->width, self->height, 0xff0000);
        wg_draw_rect_border(dest, self->x + 1, self->y + 1, self->width - 2, self->height - 2, 0x000000);
    }
}

void
wg__widget_init(struct WGWidget *self)
{
    self->parent = NULL;
    self->x = self->y = self->width = self->height = 0;
    self->flags = WFlagDefault;
    self->free = wg__widget_free;
    self->render = wg__widget_render;
    self->widget_at = wg__widget_at;
    self->on_mouse_press = wg__widget_on_mouse_press;
    self->on_mouse_release = wg__widget_on_mouse_release;
    self->on_mouse_click = wg__widget_on_mouse_click;
    self->on_mouse_longclick = wg__widget_on_mouse_longclick;
    self->on_mouse_move = wg__widget_on_mouse_move;
    self->on_keypress = wg__widget_on_keypress;
    self->focus_event = wg__widget_focus_event;
    self->request_rerender = wg__widget_request_rerender;
    self->request_showme = wg__widget_request_showme;
}

struct WGWidget *
wg_widget_new(void)
{
    struct WGWidget *self = malloc(sizeof(struct WGWidget));
    wg__widget_init(self);
    return self;
}

void
wg_widget_free(struct WGWidget *self)
{
    if (self != NULL)
        self->free(self);
}

void
wg_widget_render(struct WGWidget *self, struct WGRenderFrame *dest)
{
    if (wg_widget_is_hidden(self))
        return;
    self->render(self, dest);
}

struct WGWidget *
wg__widget_at(struct WGWidget *self, int x, int y)
{
    if (wg_widget_is_hidden(self))
        return NULL;

    if (x < self->x || x >= self->x + self->width)
        return NULL;

    if (y < self->y || y >= self->y + self->height)
        return NULL;

    return self;
}

int
wg__widget_on_mouse_press(struct WGWidget *self, int x, int y)
{
    // TODO debug goes here
    // printf("Mouse pressed at widget: %p (%d x %d)\n", self, self->x, self->y);
    return 0; // event is not handled
}

int
wg__widget_on_mouse_release(struct WGWidget *self)
{
    // printf("Mouse released at widget: %p (%d x %d)\n", self, self->x, self->y);
    return 0; // event is not handled
}

int
wg__widget_on_mouse_click(struct WGWidget *self)
{
    return 0;
}

int
wg__widget_on_mouse_longclick(struct WGWidget *self)
{
    return 0;
}

int
wg__widget_on_mouse_move(struct WGWidget *self, int x, int y)
{
    return 0;
}

void
wg__widget_request_rerender(struct WGWidget *self, struct WGWidget *originator)
{
    if (wg_widget_is_hidden(self))
        return;

    // currently we just rerender the whole window
    // (propagate to the toplevel parent until something rerenders, or not)
    if (self->parent != NULL)
        self->parent->request_rerender(self->parent, originator);
}

void
wg__widget_request_showme(struct WGWidget *self, struct WGWidget *originator)
{
    // propagate to the toplevel parent until some scroll area reacts on it (or not)
    if (self->parent != NULL)
        self->parent->request_showme(self->parent, originator);
}

int
wg__widget_on_keypress(struct WGWidget *self, const char *utf8, uint32_t keysym)
{
    return 0;  // by default a widget ignores keypress
}

int
wg__widget_focus_event(struct WGWidget *self, int is_focused)
{
    return 0;  // not focusable by default
}

void
wg_widget_request_rerender(struct WGWidget *self)
{
    self->request_rerender(self, self);
}

extern void wg__layout_precompute_positions(struct WGWidget *self);

void
wg_widget_request_showme(struct WGWidget *self)
{
    if (self->parent && wg_widget_is_layout(self->parent))
        wg__layout_precompute_positions(self->parent);

    self->request_showme(self, self);
}

int
wg_widget_mouse_click(struct WGWidget *self)
{
    return self->on_mouse_click(self);
}

int
wg_widget_keypress(struct WGWidget *self, const char *utf8, uint32_t keysym)
{
    return self->on_keypress(self, utf8, keysym);
}

void
wg_widget_set_hidden(struct WGWidget *self, int hidden)
{
    int old_hidden = wg_widget_is_hidden(self);
    hidden = (hidden != 0);
    SET_FLAG(self->flags, WFlagIsHidden, hidden);

    if (old_hidden != hidden && self->parent)
        wg_widget_request_rerender(self->parent);
}

void
wg_widget_resize(struct WGWidget *self, int width, int height)
{
    wg_log_debug("Widget %p resized to: %d, %d\n", self, width, height);
    if (self->width == width && self->height == height)
        return;

    self->width = width;
    self->height = height;
    wg_widget_request_rerender(self);
}

static inline int
intersects2d(int x1, int w1, int x2, int w2)
{
    if (x1 < x2) {
        return x1 + w1 >= x2;
    }
    return x2 + w2 >= x1;
}

int
wg_widget_intersects_rect(struct WGWidget *self, int x, int y, int width, int height)
{
    return intersects2d(self->x, self->width, x, width) &&
        intersects2d(self->y, self->height, y, height);
}
