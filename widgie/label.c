// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "label.h"

#include "text_rendering.h"

#include "settings.h"
#include "log.h"

#include <string.h>
#include <stdlib.h>

static int
ensure_render_init(struct WGLabel *label)
{
    if (label->render != NULL && (label->render->font_height != label->text_height))
    {
        wg_text_render_free(label->render);
        label->render = NULL;
    }

    if (label->render == NULL) {
        label->render = wg_text_render_new(label->text, label->text_height);
        if (label->render == NULL) {
            wg_log_error("Failed to init text render engine\n");
            return 0;
        }
    }
    return 1;
}

void
wg__label_init(struct WGLabel *self, const char *text)
{
    wg__widget_init(&self->base);

    self->base.render = wg__label_render;
    self->base.free = wg__label_free;
    self->on_text_change = NULL;
    // self->color = wg_settings()->label.color;
    self->text = NULL;
    self->text_height = wg_settings()->text.height;
    self->text_align = WG_AlignCenter;
    self->render = NULL;
    self->wrap_mode = WG_TextWrapAtChar;
    self->elide_mode = WG_TextElideLeft;
    wg_label_set_text(self, text);
}

void
wg__label_free(struct WGWidget *self)
{
    struct WGLabel *label = WG_LABEL(self);

    free(label->text);
    label->text = NULL;

    wg_text_render_free(label->render);
    label->render = NULL;

    wg__widget_free(self);
}

void
wg__label_render(struct WGWidget *self, struct WGRenderFrame *dest)
{
    if (self->width == 0)
        return;

    struct WGLabel *label = WG_LABEL(self);

    // wg_settings()->debug.show_widget_bounding_rects = 1;
    wg__widget_render(self, dest);
    // wg_settings()->debug.show_widget_bounding_rects = 0;

    if (label->text == NULL || label->text[0] == '\0')
        return;

    if (!ensure_render_init(label))
        return;

    if (self->width != label->render->width) {  // TODO also if height, wrapmode, elidemode has changed
        wg_text_render_wrap_lines(label->render, self->width, label->wrap_mode);
        wg_text_render_elide(label->render, self->width, self->height, label->elide_mode);
    }

    int delta_y = 0;
    int text_height = label->render->line_count * label->render->line_height;
    if (label->text_align & WG_AlignVCenter)
        delta_y = (self->height - text_height) / 2;
    else if (label->text_align & WG_AlignBottom)
        delta_y = self->height - text_height;

    delta_y += label->render->ascender_height;

    wg_log_debug("label text: '%s' position: %d,%d %dx%d align:%d ascender: %d delta_y: %d line_count: %d line_height: %d\n",
              label->text, self->x, self->y, self->width, self->height, label->text_align,
              label->render->ascender_height, delta_y, label->render->line_count, label->render->line_height);
    wg_text_render_paint(label->render, dest, self->x, self->y + delta_y, label->text_align);
}

struct WGWidget *
wg_label_new(const char *text)
{
    struct WGLabel *self = WG_LABEL(malloc(sizeof(struct WGLabel)));
    wg__label_init(self, text);
    return (struct WGWidget *)self;
}

void
wg_label_set_text(struct WGLabel *label, const char *text)
{
    if (text == NULL && text == label->text)
        return;

    if ((text != NULL && label->text != NULL && strcmp(text, label->text) == 0))
        return;

    free (label->text);
    label->text = text == NULL ? NULL : strdup(text);

    wg_text_render_free(label->render);
    label->render = NULL;

    wg_widget_request_rerender(WG_WIDGET(label));  // TODO needs to be some logic in the order of callbacks to behave consistent

    if (label->on_text_change)
        label->on_text_change(label);
}

void
wg_label_set_text_height(struct WGLabel *label, int new_height)
{
    label->text_height = new_height;
    wg_text_render_free(label->render);
    label->render = NULL;
    wg_widget_request_rerender(WG_WIDGET(label));
}

void
wg_label_set_text_align(struct WGLabel *label, enum WGAlignment new_align)
{
    label->text_align = new_align;
    wg_widget_request_rerender(WG_WIDGET(label));
}

void
wg_label_resize_oneline(struct WGLabel* label)
{
    struct WGWidget *widget = WG_WIDGET(label);
    widget->width = 0;
    widget->height = 0;

    if (label->text == NULL) {
        return;
    }

    if (label->render != NULL && label->render->line_count != 1)
    {
        wg_text_render_free(label->render);
        label->render = NULL;
    }

    if (!ensure_render_init(label))
        return;

    widget->width = label->render->width;
    widget->height = label->render->line_height;
}

int
wg_label_height_for_width(struct WGLabel* label, int width)
{
    if (!ensure_render_init(label)) {
        return WG_WIDGET(label)->height;
    }

    if (label->render->width != width)
        wg_text_render_wrap_lines(label->render, width, label->wrap_mode);

    return label->render->line_height * label->render->line_count;
}
