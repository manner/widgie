// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "linearlayout.h"

#include "log.h"

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

static inline int
length_for_dir(struct WGWidget *widget, enum WGDirection dir)
{
    if (dir == WG_Horizontal)
        return widget->width;
    else
        return widget->height;
}

static inline void
set_length_for_dir(struct WGWidget *widget, int length, enum WGDirection dir)
{
    if (dir == WG_Horizontal)
        widget->width = length;
    else
        widget->height = length;
}

void
wg__linear_layout_do_layout(struct WGLayout *self)
{
    struct WGLinearLayout *lself = WG_LINEAR_LAYOUT(self);
    enum WGDirection direction = lself->direction;
    enum WGDirection direction_inv = !lself->direction;

    int count_of_fits = 0;
    int occupied_space = 0;
    for (int i = 0; i < self->children_count; ++i)
    {
        struct WGWidget *child = self->children[i];
        if (wg_widget_is_hidden(child))
            continue;

        int fit = lself->fits[i];
        if (fit)
            ++count_of_fits;
        else
            occupied_space += length_for_dir(child, direction);
    }

    if (occupied_space > length_for_dir(WG_WIDGET(self), direction)) {
        occupied_space = length_for_dir(WG_WIDGET(self), direction);
    }

    int fit_widget_space = 0;
    if (count_of_fits > 0)
        fit_widget_space = (length_for_dir(WG_WIDGET(self), direction) - occupied_space) / count_of_fits;
    int my_space_inv = length_for_dir(WG_WIDGET(self), direction_inv);

    int x = WG_WIDGET(self)->x, y = WG_WIDGET(self)->y;
    // wg_log_debug("Start: occupied_space=%d count_of_fits=%d fit_widget_space=%d\n", occupied_space, count_of_fits, fit_widget_space);
    for (int i = 0; i < self->children_count; ++i)
    {
        struct WGWidget *child = self->children[i];
        if (wg_widget_is_hidden(child))
            continue;

        int fit = lself->fits[i];
        if (fit)
            set_length_for_dir(child, fit_widget_space, direction);

        // set the other direction of the child (eg. height in horizontal mode)
        set_length_for_dir(child, my_space_inv, direction_inv);

        child->x = x;
        child->y = y;

        if (child->width + x > WG_WIDGET(self)->width + WG_WIDGET(self)->x)
            child->width = WG_WIDGET(self)->width + WG_WIDGET(self)->x - x;

        if (child->height + y > WG_WIDGET(self)->height + WG_WIDGET(self)->y)
            child->height = WG_WIDGET(self)->height + WG_WIDGET(self)->y - y;

        if (direction == WG_Horizontal)
            x += child->width;
        else
            y += child->height;

        /*
            wg_log_debug("Child position %d x %d, size %d x %d\n",
                child_widget->x, child_widget->y, child_widget->width, child_widget->height);
        */
    }
    // wg_log_error("end\n");
}

void
wg__linear_layout_init(struct WGLinearLayout *self, enum WGDirection direction)
{
    wg__layout_init(&self->base);
    WG_WIDGET(self)->free = wg__linear_layout_free;
    WG_LAYOUT(self)->do_layout = wg__linear_layout_do_layout;

    self->direction = direction;
    self->fits = NULL;
}

struct WGWidget *
wg_linear_layout_new(enum WGDirection direction)
{
    struct WGLinearLayout *self = WG_LINEAR_LAYOUT(malloc(sizeof(struct WGLinearLayout)));
    wg__linear_layout_init(self, direction);
    return (struct WGWidget *)self;
}

void
wg_linear_layout_add(struct WGLinearLayout *self, struct WGWidget *child, int fit)
{
    int alloced = WG_LAYOUT(self)->children_alloced;
    wg_layout_add(WG_LAYOUT(self), child);

    if (WG_LAYOUT(self)->children_alloced != alloced) {
        // extend our item data to have the same amount of free space as the children
        alloced = WG_LAYOUT(self)->children_alloced;
        self->fits = (int *)realloc(self->fits, alloced * sizeof(int));
    }
    self->fits[WG_LAYOUT(self)->children_count - 1] = fit;
}

void
wg_linear_layout_fit_to_content(struct WGLinearLayout *self)
{
    enum WGDirection direction = self->direction;
    enum WGDirection direction_inv = !self->direction;

    int occupied_space = 0;
    int occupied_space_inv = 0;
    for (int i = 0; i < WG_LAYOUT(self)->children_count; ++i)
    {
        struct WGWidget *child = WG_LAYOUT(self)->children[i];
        if (wg_widget_is_hidden(child))
            continue;

        int fit = self->fits[i];
        if (!fit)
            occupied_space += length_for_dir(child, direction);

        int size_inv = length_for_dir(child, direction_inv);
        if (size_inv > occupied_space_inv)
            occupied_space_inv = size_inv;
    }

    set_length_for_dir(WG_WIDGET(self), occupied_space, direction);
    set_length_for_dir(WG_WIDGET(self), occupied_space_inv, direction_inv);
}

void
wg__linear_layout_free(struct WGWidget *self)
{
    free(WG_LINEAR_LAYOUT(self)->fits);
    wg__layout_free(self);
}
