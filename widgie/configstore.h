// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_CONFIG_STORE
#define WG_HEADER_CONFIG_STORE

extern char *app_path;

struct WGConfigStore {
    int (*set) (struct WGConfigStore *self, const char *key, const char *value);
    void (*load_complete)(struct WGConfigStore *self);  // called when config reading finished
    void (*free)(struct WGConfigStore *self);
    int (*print_example)(struct WGConfigStore *self, int fd);
    void (*scale_sizes)(struct WGConfigStore *self, double scale);
    struct WGConfigStore *(*new_subgroup)(struct WGConfigStore *self, const char *subgroup_name);
    const char *fname;
};

#define WG_CONFIG_STORE(x) ((struct WGConfigStore *)x)

struct WGConfigStore *wg_configstore_get(const char *name);
void wg_configstore_register(const char *name, struct WGConfigStore *store);

int wg_configstore_set(const char *group, const char *key, const char *value);
void wg_configstore_scale_sizes(double scale);

void wg_configstore_deinit(void);

/* Compute a config path. Returns a newly allocated string:
 * If XDG_CONFIG_HOME environment exists, then returns [XDG_CONFIG_HOME]/[app_name]/[filename]
 * Otherwise uses "[HOME]/.config" instead of XDG_CONFIG_HOME
 *
 * If app_name or filename is NULL, they fallback to their defaults:
 *   - app_name defaults to "widgie"
 *   - filename defaults to "widgie.conf"
 * 
 */
char *wg_config_path_ext(const char *app_name, const char *filename, const char *extension);
static inline char *wg_config_path(const char *app_name, const char *filename)
{ return wg_config_path_ext(app_name, filename, ""); }

/* Load the registered settings.
 * Configurations will be loaded in this order:
 *   - XDG_CONFIG_HOME/widgie/widgie.conf
 *   - XDG_CONFIG_HOME/app_name/widgie.conf  (only if app_name != NULL)
 *   - XDG_CONFIG_HOME/app_name/extra.conf (only if not NULL). Specify only the basename!
 * where app_name is argv[0], and extra.conf are specified in the variadic part.
 * You must end the variadic arguments with a NULL.
 *
 * Returns the last error that happens (gracefully tries loading other files as well) or
 * 0 if success.
 */
int wg_config_load(int argc, char **argv, /* extra conf */...);

/* Load registered settings from a file.
 * Prefer using wg_config_load() if that suffice.
 */
int wg_config_load_path(const char *path);

// TODO: no use yet, just for symmetry (save/log):
// int widgie_config_load_fd(int fd);
// int wg_config_load(const char *appname, /* extra conf */);

// TODO: set default before load? to support reload use case

int wg_config_save(const char *appname);
int wg_config_save_path(const char *path);
int wg_config_save_fd(int fd);

#endif  // HEADER_CONFIG_STORE