// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_TEXT_RENDERING
#define WG_HEADER_TEXT_RENDERING

#include <widgie/frame.h>
#include <widgie/uidefs.h>

struct WGTextRender {
    // depends on the text + fontsize only, computed at creation
    int font_height;
    int line_height;
    int ascender_height;
    int descender_height;

    int *char_positions;
    char *full_text;
    int char_count;

    // depends on the width x height, recomputed with wrap_lines
    int width;
    int line_count;
    char **lines;
    int *lines_width;
};

struct WGTextRender *wg_text_render_new(const char *text, int font_height);
void wg_text_render_free(struct WGTextRender *self);

// needs width x height:
void wg_text_render_wrap_lines(struct WGTextRender *self,
    int width, enum WGTextWrap wrap);
void wg_text_render_elide(struct WGTextRender *self,
    int width, int height, enum WGTextElide elide);

// do the actual render
void wg_text_render_paint_direct(struct WGRenderFrame *dest, int x, int y, char *st, int font_height);
void wg_text_render_paint(struct WGTextRender *cache, struct WGRenderFrame *dest,
                       int x, int y, enum WGAlignment horizontal_align);

void wg_text_render_deinit(void);

#endif  // HEADER_TEXT_RENDERING
