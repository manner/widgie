// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_DRAW
#define WG_HEADER_WIDGIE_DRAW

#include <stdint.h>

struct WGRenderFrame;

void wg_draw_rect(struct WGRenderFrame *dest, int x, int y, int width, int height, uint32_t color);
void wg_draw_rect_border(struct WGRenderFrame *dest, int x, int y, int width, int height, uint32_t color);

void wg_draw_hline(struct WGRenderFrame *dest, int x, int y, int width, uint32_t color);
void wg_draw_vline(struct WGRenderFrame *dest, int x, int y, int height, uint32_t color);

#endif // FB_HEADER_WIDGIE_DRAW