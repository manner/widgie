// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_APPLICATION
#define WG_HEADER_APPLICATION

#include <widgie/uidefs.h>

#include <stdint.h>

#define WG_MAX_EVENTS 10

typedef void (*KeyboardHandlerFunc)(void *userdata, const char *utf8, uint32_t keysym);
typedef void (*EventFunc)(void *userdata);

struct WGEvent {
    EventFunc func;
    void *data;
    int64_t time_ms;  // Time after which the event triggers.
                      //   INT64_MAX means "never". Negative is invalid (will be ignored with warning)
    int fd;           // -1 or a file descriptor which triggers the event if received traffic.
};

struct WGApplication {
    struct WGWindow *window;  // TODO: handle more?
    struct WGApplicationBackend *backend;

    void (*orientation_change)(enum WGOrientation orientation);

    KeyboardHandlerFunc key_handlers[4];
    void *key_handlers_data[4];
    int keyboard_grab;

    struct WGEvent events[WG_MAX_EVENTS];

    enum WGOrientation desktop_orientation;

    int exit_requested;
    int exit_code;
};

struct WGApplication *wg_application_new(void);
int wg_application_run(void);
int wg_application_dispatch(void);
void wg_application_request_exit(int exit_code);

void wg_application_watch_keyboard(KeyboardHandlerFunc handler, void *userdata);
void wg_application_key_press(const char *utf8, uint32_t keysym);
void wg_application_add_event(int64_t trigger_ms, int trigger_fd, EventFunc func, void *userdata);
int wg_application_del_event(EventFunc func);  // returns the number of removed events

static inline void wg_application_add_event_timer(int64_t trigger_ms, EventFunc func, void *userdata)
{ wg_application_add_event(trigger_ms, -1, func, userdata); }

static inline void wg_application_add_event_fd(int trigger_fd, EventFunc func, void *userdata)
{ wg_application_add_event(INT64_MAX, trigger_fd, func, userdata); }

void wg_application_orientation_change(enum WGOrientation new_orientation);

void wg_application_set_keyboard_grab(int should_grab);

void wg_application_free(void);

// a monothonic time in millisec counted from an arbitrary time in the past
int64_t wg_application_time_ms(void);

extern struct WGApplication *wg_app;

#endif // FB_HEADER_APPLICATION