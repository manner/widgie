// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "gridlayout.h"

#include "log.h"

#include "draw.h"

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


void
wg__grid_layout_do_layout(struct WGLayout *self)
{
    struct WGGridLayout *grid = WG_GRID_LAYOUT(self);

    int cell_width = grid->columns > 0 ? WG_WIDGET(self)->width / grid->columns : 0;
    int cell_height = grid->rows > 0 ? WG_WIDGET(self)->height / grid->rows : 0;

    int col = 0, row = 0;
    int child_x = WG_WIDGET(self)->x + (grid->gap_width >> 1);
    int child_y = WG_WIDGET(self)->y + (grid->gap_height >> 1);

    for (int i = 0; i < self->children_count; ++i)
    {
        struct WGWidget *child = self->children[i];
        if (wg_widget_is_hidden(child)) {
            continue;
        }
        child->width = cell_width - grid->gap_width;
        child->height = cell_height - grid->gap_height;
        child->x = child_x;
        child->y = child_y;

        ++col;
        child_x += cell_width;
        if (col >= grid->columns) {
            col = 0;
            child_x = WG_WIDGET(self)->x + (grid->gap_width >> 1);
            ++row;
            child_y += cell_height;
        }
    }
}

void
wg__grid_layout_init(struct WGGridLayout *self, int columns, int rows)
{
    wg__layout_init(&self->base);
    self->columns = columns;
    self->rows = rows;
    self->gap_width = 2;  // TODO settings
    self->gap_height = 2;
    WG_LAYOUT(self)->do_layout = wg__grid_layout_do_layout;
}

struct WGWidget *
wg_grid_layout_new(int columns, int rows)
{
    struct WGGridLayout *self = WG_GRID_LAYOUT(malloc(sizeof(struct WGGridLayout)));
    wg__grid_layout_init(self, columns, rows);
    return (struct WGWidget *)self;
}

void
wg_grid_layout_reset_children(struct WGGridLayout *self, int columns, int rows, int destroy)
{
    wg_layout_reset_children(WG_LAYOUT(self), destroy);
    self->columns = columns;
    self->rows = rows;
}

// TODO there should be a mechanism for the child to free up itself parent_widget->releaseChild(self)
