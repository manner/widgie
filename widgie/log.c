// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "log.h"

FILE *wg_log_output = NULL;
int wg_log_level = DEBUG_LEVEL_INFO;
int wg_log_displ_source = 1;

static FILE *file_we_opened = NULL;

static void
fclose_if_needed(void)
{
    if (file_we_opened && file_we_opened == wg_log_output)
    {
        fclose(file_we_opened);
        file_we_opened = NULL;
    }
}

void
wg_log_to_file(FILE *file)
{
    fclose_if_needed();
    wg_log_output = file;
}

void
wg_log_to_path(const char *path, int append)
{
    fclose_if_needed();
    file_we_opened = fopen(path, append ? "a" : "w+");
    wg_log_to_file(file_we_opened);
}

void
wg_log_deinit(void)
{
    fclose_if_needed();
}

void
wg_log_set_level(int level)
{
    wg_log_level = level;
}

void
wg_log_set_display_source(int display)
{
    wg_log_displ_source = display;
}