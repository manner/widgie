// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_LINEARLAYOUT
#define WG_HEADER_WIDGIE_LINEARLAYOUT

#include <widgie/layout.h>
#include <widgie/uidefs.h>

struct WGLinearLayoutAlignContext;

struct WGLinearLayout {
    struct WGLayout base;

    enum WGDirection direction;
    int *fits;
};

struct WGWidget *wg_linear_layout_new(enum WGDirection direction);

// "fit" is a boolean and if 1, the widget will be resized to the maximum space available.
// If "fit" is 0, the widget size won't be changed only its position
void wg_linear_layout_add(struct WGLinearLayout *self, struct WGWidget *child, int fit);

void wg_linear_layout_fit_to_content(struct WGLinearLayout *self);

#define WG_LINEAR_LAYOUT(x) ((struct WGLinearLayout *)x)

// protected
void wg__linear_layout_init(struct WGLinearLayout *self, enum WGDirection direction);
void wg__linear_layout_free(struct WGWidget *self);
void wg__linear_layout_do_layout(struct WGLayout *self);

#endif // FB_HEADER_WIDGIE_LINEARLAYOUT