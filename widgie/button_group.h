// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_BUTTON_GROUP
#define WG_HEADER_BUTTON_GROUP

#include <stdint.h>

struct WGButton;

struct WGButtonGroupItem {
    struct WGButton *button;
    struct WGButtonGroupItem *next;
    struct WGButtonGroupItem *prev;
};

struct WGButtonGroup
{
    struct WGButtonGroupItem *first;
    struct WGButtonGroupItem *last;

    struct WGButtonGroupItem *selected;
    int select_rotates;  // if 1, select_next will select the first item after the last etc.
};

struct WGButtonGroup *wg_button_group_new(void);

void wg_button_group_append(struct WGButtonGroup *self, struct WGButton *button);

void wg_button_group_select(struct WGButtonGroup *self, struct WGButtonGroupItem *item);
void wg_button_group_select_first(struct WGButtonGroup *self);
void wg_button_group_select_next(struct WGButtonGroup *self);
void wg_button_group_select_prev(struct WGButtonGroup *self);

// Note, this is O(n) for now, returns -1 if not found, otherwise the position inside the list of buttons
int wg_button_group_select_button(struct WGButtonGroup *self, struct WGButton *widget);

// returns 0 if no buttons could be clicked, 1 otherwise
int wg_button_group_click_selected(struct WGButtonGroup *self);

void wg_button_group_free(struct WGButtonGroup *self);

// Forward a keypress to the button group
void wg_button_group_keypress(struct WGButtonGroup *self, const char *utf8, uint32_t keysym);

#define WG_BUTTON_GROUP(x) ((struct WGButtonGroup *)(x))

#endif // FB_HEADER_BUTTON_GROUP
