// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_INIPARSER
#define WG_HEADER_WIDGIE_INIPARSER

#include "uidefs.h"
#include <stdint.h>

typedef int (*ConfigReceiverFunc)(const char *section,
    const char *key, const char *value, void *userdata);

/* Load a config file at "path". Receive results along with "userdata" in "receiver". */
int wg_ini_parser_load(const char *path, ConfigReceiverFunc receiver, void *userdata);

/* Process one line of config entry.
 *   line: the line to process without EOL
 *   pcurrent_group: pointer to the current group which might change according to the line.
 *          If NULL, the default group is assumed and any resulting group will not be stored.
 *   path, line_num: the path of the file and the line number (for debug logs).
 *          Specify NULL, -1 if none.
 */
int wg_ini_parser_process_line(char *line, char **pcurrent_group, const char *path, int line_num,
                            ConfigReceiverFunc receiver, void *userdata);

int wg_ini_parser_parse_bool(const char *value);
char *wg_ini_parser_parse_string(const char *value);   // equivalent of an strdup and an unescape
char **wg_ini_parser_parse_stringlist(const char *value);
uint32_t wg_ini_parser_parse_color(const char *value);
enum WGAlignment wg_ini_parser_parse_alignment(const char *value);
int wg_ini_parser_parse_int(const char *value);
long wg_ini_parser_parse_long(const char *value);
double wg_ini_parser_parse_double(const char *value);

int wg_ini_parser_write_section(int fd, const char *section);
int wg_ini_parser_write_subsection(int fd, const char *section);
int wg_ini_parser_write_bool(int fd, const char *key, int value);
int wg_ini_parser_write_string(int fd, const char *key, const char *value);
int wg_ini_parser_write_stringlist(int fd, const char *key, char **value);
int wg_ini_parser_write_color(int fd, const char *key, uint32_t value);
int wg_ini_parser_write_number(int fd, const char *key, long long value);
int wg_ini_parser_write_alignment(int fd, const char *key, enum WGAlignment alignment);
int wg_ini_parser_write_double_with_precision(int fd, const char *key, double value, int precision);
static inline int wg_ini_parser_write_double(int fd, const char *key, double value)
{ return wg_ini_parser_write_double_with_precision(fd, key, value, 2); }
int wg_ini_parser_write_comment(int fd, const char *comment);

// some helper macros for filling configs
#define WG_SET_CONFIG2(settings, in_var_name, out_var_name, typ) \
    if (strcmp(key, #in_var_name) == 0) { \
        settings->out_var_name = wg_ini_parser_parse_ ## typ (value); \
    }
#define WG_SET_CONFIG2_FREE(settings, in_var_name, out_var_name, typ, free_cmd) \
    if (strcmp(key, #in_var_name) == 0) { \
        free_cmd(settings->out_var_name); \
        settings->out_var_name = wg_ini_parser_parse_ ## typ (value); \
    }

#define WG_SET_CONFIG(settings, var_name, typ) \
    WG_SET_CONFIG2(settings, var_name, var_name, typ)
#define WG_SET_CONFIG_FREE(settings, var_name, typ, free_cmd) \
    WG_SET_CONFIG2_FREE(settings, var_name, var_name, typ, free_cmd)

#define WG_SET_CONFIG_INT(settings, var_name) WG_SET_CONFIG(settings, var_name, int)

#define WG_SET_CONFIG_STR(settings, var_name) \
    WG_SET_CONFIG_FREE(settings, var_name, string, free)

#define WG_SET_CONFIG_STRLIST(settings, var_name) \
    WG_SET_CONFIG_FREE(settings, var_name, stringlist, wg_strarray_free)

#endif  // HEADER_WIDGIE_INIPARSER