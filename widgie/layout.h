// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_LAYOUT
#define WG_HEADER_WIDGIE_LAYOUT

#include <widgie/widget.h>

struct WGLayout {
    struct WGWidget base;

    struct WGWidget **children;
    int children_alloced;
    int children_count;

    void (*do_layout)(struct WGLayout *self);
    // TODO lookup_id_for_position

    struct WGLayoutCache *cache;
};

struct WGWidget *wg_layout_new(void);

// Note, widgets should be added from left to right, top to bottom! (required by _layout_widget_at)
void wg_layout_add(struct WGLayout *self, struct WGWidget *child);

// clears the list of children with or without freeing/destroying them
void wg_layout_reset_children(struct WGLayout *self, int destroy);

void wg_layout_remove_last(struct WGLayout *self, int destroy);

// request recomputing positions and sizes of the direct children
void wg_layout_request_relayout(struct WGLayout *self);

// request recomputing positions and sizes of the direct children and their children
void wg_layout_request_relayout_recursive(struct WGLayout *self);

// Count the visible childrens. Note: this is linear complexity for now!
int wg_layout_visible_children_count(struct WGLayout *self);

#define WG_LAYOUT(x) ((struct WGLayout *)x)

// protected API, should only called by derived classes
void wg__layout_init(struct WGLayout *self);
void wg__layout_free(struct WGWidget *w);
void wg__layout_render(struct WGWidget *w, struct WGRenderFrame *dest);
void wg__layout_request_rerender(struct WGWidget *self, struct WGWidget *originator);
struct WGWidget *wg__layout_widget_at(struct WGWidget *self, int x, int y);  // default implementation is a quick search

#endif // FB_HEADER_WIDGIE_LAYOUT