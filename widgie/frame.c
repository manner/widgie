// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "frame.h"
#include "log.h"

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

struct WGRenderFrame *
wg_render_frame_new(int width, int height, uint32_t *data)
{
    struct WGRenderFrame *result = malloc(sizeof(struct WGRenderFrame));
    result->width = width;
    result->height = height;
    result->data = data;
    result->dx = result->dy = 0;
    if (data == NULL) {
        result->data = malloc(width * height * sizeof(uint32_t));
    }
    return result;
}

void
wg_render_frame_free(struct WGRenderFrame *frame)
{
    if (frame == NULL)
        return;

    free(frame->data);
    free(frame);
}

int
wg__render_frame_copy_rect_fix_positions(struct WGRenderFrame *from_frame, struct WGRenderFrame *to_frame,
    int *pfrom_x, int *pfrom_y, int *pwidth, int *pheight, int *pto_x, int *pto_y)
{
    wg_render_frame_init_position(from_frame, pfrom_x, pfrom_y);
    wg_render_frame_init_position(to_frame, pto_x, pto_y);

    if (*pfrom_x < 0 || *pto_x < 0) {
        int lower_value = *pfrom_x < *pto_x ? *pfrom_x : *pto_x;
        *pfrom_x -= lower_value; // now both of them became >= 0
        *pto_x -= lower_value;
        *pwidth += lower_value;  // we decrease the width the same amount
    }

    if (*pfrom_x >= from_frame->width || *pto_x >= to_frame->width)
        return 1; // we have nothing to do

    int from_delta = *pfrom_x + *pwidth - from_frame->width;
    int to_delta = *pto_x + *pwidth - to_frame->width;
    int higher_delta = from_delta < to_delta ? to_delta : from_delta;
    if (higher_delta > 0) {
        *pwidth -= higher_delta;
    }

    // same for the height:
    if (*pfrom_y < 0 || *pto_y < 0) {
        int lower_value = *pfrom_y < *pto_y ? *pfrom_y : *pto_y;
        *pfrom_y -= lower_value;
        *pto_y -= lower_value;
        *pheight += lower_value;
    }

    if (*pfrom_y >= from_frame->height || *pto_y >= to_frame->height)
        return 1;

    from_delta = *pfrom_y + *pheight - from_frame->height;
    to_delta = *pto_y + *pheight - to_frame->height;
    higher_delta = from_delta < to_delta ? to_delta : from_delta;
    if (higher_delta > 0) {
        *pheight -= higher_delta;
    }

    return 0;
}

void
wg_render_frame_clear(struct WGRenderFrame *frame)
{
    memset(frame->data, 0, frame->width * frame->height * sizeof(*frame->data));
}


void
wg_render_frame_copy_rect(struct WGRenderFrame *from_frame, struct WGRenderFrame *to_frame,
                       int from_x, int from_y, int width, int height, int to_x, int to_y)
{
    if (wg__render_frame_copy_rect_fix_positions(from_frame, to_frame, &from_x, &from_y, &width, &height, &to_x, &to_y))
        return;  // nothing to do

    // now our task is ensured to be doable
    uint32_t *from_pos = from_frame->data + from_x + from_frame->width * from_y;
    uint32_t *to_pos = to_frame->data + to_x + to_frame->width * to_y;
    for (int y = 0; y < height; ++y)
    {
        memcpy(to_pos, from_pos, width * sizeof(*to_pos));
        from_pos += from_frame->width;
        to_pos += to_frame->width;
    }
}

int
wg_render_frame_fix_rect(struct WGRenderFrame *frame, int *x, int *y, int *width, int *height)
{
    if (*x > frame->width || *y > frame->height) {
        *width = 0; // do not draw, no rect
        *height = 0;
        return 0;
    }

    if (*x < 0) {
        *width = *width + *x;
        *x = 0;
    }

    if (*y < 0) {
        *height = *height + *y;
        *y = 0;
    }

    if (*x + *width > frame->width)
        *width = frame->width - *x;

    if (*y + *height > frame->height)
        *height = frame->height - *y;

    return *height > 0 && *width > 0;
}
