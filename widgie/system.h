// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_SYSTEM
#define WG_HEADER_WIDGIE_SYSTEM

/* Create the base directory of the specified path (recursively) if does not exist.
   Path must be absolute.
   Returns errno, or 0 if success. */
int wg_create_basedir_of(const char *path);

/* Returns the home directory of the current user. */
const char *wg_get_homedir(void);

// TODO move here the config path creation
char *wg_alloc_cache_path(const char *app_name, const char *filename);

#endif  // WG_HEADER_WIDGIE_SYSTEM
