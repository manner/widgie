// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_CONTENTLAYOUT
#define WG_HEADER_WIDGIE_CONTENTLAYOUT

#include <widgie/layout.h>

// Layout with one item inside that fits its content
// Used for widgets containing something else on top, eg button, window
struct WGContentLayout {
    struct WGLayout base;
};

struct WGWidget *wg_contentlayout_new(void);
void wg_contentlayout_set_content(struct WGContentLayout *self, struct WGWidget *content, int destroy);
struct WGWidget *wg_contentlayout_get_content(struct WGContentLayout *self);

#define WG_CONTENT_LAYOUT(x) ((struct WGContentLayout *)x)
#define WG_CONTENT_WIDGET(x) wg_contentlayout_get_content(WG_CONTENT_LAYOUT(x))

// protected API, should only called by derived classes
void wg__contentlayout_init(struct WGContentLayout *self);
void wg__contentlayout_do_layout(struct WGLayout *self);

static inline void wg__contentlayout_render(struct WGWidget *self, struct WGRenderFrame *dest)
{ return wg__layout_render(self, dest); }

static inline void wg__contentlayout_free(struct WGWidget *self)
{ return wg__layout_free(self); }

#endif // FB_HEADER_WIDGIE_CONTENTLAYOUT