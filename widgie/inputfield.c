// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "inputfield.h"

#include "utf8.h"
#include "settings.h"
#include "text_rendering.h"
#include "log.h"

#include "application.h"  // TODO for set_focus, we'll need better
#include "window.h"

#include <stddef.h>
#include <stdlib.h>
#include <xkbcommon/xkbcommon-keysyms.h>
#include <string.h>
#include <limits.h>


void
wg__inputfield_init(struct WGInputField *self)
{
    wg__label_init(&self->base, "");

    WG_WIDGET(self)->render = wg__inputfield_render;
    WG_WIDGET(self)->free = wg__inputfield_free;
    WG_WIDGET(self)->focus_event = wg__inputfield_focus_event;
    WG_WIDGET(self)->on_keypress = wg__inputfield_keypress;
    WG_WIDGET(self)->on_mouse_press = wg__inputfield_mouse_press;
    WG_WIDGET(self)->on_mouse_click = wg__inputfield_mouse_click;

    self->cursor_shape = strdup(wg_settings()->inputfield.cursor_shape);
    self->cursor_position = 0;
    self->is_focused = 0;

    wg_label_set_text_align(WG_LABEL(self), WG_AlignLeft | WG_AlignVCenter);  // default is left aligned for inputs
}

void
wg__inputfield_free(struct WGWidget *self)
{
    free(WG_INPUTFIELD(self)->cursor_shape);
    wg__label_free(self);
}

void
wg__inputfield_render(struct WGWidget *self, struct WGRenderFrame *dest)
{
    struct WGInputField *inputfield = WG_INPUTFIELD(self);
    struct WGLabel *label = WG_LABEL(self);

    char *original_text = label->text;
    if (inputfield->is_focused) {
        // TODO this is hack for now, for correct solution, we will need coordinates of characters, and
        // render text "inverting" colors.
        // Solve this during text wrap / elide implementation.
        label->text = wg_u8_strinsert(original_text, inputfield->cursor_position, inputfield->cursor_shape);
    }

    wg_text_render_free(label->render);
    label->render = NULL;
    wg__label_render(self, dest);

    if (inputfield->is_focused) {
        free(label->text);
        label->text = original_text;
    }
}

struct WGWidget *
wg_inputfield_new(void)
{
    struct WGInputField *self = WG_INPUTFIELD(malloc(sizeof(struct WGInputField)));
    wg__inputfield_init(self);
    return WG_WIDGET(self);
}

void
wg_inputfield_set_cursor_position(struct WGInputField *self, int position)
{
    self->cursor_position = position;

    if (self->cursor_position < 0)
        self->cursor_position = 0;

    int charsize = wg_u8_strlen(WG_LABEL(self)->text);  // TODO might be not very cheap
    if (self->cursor_position > charsize)
        self->cursor_position = charsize;

    wg_widget_request_rerender(WG_WIDGET(self));
}

int
wg__inputfield_keypress(struct WGWidget *self, const char *utf8, uint32_t keysym)
{
    if (wg_widget_is_hidden(self))  // TODO move to higher level, hidden widget should not receive keypress
        return 0;

    struct WGInputField *input = WG_INPUTFIELD(self);
    switch (keysym) {
        // TODO: some other keys might be handy, eg:
        //  - Shift + moving to do selection, Ctrl+c/v for copy/paste
        //  - Insert to switch to overwrite mode
        //  - Ctrl + left/right to jump a word
        case XKB_KEY_Tab:
            // TODO: focus next
            return 0;

        case XKB_KEY_Left:
            wg_inputfield_set_cursor_position(input, input->cursor_position - 1);
            return 1;

        case XKB_KEY_Right:
            wg_inputfield_set_cursor_position(input, input->cursor_position + 1);
            return 1;

        case XKB_KEY_Home:
            wg_inputfield_set_cursor_position(input, 0);
            return 1;

        case XKB_KEY_End:
            wg_inputfield_set_cursor_position(input, wg_u8_strlen(wg_inputfield_get_text(input)));
            return 1;

        case XKB_KEY_BackSpace:
            if (input->cursor_position > 0) {
                wg_inputfield_set_cursor_position(input, input->cursor_position - 1);
                wg_input_field_del(input);
            }
            return 1;

        case XKB_KEY_Escape:
            if (input->base.text != NULL && *input->base.text != '\0') {
                wg_inputfield_set_text(input, "");
                return 1;
            } else {
                return 0;
            }

        case XKB_KEY_Delete:
            wg_input_field_del(input);
            return 1;

        case XKB_KEY_Return:
            return 0;
    }

    if (utf8 != NULL && *utf8 != '\0') {
        wg_inputfield_add_text_at_cursor(input, utf8);
        return 1;
    }

    return 0;
}

int
wg__inputfield_focus_event(struct WGWidget *self, int is_focused)
{
    wg_log_debug("%s focused: %p '%s'\n", is_focused ? "" : "not", self, WG_LABEL(self)->text);

    // TODO eg we can change the border
    WG_INPUTFIELD(self)->is_focused = is_focused;
    wg_widget_request_rerender(self);
    return 1;  // focusable
}

void
wg_inputfield_set_text(struct WGInputField *self, const char *text)
{
    // this might seem stupid, but we might need to fix the cursor position if text is too short
    wg_inputfield_set_text_and_cursor(self, text, self->cursor_position);
}

void
wg_inputfield_add_text_at_cursor(struct WGInputField *self, const char *text)
{
    int new_cursor = self->cursor_position + wg_u8_strlen(text);

    // TODO we could avoid some copy, and multiple length computation with cleverer code
    char *new_text = wg_u8_strinsert(wg_inputfield_get_text(self), self->cursor_position, text);
    wg_inputfield_set_text_and_cursor(self, new_text, new_cursor);
    free(new_text);
}

void
wg_inputfield_set_text_and_cursor(struct WGInputField *self, const char *text, int cursor)
{
    wg_label_set_text(WG_LABEL(self), text);
    wg_inputfield_set_cursor_position(self, cursor);
}

void
wg_input_field_del(struct WGInputField *self)
{
    char *new_text = wg_u8_strdel(wg_inputfield_get_text(self), self->cursor_position, 1);
    wg_inputfield_set_text(self, new_text);
    free(new_text);
}

int
wg__inputfield_mouse_click(struct WGWidget *self)
{
    return wg_window_set_focus(wg_app->window, self);
}

int
wg__inputfield_mouse_press(struct WGWidget *self, int x, int y)
{
    return 1;  // to receive click
}
