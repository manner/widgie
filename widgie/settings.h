// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_SETTINGS
#define WG_HEADER_WIDGIE_SETTINGS

#include <widgie/iniparser.h>
#include <widgie/configstore.h>
#include <widgie/uidefs.h>

#include <stdint.h>

struct WGButtonGroupSettings {
    int select_rotates;
};

struct WGButtonSettings {
    uint32_t color;
    uint32_t pressed_color;
    uint32_t selected_color;
    struct WGButtonGroupSettings group;
};

struct WGTextSettings {
    char *font;
    char *fallback_font;
    char **font_paths;
    char **fallback_font_paths;
    int store_font_paths;

    int height;
    int icon_height;
};

struct WGInputFieldSettings {
    char *cursor_shape;
};

struct WGDebugSettings {
    int show_widget_bounding_rects;
    int log_level;
    int log_display_source;
    char *log_output;
};

struct WGX11BackendSettings {
    int shm;  // try to use shared memory extension
    double scale;  // scale up/down all width/height/size configurations with this value
};

struct WGWaylandBackendSettings {
    double scale;  // scale up/down all width/height/size configurations with this value
};

struct WGXkbSettings {
    char *rules;
    char *model;
    char *layout;
    char *variant;
    char *options;
    int repeat_rate;
    int repeat_delay;
};

struct WGWindowPlacementSettings {
    enum WGAlignment align;
    int margin_width;
    int margin_height;
    int rotate;
};

struct WGFBBackendSettings {
    double scale;  // scale up/down all width/height/size configurations with this value
    char *device;  // by default "/dev/fb0"
    char *tty;     // the tty device to "disable"

    struct WGXkbSettings xkb_config;
    struct WGWindowPlacementSettings window;
    int emulate_mouse;
};

struct WGBackendSettings {
    int wayland;
    int x11;
    int framebuffer;
    char **search_paths;
    struct WGX11BackendSettings x11_config;
    struct WGWaylandBackendSettings wayland_config;
    struct WGFBBackendSettings framebuffer_config;
};

struct WGScrollSettings {
    double overscroll;
};

struct WGWindowSettings {
    enum WGSizeConstraint constraint;
    int width;
    int height;
};

struct WGWidgieSettings
{
    struct WGConfigStore base;

    struct WGBackendSettings backend;
    struct WGButtonSettings button;
    struct WGInputFieldSettings inputfield;
    struct WGTextSettings text;
    struct WGScrollSettings scroll;
    struct WGWindowSettings window;
    struct WGDebugSettings debug;
    double scale;  // the actual scale for the backend
};

void wg_settings_register(void);

void wg_settings_set_defaults(void);  // restore the default settings

struct WGWidgieSettings *wg_settings(void);

char **wg_settings_get_default_plugin_search_paths(void);

#endif