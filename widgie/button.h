// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_BUTTON
#define WG_HEADER_WIDGIE_BUTTON

#include <widgie/contentlayout.h>

#include <stdint.h>

// represents a clickable rectangle
struct WGButton {
    struct WGContentLayout base;

    int is_pressed;
    int is_selected;

    uint32_t color;
    uint32_t pressed_color;
    uint32_t selected_color;
};

struct WGWidget *wg_button_new(void);
struct WGWidget *wg_button_new_with_content(struct WGWidget *content);
struct WGWidget *wg_button_new_with_label(const char *text);

void wg_button_set_content(struct WGButton *self, struct WGWidget *content, int destroy);

static inline struct WGWidget *wg_button_get_content(struct WGButton *self)
{ return WG_CONTENT_WIDGET(self); }

void wg_button_set_pressed(struct WGButton *self, int pressed);
void wg_button_set_selected(struct WGButton *self, int selected);

#define WG_BUTTON(x) ((struct WGButton *)x)

// protected API, should only called by derived classes
void wg__button_init(struct WGButton *self);

static inline void wg__button_free(struct WGWidget *self) {
    wg__contentlayout_free(self);
}

void wg__button_render(struct WGWidget *w, struct WGRenderFrame *dest);
int wg__button_on_mouse_press(struct WGWidget *self, int x, int y);
int wg__button_on_mouse_release(struct WGWidget *self);

#endif // FB_HEADER_WIDGIE_BUTTON