// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "application.h"
#include "priv/application_backend.h"
#include "configstore.h"
#include "log.h"

#include "priv/wayland.h"
#include "priv/x11.h"
#include "priv/fb.h"
#include "window.h"
#include "text_rendering.h"
#include "settings.h"
#include "utf8.h"

#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <dlfcn.h>
#include <signal.h>
#include <poll.h>
#include <locale.h>
#include <unistd.h>


struct WGApplication *wg_app = NULL;
static void *wg_backend_plugin_lib = NULL;

static int event_fds_count = -1;
static struct pollfd event_fds[WG_MAX_EVENTS + 1];
static struct WGEvent *event_fds_event[WG_MAX_EVENTS + 1];  // pointer to the event belonging to each pollfd

static void
application_handle_window_close(struct WGWindow *window)
{
    (void) window;
    // until we do not support multiple windows, the default close handler just exits the app
    wg_application_request_exit(0);
}

static void
wg__application_trigger_event(struct WGEvent *event)
{
    EventFunc func = event->func;
    event->func = NULL;  // TODO: only delete the event if it returns so?
    event_fds_count = -1;  // forces an update of pollfds
    wg_log_debug("executing event: %d\n", (int)(event - wg_app->events));
    func(event->data);
}

static int64_t
application_dispatch_older_events(int64_t now)
{
    int max = sizeof(wg_app->events) / sizeof(wg_app->events[0]);
    for (int i = 0; i < max; ++i) {
        struct WGEvent *event = &wg_app->events[i];
        if (event->func != NULL) {
            if (event->time_ms <= now) {
                wg__application_trigger_event(event);
            }
        }
    }

    int64_t next_event_time = INT64_MAX;
    for (int i = 0; i < max; ++i) {
        struct WGEvent *event = &wg_app->events[i];
        if (event->func != NULL) {
            if (event->time_ms < next_event_time) {
                next_event_time = event->time_ms;
            }
        }
    }

    if (next_event_time == INT64_MAX)
        return -1; // no event needs to trigger

    wg_log_debug("-> next event time: %ld\n", next_event_time - now);

    return next_event_time - now;
}

int
wg_application_dispatch(void)
{
    int rc = 0;
    int64_t timeout_ms = application_dispatch_older_events(wg_application_time_ms());
    if ((rc = wg_app->backend->dispatch(wg_app->backend, timeout_ms)) < 0) {
        wg_log_debug("Backend dispatch returned %d\n", rc);
        return rc;
    }

    wg_window_dispatch(wg_app->window);

    return rc;
}

static BackendNewFunc
open_shared_backend(const char *backend_name, char **search_paths)
{
    char *so_path = NULL;
    BackendNewFunc result = NULL;

    if (search_paths == NULL) {
        wg_log_error("Error determining search paths for display server plugins\n");
        return NULL;
    }

    for (char **psearch_path = search_paths; *psearch_path != NULL; ++psearch_path)
    {
        if (wg_backend_plugin_lib != NULL) {
            dlclose(wg_backend_plugin_lib);
            wg_backend_plugin_lib = NULL;
        }
        free(so_path);

        if (asprintf(&so_path, "%s/libwidgie-%s.so", *psearch_path, backend_name) < 0) {
            wg_log_error("Failed to allocate memory\n");
            so_path = NULL;
            break;
        }
        wg_log_debug("Searching for %s\n", so_path);

        if (access(so_path, F_OK) == 0) {
            wg_backend_plugin_lib = dlopen(so_path, RTLD_LAZY);

            if (wg_backend_plugin_lib == NULL) {
                wg_log_warning("dlopen failed on shared object backend '%s': %s\n", so_path, dlerror());
                continue;
            }

            struct WGBackendPlugin *plugin_sym = dlsym(wg_backend_plugin_lib, "wg_plugin");
            if (plugin_sym == NULL) {
                wg_log_warning("dlsym failed to find some symbol in the shared object '%s': %s\n", so_path, dlerror());
                continue;
            } else {
                wg_log_debug("Loaded backend: '%s'\n", so_path);
                plugin_sym->app = wg_app;
                plugin_sym->log_displ_source = &wg_log_displ_source;
                plugin_sym->log_level = &wg_log_level;
                plugin_sym->log_output = &wg_log_output;
                plugin_sym->settings = wg_settings;
                result = plugin_sym->backend_new;
                break;
            }
        }
    }

    if (result == NULL && wg_backend_plugin_lib != NULL) {
        dlclose(wg_backend_plugin_lib);
        wg_backend_plugin_lib = NULL;
    }
    free(so_path);
    return result;
}

static void
wg__application_update_pollfds(void)
{
    if (event_fds_count >= 0) {
        return;
    }
    event_fds_count = 1;  // first is reserved for the backend for now
                          // TODO: modify all backends to use fd events instead for listening
    const int fd_max = sizeof(event_fds) / sizeof(event_fds[0]);
    const int event_max = sizeof(wg_app->events) / sizeof(wg_app->events[0]);

    for (int i = 0; i < event_max; ++i) {
        if (wg_app->events[i].func != NULL && wg_app->events[i].fd >= 0) {
            if (event_fds_count >= fd_max) {
                break;
            }
            event_fds[event_fds_count].fd = wg_app->events[i].fd;
            event_fds[event_fds_count].events = POLLIN;
            event_fds_event[event_fds_count] = &(wg_app->events[i]);
            ++event_fds_count;
        }
    }
}

static int
wg__application_wait_fd(int fd, int timeout_ms)
{
    int ret = 0;
    event_fds[0].fd = fd;
    event_fds[0].events = POLLIN;
    wg__application_update_pollfds();

    int rc = poll(event_fds, event_fds_count, timeout_ms);
    if (rc < 0) {
        wg_log_error("Error polling: %d - %s\n", errno, strerror(errno));
    }
    else if (rc > 0)
    {
        for (int i = 1; i < event_fds_count; ++i) {  // trigger fired events
            if (event_fds[i].revents != 0) {
                event_fds[i].revents = 0;
                struct WGEvent *event = event_fds_event[i];
                wg__application_trigger_event(event);
            }
        }
        ret = event_fds[0].revents != 0;
        event_fds[0].revents = 0;
    }
    return ret;
}

int
wg_application_backend_init(void)
{
    if (wg_app->backend != NULL) {
        return 0;  // backend has already been initialized
    }

    int rc = 1;
    char **search_paths = NULL;

    struct WGApplicationFuncs wgfuncs =  {
        .key_pressed = wg_application_key_press,
        .orientation_changed = wg_application_orientation_change,
        .add_event = wg_application_add_event,
        .del_event = wg_application_del_event,
        .wait_fd = wg__application_wait_fd,
    };

    (void) open_shared_backend;
    (void) wgfuncs;

    search_paths = wg_settings()->backend.search_paths;
    if (search_paths != NULL && search_paths[0] != NULL) {
        search_paths = wg_strarray_dup(search_paths);
    } else {
        search_paths = wg_settings_get_default_plugin_search_paths();
    }

    if (search_paths == NULL) {
        goto finish;  // already logged memory allocation error
    }

#ifndef DISABLE_WAYLAND
    if (wg_settings()->backend.wayland && getenv("WAYLAND_DISPLAY") != NULL) {
#  ifdef SHARED_WAYLAND
        BackendNewFunc wayland_application_new = open_shared_backend("wayland", search_paths);
        if (wayland_application_new == NULL) {
            wg_log_warning("Failed to find/load the wayland plugin\n");
        } else
#  endif
        wg_app->backend = wayland_application_new(wgfuncs);
        if (wg_app->backend) // TODO move to wayland
            wg_settings()->scale = wg_settings()->backend.wayland_config.scale;
    }
#endif

#ifndef DISABLE_X11
    if (wg_app->backend == NULL && wg_settings()->backend.x11 && getenv("DISPLAY") != NULL) {
#  ifdef SHARED_X11
        BackendNewFunc x11_application_new = open_shared_backend("x11", search_paths);
        if (x11_application_new == NULL) {
            wg_log_warning("Failed to find/load the X11 plugin\n");
        } else
#  endif
        wg_app->backend = x11_application_new(wgfuncs);
        if (wg_app->backend) // TODO move to x11
            wg_settings()->scale = wg_settings()->backend.x11_config.scale;
    }
#endif

#ifndef DISABLE_FRAMEBUFFER
    if (wg_app->backend == NULL && wg_settings()->backend.framebuffer) {
#  ifdef SHARED_FRAMEBUFFER
        BackendNewFunc fb_application_new = open_shared_backend("fb", search_paths);
        if (fb_application_new == NULL) {
            wg_log_warning("Failed to find/load the framebuffer plugin\n");
        } else
#  endif
        wg_app->backend = fb_application_new(wgfuncs);
        if (wg_app->backend) // TODO move to fb
            wg_settings()->scale = wg_settings()->backend.framebuffer_config.scale;
    }
#endif

    if (wg_app->backend == NULL) {
        wg_log_error("Failed to initialize supported backends\n");
        if (wg_backend_plugin_lib) {
            dlclose(wg_backend_plugin_lib);
            wg_backend_plugin_lib = NULL;
        }

        goto finish;
    }

    wg_configstore_scale_sizes(wg_settings()->scale);

    if (wg_app->keyboard_grab && wg_app->backend->grab_keyboard != NULL) {
        wg_app->backend->grab_keyboard(wg_app->backend, 1);
    }

    rc = 0;

finish:
    wg_strarray_free(search_paths);
    return rc;
}

int
wg_application_run(void)
{
    if (wg_app == NULL) {
        wg_log_error("Please create an application first!");
    }

    if (wg_application_backend_init())
        return 1;

    if (wg_app->window == NULL) {
        wg_log_error("Please create a window first\n");
        return 1;
    }

    if (wg_app->window->close == NULL) {
        wg_app->window->close = application_handle_window_close;
    }

    wg_application_watch_keyboard((KeyboardHandlerFunc)wg_window_event_keypress, wg_app->window);

    // TODO this is not very good this way, revisit for multi window support (Needs to be before wayland dispatch)
    if (wg_app->window->backend == NULL) {
        wg__window_init_backend(wg_app->window);

        if (wg_app->window->backend == NULL) {
            wg_log_error("Failed to initialize supported window backends\n");
            return 1;
        }
    }

    while (!wg_app->exit_requested) {
        if (wg_application_dispatch() < 0)
            break;
    }

    wg_log_debug("Exiting\n");
    return wg_app->exit_code;
}

void
wg_application_request_exit(int exit_code)
{
    wg_app->exit_requested = 1;
    wg_app->exit_code = exit_code;
}

struct WGApplication *
wg_application_new(void)
{
    if (wg_app) {
        wg_log_warning("Skipping double init of application.\n");
        return wg_app;
    }

    setlocale(LC_ALL, "");  // init locale
    wg_app = (struct WGApplication *)calloc(1, sizeof(struct WGApplication));
    if (wg_app == NULL) {
        wg_log_error("Failed to allocate memory\n");
        return NULL;
    }

    wg_settings_register();

    if (signal(SIGTERM, wg_application_request_exit) == SIG_ERR ||
        signal(SIGHUP, wg_application_request_exit) == SIG_ERR ||
        signal(SIGINT, wg_application_request_exit) == SIG_ERR)
    {
        wg_log_warning("Failed to trap signals\n");
    }

    return wg_app;
}

void
wg_application_free(void)
{
    if (wg_app == NULL)
        return;

    wg_application_set_keyboard_grab(0);

    wg_window_free(wg_app->window);
    wg_app->window = NULL;

    if (wg_app->backend != NULL)
        wg_app->backend->free(wg_app->backend);
    wg_app->backend = NULL;

    if (wg_backend_plugin_lib) {
        dlclose(wg_backend_plugin_lib);
        wg_backend_plugin_lib = NULL;
    }

    free(wg_app);
    wg_app = NULL;

    wg_text_render_deinit();
    wg_configstore_deinit();
    wg_log_deinit();
}

void
wg_application_watch_keyboard(KeyboardHandlerFunc handler, void *data)
{
    int max = sizeof(wg_app->key_handlers) / sizeof(wg_app->key_handlers[0]);
    for (int i = 0; i < max; ++i) {
        if (wg_app->key_handlers[i] == NULL) {
            wg_app->key_handlers[i] = handler;
            wg_app->key_handlers_data[i] = data;
            return;
        }
    }
    wg_log_warning("Can't register more than %d handlers for now, handler is skipped\n", max);
}

void
wg_application_key_press(const char *utf8, uint32_t keysym)
{
    int max = sizeof(wg_app->key_handlers) / sizeof(wg_app->key_handlers[0]);
    for (int i = 0; i < max; ++i) {
        KeyboardHandlerFunc func = wg_app->key_handlers[i];
        if (func != NULL)
            func(wg_app->key_handlers_data[i], utf8, keysym);
    }
}

int64_t
wg_application_time_ms(void)
{
    struct timespec ts;
    if (clock_gettime(CLOCK_BOOTTIME, &ts) == -1) {
        wg_log_error("Unable to get clock: %d - %s\n", errno, strerror(errno));
        exit(1);
    }
    return (((int64_t)ts.tv_sec) * 1000 + ts.tv_nsec / 1000000);
}

void
wg_application_add_event(int64_t trigger_ms, int trigger_fd, EventFunc func, void *userdata)
{
    if (trigger_ms < 0) {
        wg_log_warning("Skipping event scheduled for the past\n");
        return;
    }

    int max = sizeof(wg_app->events) / sizeof(wg_app->events[0]);
    int64_t current_time_ms = wg_application_time_ms();
    for (int i = 0; i < max; ++i) {
        struct WGEvent *event = &wg_app->events[i];
        if (event->func == NULL) {
            event->func = func;
            event->data = userdata;
            event->fd = trigger_fd;
            event->time_ms = (trigger_ms > INT64_MAX - current_time_ms) /* overflow */ ?
                INT64_MAX : current_time_ms + trigger_ms;

            if (event->fd >= 0)
                event_fds_count = -1;  // forces an update of pollfds

            wg_log_debug("scheduled event %d: timeout: %ld, fd: %d\n",
                (int)(event - wg_app->events), trigger_ms, trigger_fd);
            return;
        }
    }

    wg_log_warning("Can't register more than %d events for now, handler is skipped\n", max);
}

int
wg_application_del_event(EventFunc func)
{
    int number_of_events = 0;
    int max = sizeof(wg_app->events) / sizeof(wg_app->events[0]);
    for (int i = 0; i < max; ++i) {
        struct WGEvent *event = &wg_app->events[i];
        if (event->func == func) {
            wg_log_debug("deleted event %d\n", (int)(event - wg_app->events));
            event->func = NULL;
            ++number_of_events;
            if (event->fd >= 0)
                event_fds_count = -1;  // forces an update of pollfds
        }
    }
    return number_of_events;
}

void
wg_application_orientation_change(enum WGOrientation new_orientation)
{
    if (wg_app->desktop_orientation == new_orientation)
        return;

    wg_app->desktop_orientation = new_orientation;
    if (wg_app->orientation_change != NULL)
        wg_app->orientation_change(new_orientation);
}

void
wg_application_set_keyboard_grab(int should_grab)
{
    if (should_grab == wg_app->keyboard_grab)
        return;

    wg_app->keyboard_grab = should_grab;
    if (wg_app->backend != NULL && wg_app->backend->grab_keyboard) {
        wg_app->backend->grab_keyboard(wg_app->backend, should_grab);
    }
}
