// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_UTF8
#define WG_HEADER_UTF8

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>

/* is c the start of a utf8 sequence? */
#define wg_isutf(c) (((c)&0xC0)!=0x80)

/* return next character, updating an index variable */
uint32_t wg_u8_nextchar(const char *s, int *i);

/* move to next character */
void wg_u8_inc(const char *s, int *i);

/* move to previous character */
void wg_u8_dec(const char *s, int *i);

uint32_t wg_u8_lower(uint32_t ch);
const char *wg_u8_strcasestr(const char *haystack, const char *needle);

/* return a pointer to the first occurrence of ch in s, or NULL if not
   found. character index of found character returned in *charn. */
char *wg_u8_strchr(char *s, uint32_t ch, int *charn);

/* count the number of characters in a UTF-8 string */
int wg_u8_strlen(const char *s);

char *wg_u8_strdel(const char *text, int char_pos, int len);

char *wg_u8_strinsert(const char *text, int char_pos, const char *part);

int wg_strarray_len(char **list);
char **wg_strarray_ndup(char **list, int count);
char **wg_strarray_dup(char **list);
void wg_strarray_free(char **list);
int wg_strarray_cmp(char **list1, char **list2);

#define wg_strdupnull(s) (s == NULL ? NULL : strdup(s))

#endif // FB_HEADER_UTF8
