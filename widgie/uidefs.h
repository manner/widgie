// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_UIDEFS
#define WG_HEADER_UIDEFS

enum WGDirection
{
    WG_Horizontal,
    WG_Vertical
};

enum WGOrientation
{
    WG_UnknownOrientation,
    WG_Landscape,
    WG_Portrait
};

#define WG_AlignTop WG_AlignLeft
enum WGAlignment {
    WG_AlignLeft = 0,

    WG_AlignHCenter = 1,
    WG_AlignRight = 2,

    WG_AlignVCenter = 4,
    WG_AlignBottom = 8,

    WG_AlignCenter = WG_AlignHCenter | WG_AlignVCenter,

    WG_AlignJustify = 16,
};

enum WGTextWrap {
    WG_TextWrapNone,
    WG_TextWrapAtChar,
    WG_TextWrapAtWord  // TODO implement
};

enum WGTextElide {
    WG_TextElideNone,
    WG_TextElideLeft,
    WG_TextElideCenter,
    WG_TextElideRight
};

enum WGMouseEvent {
    WG_MousePress,
    WG_MouseRelease,
    WG_MouseMove
};

enum WGSizeConstraint
{
    WG_PreferredSize = 0,  // -> window can be resized freely, width/height_hint is the default size
    WG_MinimumSize   = 1,  // -> width/height_hint specifies the minimum size of the window
    WG_MaximumSize   = 2,  // -> similarly for maximum
    WG_FixedSize = WG_MinimumSize | WG_MaximumSize,  // window has a fixed size and can not get resized ("dialog" mode)
    WG_UnspecifiedConstraint,
};


#define wg_max(x, y) ((x) > (y) ? (x) : (y))
#define wg_min(x, y) ((x) < (y) ? (x) : (y))
#define wg_bound(minimum, value, maximum) wg_max(minimum, wg_min(value, maximum))

#define WG_ALLOC_TYPE(Type) ((struct Type *)malloc(sizeof(struct Type)));

#endif  // HEADER_UIDEFS
