// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_SCROLLAREA
#define WG_HEADER_WIDGIE_SCROLLAREA

#include <widgie/contentlayout.h>
#include <widgie/frame.h>

// in case content is smaller than the scrollarea, where to align it
// when the mouse gets released
enum WGSnapAlignment {
    WGSnapAlignFree,
    WGSnapAlignBeginning,
    WGSnapAlignEnd,
};

struct WGScrollArea {
    struct WGContentLayout base;

    // configs
    enum WGSnapAlignment snap_alignment;
    double overscroll;  // how much over scrolling to permit

    int dx;  // the current delta of the scrolled area
    int dy;

    int last_mouse_x;
    int last_mouse_y;
    struct WGWidget *pressed_widget; // TODO some logic duplication here with the main window
};

struct WGWidget *wg_scrollarea_new(struct WGWidget *content);

static inline void wg_scrollarea_set_content(struct WGScrollArea *self, struct WGWidget *content, int destroy)
{
    wg_contentlayout_set_content(WG_CONTENT_LAYOUT(self), content, destroy);
    if (content)
        content->x = content->y = 0;
}

/**
 * Transforms the global coordinate (x,y) of the scene which contains the scroll area,
 * to the scene "inside" == content coordinates and returns the widget there if any.
 */
struct WGWidget *wg_scrollarea_content_at(struct WGScrollArea *self, int x, int y);

void wg_scrollarea_scroll_to_start(struct WGScrollArea *self);
void wg_scrollarea_scroll_to_end(struct WGScrollArea *self);

#define WG_SCROLLAREA(x) ((struct WGScrollArea *)x)

// protected API, should only called by derived classes
void wg__scrollarea_init(struct WGScrollArea *self);
void wg__scrollarea_render(struct WGWidget *w, struct WGRenderFrame *dest);
int wg__scrollarea_on_mouse_click(struct WGWidget *self);
int wg__scrollarea_on_mouse_longclick(struct WGWidget *self);
int wg__scrollarea_on_mouse_move(struct WGWidget *self, int x, int y);
int wg__scrollarea_on_mouse_press(struct WGWidget *self, int x, int y);
int wg__scrollarea_on_mouse_release(struct WGWidget *self);
void wg__scrollarea_request_rerender(struct WGWidget *self, struct WGWidget *originator);
void wg__scrollarea_request_showme(struct WGWidget *self, struct WGWidget *originator);
void wg__scrollarea_do_layout(struct WGLayout *layout);

#endif // FB_HEADER_WIDGIE_SCROLLAREA