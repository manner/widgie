// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "button.h"

#include "settings.h"
#include "draw.h"
#include "label.h"

#include <stdlib.h>


void
wg__button_render(struct WGWidget *self, struct WGRenderFrame *dest)
{
    struct WGButton *button = WG_BUTTON(self);
    uint32_t color = button->is_pressed ? button->pressed_color :
                     button->is_selected ? button->selected_color : button->color;
    wg_draw_rect(dest, self->x, self->y, self->width - 1, self->height - 1, color);

    // TODO make border customizable, create a base "Rectangle" widget type and inherit
    wg__contentlayout_render(self, dest);
}

void
wg__button_init(struct WGButton *self)
{
    wg__contentlayout_init(&self->base);

    WG_WIDGET(self)->render = wg__button_render;
    WG_WIDGET(self)->free = wg__button_free;
    WG_WIDGET(self)->on_mouse_press = wg__button_on_mouse_press;
    WG_WIDGET(self)->on_mouse_release = wg__button_on_mouse_release;

    self->is_pressed = 0;
    self->is_selected = 0;
    self->color = wg_settings()->button.color;
    self->pressed_color = wg_settings()->button.pressed_color;
    self->selected_color = wg_settings()->button.selected_color;
}

struct WGWidget *
wg_button_new(void)
{
    struct WGButton *self = malloc(sizeof(struct WGButton));
    wg__button_init(self);
    return (struct WGWidget *)self;
}

struct WGWidget *
wg_button_new_with_content(struct WGWidget *content)
{
    struct WGWidget *button = wg_button_new();
    wg_button_set_content(WG_BUTTON(button), content, 1);
    return button;
}

struct WGWidget *
wg_button_new_with_label(const char *text)
{
    return wg_button_new_with_content(wg_label_new(text));
}

void
wg_button_set_content(struct WGButton *self, struct WGWidget *content, int destroy)
{
    wg_contentlayout_set_content(WG_CONTENT_LAYOUT(self), content, destroy);
}

void
wg_button_set_selected(struct WGButton *self, int selected)
{
    self->is_selected = selected;
    if (selected)
        wg_widget_request_showme(WG_WIDGET(self));

    wg_widget_request_rerender(WG_WIDGET(self));
}

void
wg_button_set_pressed(struct WGButton *self, int pressed)
{
    self->is_pressed = pressed;
    wg_widget_request_rerender(WG_WIDGET(self));
}

int
wg__button_on_mouse_press(struct WGWidget *self, int x, int y)
{
    wg__widget_on_mouse_press(self, x, y);

    struct WGButton *button = WG_BUTTON(self);
    wg_button_set_pressed(button, 1);
    return 1; // event is handled
}

int
wg__button_on_mouse_release(struct WGWidget *self)
{
    wg__widget_on_mouse_release(self);

    struct WGButton *button = WG_BUTTON(self);
    wg_button_set_pressed(button, 0);
    return 1; // event is handled
}