// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "configstore.h"

#include "log.h"

#include "iniparser.h"
#include "system.h"

#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <libgen.h>

static struct WGConfigGroup {
    char *name;
    struct WGConfigStore *store;
    struct WGConfigGroup *next;
} *first_group = NULL;

static struct WGConfigGroup *last_group = NULL;

char *app_path = NULL;

extern int wg_application_backend_init(void);

static void
wg_configstore_register_at(const char *name, struct WGConfigStore *store, struct WGConfigGroup *current)
{
    struct WGConfigGroup *cg = (struct WGConfigGroup *) malloc(sizeof(struct WGConfigGroup));
    if (cg == NULL || (cg->name = strdup(name)) == NULL) {
        wg_log_error("No memory to allocate config group: %s\n", name);
        free(cg);
        return;
    }

    cg->next = NULL;
    cg->store = store;

    if (first_group == NULL) {
        last_group = first_group = cg;
    } else {
        cg->next = current->next;
        current->next = cg;

        if (last_group == current)
            last_group = cg;
    }
}

static void
wg_configstore_split_child_name(const char *name, char **pparent_name, const char **pchild_name)
{
    *pparent_name = NULL;
    *pchild_name = NULL;
    char *pos = strchr(name, '/');
    if (pos == NULL)
        return;

    int i = pos - name;
    *pparent_name = strdup(name);
    (*pparent_name)[i++] = '\0';
    *pchild_name = (*pparent_name) + i;
}

static struct WGConfigGroup *
wg_configstore_get_group(const char *name)
{
    struct WGConfigGroup *group = NULL;
    for (group = first_group; group != NULL; group = group->next)
    {
        if (strcmp(group->name, name) == 0) {
            return group;
        }
    }

    return NULL;
}

void
wg_configstore_register(const char *name, struct WGConfigStore *store)
{
    struct WGConfigGroup *previous_group = last_group;

    char *parent_name;
    const char *child_name;
    wg_configstore_split_child_name(name, &parent_name, &child_name);
    if (child_name != NULL && (previous_group = wg_configstore_get_group(parent_name)) != NULL) {
        // append after the last child to preserve the sequence for saving the settings
        while (previous_group->next && strstr(previous_group->next->name, parent_name) == previous_group->next->name) {
            previous_group = previous_group->next;
        }
    } else {
        previous_group = last_group;
    }

    wg_configstore_register_at(name, store, previous_group);
}

struct WGConfigStore *
wg_configstore_get(const char *name)
{
    if (name == NULL)
        name = "";

    struct WGConfigGroup *group = wg_configstore_get_group(name);
    if (group != NULL)
        return group->store;

    if (strcmp(name, "") == 0)
        return NULL;

    struct WGConfigStore *child_store = NULL;
    char *parent_name;
    const char *child_name;
    wg_configstore_split_child_name(name, &parent_name, &child_name);
    if (child_name == NULL)
        goto finish;  // no child

    struct WGConfigGroup *parent_group = wg_configstore_get_group(parent_name);
    if (parent_group == NULL)
        goto finish;  // no such parent store

    struct WGConfigStore *parent_store = parent_group->store;
    if (parent_store == NULL || parent_store->new_subgroup == NULL)
        goto finish;  // parent store does not support having dynamic subgroups

    child_store = parent_store->new_subgroup(parent_store, child_name);
    if (child_store == NULL) {
        wg_log_warning("Parent store '%s' has failed to create dynamic subgroup: '%s'\n", parent_name, child_name);
        goto finish;
    }

    // append after the last child to preserve the sequence for saving the settings
    while (parent_group->next && strstr(parent_group->next->name, parent_name) == parent_group->next->name) {
        parent_group = parent_group->next;
    }
    wg_configstore_register_at(name, child_store, parent_group);

finish:
    free(parent_name);
    return child_store == NULL ? wg_configstore_get("") : child_store;
}

void
wg_configstore_scale_sizes(double scale)
{
    wg_log_debug("scaling sizes to %.2f\n", scale);
    struct WGConfigGroup *group = NULL;
    for (group = first_group; group != NULL; group = group->next)
    {
        if (group->store->scale_sizes)
            group->store->scale_sizes(group->store, scale);
    }
}

void
wg_configstore_deinit(void)
{
    struct WGConfigGroup *group;
    while (first_group != NULL)
    {
        group = first_group;
        first_group = group->next;

        if (group->store->free != NULL)
            group->store->free(group->store);

        free(group->name);
        free(group);
    }
    last_group = NULL;

    free(app_path);
    app_path = NULL;
}

int
wg_configstore_set(const char *group, const char *key, const char *value)
{
    struct WGConfigStore *store = wg_configstore_get(group);
    if (store == NULL) {
        wg_log_warning("Unknown config group: '%s'\n", group);
        return EINVAL;
    }

    return store->set(store, key, value);
}

char *
wg_config_path_ext(const char *app_name, const char *filename, const char *extension)
{
    const char *etc_path = getenv("XDG_CONFIG_HOME");
    if (app_name == NULL)
        app_name = "widgie";
    if (filename == NULL)
        filename = "widgie.conf";
    if (extension == NULL)
        extension = "";

    char *path = NULL;
    if (etc_path != NULL)
        asprintf(&path, "%s/%s/%s%s%s", etc_path, app_name, filename,
            *extension != '.' && *extension != '\0' ? "." : "", extension);
    else {
        const char *home_path = wg_get_homedir();
        if (home_path != NULL) {
            asprintf(&path, "%s/.config/%s/%s", home_path, app_name, filename);
        } else {
            wg_log_warning("Failed to identify HOME directory path\n");
        }
    }
    return path;
}

static const char *
_config_match2(char **argv, int *i, const char *expected, size_t expected_len)
{
    const char *current_arg = argv[*i];
    const char *result = NULL;
    if (strncmp(current_arg, expected, expected_len - 1) == 0)
    {
        if (current_arg[expected_len - 1] == '\0') {
            ++*i;
            result = argv[*i];
        } else if (current_arg[expected_len - 1] == '=') {
            result = &current_arg[expected_len];
        }
    }
    return result;
}

static int
_config_match1(char **argv, int i, const char *expected)
{
    return strcmp(argv[i], expected) == 0;
}

#define config_match2(expected) \
    _config_match2(argv, &i, expected, sizeof(expected))
#define config_match1(expected) \
    _config_match1(argv, i, expected)

static int
_process_config_arg(const char *value)
{
    char *line = strdup(value);
    if (line == NULL)
        return ENOMEM;

    int rc = wg_ini_parser_process_line(line, NULL, NULL, 0, (ConfigReceiverFunc)wg_configstore_set, NULL);
    free(line);
    return rc;
}

static void
widgie_config_print_help(const char *appname)
{
    int stdout_fd = 1;
    dprintf(stdout_fd, "Usage: %s [options]\n", appname);

    struct WGConfigStore *store = wg_configstore_get("args");  // TODO extract to variable
    if (store != NULL && store->print_example != NULL) {
        store->print_example(store, stdout_fd);
    }

    dprintf(stdout_fd, "Common widgie options:\n"
           "  --store-default-config\n"
           "  --print-default-config\n"
           "     Stores all configuration defaults to the filesystem (or prints them to standard output)\n"
           "     to help a quick start of customization.\n"
           "\n"
           "  [--version | -V]\n"
           "  [--help | -h]\n"
           "    Show version or help and exit.\n"
           "\n"
           "  --config \"key=value\"\n"
           "  --config \"[section] key=value\"\n"
           "    Override configurations. This has the highest priority, overrides the config even if\n"
           "    it gets set in a config file.\n"
           "    Section defaults to 'default', and can be omitted.\n"
           "    Escaping is supported the same way as in the config file, but beware that the shell\n"
           "    also processes its own escape sequences first.\n"
           "\n"
           "  --config-file PATH      Instead of the default config file places, load the configuration\n"
           "                          from the specified file. Can be specified multiple times, configs will\n"
           "                          be loaded in the specified order (last override wins).\n"
           "\n");
    // TODO other ideas: short for log level change: -d? or -v?
}

static int
widgie_config_load_args(int argc, char **argv)
{
    // TODO [widgie] window.resize=Fixed/Minimum/Maximum/Free, window.width, window.height
    // check all other struct WGmembers if they make sense to extend
    struct WGConfigStore *store = wg_configstore_get("args");  // TODO extract to define / constant
    const char *arg_value = NULL;
    int rc = 0;
    for (int i = 1; i < argc; ++i) {
        if ((arg_value = config_match2("--config")) != NULL) {
            int res = _process_config_arg(arg_value);
            if (res != 0)
                rc = res;

        // these are handled at argument preload, so just skip
        } else if (config_match2("--config-file")) {
        } else if (config_match1("--print-default-config")) {
        } else if (config_match1("--store-default-config")) {

        } else if (config_match1("--help") || config_match1("-h")) {
            widgie_config_print_help(basename(argv[0]));
            exit(0);

        } else if (config_match1("-V") || config_match1("--version")) {
            printf("%s %s\n", basename(argv[0]), VERSION);  // TODO could come from wg_app in future
            exit(0);

        } else if (store != NULL && store->set(store, argv[i], NULL) == 0) {
            // successfully handled without value

        // } else TODO handle if argument has a '='
        } else if (store != NULL && store->set(store, argv[i], argv[i+1]) == 0) {
            // successfully handled arg with value
            ++i;
        } else {
            wg_log_warning("Skipping unknown argument: '%s'\n", argv[i]);
            rc = EINVAL;
        }
    }

    return rc;
}

// TODO move it to somewhere fs.h? and create a test
static char *
detect_app_path(char *arg0)
{
    char *result = realpath(arg0, NULL);  // this won't work if called from PATH;

    if (result == NULL || *result == '\0')
    {
        char path[2048];
        int len;
        if ((len = readlink("/proc/self/exe", path, sizeof(path) - 1)) >= 0) {
            path[len] = '\0';
            result = strdup(path);
        }
    }

    if (result == NULL || *result == '\0')
    {
        result = strdup("/noidea");  // TODO search on PATH maybe, or ask kernel
    }

    wg_log_debug("detected executable path: %s\n", result ? result : "(null)");

    return result;
}

static void
widgie_config__load_complete(void)
{
    struct WGConfigGroup *group = NULL;
    for (group = first_group; group != NULL; group = group->next)
    {
        if (group->store->load_complete)
            group->store->load_complete(group->store);
    }
}

int
wg_config_load(int argc, char **argv, ...)
{
    int rc = 0;
    char *path = NULL;
    int config_file_override = 0;
    free(app_path);
    app_path = detect_app_path(argv[0]);
    const char *app_name = basename(argv[0]);

    // arg preload: handle --config-file
    const char *arg_value = NULL;
    for (int i = 1; i < argc; ++i) {
        if ((arg_value = config_match2("--config-file")) != NULL) {
            config_file_override = 1;
            rc = wg_config_load_path(arg_value);
            if (rc != 0) {
                wg_log_warning("Error loading config file '%s': %d - %s\n", arg_value, rc, strerror(rc));
            }
        } else if (config_match1("--print-default-config")) {
            rc = wg_config_save_fd(1);
            exit(rc);
        } else if (config_match1("--store-default-config")) {
            rc = wg_config_save(app_name);
            exit(rc);
        }
    }

    // always try to load default widgie conf
    path = wg_config_path(NULL, NULL);
    if (path == NULL)
        return ENOMEM;

    int res = wg_config_load_path(path);
    if (res != 0)
        rc = res;

    free(path);
    path = NULL;

    if (!config_file_override && \
        app_name != NULL && \
        (path = wg_config_path(app_name, NULL)) != NULL)
    {
        res = wg_config_load_path(path);
        if (res != 0)
            rc = res;

        va_list v;
        va_start(v, argv);

        const char *extra_conf;
        while ((extra_conf = va_arg(v, const char *)) != NULL) {
            free(path);
            path = wg_config_path(app_name, extra_conf);
            if (path == NULL) {
                wg_log_error("Memory allocation failure during config load: %s/%s\n", app_name, extra_conf);
                rc = ENOMEM;
                break;
            }

            res = wg_config_load_path(path);
            if (res != 0)
                rc = res;
        }
        va_end(v);
    }

    res = widgie_config_load_args(argc, argv);
    if (res != 0)
        rc = res;

    free(path);

    widgie_config__load_complete();

    // This hack is because the API user will expect to have the sizes right after the load call.
    // However, backends will refresh the size values according to scale, so we enforce it here.
    // Might be a better idea to move the config/args load logic to be controlled by the app TODO
    if (wg_application_backend_init() != 0) {
        rc = 1;
    }
    return rc;
}
 
int
wg_config_load_path(const char *path)
{
    int rc = 0;
    if (path == NULL) {
        wg_log_error("path == NULL\n");
        return EINVAL;
    }

    rc = wg_ini_parser_load(path, (ConfigReceiverFunc)wg_configstore_set, NULL);
    wg_log_debug("Loaded config '%s' with result: %d\n", path, rc);
    return rc;
}

static int
wg_config_save_store_fd(int fd, const char *section, struct WGConfigStore *store)
{
    if (store->print_example == NULL || strcmp(section, "args") == 0)
        return 0;  // this config group does not support saving

    if (*section == '\0') {
        section = "default";
    }
    char *subsection = NULL;
    if ((subsection = strchr(section, '/')) != NULL) {
        wg_ini_parser_write_subsection(fd, ++subsection);
    } else {
        wg_ini_parser_write_section(fd, section);
    }
    int rc = store->print_example(store, fd);
    if (rc != 0)
        return rc;

    if (write(fd, "\n", 1) < 0)
        rc = errno;
    return rc;
}

int
wg_config_save(const char *appname)
{
    int rc = 0;
    int fd = -1;
    const char *fname = NULL;
    char *path = NULL;

    struct WGConfigGroup *group;
    for (group = first_group; group != NULL; group = group->next)
    {
        struct WGConfigStore *store = group->store;
        if (store->print_example == NULL || strcmp(group->name, "args") == 0)
            continue;  // this config group does not support saving

        const char *new_fname = store->fname && *store->fname ? store->fname : "config";
        if (fd < 0 || fname == NULL || strcmp(new_fname, fname) != 0) {
            // we need to open a new file

            // TODO: in case we would use the same filename for non sequential stores, this would overwrite an already saved file
            
            if (path != NULL) {
                wg_log_info("File written: %s\n", path);
                free(path);
            }
            path = wg_config_path(appname, new_fname);
            if (path == NULL) {
                rc = ENOMEM;
                goto finish;
            }

            if ((rc = wg_create_basedir_of(path)) != 0) {
                goto finish;
            }

            fd = creat(path, S_IRUSR | S_IWUSR | S_IRGRP);
            wg_log_debug("Opening '%s' -> %d\n", path, fd);
            if (fd < 0) {
                rc = errno;
                goto finish;
            }
        }
        fname = new_fname;

        rc = wg_config_save_store_fd(fd, group->name, store);
        if (rc != 0) {
            goto finish;
        }
    }

finish:
    if (fd >= 0 && close(fd) < 0 && rc == 0)
        rc = errno;

    if (rc != 0) {
        wg_log_warning("Error writing config file '%s': %d - %s\n", path ? path : "?", rc, strerror(rc));
    } else if (path != NULL) {
        wg_log_info("File written: %s\n", path);
        free(path);
    }
    return rc;
}

int
wg_config_save_fd(int fd)
{
    int rc = 0;
    for (struct WGConfigGroup *group = first_group; group != NULL; group = group->next)
    {
        rc = wg_config_save_store_fd(fd, group->name, group->store);
        if (rc != 0) {
            goto finish;
        }
    }

finish:
    return rc;
}

int
wg_config_save_path(const char *path)
{
    int rc = 0;

    if ((rc = wg_create_basedir_of(path)) != 0) {
        return rc;
    }

    int fd = creat(path, S_IRUSR | S_IWUSR | S_IRGRP);
    wg_log_debug("Opening '%s' -> %d\n", path, fd);
    if (fd < 0) {
        rc = errno;
        goto finish;
    }

    rc = wg_config_save_fd(fd);
    if (rc == 0) {
        wg_log_info("File written: %s\n", path);
    }

finish:
    if (fd >= 0 && close(fd) < 0 && rc == 0)
        rc = errno;

    if (rc != 0) {
        wg_log_warning("Error writing config file '%s': %d - %s\n", path, rc, strerror(rc));
    }

    return rc;
}
