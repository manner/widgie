// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_WIDGET
#define WG_HEADER_WIDGIE_WIDGET

struct WGRenderFrame;

#include "uidefs.h"

#include <stdint.h>

enum WGWidgetFlags {
    WFlagDefault = 0,
    WFlagIsHidden = 1 << 0,
    WFlagIsLayout = 1 << 1,
};

// represents an empty widget
struct WGWidget {
    int x;
    int y;
    int width;
    int height;

    enum WGWidgetFlags flags;

    struct WGWidget *parent;

    int (*on_mouse_press)(struct WGWidget *self, int x, int y);
    int (*on_mouse_release)(struct WGWidget *self);
    int (*on_mouse_longclick)(struct WGWidget *self);
    int (*on_mouse_click)(struct WGWidget *self);
    int (*on_mouse_move)(struct WGWidget *self, int x, int y);  // this is only between press and release for now
    int (*on_keypress)(struct WGWidget *self, const char *utf8, uint32_t keysym);
    int (*focus_event)(struct WGWidget *self, int is_focused);

    struct WGWidget *(*widget_at)(struct WGWidget *self, int x, int y);
    void (*render)(struct WGWidget *self, struct WGRenderFrame *dest);
    void (*request_rerender)(struct WGWidget *self, struct WGWidget *originator);
    void (*request_showme)(struct WGWidget *self, struct WGWidget *originator);
    void (*free)(struct WGWidget *self);
};

struct WGWidget *wg_widget_new(void);
void wg_widget_free(struct WGWidget *w);
void wg_widget_render(struct WGWidget *w, struct WGRenderFrame *dest);
void wg_widget_request_rerender(struct WGWidget *w);
void wg_widget_request_showme(struct WGWidget *w);
int wg_widget_mouse_click(struct WGWidget *w);
int wg_widget_keypress(struct WGWidget *self, const char *utf8, uint32_t keysym);
void wg_widget_set_hidden(struct WGWidget *self, int hidden);
void wg_widget_resize(struct WGWidget *self, int width, int height);


static inline int wg_widget_is_visible(struct WGWidget *self)
{ return (self->flags & WFlagIsHidden) == 0; }

static inline int wg_widget_is_hidden(struct WGWidget *self)
{ return (self->flags & WFlagIsHidden) != 0; }

static inline int wg_widget_is_layout(struct WGWidget *self)
{ return (self->flags & WFlagIsLayout) != 0; }

int wg_widget_intersects_rect(struct WGWidget *self, int x, int y, int width, int height);

#define WG_WIDGET(x) ((struct WGWidget *)x)

// protected API, should only called by derived classes
void wg__widget_init(struct WGWidget *self);
void wg__widget_free(struct WGWidget *self);
void wg__widget_render(struct WGWidget *self, struct WGRenderFrame *dest);
struct WGWidget *wg__widget_at(struct WGWidget *self, int x, int y);
int wg__widget_on_mouse_press(struct WGWidget *self, int x, int y);
int wg__widget_on_mouse_release(struct WGWidget *self);
int wg__widget_on_mouse_click(struct WGWidget *self);
int wg__widget_on_mouse_longclick(struct WGWidget *self);
int wg__widget_on_mouse_move(struct WGWidget *self, int x, int y);
void wg__widget_request_rerender(struct WGWidget *self, struct WGWidget *originator);
void wg__widget_request_showme(struct WGWidget *self, struct WGWidget *originator);
int wg__widget_on_keypress(struct WGWidget *self, const char *utf8, uint32_t keysym);
int wg__widget_focus_event(struct WGWidget *self, int is_focused);

#endif // FB_HEADER_WIDGIE_WIDGET
