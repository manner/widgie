// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "system.h"

#include "log.h"

#include <errno.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>

int
wg_create_basedir_of(const char *path_)
{
    int rc = 0;
    char *path = strdup(path_);
    if (path == NULL)
        return ENOMEM;

    char *path_sep = strrchr(path, '/');
    if (path_sep == NULL) {
        wg_log_error("Programming error, this function is not yet capable of handling relative paths.\n");
        rc = EINVAL;
        goto finish;
    }
    *path_sep = '\0';

    for (int retry = 0; retry < 2; ++retry) {
        if (mkdir(path, 0700) != 0)
        {
            rc = errno;
            switch (rc)
            {
                case EEXIST:
                    rc = 0;
                    wg_log_debug("Directory exists: %s\n", path);
                    break;
                case ENOENT:
                    rc = wg_create_basedir_of(path);
                    if (rc == 0)
                        continue;
                    break;
                default:
                    wg_log_error("Failed to create directory '%s': %d - %s\n", path, rc, strerror(rc));
                    break;
            };
        } else {
            wg_log_debug("Directory created successfully: %s\n", path);
        }
    }

finish:
    free(path);
    return rc;
}

const char *
wg_get_homedir(void)
{
    const char *homedir = NULL;
    if ((homedir = getenv("HOME")) == NULL) {
        struct passwd *pw = getpwuid(getuid());
        if (pw == NULL) {
            wg_log_warning("Failed to get home directory with getpwuid: %s\n", strerror(errno));
        } else {
            homedir = pw->pw_dir;
        }
    }

    return homedir;
}

char *
wg_alloc_cache_path(const char *app_name, const char *filename)
{
    char *result = NULL;
    const char *xdg_cache_home = getenv("XDG_CACHE_HOME");
    if (xdg_cache_home != NULL && *xdg_cache_home != '\0') {
        if (asprintf(&result, "%s/%s/%s", xdg_cache_home, app_name, filename) < 0)
            result = NULL;
    } else {
        const char *home = wg_get_homedir();
        if (home != NULL && *home != '\0') {
            if (asprintf(&result, "%s/.cache/%s/%s", home, app_name, filename) < 0)
                result = NULL;
        }
    }

    return result;
}
