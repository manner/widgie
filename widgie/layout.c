// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "layout.h"
#include "frame.h"
#include "draw.h"

#include "log.h"

#include <stdlib.h>
#include <stdio.h>


struct WGLayoutCache
{
    int x;  // TODO these are avoidable if we would not store absolute coordinates
    int y;
    int width;
    int height;
    int children_count;
    int rerender_requested;
};

void
wg__layout_free(struct WGWidget *self)
{
    struct WGLayout *layout = WG_LAYOUT(self);
    wg_layout_reset_children(layout, 1);

    free(layout->children);
    layout->children = NULL;

    free(layout->cache);

    wg__widget_free(self);
}

// TODO: a simple grid could work without lookup
static int
_lookup_id_for_position(struct WGLayout *self, int x, int y, int nearest)
{
    int min = 0, max = self->children_count - 1;
    int first_visible = 0;
    while (min <= max)
    {
        int i = (min + max) >> 1;
        first_visible = i;

        struct WGWidget **pchild = &self->children[first_visible];
        for (first_visible = i; first_visible <= max && wg_widget_is_hidden(*pchild);
             ++first_visible, ++pchild)
        {}

        if (first_visible > max) {  // we only had hidden widgets
            max = i - 1;
            continue;
        }

        struct WGWidget *child = *pchild;

        if (y < child->y) {
            // the position is "lower" then the guess
            max = i - 1;
            continue;
        }

        if (y > child->y + child->height) {
            // the position is "higher" then the guess
            min = first_visible + 1;
            continue;
        }

        if (x < child->x) {
            // the position is "lower" then the guess
            max = i - 1;
            continue;
        }

        if (x > child->x + child->width) {
            // the position is "higher" then the guess
            min = first_visible + 1;
            continue;
        }

        // found it!
        return first_visible;
    }
    return nearest ? first_visible : -1;
}

void
wg_layout_reset_children(struct WGLayout *self, int destroy)
{
    if (destroy) {
        struct WGWidget **pchild = self->children;
        for (int i = 0; i < self->children_count; ++i, ++pchild)
            (*pchild)->free(*pchild);
    }

    self->children_count = 0;
    wg_layout_request_relayout(self);
}

void
wg_layout_remove_last(struct WGLayout *self, int destroy)
{
    if (self->children_count <= 0)
        return;

    --self->children_count;

    if (destroy)
        wg_widget_free(self->children[self->children_count]);

    self->children[self->children_count] = NULL;
}

void
wg__layout_relayout_if_needed(struct WGWidget *self)
{
    // We do not relayout always, only when width, height or widget count changes
    struct WGLayout *layout = WG_LAYOUT(self);
    if (layout->do_layout != NULL) {
        if (layout->cache->width != self->width ||
            layout->cache->height != self->height ||
            layout->cache->x != self->x ||
            layout->cache->y != self->y ||
            layout->cache->children_count != layout->children_count)
        {
            layout->do_layout(layout);
            layout->cache->width = self->width;
            layout->cache->height = self->height;
            layout->cache->children_count = layout->children_count;
            layout->cache->x = self->x;
            layout->cache->y = self->y;
        }
    }
}

void
wg__layout_render(struct WGWidget *self, struct WGRenderFrame *dest)
{
    wg__widget_render(self, dest);

    struct WGLayout *layout = WG_LAYOUT(self);
    layout->cache->rerender_requested = 0;

    wg__layout_relayout_if_needed(self);

    // find the first widget we'll need to render
    // For this we query the closest one to (0,0) coordinate on the frame
    int from = _lookup_id_for_position(layout, 0 - dest->dx, 0 - dest->dy, 1);

    struct WGWidget **pchild = &layout->children[from];
    for (int i = from; i < layout->children_count; ++i, ++pchild)
    {
        struct WGWidget *child = *pchild;

        if (wg_widget_is_visible(child))
        {
            if (child->y + dest->dy > dest->height) {
                break; // since widgets are always ordered from top to bottom
                       // we will not have any more widgets to render
            }

            child->render(child, dest);
        }
    }
}

void
wg_layout_add(struct WGLayout *self, struct WGWidget *widget)
{
    ++self->children_count;
    if (self->children == NULL || self->children_count >= self->children_alloced)
    {
        self->children_alloced = self->children_alloced == 0 ? 16 : 2 * self->children_alloced;

        self->children = (struct WGWidget **)realloc(self->children, self->children_alloced * sizeof(struct WGWidget *));
        if (self->children == NULL) {
            wg_log_error("Realloc has failed\n");
            self->children_count = 0;
            self->children_alloced = 0;
            return;
        }
    }

    self->children[self->children_count - 1] = widget;
    widget->parent = WG_WIDGET(self);
}

void
wg__layout_init(struct WGLayout *self)
{
    wg__widget_init(&self->base);

    self->base.flags |= WFlagIsLayout;
    self->base.render = wg__layout_render;
    self->base.free = wg__layout_free;
    self->base.widget_at = wg__layout_widget_at;
    self->base.request_rerender = wg__layout_request_rerender;

    self->do_layout = NULL;
    self->cache = (struct WGLayoutCache *)malloc(sizeof(struct WGLayoutCache));
    self->cache->height = self->cache->width = self->cache->children_count = -1;
    self->cache->rerender_requested = 0;

    self->children_alloced = 0;
    self->children = NULL;
    self->children_count = 0;
}

void
wg__layout_precompute_positions(struct WGWidget *self)
{
    if (self->parent && wg_widget_is_layout(self->parent)) {
        wg__layout_precompute_positions(self->parent);
    }
    wg__layout_relayout_if_needed(self);
}

void
wg__layout_request_rerender(struct WGWidget *self, struct WGWidget *originator)
{
    // only propagate the first rerender request
    if (WG_LAYOUT(self)->cache->rerender_requested)
        return;

    WG_LAYOUT(self)->cache->rerender_requested = 1;
    wg__widget_request_rerender(self, originator);
}

struct WGWidget *
wg_layout_new(void)
{
    struct WGLayout *w = WG_LAYOUT(malloc(sizeof(struct WGLayout)));
    wg__layout_init(w);
    return (struct WGWidget *)w;
}


struct WGWidget *
wg__layout_widget_at(struct WGWidget *self, int x, int y)
{
    struct WGWidget *result = wg__widget_at(self, x, y);

    if (result != NULL) {
        // we are matched! lets find a child
        struct WGLayout *layout = WG_LAYOUT(self);
        int i = _lookup_id_for_position(layout, x, y, 0);
        if (i >= 0) {
            result = layout->children[i];
            result = result->widget_at(result, x, y);
        }
    }

    return result;
}

void
wg_layout_request_relayout(struct WGLayout *self)
{
    if (self->cache) {
        self->cache->children_count = -1;
    }
    wg_widget_request_rerender(WG_WIDGET(self));
}

void
wg_layout_request_relayout_recursive(struct WGLayout *self)
{
    wg_layout_request_relayout(self); // TODO: avoid the too many rerender requests
    struct WGWidget **pchild = self->children;
    for (int i = 0; i < self->children_count; ++i, ++pchild)
    {
        if (wg_widget_is_layout(*pchild)) {
            wg_layout_request_relayout_recursive(WG_LAYOUT(*pchild));
        }
    }
}

int
wg_layout_visible_children_count(struct WGLayout *self)
{
    int visible_children = 0;
    struct WGWidget **pchild = self->children;
    for (int i = 0; i < WG_LAYOUT(self)->children_count; ++i, ++pchild)
    {
        if (wg_widget_is_visible(*pchild))
            ++visible_children;
    }
    return visible_children;
}