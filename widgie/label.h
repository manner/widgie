// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_LABEL
#define WG_HEADER_WIDGIE_LABEL

#include <widgie/widget.h>
#include <widgie/uidefs.h>

// shows a text
struct WGLabel {
    struct WGWidget base;

    void (*on_text_change)(struct WGLabel *self);

    // TODO uint32_t color;
    char *text;  // prefer to change this with setter
    int text_height;
    enum WGAlignment text_align;
    enum WGTextElide elide_mode;
    enum WGTextWrap wrap_mode;

    struct WGTextRender *render;  // internal
};

struct WGWidget *wg_label_new(const char *text);

#define WG_LABEL(x) ((struct WGLabel *)x)

static inline const char *wg_label_get_text(struct WGLabel *self)
{ return self->text; }

void wg_label_set_text(struct WGLabel *label, const char *text);
void wg_label_set_text_height(struct WGLabel *label, int new_height);
void wg_label_set_text_align(struct WGLabel *label, enum WGAlignment new_align);

void wg_label_resize_oneline(struct WGLabel* label);
int wg_label_height_for_width(struct WGLabel* label, int width);
static inline void wg_label_resize_multiline(struct WGLabel* label, int width)
{
    wg_widget_resize(WG_WIDGET(label), width, wg_label_height_for_width(label, width));
}

// protected API, should only called by derived classes
void wg__label_init(struct WGLabel *self, const char *text);
void wg__label_free(struct WGWidget *w);
void wg__label_render(struct WGWidget *w, struct WGRenderFrame *dest);

#endif // FB_HEADER_WIDGIE_LABEL