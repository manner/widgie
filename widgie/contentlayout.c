// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "contentlayout.h"

#include <stddef.h>
#include <stdlib.h>

void
wg__contentlayout_do_layout(struct WGLayout *self)
{
    struct WGWidget *wself = WG_WIDGET(self);
    struct WGWidget *content = WG_CONTENT_WIDGET(self);

    if (content == NULL)
        return;

    content->x = wself->x;
    content->y = wself->y;
    content->width = wself->width;
    content->height = wself->height;
}

void
wg__contentlayout_init(struct WGContentLayout *self)
{
    wg__layout_init(&self->base);
    WG_LAYOUT(self)->do_layout = wg__contentlayout_do_layout;
}

struct WGWidget *
wg_contentlayout_new(void)
{
    struct WGContentLayout *self = WG_CONTENT_LAYOUT(malloc(sizeof(struct WGContentLayout)));
    wg__contentlayout_init(self);
    return WG_WIDGET(self);
}

void
wg_contentlayout_set_content(struct WGContentLayout *self, struct WGWidget *content, int destroy)
{
    wg_layout_reset_children(WG_LAYOUT(self), destroy);
    if (content != NULL)
        wg_layout_add(WG_LAYOUT(self), content);
}

struct WGWidget *
wg_contentlayout_get_content(struct WGContentLayout *self)
{
    struct WGLayout *lself = WG_LAYOUT(self);
    return (lself->children_count < 1) ? NULL : lself->children[0];
}