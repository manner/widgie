// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_FB_FRAME
#define WG_HEADER_FB_FRAME

#include "../frame.h"

void wg_render_frame_copy_rect_a(struct WGRenderFrame *from_frame, struct WGRenderFrame *to_frame,
                            int from_x, int from_y, int width, int height, int to_x, int to_y);

void wg_render_frame_rotate_xy(struct WGRenderFrame *frame, int *px, int *py, int rotation);
void wg_render_frame_copy_rect_rotated(struct WGRenderFrame *from_frame, struct WGRenderFrame *to_frame,
    int from_x, int from_y, int width, int height, int to_x, int to_y, int rotation);

#endif // WG_HEADER_FB_FRAME
