// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "x11.h"
#include "window_backend.h"
#include "application_backend.h"
#include "../window.h"
#include "../application.h"
#include "../frame.h"
#include "../log.h"
#include "../settings.h"

#include <sys/ipc.h>
#include <sys/shm.h>

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <xcb/xcb.h>
#include <xcb/shm.h>
#include <xcb/xcb_image.h>
#include <xcb/xproto.h>
#include <xcb/xcb_atom.h>
#ifdef HAVE_XCB_ERRORS
#include <xcb/xcb_errors.h>  // TODO remove
#endif // HAVE_XCB_ERRORS

#include <xcb/xkb.h>
#include <xkbcommon/xkbcommon.h>
#include <xkbcommon/xkbcommon-x11.h>

#define X11_WINBACKEND(x) ((struct WGX11WindowBackend *)x)
#define X11_APPBACKEND(x) ((struct WGX11ApplicationBackend *)x)

// Here you can enable debugs:
#define DEBUG_X11             WG_DEBUG_HIDE
#define DEBUG_PAINT           WG_DEBUG_HIDE
#define DEBUG_EV_KEY          WG_DEBUG_HIDE
#define DEBUG_EV_MOUSE        WG_DEBUG_HIDE
#define DEBUG_EV_MOUSE_MOTION WG_DEBUG_HIDE

struct XkbInput {
    struct xkb_context *context;
    struct xkb_keymap *keymap;
    struct xkb_state *state;
    uint32_t device_id;
    uint8_t first_xkb_event;
};

struct WGX11ApplicationBackend {
    struct WGApplicationBackend base;
    xcb_connection_t *conn;
    xcb_screen_t *screen;

    struct XkbInput input;

    int use_shm;  // use shared memory for image passing if 1
    uint8_t shm_completion_event_id;
};

enum ShmDrawState {
    ShmDrawReady,
    ShmDrawBlocked,
    ShmDrawFrameSkipped,
};

struct WGX11WindowBackend {
    struct WGWindowBackend base;

    xcb_drawable_t window;
    xcb_gcontext_t window_gc;
    xcb_atom_t delete_window_atom;
    xcb_atom_t protocols_atom;

    xcb_shm_segment_info_t shminfo;
    enum ShmDrawState shm_state;
    xcb_image_t *image;
};

static struct WGX11ApplicationBackend *x11_backend = NULL;

#ifdef SHARED_X11
#include "shared_backend.h"
WG_REGISTER_BACKEND(x11_application_new);
#endif

/*
 * Check if the X Shared Memory extension is available.
 * Return:  0 = not available
 *          1 = shared XImage support available
 *          2 = shared Pixmap support available also
 */
static int
check_for_xshm(xcb_connection_t *conn, uint8_t *shm_completion_event_id)
{
    xcb_shm_query_version_reply_t* reply;
    reply = xcb_shm_query_version_reply(conn, xcb_shm_query_version(conn), NULL);
    if (!reply)
        return 0;
    DEBUG_X11("xshm version %i.%i (shared pixmaps: %s)\n", reply->major_version,
              reply->minor_version, reply->shared_pixmaps ? "yes" : "no");

    if (shm_completion_event_id != NULL)
        *shm_completion_event_id = xcb_get_extension_data(conn, &xcb_shm_id)->first_event + XCB_SHM_COMPLETION;
    int rc = reply->shared_pixmaps ? 2 : 1;
    free(reply);
    return rc;
}

xcb_image_t *
alloc_xshm_image(xcb_shm_segment_info_t *shminfo, int width, int height)
{
    xcb_image_t *img = NULL;  // TODO check if current shm stuff could be reused
    xcb_generic_error_t *error = NULL;
    shminfo->shmaddr = (void *)-1;

    img = xcb_image_create_native(x11_backend->conn, width, height,
                                  XCB_IMAGE_FORMAT_Z_PIXMAP, x11_backend->screen->root_depth,
                                  0, ~0, 0);
    if (img == NULL) {
        wg_log_error("Failed to create image\n");
        goto finish;
    }

    shminfo->shmid = shmget( IPC_PRIVATE, img->size, IPC_CREAT | 0777);
    if (shminfo->shmid < 0) {
        wg_log_error("Shared memory error (shmget: %d - %s), disabling.\n", errno, strerror(errno));
        goto finish;
    }

    shminfo->shmaddr = shmat(shminfo->shmid, 0, 0);
    if (shminfo->shmaddr == (uint8_t *) -1) {
        wg_log_error("Shared memory error (shmat: %d - %s), disabling.\n", errno, strerror(errno));
        goto finish;
    }

    shminfo->shmseg = xcb_generate_id(x11_backend->conn);
    xcb_void_cookie_t cookie = xcb_shm_attach_checked(x11_backend->conn, shminfo->shmseg, shminfo->shmid, 0);
    error = xcb_request_check(x11_backend->conn, cookie);
    if (error != NULL) {
        wg_log_error("Shared memory error (xcb_shm_attach has failed), disabling.\n");
        goto finish;
    }

    if (shmctl(shminfo->shmid, IPC_RMID, 0) < 0) {
        wg_log_error("Shared memory error (shmctl has failed): %d - %s.\n", errno, strerror(errno));
        xcb_shm_detach(x11_backend->conn, shminfo->shmseg);
        goto finish;
    }

    img->data = shminfo->shmaddr;

finish:
    if (img != NULL && img->data == NULL) {
        if (shminfo->shmaddr != (void *)-1)
            shmdt(shminfo->shmaddr);
        xcb_image_destroy(img);
        img = NULL;
    }

    free(error);
    return img;
}

xcb_image_t *
alloc_image(int width, int height)
{
    return xcb_image_create_native(x11_backend->conn, width, height,
        XCB_IMAGE_FORMAT_Z_PIXMAP, x11_backend->screen->root_depth, NULL, 0, NULL);
}

static void
x11_window_deinit_backbuffer(struct WGX11WindowBackend *x11_self)
{
    if (x11_self->image != NULL) {
        xcb_image_destroy(x11_self->image);
        if (x11_backend->use_shm) {
            xcb_shm_detach(x11_backend->conn, x11_self->shminfo.shmseg);
            shmdt( x11_self->shminfo.shmaddr );
        }
        x11_self->image = NULL;
    }
}

static void
x11_application_free(struct WGApplicationBackend *self)
{
    DEBUG_X11("\n");
    struct WGX11ApplicationBackend *x11_self = X11_APPBACKEND(self);

    // TODO: free x11_self->screen?
    if (x11_self->conn) {
        xcb_disconnect(x11_self->conn);
        x11_self->conn = NULL;
    }

    if (x11_self->input.state != NULL) {
        xkb_state_unref(x11_self->input.state);
        x11_self->input.state = NULL;
    }
    if (x11_self->input.keymap != NULL) {
        xkb_keymap_unref(x11_self->input.keymap);
        x11_self->input.keymap = NULL;
    }
    if (x11_self->input.context != NULL) {
        xkb_context_unref(x11_self->input.context);
        x11_self->input.context = NULL;
    }

    free(self);
}

static void
x11_put_background_buffer(struct WGWindow *win, xcb_image_t *image)
{
    if (image == NULL) {
        DEBUG_PAINT("Not rendering, because of null image\n");
        return;
    }
    struct WGX11WindowBackend *wbackend = X11_WINBACKEND(win->backend);

    int width = WG_WIDGET(win)->width;
    int height = WG_WIDGET(win)->height;
    if (x11_backend->use_shm) {  // TODO abstract the use_shm behind function pointers
        DEBUG_PAINT("shm put image %p %dx%d\n", image, width, height);
        if (wbackend->shm_state == ShmDrawReady) {
            xcb_image_shm_put(x11_backend->conn, wbackend->window, wbackend->window_gc,
                            image, wbackend->shminfo, 0, 0, 0, 0, width, height, 1 /* send event */);
            wbackend->shm_state = ShmDrawBlocked;
            xcb_flush(x11_backend->conn);  // not sure why, but without this it does not seem to work
        }
    } else {
        DEBUG_PAINT("non-shm put image\n");
        xcb_image_put(x11_backend->conn, wbackend->window, wbackend->window_gc, image, 0, 0, 0);  // TODO width, height!
    }
}

static int
select_xkb_events_for_device(xcb_connection_t *conn, int32_t device_id)
{
    enum {
        required_events =
            (XCB_XKB_EVENT_TYPE_NEW_KEYBOARD_NOTIFY |
             XCB_XKB_EVENT_TYPE_MAP_NOTIFY |
             XCB_XKB_EVENT_TYPE_STATE_NOTIFY),

        required_nkn_details =
            (XCB_XKB_NKN_DETAIL_KEYCODES),

        required_map_parts =
            (XCB_XKB_MAP_PART_KEY_TYPES |
             XCB_XKB_MAP_PART_KEY_SYMS |
             XCB_XKB_MAP_PART_MODIFIER_MAP |
             XCB_XKB_MAP_PART_EXPLICIT_COMPONENTS |
             XCB_XKB_MAP_PART_KEY_ACTIONS |
             XCB_XKB_MAP_PART_VIRTUAL_MODS |
             XCB_XKB_MAP_PART_VIRTUAL_MOD_MAP),

        required_state_details =
            (XCB_XKB_STATE_PART_MODIFIER_BASE |
             XCB_XKB_STATE_PART_MODIFIER_LATCH |
             XCB_XKB_STATE_PART_MODIFIER_LOCK |
             XCB_XKB_STATE_PART_GROUP_BASE |
             XCB_XKB_STATE_PART_GROUP_LATCH |
             XCB_XKB_STATE_PART_GROUP_LOCK),
    };

    static const xcb_xkb_select_events_details_t details = {
        .affectNewKeyboard = required_nkn_details,
        .newKeyboardDetails = required_nkn_details,
        .affectState = required_state_details,
        .stateDetails = required_state_details,
    };

    xcb_void_cookie_t cookie =
        xcb_xkb_select_events_aux_checked(conn,
                                          device_id,
                                          required_events,    /* affectWhich */
                                          0,                  /* clear */
                                          0,                  /* selectAll */
                                          required_map_parts, /* affectMap */
                                          required_map_parts, /* map */
                                          &details);          /* details */

    xcb_generic_error_t *error = xcb_request_check(conn, cookie);
    if (error) {
        free(error);
        return -1;
    }

    return 0;
}

static void
x11_window_draw(struct WGWindowBackend *self)
{
    struct WGWindow *win = self->wg.window;
    DEBUG_PAINT("Window: %p\n", win);
    struct WGX11WindowBackend *wbackend = X11_WINBACKEND(self);

    if (wbackend->image == NULL) {
        DEBUG_PAINT("Not yet ready for drawing\n");
        return;
    }

    if (wbackend->shm_state != ShmDrawReady) {
        DEBUG_PAINT("Skipping frame to wait for shared memory area\n");
        wbackend->shm_state = ShmDrawFrameSkipped;
        return;
    }

    struct WGWidget *widget = WG_WIDGET(win);

    struct WGRenderFrame frame = {widget->width, widget->height, (uint32_t *)wbackend->image->data};
    memset(wbackend->image->data, 0, widget->width * widget->height * sizeof(uint32_t));
    WG_WIDGET(win)->render(widget, &frame);

    x11_put_background_buffer(win, wbackend->image);
}

static xcb_generic_event_t *
next_event_timeout(xcb_connection_t *conn, int timeout_ms)
{
    xcb_generic_event_t *event = xcb_poll_for_event(conn);
    int fd = xcb_get_file_descriptor(conn);
    if (event == NULL && (timeout_ms <= 0 || x11_backend->base.wg.wait_fd(fd, timeout_ms))) {
        event = xcb_wait_for_event(conn);
        if (event == NULL) {
            wg_log_error("Error communicating with the X server\n");
            wg_app->exit_requested = wg_app->exit_code = 1;
        }
    }
    return event;
}

static int
x11_backbuffer_resize(struct WGX11WindowBackend *wbackend, int width, int height)
{
    // TODO: ximage->width >= width could also get handled if frame would support line_width
    if (wbackend->image != NULL && wbackend->image->width == width && wbackend->image->height >= height)
        return 0;  // we have enough space

    if (wbackend->image != NULL)
        x11_window_deinit_backbuffer(wbackend);

    if (x11_backend->use_shm) {
        DEBUG_PAINT("init shared memory backbuffer\n");
        wbackend->image = alloc_xshm_image(&wbackend->shminfo, width, height);
        if (wbackend->image == NULL) {
            wg_log_warning("Couldn't allocate shared memory XImage, falling back to copying. Performance will be worse.\n");
            x11_backend->use_shm = 0;
        }
    }

    if (!x11_backend->use_shm) {
        DEBUG_PAINT("init non-shared backbuffer\n");
        wbackend->image = alloc_image(width, height);
    }

    if (wbackend->image == NULL) {
        wg_log_error("Backbuffer init has failed\n");
        return -1;
    }

    return 0;
}

static int
xkb_update_keymap(struct WGX11ApplicationBackend *backend)
{
    int rc = -1;
    backend->input.keymap = xkb_x11_keymap_new_from_device(backend->input.context,
        backend->conn, backend->input.device_id, XKB_KEYMAP_COMPILE_NO_FLAGS);
    if (backend->input.keymap == NULL) {
        wg_log_error("xkb_x11_keymap_new_from_device has failed\n");
        goto finish;
    }

    backend->input.state = xkb_x11_state_new_from_device(backend->input.keymap,
        backend->conn, backend->input.device_id);
    if (backend->input.state == NULL) {
        wg_log_error("xkb_x11_state_new_from_device has failed\n");
        goto finish;
    }

    rc = 0;

finish:
    return rc;
}

static void
x11_grab_keyboard(xcb_connection_t *conn, xcb_screen_t *screen)
{
    int success = 0;
    for (uint32_t i = 0; i < 1000; ++i) {
        xcb_grab_keyboard_cookie_t cookie = xcb_grab_keyboard(conn, 1, screen->root,
            XCB_CURRENT_TIME, XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);

        xcb_grab_keyboard_reply_t *reply = xcb_grab_keyboard_reply(conn, cookie, NULL);
        success = (reply != NULL && reply->status == XCB_GRAB_STATUS_SUCCESS);
        free(reply);

        DEBUG_X11("grab keyboard success: %d\n", success);
        if (success)
            break;

        usleep(1000);
    }
    if (!success) {
        wg_log_warning("Failed to grab keyboard");
    }
}

static void
x11_application_grab_keyboard(struct WGApplicationBackend *self, int grab)
{
    (void) self;
    if (grab) {
        x11_grab_keyboard(x11_backend->conn, x11_backend->screen);
    } else {
        xcb_ungrab_keyboard(x11_backend->conn, XCB_CURRENT_TIME);
    }
}

static void
x11_process_event_xkb(xcb_generic_event_t *gevent)
{
    union xkb_event {
        struct {
            uint8_t response_type;
            uint8_t xkbType;
            uint16_t sequence;
            xcb_timestamp_t time;
            uint8_t deviceID;
        } any;
        xcb_xkb_new_keyboard_notify_event_t new_keyboard_notify;
        xcb_xkb_map_notify_event_t map_notify;
        xcb_xkb_state_notify_event_t state_notify;
    } *event = (union xkb_event *) gevent;

    if (event->any.deviceID != x11_backend->input.device_id)
        return;

    switch (event->any.xkbType) {
    case XCB_XKB_NEW_KEYBOARD_NOTIFY:
        DEBUG_EV_KEY("Xkb new keyboard event\n");
        if (event->new_keyboard_notify.changed & XCB_XKB_NKN_DETAIL_KEYCODES)
            xkb_update_keymap(x11_backend);
        break;

    case XCB_XKB_MAP_NOTIFY:
        DEBUG_EV_KEY("Xkb update keymap event\n");
        xkb_update_keymap(x11_backend);
        break;

    case XCB_XKB_STATE_NOTIFY:
        DEBUG_EV_KEY("Xkb state event\n");
        xkb_state_update_mask(x11_backend->input.state,
                              event->state_notify.baseMods,
                              event->state_notify.latchedMods,
                              event->state_notify.lockedMods,
                              event->state_notify.baseGroup,
                              event->state_notify.latchedGroup,
                              event->state_notify.lockedGroup);
        break;
    }
}

static void
x11_process_event_keypress(struct WGX11ApplicationBackend *backend, xcb_key_press_event_t *event)
{
    char buf[128];
    uint32_t keycode = event->detail;
    xkb_keysym_t sym = xkb_state_key_get_one_sym(backend->input.state, keycode);

    xkb_keysym_get_name(sym, buf, sizeof(buf));
    DEBUG_EV_KEY("key press: sym: %-12s (%d), ", buf, sym);

    xkb_state_key_get_utf8(backend->input.state, keycode, buf, sizeof(buf));
    DEBUG_EV_KEY("utf8: '%s'\n", buf);

    struct WGApplicationBackend *b = WG_APPBACKEND(backend);
    if (b->wg.key_pressed != NULL)
        b->wg.key_pressed(buf, sym);
}

static void
x11_process_event_error(xcb_generic_error_t *err)
{
    // output general errors to aid debugging problems
#ifdef HAVE_XCB_ERRORS
            xcb_errors_context_t *err_ctx;
            xcb_errors_context_new(x11_self->conn, &err_ctx);
            const char *major, *minor, *extension, *error;
            major = xcb_errors_get_name_for_major_code(err_ctx, err->major_code);
            minor = xcb_errors_get_name_for_minor_code(err_ctx, err->major_code, err->minor_code);
            error = xcb_errors_get_name_for_error(err_ctx, err->error_code, &extension);
            wg_log_error("XCB Error: %s:%s, %s:%s, resource %u sequence %u\n",
                error, extension ? extension : "no_extension",
                major, minor ? minor : "no_minor",
                (unsigned int)err->resource_id,
                (unsigned int)err->sequence);
            xcb_errors_context_free(err_ctx);
#else  // HAVE_XCB_ERRORS
        wg_log_error("XCB Error: %d, %d:%d, resource %u sequence %u\n", err->error_code,
                err->major_code, err->minor_code, (unsigned int)err->resource_id,
                (unsigned int)err->sequence);  // TODO maybe a more human friendly way?
#endif
}

static void
x11_process_event_expose(struct WGWindow *wgwindow, xcb_expose_event_t *event)
{
    DEBUG_X11("Expose event: %d\n", ((xcb_expose_event_t *)event)->count);
    struct WGX11WindowBackend *wbackend = (struct WGX11WindowBackend *)wgwindow->backend;
    if (event->count == 0) { /* the window was exposed: redraw it! */
        x11_put_background_buffer(wgwindow, wbackend->image);
    }
}

static int
x11_process_event_config_notify(struct WGWindow *wgwindow, xcb_configure_notify_event_t *event)
{
    DEBUG_X11("Configure event: %d x %d\n", xce->width, xce->height);
    struct WGX11WindowBackend *wbackend = (struct WGX11WindowBackend *)wgwindow->backend;

    if (x11_backbuffer_resize(wbackend, event->width, event->height) < 0)
        return -1;

    // resize the widget
    wgwindow->backend->wg.resize(wgwindow, event->width, event->height);
    return 0;
}

static void
x11_process_event_client_msg(struct WGWindow *wgwindow, xcb_client_message_event_t *event)
{
    struct WGX11WindowBackend *wbackend = (struct WGX11WindowBackend *)wgwindow->backend;

    if (event->type == wbackend->protocols_atom && event->format == 32 &&
        event->data.data32[0] == wbackend->delete_window_atom)
    {
        DEBUG_X11("Client message: window close request\n");
        wgwindow->backend->wg.close(wgwindow);
    }
    else
    {
        DEBUG_X11("Client message\n");
    }
}

static void
x11_process_event_shm_completion(struct WGX11WindowBackend *wbackend)
{
    enum ShmDrawState shm_state = wbackend->shm_state;
    wbackend->shm_state = ShmDrawReady;
    if (shm_state == ShmDrawFrameSkipped) {
        x11_window_draw(WG_WINBACKEND(wbackend));
    }
}

static int
x11_process_event(struct WGX11ApplicationBackend *x11_self, xcb_generic_event_t *event)
{
    // TODO multi window
    struct WGX11WindowBackend *wbackend = X11_WINBACKEND(wg_app->window->backend);
    struct WGWindow *wgwindow = wg_app->window;

    int rc = 0;
    switch (event->response_type & ~0x80)
    {
        case 0:
            x11_process_event_error((xcb_generic_error_t *)event);
            break;

        case XCB_EXPOSE:
            x11_process_event_expose(wgwindow, (xcb_expose_event_t *)event);
            break;

        case XCB_KEY_PRESS:
            x11_process_event_keypress(x11_self, (xcb_key_press_event_t *)event);
            break;

        // TODO: multitouch?
        case XCB_BUTTON_PRESS: {
            xcb_button_press_event_t *ev = (xcb_button_press_event_t *)event;
            DEBUG_EV_MOUSE("Pressed a button at: (%i,%i)\n", ev->event_x, ev->event_y);
            wgwindow->backend->wg.mouse_event(wgwindow, WG_MousePress, 0 /* event_id */, ev->event_x, ev->event_y);
            break;
        }

        case XCB_BUTTON_RELEASE: {
            xcb_button_release_event_t *ev = (xcb_button_release_event_t *)event;
            DEBUG_EV_MOUSE("Released a button at: (%i,%i)\n", ev->event_x, ev->event_y);
            wgwindow->backend->wg.mouse_event(wgwindow, WG_MouseRelease, 0 /* event_id */, ev->event_x, ev->event_y);
            break;
        }

        case XCB_MOTION_NOTIFY: {
            xcb_motion_notify_event_t *ev = (xcb_motion_notify_event_t *)event;
            DEBUG_EV_MOUSE_MOTION("Pointer motion: (%i,%i)\n", ev->event_x, ev->event_y);
            wgwindow->backend->wg.mouse_event(wgwindow, WG_MouseMove, 0 /* event_id */, ev->event_x, ev->event_y);
            break;
        }

        case XCB_CONFIGURE_NOTIFY:
            rc = x11_process_event_config_notify(wgwindow, (xcb_configure_notify_event_t *)event);
            break;

        case XCB_CLIENT_MESSAGE:
            x11_process_event_client_msg(wgwindow, (xcb_client_message_event_t *)event);
            break;

        default: {
            // events of x11 extensions
            if (event->response_type == x11_backend->input.first_xkb_event)
                x11_process_event_xkb(event);

            else if (event->response_type == x11_backend->shm_completion_event_id) {
                x11_process_event_shm_completion(wbackend);
            }
            break;
        }
    }

    return rc;
}

static void
x11_orientation_detect(void)
{
    int width = x11_backend->screen->width_in_pixels;
    int height = x11_backend->screen->height_in_pixels;
    DEBUG_X11("display size is %d x %d\n", width, height);

    enum WGOrientation orientation = width < height ? WG_Portrait : WG_Landscape;
    WG_APPBACKEND(x11_backend)->wg.orientation_changed(orientation);
}

static int
x11_application_dispatch(struct WGApplicationBackend *self, int timeout_ms)
{
    // DEBUG_X11("timeout: %d\n", timeout_ms);
    struct WGX11ApplicationBackend *x11_self = X11_APPBACKEND(self);

    xcb_generic_event_t *event = next_event_timeout(x11_self->conn, timeout_ms);

    // lets still process all x11 events before returning
    while (event != NULL) {
        x11_process_event(x11_self, event);
        free(event);
        event = xcb_poll_for_event(x11_self->conn);
    }
    free(event);

    return 0;
}

int
xkb_init(struct WGX11ApplicationBackend *backend)
{
    uint16_t major, minor;
    if (xkb_x11_setup_xkb_extension(backend->conn, 
        XKB_X11_MIN_MAJOR_XKB_VERSION, XKB_X11_MIN_MINOR_XKB_VERSION, 0,
        &major, &minor, &backend->input.first_xkb_event, NULL) != 1)
    {
       wg_log_error("xkb extension not available\n");
       goto error;
    }

    backend->input.context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
    if (backend->input.context == NULL) {
        wg_log_error("xkb_context_new failed\n");
        goto error;
    }

    backend->input.device_id = xkb_x11_get_core_keyboard_device_id(backend->conn);
    if (backend->input.device_id == -1) {
        wg_log_error("xkb_x11_get_core_keyboard_device_id has failed\n");
        goto error;
    }

    if (xkb_update_keymap(backend) < 0) {
        wg_log_error("Xkeyboard init has failed\n");
        goto error;
    }

    if (select_xkb_events_for_device(backend->conn, backend->input.device_id)) {
        wg_log_error("Failed to select xkb events for device\n");
        goto error;
    }

    return 0;

error:
    return -1;  // TODO free up?
}

struct WGApplicationBackend *
x11_application_new(struct WGApplicationFuncs wgfuncs)
{
    struct WGX11ApplicationBackend *backend = X11_APPBACKEND(
        calloc(1, sizeof(struct WGX11ApplicationBackend)));
    if (backend == NULL) {
        wg_log_error("Failed to allocate memory.\n");
        goto error;
    }
    backend->base.wg = wgfuncs;
    backend->base.window_new = x11_window_new;
    backend->base.free = x11_application_free;
    backend->base.dispatch = x11_application_dispatch;
    backend->base.grab_keyboard = x11_application_grab_keyboard;
    backend->use_shm = wg_settings()->backend.x11_config.shm;

    /* use the information from the environment variable DISPLAY 
       to create the X connection:
    */
    backend->conn = xcb_connect(NULL, NULL);
    if (backend->conn == NULL) {
        wg_log_error("Failed to open x11 display.\n");
        goto error;
    }
    int rc = xcb_connection_has_error(backend->conn);
    if (rc > 0) {
        wg_log_error("Failed to open x11 display. Error num: %d\n", rc);
        goto error;
    }

    /* Get the first screen */
    backend->screen = xcb_setup_roots_iterator(xcb_get_setup (backend->conn)).data;
    if (backend->screen == NULL) {
        wg_log_error("Failed to find screen.\n");
        goto error;
    }

    if (backend->use_shm && !check_for_xshm(backend->conn, &backend->shm_completion_event_id)) {
       DEBUG_X11("shared memory extension not available\n");
       backend->use_shm = 0;
    }

    if (xkb_init(backend))
        goto error;

    x11_backend = backend;
    x11_orientation_detect();

    goto finally;

error:
    if (backend != NULL) {
        x11_application_free(WG_APPBACKEND(backend));
        backend = NULL;
    }

finally:
    return WG_APPBACKEND(backend);
}

static void
x11_window_deinit_surface(struct WGX11WindowBackend *x11_self)
{
    DEBUG_X11("\n");

    if (x11_self->window_gc != 0) {
        xcb_free_gc(x11_backend->conn, x11_self->window_gc);
        x11_self->window_gc = 0;
    }

    if (x11_self->window != 0) {
        xcb_destroy_window(x11_backend->conn, x11_self->window);
        x11_self->window = 0;
    }

    x11_window_deinit_backbuffer(x11_self);
}

static void
x11_window_free(struct WGWindowBackend *self)
{
    DEBUG_X11("\n");
    x11_window_deinit_surface(X11_WINBACKEND(self));
    free(self);
}

static void
x11_apply_toplevel_configs(struct WGWindow *win, struct WGX11WindowBackend *winbackend)
{
    DEBUG_X11("\n");

    struct WMSizeHints
    {
        uint32_t flags;
        int32_t  x, y;
        int32_t  width, height;
        int32_t  min_width, min_height;
        int32_t  max_width, max_height;
        int32_t  width_inc, height_inc;
        int32_t  min_aspect_num, min_aspect_den;
        int32_t  max_aspect_num, max_aspect_den;
        int32_t  base_width, base_height;
        uint32_t win_gravity;
    } hints = { 0 };

    enum WMSizeHintsFlag
    {
        WM_SIZE_HINT_US_POSITION   = 1U << 0,
        WM_SIZE_HINT_US_SIZE       = 1U << 1,
        WM_SIZE_HINT_P_POSITION    = 1U << 2,
        WM_SIZE_HINT_P_SIZE        = 1U << 3,
        WM_SIZE_HINT_P_MIN_SIZE    = 1U << 4,
        WM_SIZE_HINT_P_MAX_SIZE    = 1U << 5,
        WM_SIZE_HINT_P_RESIZE_INC  = 1U << 6,
        WM_SIZE_HINT_P_ASPECT      = 1U << 7,
        WM_SIZE_HINT_BASE_SIZE     = 1U << 8,
        WM_SIZE_HINT_P_WIN_GRAVITY = 1U << 9
    };

    if (win->size_constraint & WG_MinimumSize) {
        DEBUG_X11("Setting min size: %dx%d\n", win->width_hint, win->height_hint);
        hints.flags |= WM_SIZE_HINT_P_MIN_SIZE;
        hints.min_width = win->width_hint;
        hints.min_height = win->height_hint;
    }

    if (win->size_constraint & WG_MaximumSize) {
        DEBUG_X11("Setting max size: %dx%d\n", win->width_hint, win->height_hint);
        hints.flags |= WM_SIZE_HINT_P_MAX_SIZE;
        hints.max_width = win->width_hint;
        hints.max_height = win->height_hint;
    }

    hints.flags |= WM_SIZE_HINT_P_SIZE;
    hints.width = win->width_hint;
    hints.height = win->height_hint;

    xcb_change_property(x11_backend->conn, XCB_PROP_MODE_REPLACE, winbackend->window,
        XCB_ATOM_WM_NORMAL_HINTS, XCB_ATOM_WM_SIZE_HINTS,
        32, sizeof(struct WMSizeHints) >> 2, &hints);

    /* Set the title of the window */
    xcb_change_property(x11_backend->conn, XCB_PROP_MODE_REPLACE, winbackend->window,
                        XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8, strlen(win->title), win->title);
}

static void
x11_register_window_close(xcb_connection_t *conn, struct WGX11WindowBackend *winbackend)
{
	xcb_intern_atom_reply_t *protocol_reply = xcb_intern_atom_reply(conn,
        xcb_intern_atom(conn, 1, strlen("WM_PROTOCOLS"), "WM_PROTOCOLS"), NULL);
    xcb_intern_atom_reply_t *wm_delete_window_reply = xcb_intern_atom_reply(conn,
        xcb_intern_atom(conn, 0, strlen ("WM_DELETE_WINDOW"), "WM_DELETE_WINDOW"), NULL);
    if (protocol_reply == NULL || wm_delete_window_reply == NULL) {
        wg_log_warning("Failed to hook on x11 window close\n");
    } else {
        winbackend->protocols_atom = protocol_reply->atom;
        winbackend->delete_window_atom = wm_delete_window_reply->atom;
        xcb_change_property(conn, XCB_PROP_MODE_REPLACE, winbackend->window,
            protocol_reply->atom, XCB_ATOM_ATOM, 32, 1, &(wm_delete_window_reply->atom));
    }
    free(protocol_reply);
    free(wm_delete_window_reply);
}

static void
x11_apply_motif_hints(xcb_connection_t *conn, xcb_drawable_t window, uint32_t flags)
{
    xcb_intern_atom_reply_t *reply = xcb_intern_atom_reply(conn,
        xcb_intern_atom(conn, 0, strlen ("_MOTIF_WM_HINTS"), "_MOTIF_WM_HINTS"), NULL);

    struct MotifHints
    {
        uint32_t   flags;
        uint32_t   functions;
        uint32_t   decorations;
        int32_t    input_mode;
        uint32_t   status;
    } hints = {
        .flags = flags,
    };

    if (reply == NULL) {
        wg_log_warning("Failed to apply motif wm hints\n");
    } else {
        xcb_change_property(x11_backend->conn, XCB_PROP_MODE_REPLACE, window,
                            reply->atom, XCB_ATOM_INTEGER, 32, 5, &hints);
    }
    free(reply);
    reply = NULL;
}

static int
x11_window_init_surface(struct WGWindow *win, struct WGX11WindowBackend *winbackend)
{
    DEBUG_X11("win=%p\n", win);

    /* Ask for our window's Id */
    winbackend->window = xcb_generate_id(x11_backend->conn);

    uint32_t mask = XCB_CW_BIT_GRAVITY | XCB_CW_EVENT_MASK;
    uint32_t values[] = {
            XCB_GRAVITY_STATIC,
            XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_MOTION |
            XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_STRUCTURE_NOTIFY
    };

    /* Create the window */
    DEBUG_X11("xcb_create_window width_hint=%d, height_hint=%d\n", win->width_hint, win->height_hint);
    int screen_width = x11_backend->screen->width_in_pixels;
    int screen_height = x11_backend->screen->height_in_pixels;

    // x11 does not like 0 size widgets, so lets figure out a default size
    if (win->width_hint <= 0)
        win->width_hint = screen_width * 8 / 10;
    if (win->height_hint <= 0)
        win->height_hint = screen_height * 8 / 10;

    int x = (screen_width - win->width_hint) / 2;
    int y = (screen_height - win->height_hint) / 2;

    xcb_void_cookie_t cookie = xcb_create_window_checked(x11_backend->conn,
        XCB_COPY_FROM_PARENT, winbackend->window,
        x11_backend->screen->root, x, y,
        win->width_hint, win->height_hint, 0 /* border width */,
        XCB_WINDOW_CLASS_INPUT_OUTPUT, x11_backend->screen->root_visual, mask, values);

    xcb_generic_error_t *error = xcb_request_check(x11_backend->conn, cookie);
    if (error) {
        wg_log_error("Error creating window: %d\n", error->error_code);
        free(error);
        goto fail;
    }

    // disable window title
    x11_apply_motif_hints(x11_backend->conn, winbackend->window, 2 /* flags */);

    x11_register_window_close(x11_backend->conn, winbackend);

    x11_apply_toplevel_configs(win, winbackend);

    winbackend->window_gc = xcb_generate_id(x11_backend->conn);

    mask = XCB_GC_GRAPHICS_EXPOSURES;
    uint32_t valgc[] = {
        0 /* no graphics exposures */
    };
    xcb_create_gc(x11_backend->conn, winbackend->window_gc, winbackend->window, mask, valgc);

    /* Map the window on the screen */
    cookie = xcb_map_window(x11_backend->conn, winbackend->window);
    error = xcb_request_check(x11_backend->conn, cookie);
    if (error) {
        wg_log_error("Error mapping window: %d\n", error->error_code);
        goto fail;
    }

    return 0;
fail:
    x11_window_deinit_surface(winbackend);
    return 1;
}

static void
x11_apply_window_configs(struct WGWindowBackend *self)
{
    DEBUG_X11("\n");
    struct WGWindow *win = self->wg.window;
    struct WGX11WindowBackend *xbackend = X11_WINBACKEND(self);

    if (self->wg.violates_size_constraints(win)) {  // TODO: maybe move this to window.c? is surface deinit/init needed for x11?
        x11_window_deinit_surface(xbackend);
        x11_window_init_surface(win, xbackend);
    } else {
        x11_apply_toplevel_configs(win, xbackend);
    }
}

struct WGWindowBackend *
x11_window_new(struct WGWindowFuncs *wgfuncs)
{
    DEBUG_X11("\n");
    struct WGX11WindowBackend *winbackend = X11_WINBACKEND(calloc(1, sizeof(struct WGX11WindowBackend)));
    if (winbackend == NULL) {
        wg_log_error("Failed to allocate memory\n");
        goto error;
    }

    winbackend->base.wg = *wgfuncs;
    winbackend->base.draw = x11_window_draw;
    winbackend->base.free = x11_window_free;
    winbackend->base.apply_window_configs = x11_apply_window_configs;

    if (x11_window_init_surface(wgfuncs->window, winbackend))
        goto error;

    goto finally;

error:
    if (winbackend != NULL) {
        x11_window_free(WG_WINBACKEND(winbackend));
        winbackend = NULL;
    }

finally:
    DEBUG_X11("result: %p\n", winbackend);
    return WG_WINBACKEND(winbackend);
}
