// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

/*
 * Common code for the backend plugins in case they work as a shared object.
 * Only include it if so.
 */

#define WG_REGISTER_BACKEND(backend_new_func) \
    widgie_dso_public struct WGBackendPlugin wg_plugin = {WG_BACKEND_PLUGIN_API_VERSION, backend_new_func}

// some defines so globals work seemless
#define wg_app wg_plugin.app
#define wg_log_level (*wg_plugin.log_level)
#define wg_log_displ_source (*wg_plugin.log_displ_source)
#define wg_log_output (*wg_plugin.log_output)
#define wg_settings wg_plugin.settings
