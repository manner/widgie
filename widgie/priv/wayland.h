// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WAYLAND
#define WG_HEADER_WAYLAND

#include "application_backend.h"
#include "window_backend.h"

struct WGWindow;

struct WGApplicationBackend *wayland_application_new(struct WGApplicationFuncs wgfuncs);
struct WGWindowBackend *wayland_window_new(struct WGWindowFuncs *wgfuncs);

#endif // FB_HEADER_WAYLAND