// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_APPLICATION_BACKEND
#define WG_HEADER_APPLICATION_BACKEND

#include "../uidefs.h"

#include <stdint.h>
#include <stdio.h>

struct WGWindowBackend;
struct WGWindow;
struct WGWindowFuncs;
typedef void (*EventFunc)(void *userdata);

/*
 * The backend API through which widgie communicates with the actual display 
 * server implementation (eg. x11, wayland) and vica versa.
 */

// functions called by the engine (eg. wayland)
struct WGApplicationFuncs {
    void (*key_pressed)(const char *utf8, uint32_t keysym);
    void (*orientation_changed)(enum WGOrientation new_orientation);
    void (*add_event)(int64_t trigger_ms, int trigger_fd, EventFunc func, void *userdata);
    int (*del_event)(EventFunc func);
    int (*wait_fd)(int fd, int timeout_ms);
};

typedef struct WGApplicationBackend * (*BackendNewFunc)(struct WGApplicationFuncs wgfuncs);

struct WGApplicationBackend {
    struct WGApplicationFuncs wg;

    // called by Application
    struct WGWindowBackend *(*window_new)(struct WGWindowFuncs *wgfuncs);
    int (*dispatch)(struct WGApplicationBackend *self, int timeout_ms);
    void (*free)(struct WGApplicationBackend *self);
    void (*grab_keyboard)(struct WGApplicationBackend *self, int should_grab);

};

#define WG_APPBACKEND(x) ((struct WGApplicationBackend *)x)

// make a symbol public
#define widgie_dso_public __attribute__((__visibility__("default")))

/*
 * The backend can also reside in a shared object so we can avoid linking to
 * eg. x11 libs when running on wayland.
 * The plugin below is the API for that, a public symbol loaded from the shared object.
 */
#define WG_BACKEND_PLUGIN_API_VERSION 1
struct WGBackendPlugin {
    int version;
    BackendNewFunc backend_new;

    struct WGApplication *app;

    int *log_level;
    int *log_displ_source;
    FILE **log_output;

    struct WGWidgieSettings *(*settings)(void);
};

#endif  // HEADER_APPLICATION_BACKEND