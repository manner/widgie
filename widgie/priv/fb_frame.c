// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "../frame.h"

// Copy rect without positions where alpha is 0. Used for rendering mouse.
void
wg_render_frame_copy_rect_a(struct WGRenderFrame *from_frame, struct WGRenderFrame *to_frame,
                       int from_x, int from_y, int width, int height, int to_x, int to_y)
{
    if (wg__render_frame_copy_rect_fix_positions(from_frame, to_frame, &from_x, &from_y, &width, &height, &to_x, &to_y))
        return;  // nothing to do

    // now our task is ensured to be doable
    uint32_t *from_pos = from_frame->data + from_x + from_frame->width * from_y;
    uint32_t *to_pos = to_frame->data + to_x + to_frame->width * to_y;
    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x, ++from_pos, ++to_pos)
        {
            if (0xff000000 & *from_pos)
                *to_pos = *from_pos;
        }
        from_pos += from_frame->width - width;
        to_pos += to_frame->width - width;
    }
}

static int
render_frame_copy_rect_fix_positions_rotated(struct WGRenderFrame *from_frame, struct WGRenderFrame *to_frame,
    int *pfrom_x, int *pfrom_y, int *pwidth, int *pheight, int *pto_x, int *pto_y, int rotation)
{
    int orig_width = to_frame->width;
    if (rotation & 1) {  // +/-90 degrees rotated, switch width, height
        to_frame->width = to_frame->height;
        to_frame->height = orig_width;
    }

    int is_empty = wg__render_frame_copy_rect_fix_positions(from_frame, to_frame, pfrom_x, pfrom_y, pwidth, pheight, pto_x, pto_y);

    if (rotation & 1) {  // +/-90 degrees rotated, switch back width, height
        to_frame->height = to_frame->width;
        to_frame->width = orig_width;
    }
    return is_empty;
}

void
wg_render_frame_rotate_xy(struct WGRenderFrame *frame, int *px, int *py, int rotation)
{
    int x = *px, y = *py;
    rotation = rotation < 0 ? -((-rotation) & 3) : rotation & 3;
    switch (rotation) {
        case 0:
            break;
        case 1:
            *px = frame->width - y - 1;
            *py = x;
            break;
        case -1:
            *px = y;
            *py = frame->width - x - 1;
            break;
        case -2:
        case 2:
            *px = frame->width - x - 1;
            *py = frame->height - y - 1;
            break;
        case 3:
            *px = y;
            *py = frame->height - x - 1;
            break;
        case -3:
            *px = frame->height - y - 1;
            *py = x;
            break;
    }
}

static void
copy_rect_compute_step(struct WGRenderFrame *to_frame, int rotation, int *pto_stepx, int *pto_stepy)
{
    switch (rotation & 0x3) {
        case 0:
            *pto_stepx = 1;
            *pto_stepy = to_frame->width;
            break;
        case 1:
            *pto_stepx = to_frame->width;
            *pto_stepy = -1;
            break;
        case 2:
            *pto_stepx = -1;
            *pto_stepy = -to_frame->width;
            break;
        case 3:
            *pto_stepx = -to_frame->width;
            *pto_stepy = 1;
            break;
    };
}

void
wg_render_frame_copy_rect_rotated(struct WGRenderFrame *from_frame, struct WGRenderFrame *to_frame,
    int from_x, int from_y, int width, int height, int to_x, int to_y, int rotation)
{
    if (render_frame_copy_rect_fix_positions_rotated(from_frame, to_frame, &from_x, &from_y, &width, &height, &to_x, &to_y, rotation))
        return;  // nothing to do
    // now our task is ensured to be doable

    wg_render_frame_rotate_xy(to_frame, &to_x, &to_y, rotation);
    int to_stepx, to_stepy;
    copy_rect_compute_step(to_frame, rotation, &to_stepx, &to_stepy);

    uint32_t *from_pos = from_frame->data + from_x + from_frame->width * from_y;
    uint32_t *to_pos = to_frame->data + to_x + to_frame->width * to_y;
    for (int y = 0; y < height; ++y)
    {
        uint32_t *orig_to_pos = to_pos;
        for (int x = 0; x < width; ++x, ++from_pos, to_pos+=to_stepx)
        {
            *to_pos = *from_pos;
        }
        from_pos += from_frame->width - width;
        to_pos = orig_to_pos + to_stepy;
    }
}
