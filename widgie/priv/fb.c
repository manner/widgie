// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "fb.h"
#include "fb_mouse.h"
#include "fb_frame.h"

#include "window_backend.h"
#include "application_backend.h"
#include "../window.h"
#include "../application.h"
#include "../log.h"
#include "../settings.h"

#include <linux/fb.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <termios.h>

#include <xkbcommon/xkbcommon-keysyms.h>
#include <xkbcommon/xkbcommon.h>

#include <libinput.h>

#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>


#define FB_WINBACKEND(x) ((struct WGFbWindowBackend *)x)
#define FB_APPBACKEND(x) ((struct WGFbApplicationBackend *)x)

// Here you can enable debugs:
#define DEBUG_FB              WG_DEBUG_HIDE
#define DEBUG_PAINT           WG_DEBUG_HIDE
#define DEBUG_EV_KEY          WG_DEBUG_HIDE
#define DEBUG_EV_MOUSE        WG_DEBUG_HIDE
#define DEBUG_EV_MOUSE_MOTION WG_DEBUG_HIDE

enum WGFbMousePointerEmulation {
    HideMouse,
    ShowMouse,
    DisableMouse
};

struct Mouse {
    enum WGFbMousePointerEmulation emulation;
    double x;  // the current mouse position in the orientation of widgie window
    double y;
    int rendered_x;  // the x,y coords where the mouse has last been rendered
    int rendered_y;
    struct WGRenderFrame frame;  // the look of the mouse oriented to screen dimensions
    struct WGRenderFrame background;
};

struct WGFbApplicationBackend {
    struct WGApplicationBackend base;

    int fbfd;  // the file descriptor of the framebuffer device (eg. /dev/fd0)
    struct WGRenderFrame screen_frame;  // a frame to the raw pixel data of the framebuffer

    int rotation;  // the rotation difference between the screen and the widgie window
                   // (0 = same, 1 = rotated 90 degrees counterclockwise, 2 = 180 degrees ...)

    struct udev *udev;
    struct libinput *input;
    struct termios original_term;  // the original state of the terminal (before widgie modifications)
    int tty;
    struct xkb_state *xkb_state;

    struct Mouse mouse;

    int keyrepeat_delay0;
    int keyrepeat_delay1;
    uint32_t pressed_keycode;
};

struct WGFbWindowBackend {
    struct WGWindowBackend base;
    int x;
    int y;
};

static struct WGFbApplicationBackend *fb_backend = NULL;

#ifdef SHARED_FRAMEBUFFER
#include "shared_backend.h"
WG_REGISTER_BACKEND(fb_application_new);
#endif

void
mouse_coord_to_screen_coord(int *px, int *py)
{
    struct WGRenderFrame *frame = &fb_backend->screen_frame;
    *px = fb_backend->mouse.x;
    *py = fb_backend->mouse.y;
    wg_render_frame_rotate_xy(frame, px, py, fb_backend->rotation);
}

void
screen_coord_to_mouse_coord(int x, int y)
{
    struct WGRenderFrame *frame = &fb_backend->screen_frame;
    wg_render_frame_rotate_xy(frame, &x, &y, - fb_backend->rotation);
    fb_backend->mouse.x = x;
    fb_backend->mouse.y = y;
}

static void
fb_application_free(struct WGApplicationBackend *self)
{
    DEBUG_FB("\n");
    struct WGFbApplicationBackend *fb_self = FB_APPBACKEND(self);

    if (fb_self->xkb_state != NULL) {
        xkb_state_unref(fb_self->xkb_state);
        fb_self->xkb_state = NULL;
    }

    if (fb_self->input != NULL) {
        libinput_unref(fb_self->input);
        fb_self->input = NULL;
    }

    if (fb_self->udev != NULL) {
        udev_unref(fb_self->udev);
        fb_self->udev = NULL;
    }

    if (fb_self->tty >= 0) {
        dprintf(fb_self->tty, "\033[?25h"); // switch cursor on
        if (tcsetattr(fb_self->tty, TCSAFLUSH, &fb_self->original_term) == -1) {
            wg_log_warning("Failed to restore original terminal state\n");
        }
        close(fb_self->tty);
        fb_self->tty = -1;
    } 

    if (fb_self->screen_frame.data != NULL) {
        // "remove" our application from the screen after our application closes
        wg_render_frame_clear(&(fb_self->screen_frame));
        munmap (fb_self->screen_frame.data, fb_self->screen_frame.width * fb_self->screen_frame.height * sizeof(uint32_t));
        fb_self->screen_frame.data = NULL;
    }

    if (fb_self->mouse.frame.data != NULL) {
        free(fb_self->mouse.frame.data);
        fb_self->mouse.frame.data = NULL;
    }

    if (fb_self->mouse.background.data != NULL) {
        free(fb_self->mouse.background.data);
        fb_self->mouse.background.data = NULL;
    }

    if (fb_self->fbfd >= 0) {
        close (fb_self->fbfd);
        fb_self->fbfd = -1;
    }

    free(fb_self->mouse.background.data);
    fb_self->mouse.background.data = NULL;

    free(fb_self);

    if (fb_backend == fb_self)
        fb_backend = NULL;
}

static void
render_mouse_remove(struct WGFbApplicationBackend *fb_self)
{
    if (fb_self->mouse.emulation != ShowMouse)
        return;

    if (fb_self->mouse.background.width > 0) {
        wg_render_frame_copy_rect(&fb_self->mouse.background, &fb_self->screen_frame, 0, 0,
            fb_self->mouse.background.width, fb_self->mouse.background.height,
            fb_self->mouse.rendered_x, fb_self->mouse.rendered_y);
    }
}

static void
render_mouse_add(struct WGFbApplicationBackend *fb_self)
{
    if (fb_self->mouse.emulation != ShowMouse)
        return;

    int mouse_x, mouse_y;
    mouse_coord_to_screen_coord(&mouse_x, &mouse_y);

    // backup underlying teritory
    if (fb_self->mouse.background.width == 0)
    {
        fb_self->mouse.background.width = fb_self->mouse.frame.width;
        fb_self->mouse.background.height = fb_self->mouse.frame.height;
        fb_self->mouse.background.data = malloc(fb_self->mouse.frame.width * fb_self->mouse.frame.height * 4);
    }

    wg_render_frame_copy_rect(&fb_self->screen_frame, &fb_self->mouse.background, mouse_x, mouse_y,
        fb_self->mouse.frame.width, fb_self->mouse.frame.height, 0, 0);

    // render the mouse
    wg_render_frame_copy_rect_a(&fb_self->mouse.frame, &fb_self->screen_frame, 0, 0,
        fb_self->mouse.frame.width, fb_self->mouse.frame.height, mouse_x, mouse_y);

    fb_self->mouse.rendered_x = mouse_x;
    fb_self->mouse.rendered_y = mouse_y;
}

static void
fb_window_draw(struct WGWindowBackend *self)
{
    struct WGWindow *win = self->wg.window;
    struct WGFbWindowBackend *wbackend = FB_WINBACKEND(self);

    struct WGWidget *widget = WG_WIDGET(win);
    DEBUG_PAINT("Painting window: %p %dx%d to framebuffer: %dx%d\n", win,
        widget->width, widget->height, fb_backend->screen_frame.width, fb_backend->screen_frame.height);

    struct WGRenderFrame *backbuffer = wg_render_frame_new(
        WG_WIDGET(win)->width, WG_WIDGET(win)->height, NULL);
    wg_render_frame_clear(backbuffer);
    WG_WIDGET(win)->render(widget, backbuffer);

    render_mouse_remove(fb_backend);
    wg_render_frame_copy_rect_rotated(backbuffer, &fb_backend->screen_frame, 0, 0,
        backbuffer->width, backbuffer->height, wbackend->x, wbackend->y, fb_backend->rotation);
    render_mouse_add(fb_backend);
    wg_render_frame_free(backbuffer);
}

static void
fb_orientation_detect(void)
{
    DEBUG_FB("display size is %d x %d\n", fb_backend->screen_frame.width, fb_backend->screen_frame.height);

    enum WGOrientation orientation = fb_backend->screen_frame.width < fb_backend->screen_frame.height ? WG_Portrait : WG_Landscape;
    if (fb_backend->rotation % 2 == 1)
        orientation = orientation == WG_Landscape ? WG_Portrait : WG_Landscape;
    WG_APPBACKEND(fb_backend)->wg.orientation_changed(orientation);
}

static void
handle_event_device(struct WGFbApplicationBackend *self, struct libinput_device *event, int is_added)
{
    const char *name = libinput_device_get_name(event);
    DEBUG_FB("Device %s: '%s'\n", is_added ? "added" : "removed", name);
    (void) name;
    (void) is_added;
    (void) self;
}

// TODO some common keyrepeat code here with wayland backend
static void
fb_notify_keycode(struct WGFbApplicationBackend *self, uint32_t key)
{
    char buf[128];
    xkb_keysym_t sym = xkb_state_key_get_one_sym(self->xkb_state, key);

    xkb_keysym_get_name(sym, buf, sizeof(buf));
    DEBUG_EV_KEY("key %s: sym: %-12s (%d), ", "press", buf, sym);

    xkb_state_key_get_utf8(self->xkb_state, key, buf, sizeof(buf));
    DEBUG_EV_KEY("utf8: '%s'\n", buf);
    if (WG_APPBACKEND(self)->wg.key_pressed != NULL)
    {
        WG_APPBACKEND(self)->wg.key_pressed(buf, sym);
    }
}

static void
fb_keyrepeat_check(void *data)
{
    struct WGFbApplicationBackend *self = data;
    if (self->pressed_keycode == 0)
        return;  // Stop repeating

    fb_notify_keycode(self, self->pressed_keycode);
    self->base.wg.add_event(self->keyrepeat_delay1, -1, fb_keyrepeat_check, data);
}

static void
handle_event_key(struct WGFbApplicationBackend *self, struct libinput_event_keyboard *event)
{
    uint32_t key = libinput_event_keyboard_get_key(event);
    enum libinput_key_state state = libinput_event_keyboard_get_key_state(event);

    key += 8;

    self->base.wg.del_event(fb_keyrepeat_check);

    if (state == LIBINPUT_KEY_STATE_PRESSED) {
        self->pressed_keycode = key;
        fb_notify_keycode(self, self->pressed_keycode);
        self->base.wg.add_event(self->keyrepeat_delay0, -1, fb_keyrepeat_check, self);
    } else {
        self->pressed_keycode = 0;
    }

    xkb_state_update_key(self->xkb_state, key, state == LIBINPUT_KEY_STATE_PRESSED ? XKB_KEY_DOWN : XKB_KEY_UP);
}

static void
mouse_event(struct WGWindow *win, enum WGMouseEvent type, int event_id)
{
    struct WGFbWindowBackend *wbackend = FB_WINBACKEND(win->backend);
    win->backend->wg.mouse_event(win, type, 0, fb_backend->mouse.x - wbackend->x, fb_backend->mouse.y - wbackend->y);
}

static void
handle_event_pointer_motion(struct WGWindow *win, struct libinput_event_pointer *event)
{
    fb_backend->mouse.x += libinput_event_pointer_get_dx(event);
    fb_backend->mouse.y += libinput_event_pointer_get_dy(event);
    fb_backend->mouse.x = wg_bound(0.0, fb_backend->mouse.x, (fb_backend->rotation & 1) ? fb_backend->screen_frame.height : fb_backend->screen_frame.width);
    fb_backend->mouse.y = wg_bound(0.0, fb_backend->mouse.y, (fb_backend->rotation & 1) ? fb_backend->screen_frame.width : fb_backend->screen_frame.height);

    DEBUG_EV_MOUSE_MOTION("Mouse moves: %dx%d\n", (int)fb_backend->mouse.x, (int)fb_backend->mouse.y);
    mouse_event(win, WG_MouseMove, 0);
    if (fb_backend->mouse.emulation == HideMouse)
        fb_backend->mouse.emulation = ShowMouse;
}

static void
handle_event_pointer_button(struct WGWindow *win, struct libinput_event_pointer *event)
{
    // TODO handle right clicks? uint32_t button = libinput_event_pointer_get_button(event);
    enum libinput_button_state state = libinput_event_pointer_get_button_state(event);
    struct WGFbWindowBackend *wbackend = FB_WINBACKEND(win->backend);

    enum WGMouseEvent wg_state;
    switch (state) {
        case LIBINPUT_BUTTON_STATE_RELEASED:
            wg_state = WG_MouseRelease;
            break;
        case LIBINPUT_BUTTON_STATE_PRESSED:
            wg_state = WG_MousePress;
            break;
        default:
            return;
    }

    DEBUG_EV_MOUSE("Mouse %s\n", wg_state == WG_MousePress ? "press" : "release");
    win->backend->wg.mouse_event(win, wg_state, 0, fb_backend->mouse.x - wbackend->x, fb_backend->mouse.y - wbackend->y);
}

static void
handle_event_touch(struct WGWindow *win, enum WGMouseEvent wg_state, struct libinput_event_touch *event)
{
    if (wg_state == WG_MouseMove || wg_state == WG_MousePress) {
        int x = libinput_event_touch_get_x_transformed(event, fb_backend->screen_frame.width);
        int y = libinput_event_touch_get_y_transformed(event, fb_backend->screen_frame.height);
        screen_coord_to_mouse_coord(x, y);
        DEBUG_EV_MOUSE_MOTION("Touch %s: %dx%d\n", wg_state == WG_MousePress ? "press" : "motion",
            (int)fb_backend->mouse.x, (int)fb_backend->mouse.y);
    } else {
        DEBUG_EV_MOUSE_MOTION("Touch release\n");
    }

    mouse_event(win, wg_state, 0);
    if (fb_backend->mouse.emulation == ShowMouse) {
        render_mouse_remove(fb_backend);
        fb_backend->mouse.emulation = HideMouse;
    }
}

static int
fb_application_dispatch(struct WGApplicationBackend *self, int timeout_ms)
{
    // DEBUG_FB("timeout: %d\n", timeout_ms);
    struct WGFbApplicationBackend *fb_self = FB_APPBACKEND(self);

    // TODO multi window
    struct WGWindow *win = wg_app->window;

    int input_fd = libinput_get_fd(fb_self->input);

    struct libinput_event *event = NULL;
    libinput_dispatch(fb_self->input);
    event = libinput_get_event(fb_self->input);
    if (event == NULL && self->wg.wait_fd(input_fd, timeout_ms)) {
        libinput_dispatch(fb_self->input);
        event = libinput_get_event(fb_self->input);
    }

    int should_render_mouse = 0;
    while (event != NULL)
    {
        enum libinput_event_type type = libinput_event_get_type(event);
        switch (type) {
            case LIBINPUT_EVENT_DEVICE_ADDED:
            case LIBINPUT_EVENT_DEVICE_REMOVED:
                handle_event_device(fb_self, libinput_event_get_device(event), type == LIBINPUT_EVENT_DEVICE_ADDED);
                break;

            case LIBINPUT_EVENT_KEYBOARD_KEY:
                handle_event_key(fb_self, libinput_event_get_keyboard_event(event));
                break;

            case LIBINPUT_EVENT_POINTER_MOTION:
                handle_event_pointer_motion(win, libinput_event_get_pointer_event(event));
                should_render_mouse = ((int)fb_self->mouse.x) != fb_self->mouse.rendered_x || ((int)fb_self->mouse.y) != fb_self->mouse.rendered_y;
                break;

            case LIBINPUT_EVENT_POINTER_BUTTON:
                handle_event_pointer_button(win, libinput_event_get_pointer_event(event));
                break;

            // TODO: scroll

            case LIBINPUT_EVENT_TOUCH_DOWN:
                handle_event_touch(win, WG_MousePress, libinput_event_get_touch_event(event));
                break;
            case LIBINPUT_EVENT_TOUCH_UP:
                handle_event_touch(win, WG_MouseRelease, libinput_event_get_touch_event(event));
                break;
            case LIBINPUT_EVENT_TOUCH_MOTION:
                handle_event_touch(win, WG_MouseMove, libinput_event_get_touch_event(event));
                break;
            case LIBINPUT_EVENT_TOUCH_CANCEL:
                break;
            case LIBINPUT_EVENT_TOUCH_FRAME:
                break;

            default:
                break;
        }

        libinput_event_destroy(event);
        libinput_dispatch(fb_self->input);
        event = libinput_get_event(fb_self->input);
    }

    if (should_render_mouse != 0) {
        render_mouse_remove(fb_self);
        render_mouse_add(fb_self);
    }

    return 0;
}

static int open_restricted(const char *path, int flags, void *user_data)
{
        int fd = open(path, flags);
        return fd < 0 ? -errno : fd;
}
 
static void close_restricted(int fd, void *user_data)
{
        close(fd);
}

const static struct libinput_interface libinput_interface = {
        .open_restricted = open_restricted,
        .close_restricted = close_restricted,
};

struct WGApplicationBackend *
fb_application_new(struct WGApplicationFuncs wgfuncs)
{
    DEBUG_FB("\n");
    struct WGFbApplicationBackend *backend = FB_APPBACKEND(
        calloc(1, sizeof(struct WGFbApplicationBackend)));
    if (backend == NULL) {
        wg_log_error("Failed to allocate memory.\n");
        goto error;
    }
    backend->base.wg = wgfuncs;
    backend->base.window_new = fb_window_new;
    backend->base.free = fb_application_free;
    backend->base.dispatch = fb_application_dispatch;
    backend->base.grab_keyboard = NULL;
    backend->tty = -1;

    struct WGFBBackendSettings *settings = &wg_settings()->backend.framebuffer_config;
    DEBUG_FB("Emulate mouse: %d\n", settings->emulate_mouse);
    backend->mouse.emulation = settings->emulate_mouse ? HideMouse : DisableMouse;

    DEBUG_FB("Setting up tty\n");
    if (settings->tty != NULL && *settings->tty != '\0') {
        backend->tty = open(settings->tty, O_RDWR|O_NOCTTY);  // failing to open these should not be fatal
        if (backend->tty >= 0) {
            dprintf(backend->tty, "\033[?25l"); // switch cursor off
            struct termios raw;
            if (tcgetattr(backend->tty, &(backend->original_term)) != -1 && tcgetattr(backend->tty, &raw) != -1)
            {
                raw.c_iflag &= ~(INPCK | ISTRIP | IXON | BRKINT | ICRNL);
                raw.c_cflag |= (CS8);
                raw.c_lflag &= ~(ECHO | ICANON | IEXTEN);
                raw.c_cc[VMIN] = 0;
                raw.c_cc[VTIME] = 1;
                if (tcsetattr(backend->tty, TCSAFLUSH, &raw) == -1) {
                    DEBUG_FB("Failed to init raw mode\n");
                }
            }
            else {
                DEBUG_FB("Failed to get termios state of tty\n");
            }
        } else {
            DEBUG_FB("Failed to open tty\n");
        }
    }

    /* init the framebuffer */
    const char *fbdev = settings->device;
    DEBUG_FB("Open framebuffer device '%s'\n", fbdev);
    backend->fbfd = open(fbdev, O_RDWR);
    if (backend->fbfd < 0) {
        wg_log_error("Failed to open the framebuffer device '%s': %d - %s\n", fbdev, errno, strerror(errno));
        goto error;
    }

    struct fb_fix_screeninfo finfo;
    struct fb_var_screeninfo vinfo;
    if (ioctl(backend->fbfd, FBIOGET_VSCREENINFO, &vinfo) < 0 ||
        ioctl (backend->fbfd, FBIOGET_FSCREENINFO, &finfo) < 0) {
        wg_log_error("Failed to identify framebuffer screen information '%s': %d - %s\n", fbdev, errno, strerror(errno));
        goto error;
    }

    if (vinfo.bits_per_pixel != 32) {
        wg_log_error("Only 32 bit pixels are supported for now, but current bpp is %d. "
            "Switch framebuffer mode to xRGB!\n", vinfo.bits_per_pixel);
        goto error;
    }

    backend->screen_frame.width = finfo.line_length / sizeof(uint32_t);
    backend->screen_frame.height = vinfo.yres;
    size_t len = finfo.line_length * vinfo.yres;

    uint32_t *screen = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, backend->fbfd, 0);
    if (screen == MAP_FAILED) {
        wg_log_error("mmap failed: %d - %s\n", errno, strerror(errno));
        goto error;
    }
    backend->screen_frame.data = screen;

    backend->rotation = settings->window.rotate;

    DEBUG_FB("Initializing udev and libinput\n");
    backend->udev = udev_new();
    backend->input = libinput_udev_create_context(&libinput_interface, NULL, backend->udev);
    if (backend->input == NULL) {
        wg_log_error("Failed to create libinput context\n");
        goto error;
    }

    if (libinput_udev_assign_seat(backend->input, "seat0") < 0) {
        wg_log_error("Failed to assign seat\n");
        goto error;
    }

    struct xkb_context *xkb_context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
    if (xkb_context == NULL) {
        wg_log_error("Failed to init xkb context\n");
        goto error;
    }
    struct xkb_rule_names names = {
        .rules = settings->xkb_config.rules,
        .model = settings->xkb_config.model,
        .layout = settings->xkb_config.layout,
        .variant = settings->xkb_config.variant,
        .options = settings->xkb_config.options
    };
    struct xkb_keymap *xkb_keymap = xkb_keymap_new_from_names(xkb_context, &names, XKB_KEYMAP_COMPILE_NO_FLAGS);
    if (xkb_keymap == NULL) {
        wg_log_error("Failed to create xkb keymap\n");
        goto error;
    }

    backend->xkb_state = xkb_state_new(xkb_keymap);
    if (backend->xkb_state == NULL) {
        wg_log_error("Failed to allocate xkb_state\n");
        goto error;
    }

    xkb_keymap_unref(xkb_keymap);
    xkb_context_unref(xkb_context);

    backend->mouse.frame.width = (backend->rotation & 1) ? mouse_default.height : mouse_default.width;
    backend->mouse.frame.height = (backend->rotation & 1) ? mouse_default.width : mouse_default.height;
    backend->mouse.frame.data = malloc(mouse_default.width * mouse_default.height * 4);
    wg_render_frame_copy_rect_rotated(&mouse_default, &backend->mouse.frame, 0, 0, mouse_default.width, mouse_default.height, 0, 0, backend->rotation);

    backend->keyrepeat_delay0 = settings->xkb_config.repeat_delay;
    backend->keyrepeat_delay1 = 1000.0 / settings->xkb_config.repeat_rate;

    DEBUG_FB("Framebuffer backend initialized\n");

    fb_backend = backend;
    fb_orientation_detect();

    goto finally;

error:
    if (backend != NULL) {
        fb_application_free(WG_APPBACKEND(backend));
        backend = NULL;
    }

finally:
    return WG_APPBACKEND(backend);
}

static void
fb_window_free(struct WGWindowBackend *self)
{
    DEBUG_FB("\n");

    struct WGFbWindowBackend *wbackend = FB_WINBACKEND(self);
    (void) wbackend;

    free(self);
}

static void
fb_apply_window_configs(struct WGWindowBackend *self)
{
    struct WGWindow *win = self->wg.window;
    struct WGFbWindowBackend *xbackend = FB_WINBACKEND(self);

    DEBUG_FB("Sizehint: %dx%d constraint: %d\n", win->width_hint, win->height_hint, win->size_constraint);

    struct WGWindowPlacementSettings *settings = &wg_settings()->backend.framebuffer_config.window;
    int screen_width = (fb_backend->rotation & 1) ? fb_backend->screen_frame.height : fb_backend->screen_frame.width;
    int screen_height = (fb_backend->rotation & 1) ? fb_backend->screen_frame.width : fb_backend->screen_frame.height;
    int margin_x = (fb_backend->rotation & 1) ? settings->margin_height : settings->margin_width;
    int margin_y = (fb_backend->rotation & 1) ? settings->margin_width : settings->margin_height;

    int width = screen_width - margin_x;
    int height = screen_height - margin_y;
    if ((win->size_constraint & WG_MaximumSize) != 0) {
        width = wg_min(win->width_hint, width);
        height = wg_min(win->height_hint, height);
    }

    DEBUG_FB("Requesting window to change size to: %dx%d\n", width, height);
    self->wg.resize(win, width, height);

    DEBUG_FB("Computing alignment: %d, x margin: %d, y margin: %d\n", settings->align, settings->margin_width, settings->margin_height);

    xbackend->x = settings->margin_width;
    xbackend->y = settings->margin_height;
    if (settings->align & WG_AlignHCenter)
        xbackend->x = (screen_width - WG_WIDGET(win)->width) / 2;
    if (settings->align & WG_AlignVCenter)
        xbackend->y = (screen_height - WG_WIDGET(win)->height) / 2;
    if (settings->align & WG_AlignRight)
        xbackend->x = screen_width - WG_WIDGET(win)->width - margin_x;
    if (settings->align & WG_AlignBottom)
        xbackend->y = screen_height - WG_WIDGET(win)->height - margin_y;

    DEBUG_FB("Showing window at position: %d,%d\n", xbackend->x, xbackend->y);
}

struct WGWindowBackend *
fb_window_new(struct WGWindowFuncs *wgfuncs)
{
    DEBUG_FB("\n");
    struct WGFbWindowBackend *winbackend = FB_WINBACKEND(calloc(1, sizeof(struct WGFbWindowBackend)));
    if (winbackend == NULL) {
        wg_log_error("Failed to allocate memory\n");
        goto error;
    }

    winbackend->base.wg = *wgfuncs;
    winbackend->base.draw = fb_window_draw;
    winbackend->base.free = fb_window_free;
    winbackend->base.apply_window_configs = fb_apply_window_configs;

    fb_apply_window_configs(&winbackend->base);
    goto finally;

error:
    if (winbackend != NULL) {
        fb_window_free(WG_WINBACKEND(winbackend));
        winbackend = NULL;
    }

finally:
    DEBUG_FB("result: %p\n", winbackend);
    return WG_WINBACKEND(winbackend);
}
