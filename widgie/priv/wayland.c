// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "wayland.h"
#include "shm.h"
#include "widgie/window.h"
#include "widgie/application.h"
#include "widgie/frame.h"
#include "widgie/log.h"
#include "widgie/settings.h"

#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include <wayland-client-core.h>
#include <wayland-client.h>
#include "xdg-shell-client-protocol.h"
#include <xkbcommon/xkbcommon.h>

// TODO move to subdir and split to wayland_window, wayland_app, wayland_input

#define WAYLAND_WINBACKEND(x) ((struct WGWaylandWindowBackend *)x)
#define WAYLAND_APPBACKEND(x) ((struct WGWaylandApplicationBackend *)x)

// Here you can enable debugs:
#define DEBUG_EV_KEY          WG_DEBUG_HIDE
#define DEBUG_EV_MOUSE        WG_DEBUG_HIDE
#define DEBUG_EV_MOUSE_MASK   ~POINTER_EVENT_MOTION
//#define DEBUG_EV_MOUSE_MASK   0xffff

#define DEBUG_WL WG_DEBUG_HIDE

enum WGpointer_event_mask {
    POINTER_EVENT_ENTER = 1 << 0,
    POINTER_EVENT_LEAVE = 1 << 1,
    POINTER_EVENT_MOTION = 1 << 2,
    POINTER_EVENT_BUTTON = 1 << 3,
    POINTER_EVENT_AXIS = 1 << 4,
    POINTER_EVENT_AXIS_SOURCE = 1 << 5,
    POINTER_EVENT_AXIS_STOP = 1 << 6,
    POINTER_EVENT_AXIS_DISCRETE = 1 << 7,
};

enum WGtouch_event_mask {
    TOUCH_EVENT_DOWN = 1 << 0,
    TOUCH_EVENT_UP = 1 << 1,
    TOUCH_EVENT_MOTION = 1 << 2,
};

struct WGpointer_event {
    uint32_t event_mask;
    wl_fixed_t surface_x, surface_y;
    uint32_t button, state;
    uint32_t time;
    uint32_t serial;
    struct WG{
        bool valid;
        wl_fixed_t value;
        int32_t discrete;
    } axes[2];
    uint32_t axis_source;
};

struct WGtouch_event {
    uint32_t event_mask;
    wl_fixed_t surface_x, surface_y;
    uint32_t button, state;
    uint32_t time;
    uint32_t event_id;
};

struct WGWaylandInput
{
    struct wl_seat *wl_seat;   // input handling (keyboard, pointer, touchscreen)

    struct wl_pointer *wl_pointer;  // mouse
    struct WGpointer_event pointer_event;
    struct wl_touch *wl_touch;      // touch screen
    struct WGtouch_event touch_event;

    struct wl_keyboard *wl_keyboard;
    struct xkb_state *xkb_state;
    struct xkb_context *xkb_context;
    struct xkb_keymap *xkb_keymap;
    int32_t keyrepeat_delay1;
    int32_t keyrepeat_delay0;
    uint32_t pressed_keycode;
};

struct WGWaylandApplicationBackend {
    struct WGApplicationBackend base;

    struct wl_display *wl_display;   // a connection with wayland
    struct wl_output *wl_output;
    struct wl_registry *wl_registry; // something for creating / destroying objects
    struct wl_compositor *wl_compositor;
    struct xdg_wm_base *xdg_wm_base;
    struct wl_shm *wl_shm;           // manages the shared memory between the server and the client
    uint32_t output_scale_factor;

    struct WGWaylandInput input;
};

struct WGWaylandWindowBackend {
    struct WGWindowBackend base;

    struct wl_surface *wl_surface;    /* A surface is a box of pixels that can be displayed on-screen */
    struct xdg_surface *xdg_surface;
    struct xdg_toplevel *xdg_toplevel;  // a surface can be toplevel or popup
    int configured;
};

#ifdef SHARED_WAYLAND
#include "shared_backend.h"
WG_REGISTER_BACKEND(wayland_application_new);
#endif

static struct WGWaylandApplicationBackend *wl_backend = NULL;

static inline int
round_up(int num, int divisor)
{
    int remainder = num % divisor;
    return remainder == 0 ? num : num - remainder + divisor;
}

static void
toplevel_configure_handler(void *data, struct xdg_toplevel *xdg_toplevel, int32_t width, int32_t height, struct wl_array *states)
{
    struct WGWindow *window = data;
    DEBUG_WL("requested window size %dx%d, scale factor %d\n", width, height, wl_backend->output_scale_factor);

    if (width == 0 || height == 0) {
        width = round_up(window->width_hint, wl_backend->output_scale_factor);
        height = round_up(window->height_hint, wl_backend->output_scale_factor);
        DEBUG_WL("using sizehints instead: %dx%d\n", width, height);
    } else {
        width *= wl_backend->output_scale_factor;
        height *= wl_backend->output_scale_factor;
    }

    window->backend->wg.resize(window, width, height);
}

void
toplevel_close_handler(void *data, struct xdg_toplevel *xdg_toplevel)
{
    DEBUG_WL("close request\n");
    struct WGWindow *window = data;
    window->backend->wg.close(window);
}

static const struct xdg_toplevel_listener toplevel_listener = {
    .configure = toplevel_configure_handler,
    .close = toplevel_close_handler,
};

static void
wl_buffer_release(void *data, struct wl_buffer *wl_buffer)
{
    DEBUG_WL("\n");
    /* Sent by the compositor when it's no longer using this buffer */
    wl_buffer_destroy(wl_buffer);
}

static const struct wl_buffer_listener wl_buffer_listener = {
    .release = wl_buffer_release,
};

static struct wl_buffer *
draw_frame(struct WGWindow *win)
{
    DEBUG_WL("\n");
    int width = WG_WIDGET(win)->width, height = WG_WIDGET(win)->height;
    struct WGRenderFrame frame = {width, height, NULL};

    int stride = width * 4;
    int size = stride * height;

    // TODO do we need to reallocate always, or we can reuse?
    int fd = allocate_shm_file(size);
    if (fd == -1) {
        return NULL;
    }

    frame.data = mmap(NULL, size,
            PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (frame.data == MAP_FAILED) {
        close(fd);
        return NULL;
    }

    // TODO: probably we could store the pool and just create buffers?
    struct wl_shm_pool *pool = wl_shm_create_pool(wl_backend->wl_shm, fd, size);
    struct wl_buffer *buffer = wl_shm_pool_create_buffer(pool, 0,
            width, height, stride, WL_SHM_FORMAT_XRGB8888);
    wl_shm_pool_destroy(pool);
    close(fd);

    WG_WIDGET(win)->render(WG_WIDGET(win), &frame);

    munmap(frame.data, size);
    wl_buffer_add_listener(buffer, &wl_buffer_listener, NULL);
    return buffer;
}

static void
xdg_surface_configure(void *data,
        struct xdg_surface *xdg_surface, uint32_t serial)
{
    DEBUG_WL("xdg_surface_configure\n");
    xdg_surface_ack_configure(xdg_surface, serial);

    struct WGWindow *win = data;
    struct WGWaylandWindowBackend *winbackend = WAYLAND_WINBACKEND(win->backend);
    winbackend->configured = 1;

    struct wl_buffer *buffer = draw_frame(win);
    wl_surface_attach(winbackend->wl_surface, buffer, 0, 0);
    wl_surface_commit(winbackend->wl_surface);
}

static const struct xdg_surface_listener xdg_surface_listener = {
    .configure = xdg_surface_configure,
};

static void
xdg_wm_base_ping(void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial)
{
    DEBUG_WL("\n");
    xdg_wm_base_pong(xdg_wm_base, serial);
}

static const struct xdg_wm_base_listener xdg_wm_base_listener = {
    .ping = xdg_wm_base_ping,
};

static void
wl_pointer_enter(void *data, struct wl_pointer *wl_pointer,
                 uint32_t serial, struct wl_surface *surface,
                 wl_fixed_t surface_x, wl_fixed_t surface_y)
{
    struct WGWaylandInput *input_state = data;
    input_state->pointer_event.event_mask |= POINTER_EVENT_ENTER;
    input_state->pointer_event.serial = serial;
    input_state->pointer_event.surface_x = surface_x,
    input_state->pointer_event.surface_y = surface_y;
}

static void
wl_pointer_leave(void *data, struct wl_pointer *wl_pointer,
                 uint32_t serial, struct wl_surface *surface)
{
    struct WGWaylandInput *client_state = data;
    client_state->pointer_event.serial = serial;
    client_state->pointer_event.event_mask |= POINTER_EVENT_LEAVE;
}

static void
wl_pointer_motion(void *data, struct wl_pointer *wl_pointer, uint32_t time,
                  wl_fixed_t surface_x, wl_fixed_t surface_y)
{
    struct WGWaylandInput *client_state = data;
    client_state->pointer_event.event_mask |= POINTER_EVENT_MOTION;
    client_state->pointer_event.time = time;
    client_state->pointer_event.surface_x = surface_x,
    client_state->pointer_event.surface_y = surface_y;
}

static void
wl_pointer_button(void *data, struct wl_pointer *wl_pointer, uint32_t serial,
                  uint32_t time, uint32_t button, uint32_t state)
{
    struct WGWaylandInput *client_state = data;
    client_state->pointer_event.event_mask |= POINTER_EVENT_BUTTON;
    client_state->pointer_event.time = time;
    client_state->pointer_event.serial = serial;
    client_state->pointer_event.button = button,
    client_state->pointer_event.state = state;
}

static void
wl_pointer_axis(void *data, struct wl_pointer *wl_pointer, uint32_t time,
               uint32_t axis, wl_fixed_t value)
{
    struct WGWaylandInput *client_state = data;
    client_state->pointer_event.event_mask |= POINTER_EVENT_AXIS;
    client_state->pointer_event.time = time;
    client_state->pointer_event.axes[axis].valid = true;
    client_state->pointer_event.axes[axis].value = value;
}

static void
wl_pointer_axis_source(void *data, struct wl_pointer *wl_pointer,
               uint32_t axis_source)
{
    struct WGWaylandInput *client_state = data;
    client_state->pointer_event.event_mask |= POINTER_EVENT_AXIS_SOURCE;
    client_state->pointer_event.axis_source = axis_source;
}

static void
wl_pointer_axis_stop(void *data, struct wl_pointer *wl_pointer,
               uint32_t time, uint32_t axis)
{
    struct WGWaylandInput *client_state = data;
    client_state->pointer_event.time = time;
    client_state->pointer_event.event_mask |= POINTER_EVENT_AXIS_STOP;
    client_state->pointer_event.axes[axis].valid = true;
}

static void
wl_pointer_axis_discrete(void *data, struct wl_pointer *wl_pointer,
               uint32_t axis, int32_t discrete)
{
    struct WGWaylandInput *client_state = data;
    client_state->pointer_event.event_mask |= POINTER_EVENT_AXIS_DISCRETE;
    client_state->pointer_event.axes[axis].valid = true;
    client_state->pointer_event.axes[axis].discrete = discrete;
}


static void
wl_pointer_frame(void *data, struct wl_pointer *wl_pointer)
{
    const int event_id = 0;  // mouse does not support multi touch, so we use a fix event id
    struct WGWaylandInput *client_state = data;
    struct WGpointer_event *event = &client_state->pointer_event;
    struct WGWindow *win = wg_app->window;  // TODO propagate here the winbackend for multi window support

    #define debug_mouse(...) do if (event->event_mask & DEBUG_EV_MOUSE_MASK) { DEBUG_EV_MOUSE(__VA_ARGS__); } while(0)

    debug_mouse("pointer frame @ %d: ", event->time);

    if (event->event_mask & POINTER_EVENT_ENTER) {
        debug_mouse("entered %f, %f ",
                wl_fixed_to_double(event->surface_x),
                wl_fixed_to_double(event->surface_y));
    }

    if (event->event_mask & POINTER_EVENT_LEAVE) {
        debug_mouse("leave ");
    }

    if (event->event_mask & POINTER_EVENT_MOTION) {
        debug_mouse("motion %f, %f ",
            wl_fixed_to_double(event->surface_x),
            wl_fixed_to_double(event->surface_y));
    }

    if (event->event_mask & POINTER_EVENT_BUTTON) {
        char *state = event->state == WL_POINTER_BUTTON_STATE_RELEASED ?
            "released" : "pressed";
        (void) state;
        debug_mouse("button %d %s ", event->button, state);
    }

    uint32_t axis_events = POINTER_EVENT_AXIS
        | POINTER_EVENT_AXIS_SOURCE
        | POINTER_EVENT_AXIS_STOP
        | POINTER_EVENT_AXIS_DISCRETE;
    char *axis_name[2] = {
        [WL_POINTER_AXIS_VERTICAL_SCROLL] = "vertical",
        [WL_POINTER_AXIS_HORIZONTAL_SCROLL] = "horizontal",
    };
    char *axis_source[4] = {
        [WL_POINTER_AXIS_SOURCE_WHEEL] = "wheel",
        [WL_POINTER_AXIS_SOURCE_FINGER] = "finger",
        [WL_POINTER_AXIS_SOURCE_CONTINUOUS] = "continuous",
        [WL_POINTER_AXIS_SOURCE_WHEEL_TILT] = "wheel tilt",
    };
    if (event->event_mask & axis_events) {
        for (size_t i = 0; i < 2; ++i) {
            if (!event->axes[i].valid) {
                continue;
            }

            debug_mouse("%s axis ", axis_name[i]);
            (void) axis_name;

            if (event->event_mask & POINTER_EVENT_AXIS) {
                debug_mouse("value %f ", wl_fixed_to_double(
                        event->axes[i].value));
            }
            if (event->event_mask & POINTER_EVENT_AXIS_DISCRETE) {
                debug_mouse("discrete %d ",
                        event->axes[i].discrete);
            }

            if (event->event_mask & POINTER_EVENT_AXIS_SOURCE) {
                debug_mouse("via %s ",
                        axis_source[event->axis_source]);
                (void) axis_source;
            }

            if (event->event_mask & POINTER_EVENT_AXIS_STOP) {
                debug_mouse("(stopped) ");
            }
        }
    }

    debug_mouse("\n");

    int x = wl_fixed_to_double(event->surface_x) * wl_backend->output_scale_factor;
    int y = wl_fixed_to_double(event->surface_y) * wl_backend->output_scale_factor;
    if (event->event_mask & POINTER_EVENT_MOTION) {
        win->backend->wg.mouse_event(win, WG_MouseMove, event_id, x, y);
    }

    if (event->event_mask & POINTER_EVENT_BUTTON) {
        if (event->state == WL_POINTER_BUTTON_STATE_PRESSED) {
            win->backend->wg.mouse_event(win, WG_MousePress, event_id, x, y);
        }

        if (event->state == WL_POINTER_BUTTON_STATE_RELEASED) {
            win->backend->wg.mouse_event(win, WG_MouseRelease, event_id, x, y);
        }
    }

    event->event_mask = 0;
}

static const struct wl_pointer_listener wl_pointer_listener = {
    .enter = wl_pointer_enter,
    .leave = wl_pointer_leave,
    .motion = wl_pointer_motion,
    .button = wl_pointer_button,
    .axis = wl_pointer_axis,
    .frame = wl_pointer_frame,
    .axis_source = wl_pointer_axis_source,
    .axis_stop = wl_pointer_axis_stop,
    .axis_discrete = wl_pointer_axis_discrete,
};

static void
wl_touch_down(void *data, struct wl_touch *wl_touch, uint32_t serial, uint32_t time,
              struct wl_surface *surface, int32_t id, wl_fixed_t x, wl_fixed_t y)
{
    DEBUG_WL("\n");
    struct WGWaylandInput *client_state = data;
    struct WGtouch_event *event = &client_state->touch_event;
    event->event_mask |= TOUCH_EVENT_DOWN;
    event->surface_x = x;
    event->surface_y = y;
    event->time = time;
    event->event_id = id;
}

static void
wl_touch_up(void *data, struct wl_touch *wl_touch, uint32_t serial, uint32_t time, int32_t id)
{
    DEBUG_WL("\n");
    struct WGWaylandInput *client_state = data;
    struct WGtouch_event *event = &client_state->touch_event;
    event->event_mask |= TOUCH_EVENT_UP;
    event->time = time;
    event->event_id = id;
}

static void
wl_touch_motion(void *data, struct wl_touch *wl_touch, uint32_t time, int32_t id,
		       wl_fixed_t x, wl_fixed_t y)
{
    DEBUG_WL("\n");
    struct WGWaylandInput *client_state = data;
    struct WGtouch_event *event = &client_state->touch_event;

    event->event_mask |= TOUCH_EVENT_MOTION;
    event->time = time;
    event->surface_x = x;
    event->surface_y = y;
    event->event_id = id;
}

static void
wl_touch_frame(void *data, struct wl_touch *wl_touch)
{
    DEBUG_WL("\n");
    struct WGWaylandInput *client_state = data;
    struct WGtouch_event *event = &client_state->touch_event;
    struct WGWindow *win = wg_app->window;  // TODO multi window support

//    #define debug_touch(...) if (event->event_mask & DEBUG_EV_TOUCH_MASK) DEBUG_EV_MOUSE(__VA_ARGS__)

//    debug_touch("touch frame @ %d: ", event->time);

    int x = wl_fixed_to_double(event->surface_x) * wl_backend->output_scale_factor;
    int y = wl_fixed_to_double(event->surface_y) * wl_backend->output_scale_factor;
    if (event->event_mask & TOUCH_EVENT_MOTION) {
        win->backend->wg.mouse_event(win, WG_MouseMove, event->event_id, x, y);
    }

    if (event->event_mask & TOUCH_EVENT_DOWN) {
        win->backend->wg.mouse_event(win, WG_MousePress, event->event_id, x, y);
    }

    if (event->event_mask & TOUCH_EVENT_UP) {
        win->backend->wg.mouse_event(win, WG_MouseRelease, event->event_id, x, y);
    }

    event->event_mask = 0;
}

static void
wl_touch_cancel(void *data, struct wl_touch *wl_touch)
{
    DEBUG_WL("\n");
    struct WGWaylandInput *client_state = data;
    struct WGtouch_event *event = &client_state->touch_event;
    event->event_mask = 0;
}

static void
wl_touch_shape(void *data, struct wl_touch *wl_touch, int32_t id, wl_fixed_t major, wl_fixed_t minor)
{ }

static void
wl_touch_orientation(void *data, struct wl_touch *wl_touch, int32_t id, wl_fixed_t orientation)
{ }

static const struct wl_touch_listener wl_touch_listener = {
    .down = wl_touch_down,
    .up = wl_touch_up,
    .motion = wl_touch_motion,
    .frame = wl_touch_frame,
    .cancel = wl_touch_cancel,
    .shape = wl_touch_shape,
    .orientation = wl_touch_orientation
};


static void
wl_keyboard_keymap(void *data, struct wl_keyboard *wl_keyboard,
               uint32_t format, int32_t fd, uint32_t size)
{
    struct WGWaylandInput *client_state = data;

    if (format != WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1) {
        wg_log_error("Unsupported keyboard keymap format\n");
        return;
    }

    char *map_shm = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
    if (map_shm == MAP_FAILED)
    {
        wg_log_error("Error mmap failed: %s(%d)\n", strerror(errno), errno);
        return;
    }

    struct xkb_keymap *xkb_keymap = xkb_keymap_new_from_string(
        client_state->xkb_context, map_shm,
        XKB_KEYMAP_FORMAT_TEXT_V1, XKB_KEYMAP_COMPILE_NO_FLAGS);
    munmap(map_shm, size);
    close(fd);

    struct xkb_state *xkb_state = xkb_state_new(xkb_keymap);
    if (xkb_state == NULL) {
        wg_log_error("Failed to allocate xkb_state\n");
        return;
    }

    xkb_keymap_unref(client_state->xkb_keymap);
    xkb_state_unref(client_state->xkb_state);
    client_state->xkb_keymap = xkb_keymap;
    client_state->xkb_state = xkb_state;
}

static void
wl_keyboard_enter(void *data, struct wl_keyboard *wl_keyboard,
                  uint32_t serial, struct wl_surface *surface,
                  struct wl_array *keys)
{
}

static void
wl_notify_keycode(struct WGWaylandInput *client_state, uint32_t keycode)
{
    char buf[128];
    xkb_keysym_t sym = xkb_state_key_get_one_sym(client_state->xkb_state, keycode);

    xkb_keysym_get_name(sym, buf, sizeof(buf));
    DEBUG_EV_KEY("key %s: sym: %-12s (%d), ",
        (state == WL_KEYBOARD_KEY_STATE_PRESSED) ? "press" : "release", buf, sym);

    xkb_state_key_get_utf8(client_state->xkb_state, keycode, buf, sizeof(buf));
    DEBUG_EV_KEY("utf8: '%s'\n", buf);

    if (wl_backend->base.wg.key_pressed != NULL)
    {
        wl_backend->base.wg.key_pressed(buf, sym);
    }
}

static void
wl_keyrepeat_check(void *data)
{
    struct WGWaylandInput *client_state = data;
    if (client_state->pressed_keycode == 0)
        return;  // Stop repeating

    wl_notify_keycode(client_state, client_state->pressed_keycode);
    wl_backend->base.wg.add_event(client_state->keyrepeat_delay1, -1, wl_keyrepeat_check, data);
}

static void
wl_keyboard_key(void *data, struct wl_keyboard *wl_keyboard,
               uint32_t serial, uint32_t time, uint32_t key, uint32_t state)
{
    struct WGWaylandInput *client_state = data;

    wl_backend->base.wg.del_event(wl_keyrepeat_check);

    if (client_state->xkb_state == NULL)
        return;

    if (state == WL_KEYBOARD_KEY_STATE_PRESSED) {
        client_state->pressed_keycode = key + 8;
        wl_notify_keycode(client_state, client_state->pressed_keycode);
        wl_backend->base.wg.add_event(client_state->keyrepeat_delay0, -1, wl_keyrepeat_check, data);
    } else {
        client_state->pressed_keycode = 0;
    }
}

static void
wl_keyboard_leave(void *data, struct wl_keyboard *wl_keyboard,
               uint32_t serial, struct wl_surface *surface)
{
    DEBUG_EV_KEY("keyboard leave\n");
}

static void
wl_keyboard_modifiers(void *data, struct wl_keyboard *wl_keyboard,
               uint32_t serial, uint32_t mods_depressed,
               uint32_t mods_latched, uint32_t mods_locked,
               uint32_t group)
{
    DEBUG_EV_KEY("keyboard modifiers\n");
    struct WGWaylandInput *client_state = data;
    if (client_state->xkb_state)
        xkb_state_update_mask(client_state->xkb_state,
            mods_depressed, mods_latched, mods_locked, 0, 0, group);
}

static void
wl_keyboard_repeat_info(void *data, struct wl_keyboard *wl_keyboard,
               int32_t rate, int32_t delay)
{
    DEBUG_WL("rate: %d, delay: %d\n", rate, delay);
    struct WGWaylandInput *state = data;
    state->keyrepeat_delay0 = delay;
    state->keyrepeat_delay1 = 1000.0 / rate;
}

static const struct wl_keyboard_listener wl_keyboard_listener = {
    .keymap = wl_keyboard_keymap,
    .enter = wl_keyboard_enter,
    .leave = wl_keyboard_leave,
    .key = wl_keyboard_key,
    .modifiers = wl_keyboard_modifiers,
    .repeat_info = wl_keyboard_repeat_info,
};

static void
wl_seat_capabilities(void *data, struct wl_seat *wl_seat, uint32_t capabilities)
{
    struct WGWaylandInput *state = data;

    bool have_pointer = capabilities & WL_SEAT_CAPABILITY_POINTER;

    DEBUG_WL("seat capabilities - have pointer? %d %p\n", have_pointer, state->wl_pointer);
    if (have_pointer && state->wl_pointer == NULL) {
        state->wl_pointer = wl_seat_get_pointer(state->wl_seat);
        wl_pointer_add_listener(state->wl_pointer, &wl_pointer_listener, state);
    } else if (!have_pointer && state->wl_pointer != NULL) {
        wl_pointer_release(state->wl_pointer);
        state->wl_pointer = NULL;
    }

    bool have_touch = capabilities & WL_SEAT_CAPABILITY_TOUCH;
    DEBUG_WL("seat capabilities - have touch? %d %p\n", have_touch, state->wl_touch);
    if (have_touch && state->wl_touch == NULL) {
        state->wl_touch = wl_seat_get_touch(state->wl_seat);
        wl_touch_add_listener(state->wl_touch, &wl_touch_listener, state);
    } else if (!have_touch && state->wl_touch) {
        wl_touch_release(state->wl_touch);
        state->wl_touch = NULL;
    }

    bool have_keyboard = capabilities & WL_SEAT_CAPABILITY_KEYBOARD;

    if (have_keyboard && state->wl_keyboard == NULL) {
        state->wl_keyboard = wl_seat_get_keyboard(state->wl_seat);
        wl_keyboard_add_listener(state->wl_keyboard,
                                 &wl_keyboard_listener, state);
    } else if (!have_keyboard && state->wl_keyboard != NULL) {
        wl_keyboard_release(state->wl_keyboard);
        state->wl_keyboard = NULL;
    }
}

static void
wl_seat_name(void *data, struct wl_seat *wl_seat, const char *name)
{
    DEBUG_WL("seat name: %s\n", name);
}

static const struct wl_seat_listener wl_seat_listener = {
    .capabilities = wl_seat_capabilities,
    .name = wl_seat_name,
};

static void
wl_output_geometry(void *data, struct wl_output *wl_output, int32_t x, int32_t y,
            int32_t physical_width, int32_t physical_height, int32_t subpixel, const char *make,
            const char *model, int32_t transform)
{
    DEBUG_WL("physical_geometry=(%d,%d %dx%d) transform=%d\n", x, y, physical_width, physical_height, transform);
    struct WGApplicationBackend *backend = WG_APPBACKEND(data);

    enum WGOrientation orientation = physical_width < physical_height ? WG_Portrait : WG_Landscape;
    if (transform % 2 == 1) {  // rotated
        orientation = orientation == WG_Portrait ? WG_Landscape : WG_Portrait;
    }
    backend->wg.orientation_changed(orientation);
}

static void
wl_output_mode(void *data, struct wl_output *wl_output, uint32_t flags,
               int32_t width, int32_t height, int32_t refresh)
{ }

static void
wl_output_done(void *data, struct wl_output *wl_output)
{ }

static inline struct WGWaylandBackendSettings *
wl_settings(void) {
    return &wg_settings()->backend.wayland_config;
}


static void
wl_output_scale(void *data, struct wl_output *wl_output, int32_t factor)
{
    DEBUG_WL("Wayland output scale factor: %d, widgie scale: %.2f\n", (int)factor, wl_settings()->scale);
    struct WGWaylandApplicationBackend *backend = data;
    wl_settings()->scale /= backend->output_scale_factor;
    backend->output_scale_factor = factor;   // TODO two outputs? or if the scale factor changes at runtime?
    wl_settings()->scale *= factor;
}

static const struct wl_output_listener wl_output_listener = {
    .geometry = wl_output_geometry,
    .mode = wl_output_mode,
    .done = wl_output_done,
    .scale = wl_output_scale,
};

static void
registry_global(void *data, struct wl_registry *wl_registry,
        uint32_t name, const char *interface, uint32_t version)
{
    DEBUG_WL("interface: %s, version: %u\n", interface, version);
    // TODO: have a minimum and max supported version and request the one based on version argument
    struct WGWaylandApplicationBackend *backend = data;
    if (strcmp(interface, wl_shm_interface.name) == 0) {
        backend->wl_shm = wl_registry_bind(
                wl_registry, name, &wl_shm_interface, 1);
    } else if (strcmp(interface, wl_output_interface.name) == 0) {
        backend->wl_output = wl_registry_bind(
                wl_registry, name, &wl_output_interface, 2);
        wl_output_add_listener(backend->wl_output, &wl_output_listener, backend);
    } else if (strcmp(interface, wl_compositor_interface.name) == 0) {
        backend->wl_compositor = wl_registry_bind(
                wl_registry, name, &wl_compositor_interface, 4);
    } else if (strcmp(interface, xdg_wm_base_interface.name) == 0) {
        backend->xdg_wm_base = wl_registry_bind(
                wl_registry, name, &xdg_wm_base_interface, 1);
        xdg_wm_base_add_listener(backend->xdg_wm_base,
                &xdg_wm_base_listener, backend);
    } else if (strcmp(interface, wl_seat_interface.name) == 0) {
        backend->input.wl_seat = wl_registry_bind(
            wl_registry, name, &wl_seat_interface, 7);
        wl_seat_add_listener(backend->input.wl_seat, &wl_seat_listener, &backend->input);
    }
}

// TODO add debug to every callback

static void
registry_global_remove(void *data,
        struct wl_registry *wl_registry, uint32_t name)
{
    DEBUG_WL("name: %"PRIu32"\n", name);
    struct WGWaylandApplicationBackend *backend = data;
    (void) backend;
}

static const struct wl_registry_listener wl_registry_listener = {
    .global = registry_global,
    .global_remove = registry_global_remove,
};

void
wayland_window_draw(struct WGWindowBackend *self)
{
    struct WGWaylandWindowBackend *wbackend = WAYLAND_WINBACKEND(self);
    DEBUG_WL("Window: %p\n", self->wg.window);

    if (!wbackend->configured) {
        DEBUG_WL("Not yet ready for drawing: wl_surface=%p xdg_surface=%p xdg_toplevel=%p\n",
                 wbackend->wl_surface, wbackend->xdg_surface, wbackend->xdg_toplevel);
        return;
    }
    wl_surface_damage_buffer(wbackend->wl_surface, 0, 0, INT32_MAX, INT32_MAX);
    struct wl_buffer *buffer = draw_frame(self->wg.window);
    wl_surface_attach(wbackend->wl_surface, buffer, 0, 0);
    wl_surface_commit(wbackend->wl_surface);
}

int
wayland_application_dispatch(struct WGApplicationBackend *self, int timeout_ms)
{
    DEBUG_WL("timeout: %d\n", timeout_ms);
    struct WGWaylandApplicationBackend *wl_self = WAYLAND_APPBACKEND(self);

    struct wl_display *display = wl_self->wl_display;
    while (wl_display_prepare_read(display) != 0)
        wl_display_dispatch_pending(display);
    wl_display_flush(display);

    int rc = self->wg.wait_fd(wl_display_get_fd(display), timeout_ms);
    if (rc) {
        DEBUG_WL("wl_display_dispatch\n");
        wl_display_read_events(display);
    } else {
        wl_display_cancel_read(display);
    }

    rc = wl_display_dispatch_pending(display);

    DEBUG_WL("result: %d\n", rc);
    return rc;
}

void
wayland_application_free(struct WGApplicationBackend *self)
{
    DEBUG_WL("\n");
    struct WGWaylandApplicationBackend *wl_self = WAYLAND_APPBACKEND(self);

    self->wg.del_event(wl_keyrepeat_check);

    // TODO: most of these are probably to be removed at registry_global_remove?
    if (wl_self->input.wl_touch) {
        wl_touch_destroy(wl_self->input.wl_touch);
        wl_self->input.wl_touch = NULL;
    }

    if (wl_self->input.wl_keyboard) {
        wl_keyboard_destroy(wl_self->input.wl_keyboard);
        wl_self->input.wl_keyboard = NULL;
    }

    if (wl_self->input.wl_pointer) {
        wl_pointer_destroy(wl_self->input.wl_pointer);
        wl_self->input.wl_pointer = NULL;
    }

    if (wl_self->input.wl_seat) {
        wl_seat_destroy(wl_self->input.wl_seat);
        wl_self->input.wl_seat = NULL;
    }

    if (wl_self->wl_registry) {
        wl_registry_destroy(wl_self->wl_registry);
        wl_self->wl_registry = NULL;
    }

    if (wl_self->xdg_wm_base) {
        xdg_wm_base_destroy(wl_self->xdg_wm_base);
        wl_self->xdg_wm_base = NULL;
    }

    // TODO there is certainly still work to do here
    if (wl_self->wl_output)
        wl_output_destroy(wl_self->wl_output);
    wl_self->wl_output = NULL;

    if (wl_self->wl_shm)
        wl_shm_destroy(wl_self->wl_shm);
    wl_self->wl_shm = NULL;

    if (wl_self->wl_compositor)
        wl_compositor_destroy(wl_self->wl_compositor);
    wl_self->wl_compositor = NULL;

    if (wl_self->wl_display)
        wl_display_disconnect(wl_self->wl_display);
    wl_self->wl_display = NULL;

    if (wl_self->input.xkb_keymap)
        xkb_keymap_unref(wl_self->input.xkb_keymap);
    wl_self->input.xkb_keymap = NULL;

    if (wl_self->input.xkb_state)
        xkb_state_unref(wl_self->input.xkb_state);
    wl_self->input.xkb_state = NULL;

    if (wl_self->input.xkb_context)
        xkb_context_unref(wl_self->input.xkb_context);
    wl_self->input.xkb_context = NULL;

    free(self);
}

struct WGApplicationBackend *
wayland_application_new(struct WGApplicationFuncs wgfuncs)
{
    DEBUG_WL("\n");
    struct WGWaylandApplicationBackend *backend = WAYLAND_APPBACKEND(calloc(1, sizeof(struct WGWaylandApplicationBackend)));
    if (backend == NULL) {
        wg_log_error("Failed to allocate memory.\n");
        goto error;
    }
    backend->base.wg = wgfuncs;
    backend->base.window_new = wayland_window_new;
    backend->base.free = wayland_application_free;
    backend->base.dispatch = wayland_application_dispatch;
    backend->base.grab_keyboard = NULL;

    backend->output_scale_factor = 1.0;   // in case no scaling info arrives

    backend->wl_display = wl_display_connect(NULL);
    if (backend->wl_display == NULL) {
        wg_log_error("wl_display_connect failed\n");
        goto error;
    }

    backend->wl_registry = wl_display_get_registry(backend->wl_display);
    if (backend->wl_registry == NULL) {
        wg_log_error("wl_display_get_registry failed\n");
        goto error;
    }
    backend->input.xkb_context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
    if (backend->input.xkb_context == NULL) {
        wg_log_error("xkb_context_new failed\n");
        goto error;
    }

    wl_registry_add_listener(backend->wl_registry, &wl_registry_listener, backend);
    wl_display_roundtrip(backend->wl_display); // TODO probably there is a better way
    wl_display_roundtrip(backend->wl_display); // TODO first round creates the registry items, another round inits display size

    if (backend->wl_compositor == NULL) {
        wg_log_error("No compositor has appeared suddenly.\n");
        goto error;
    }

    wl_backend = backend;
    goto finally;

error:
    if (backend != NULL) {
        wayland_application_free(WG_APPBACKEND(backend));
        backend = NULL;
    }

finally:
    return WG_APPBACKEND(backend);
}

static void
wayland_window_deinit_surface(struct WGWaylandWindowBackend *wl_self)
{
    DEBUG_WL("\n");
    if (wl_self->xdg_toplevel) {
        xdg_toplevel_destroy(wl_self->xdg_toplevel);
        wl_self->xdg_toplevel = NULL;
    }

    if (wl_self->xdg_surface) {
        xdg_surface_destroy(wl_self->xdg_surface);
        wl_self->xdg_surface = NULL;
    }

    if (wl_self->wl_surface) {
        wl_surface_destroy(wl_self->wl_surface);
        wl_self->wl_surface = NULL;
    }

    wl_self->configured = 0;
}

static void
wayland_window_free(struct WGWindowBackend *self)
{
    DEBUG_WL("\n");
    wayland_window_deinit_surface(WAYLAND_WINBACKEND(self));
    free(self);
}

static void
wayland_apply_toplevel_configs(struct WGWindow *win, struct WGWaylandWindowBackend *winbackend)
{
    // wayland seems to expect height/width without the scale factor
    int height = win->height_hint / wl_backend->output_scale_factor, width = win->width_hint / wl_backend->output_scale_factor;
    if (win->size_constraint & WG_MinimumSize) {
        DEBUG_WL("Setting min size: %dx%d\n", width, height);
        xdg_toplevel_set_min_size(winbackend->xdg_toplevel, width, height);
    }
    if (win->size_constraint & WG_MaximumSize) {
        DEBUG_WL("Setting max size: %dx%d\n", width, height);
        xdg_toplevel_set_max_size(winbackend->xdg_toplevel, width, height);
    }

    xdg_toplevel_set_title(winbackend->xdg_toplevel, win->title);
    wl_surface_commit(winbackend->wl_surface);
}

static int
wayland_window_init_surface(struct WGWindow *win, struct WGWaylandWindowBackend *winbackend)
{
    DEBUG_WL("win=%p\n", win);
    winbackend->configured = 0;
    if (wl_backend->wl_compositor == NULL) {
        wg_log_error("Unable to init surface: wl_compositor is null\n");
        return 1;
    }
    winbackend->wl_surface = wl_compositor_create_surface(wl_backend->wl_compositor);
    if (winbackend->wl_surface == NULL) {
        wg_log_error("Failed to create wl_surface\n");
        return 1;
    }

    wl_surface_set_buffer_scale(winbackend->wl_surface, wl_backend->output_scale_factor);
    DEBUG_WL("Applied scale factor: %d\n", (int)wl_backend->output_scale_factor);

    if (wl_backend->xdg_wm_base == NULL) {
        wg_log_error("Unable to init surface: xdg_wm_base is null\n");
        return 1;
    }
    winbackend->xdg_surface = xdg_wm_base_get_xdg_surface(wl_backend->xdg_wm_base, winbackend->wl_surface);
    if (winbackend->xdg_surface == NULL) {
        wg_log_error("Failed to create xdg_surface\n");
        return 1;
    }
    xdg_surface_add_listener(winbackend->xdg_surface, &xdg_surface_listener, win);

    winbackend->xdg_toplevel = xdg_surface_get_toplevel(winbackend->xdg_surface);
    if (winbackend->xdg_surface == NULL) {
        wg_log_error("Failed to get xdg_toplevel\n");
        return 1;
    }
    xdg_toplevel_add_listener(winbackend->xdg_toplevel, &toplevel_listener, win);

    wayland_apply_toplevel_configs(win, winbackend);
    return 0;
}

static void
wayland_apply_window_configs(struct WGWindowBackend *self)
{
    DEBUG_WL("\n");

    struct WGWaylandWindowBackend *wbackend = WAYLAND_WINBACKEND(self);
    if (self->wg.violates_size_constraints(self->wg.window)) {
        wayland_window_deinit_surface(wbackend);
        wayland_window_init_surface(self->wg.window, wbackend);
    } else {
        wayland_apply_toplevel_configs(self->wg.window, wbackend);
    }
}

struct WGWindowBackend *
wayland_window_new(struct WGWindowFuncs *wgfuncs)
{
    DEBUG_WL("\n");
    struct WGWaylandWindowBackend *winbackend = WAYLAND_WINBACKEND(calloc(1, sizeof(struct WGWaylandWindowBackend)));
    if (winbackend == NULL) {
        wg_log_error("Failed to allocate memory\n");
        goto error;
    }

    winbackend->base.wg = *wgfuncs;
    winbackend->base.draw = wayland_window_draw;
    winbackend->base.free = wayland_window_free;
    winbackend->base.apply_window_configs = wayland_apply_window_configs;

    if (wayland_window_init_surface(winbackend->base.wg.window, winbackend))
        goto error;

    goto finally;

error:
    if (winbackend != NULL) {
        wayland_window_free(WG_WINBACKEND(winbackend));
        winbackend = NULL;
    }

finally:
    return WG_WINBACKEND(winbackend);
}
