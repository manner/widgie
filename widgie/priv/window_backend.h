// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WINDOW_BACKEND
#define WG_HEADER_WINDOW_BACKEND

#include "../uidefs.h"

struct WGWindow;
struct WGRenderFrame;

// called/used by the backend
struct WGWindowFuncs {
    struct WGWindow *window;
    void (*mouse_event)(struct WGWindow *self, enum WGMouseEvent type, int event_id, int x, int y);
    int (*violates_size_constraints)(struct WGWindow *self);
    void (*resize)(struct WGWindow *self, int width, int height);
    void (*close)(struct WGWindow *self);
};

struct WGWindowBackend {
    struct WGWindowFuncs wg;

    // called by widgie
    void (*draw)(struct WGWindowBackend *self);
    void (*free)(struct WGWindowBackend *self);
    void (*apply_window_configs)(struct WGWindowBackend *self);
};

#define WG_WINBACKEND(x) ((struct WGWindowBackend *)x)

#endif  // HEADER_WINDOW_BACKEND
