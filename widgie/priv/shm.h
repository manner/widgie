// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_SHM
#define WG_HEADER_SHM

#include <stddef.h>

int allocate_shm_file(size_t size);

#endif // SHM