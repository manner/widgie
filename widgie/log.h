// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_LOG
#define WG_HEADER_LOG

#include <stdio.h>

extern int wg_log_level;
extern int wg_log_displ_source;
extern FILE *wg_log_output;

#define WG_LOG_OUTPUT (wg_log_output ? wg_log_output : stderr)

#define WG_DEBUG_DISPLAY_CODE_POSITION(output) \
    if (wg_log_displ_source) { \
        fprintf(output, "%s(%s:%d): ", __FUNCTION__, __FILE__, __LINE__); \
    }

#define WG_DEBUG_SHOW(...) do { \
    WG_DEBUG_DISPLAY_CODE_POSITION(WG_LOG_OUTPUT) \
    fprintf(WG_LOG_OUTPUT, __VA_ARGS__); \
} while (0)

#define WG_DEBUG_SHOW_STDERR(...) do { \
    WG_DEBUG_DISPLAY_CODE_POSITION(stderr) \
    fprintf(stderr, __VA_ARGS__); \
} while (0)

#define WG_DEBUG_HIDE(...)

#define wg_log_error    WG_DEBUG_SHOW

enum WGDebugLevel {
    DEBUG_LEVEL_ERROR,
    DEBUG_LEVEL_WARNING,
    DEBUG_LEVEL_INFO,
    DEBUG_LEVEL_DEBUG,
    DEBUG_LEVEL_TRACE
};

#define wg_log_warning  if (wg_log_level >= DEBUG_LEVEL_WARNING) WG_DEBUG_SHOW
#define wg_log_info     if (wg_log_level >= DEBUG_LEVEL_INFO) WG_DEBUG_SHOW
#define wg_log_debug    if (wg_log_level >= DEBUG_LEVEL_DEBUG) WG_DEBUG_SHOW
#define wg_log_trace    if (wg_log_level >= DEBUG_LEVEL_TRACE) WG_DEBUG_SHOW

void wg_log_set_display_source(int display);
void wg_log_set_level(int level);
void wg_log_to_file(FILE *file);
void wg_log_to_path(const char *path, int append);

static inline void wg_log_to_stderr(void)
{ wg_log_to_file(stderr); }

void wg_log_deinit(void);

#endif // WG_HEADER_LOG
