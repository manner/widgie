// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_FRAME
#define WG_HEADER_FRAME

#include <stdint.h>

struct WGRenderFrame
{
    int width;
    int height;
    uint32_t *data;

    int dx;  // a delta that gets applied automatically by the rendering functions to the coordinates
    int dy;
};

struct WGRenderFrame *wg_render_frame_new(int width, int height, uint32_t *data);
void wg_render_frame_free(struct WGRenderFrame *frame);

void wg_render_frame_clear(struct WGRenderFrame *frame);

void wg_render_frame_copy_rect(struct WGRenderFrame *from_frame, struct WGRenderFrame *to_frame,
                            int from_x, int from_y, int width, int height, int to_x, int to_y);

int wg_render_frame_fix_rect(struct WGRenderFrame *frame, int *px, int *py, int *pwidth, int *pheight);

static inline void wg_render_frame_init_position(struct WGRenderFrame *frame, int *x, int *y)
{
    *x += frame->dx;
    *y += frame->dy;
}

// protected
int wg__render_frame_copy_rect_fix_positions(struct WGRenderFrame *from_frame, struct WGRenderFrame *to_frame,
                                             int *pfrom_x, int *pfrom_y, int *pwidth, int *pheight, int *pto_x, int *pto_y);

#endif // FB_HEADER_FRAME
