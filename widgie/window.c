// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "window.h"
#include "application.h"
#include "contentlayout.h"
#include "layout.h"
#include "settings.h"
#include "priv/application_backend.h"
#include "priv/window_backend.h"

#include "log.h"

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

const int max_mouse_events = 4;

#if DEBUG_WIDGIE_WINDOW
#define win_debug wg_log_debug
#define win_trace wg_log_trace
#else
#define win_debug WG_DEBUG_HIDE
#define win_trace WG_DEBUG_HIDE
#endif

struct WGMouseEventState
{
    int id;
    struct WGWidget *pressed_widget;
    int last_press_x;
    int last_press_y;
    int is_click;
    int oldness;
    int64_t press_time;
};

static struct WGMouseEventState *
_mouse_event_state_by_id(struct WGWindow *self, int event_id)
{
    struct WGMouseEventState *state = self->mouse_events;
    for (int i = 0; i < max_mouse_events; ++i, ++state)
    {
        if (state->id == event_id)
            return state;
    }
    return NULL;
}

static struct WGMouseEventState *
_mouse_event_state_register(struct WGWindow *self, int event_id)
{
    struct WGMouseEventState *state = self->mouse_events;
    struct WGMouseEventState *oldest_state = self->mouse_events;
    for (int i = 0; i < max_mouse_events; ++i, ++state)
    {
        if (state->oldness < oldest_state->oldness)
            oldest_state = state;

        if (state->id > 0)
            continue; // occupied slot

        state->id = event_id;
        static int oldness = 0;
        state->oldness = ++oldness;
        return state;
    }

    // If we are full, we might not get releases for all press events.
    // So we are trying to free up the oldest one.
    // Hopefully this never happens, because oldness might overflow.
    return oldest_state;
}

void
wg_window_event_mouse_press(struct WGWindow *self, int event_id, int x, int y)
{
    struct WGMouseEventState *event_state = _mouse_event_state_register(self, event_id);
    win_debug("mouse press id: %d at %d x %d\n", event_id, x, y);

    struct WGWidget *content = WG_CONTENT_WIDGET(self);
    if (content == NULL)
        return;

    event_state->is_click = 1;
    event_state->press_time = wg_application_time_ms();

    struct WGWidget *widget = content->widget_at(content, x, y);
    while (widget) {
        int is_handled = widget->on_mouse_press(widget, x, y);
        /* TODO debuggability
          fprintf(stderr, "Widget at: %p is_handled: %d\n", widget, is_handled);
         */
        if (is_handled) {
            event_state->pressed_widget = widget;
            event_state->last_press_x = x;
            event_state->last_press_y = y;
            break;
        }

        widget = widget->parent;
    }
}

void
wg_window_event_mouse_release(struct WGWindow *self, int event_id, int x, int y)
{
    (void) x;  // TODO propagate to click
    (void) y;
    struct WGMouseEventState *event_state = _mouse_event_state_by_id(self, event_id);

    win_debug("mouse release id: %d at %d x %d\n", event_id, x, y);

    if (WG_CONTENT_WIDGET(self) == NULL)
        return;

    if (event_state && event_state->pressed_widget) {
        if (event_state->is_click)
        {
            if (wg_application_time_ms() - event_state->press_time > 500)  // TODO: make it configurable
                event_state->pressed_widget->on_mouse_longclick(event_state->pressed_widget);
            else
                event_state->pressed_widget->on_mouse_click(event_state->pressed_widget);
        }

        event_state->pressed_widget->on_mouse_release(event_state->pressed_widget);

        event_state->id = 0;
        event_state->pressed_widget = NULL;
    }
}

void
wg_window_event_mouse_move(struct WGWindow *self, int event_id, int x, int y)
{
    struct WGMouseEventState *event_state = _mouse_event_state_by_id(self, event_id);
    if (event_state == NULL) {
        wg_log_warning("Ignoring mouse move without press\n");
        return;
    }

    win_trace("mouse move id: %d at %d x %d\n", event_id, x, y);

    // TODO maybe this should depend on dpi
    if (abs(event_state->last_press_x - x) >= 20 ||
        abs(event_state->last_press_y - y) >= 20)
    {
        event_state->is_click = 0;
    }

    if (event_state->pressed_widget) {
        event_state->pressed_widget->on_mouse_move(event_state->pressed_widget, x, y);
    }
}


void
wg_window_set_content(struct WGWindow *self, struct WGWidget *content, int destroy)
{
    wg_contentlayout_set_content(WG_CONTENT_LAYOUT(self), content, destroy);
}

void
wg__window_request_rerender(struct WGWidget *self, struct WGWidget *originator)
{
    (void) originator;
    WG_WINDOW(self)->rerender_requested = 1;
}

void
wg__window_init(struct WGWindow *win, const char *title,
             enum WGSizeConstraint constraint, int width, int height)
{
    wg__contentlayout_init(&win->base);
    WG_WIDGET(win)->free = wg__window_free;
    WG_WIDGET(win)->request_rerender = wg__window_request_rerender;

    win->title = strdup(title);
    win->mouse_events = (struct WGMouseEventState *)calloc(max_mouse_events, sizeof(struct WGMouseEventState));
    win->backend = NULL;
    win->rerender_requested = 0;
    win->close = NULL;
    win->focused_widget = NULL;
    win->size_constraint = constraint;
    wg_window_set_size_hint(win, width, height);
}

void
wg__window_init_backend(struct WGWindow *self)
{
    struct WGWindowFuncs wgfuncs = {
        .window = self,
        .mouse_event = wg_window_event_mouse,
        .violates_size_constraints = wg__window_violates_constraints,
        .resize = (void (*)(struct WGWindow *self, int width, int height))wg_widget_resize,
        .close = wg_window_close
    };
    self->backend = wg_app->backend->window_new(&wgfuncs);
}

struct WGWindow *
wg_window_new(const char *title, enum WGSizeConstraint constraint)
{
    struct WGWindow *win = (struct WGWindow *)malloc(sizeof(struct WGWindow));
    if (win == NULL) {
        wg_log_error("Failed to allocate memory\n");
        return NULL;
    }

    // constraint and window sizes can be overridden through config
    if (wg_settings()->window.constraint != WG_UnspecifiedConstraint)
        constraint = wg_settings()->window.constraint;

    wg__window_init(win, title, constraint, 0, 0);

    return win;
}

void
wg__window_free(struct WGWidget *self)
{
    if (self == NULL)
        return;

    struct WGWindow *win = WG_WINDOW(self);

    if (win->backend) {
        win->backend->free(win->backend);
        win->backend = NULL;
    }

    free(win->mouse_events);
    win->mouse_events = NULL;

    free(win->title);
    win->title = NULL;

    wg__contentlayout_free(self);
}

void
wg_window_free(struct WGWindow *self)
{
    wg_widget_free(WG_WIDGET(self));
}

void
wg_window_dispatch(struct WGWindow *self)
{
    if (self->rerender_requested) {
        self->rerender_requested = 0;
        self->backend->draw(self->backend);
    }
}

void
wg_window_close(struct WGWindow *window)
{
    if (window->close != NULL)
        window->close(window);
}

int
wg_window_set_focus(struct WGWindow *self, struct WGWidget *new_focused_widget)
{
    int rc = 0;
    while (new_focused_widget != NULL &&
           (rc = new_focused_widget->focus_event(new_focused_widget, 1)) == 0)
    {
        new_focused_widget = new_focused_widget->parent;
    }

    if (self->focused_widget != NULL && self->focused_widget != new_focused_widget) {
        self->focused_widget->focus_event(self->focused_widget, 0);
    }

    self->focused_widget = new_focused_widget;
    return rc;
}

void
wg_window_event_keypress(struct WGWindow *self, const char *utf8, uint32_t keysym)
{
    struct WGWidget *widget = self->focused_widget;
    while (widget != NULL &&
           widget->on_keypress(widget, utf8, keysym) == 0)
    {
        widget = widget->parent;
    }
}

int
wg__window_violates_constraints(struct WGWindow *window)
{
    struct WGWidget *widget = WG_WIDGET(window);
    if ((window->size_constraint & WG_MinimumSize) != 0 &&
        (window->height_hint < widget->height || window->width_hint < widget->width)) {
        return 1;
    }

    if ((window->size_constraint & WG_MaximumSize) != 0 &&
        (window->height_hint > widget->height || window->width_hint > widget->width)) {
        return 1;
    }

    return 0;
}

void
wg_window_invalidate_layouts(struct WGWindow *window)
{
    struct WGWidget *content = WG_CONTENT_WIDGET(window);
    if (content == NULL || !wg_widget_is_layout(content))
        return;

    wg_layout_request_relayout_recursive(WG_LAYOUT(content));
}

void
wg_window_set_size_hint(struct WGWindow *window, int width, int height)
{
    // hints can be overridden from config
    window->width_hint = wg_settings()->window.width > 0 ? wg_settings()->window.width : width;
    window->height_hint = wg_settings()->window.height > 0 ? wg_settings()->window.height : height;
    if (window->backend != NULL) {
        window->backend->apply_window_configs(window->backend);
    }
}

void
wg_window_event_mouse(struct WGWindow *self, enum WGMouseEvent type, int event_id, int x, int y)
{
    switch(type) {
        case WG_MousePress:
            wg_window_event_mouse_press(self, event_id, x, y);
            break;
        case WG_MouseRelease:
            wg_window_event_mouse_release(self, event_id, x, y);
            break;
        case WG_MouseMove:
            wg_window_event_mouse_move(self, event_id, x, y);
            break;
        default:
            wg_log_error("Got invalid mouse event type: %d\n", type);
            break;
    }
}
