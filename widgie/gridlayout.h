// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_WIDGIE_GRIDLAYOUT
#define WG_HEADER_WIDGIE_GRIDLAYOUT

#include <widgie/layout.h>

struct WGGridLayoutAlignContext;

struct WGGridLayout {
    struct WGLayout base;

    int columns;  // Note: only change these with grid_layout_reset_children!  (TODO move to private)
    int rows;

    int gap_width;  // gap between the child widgets in pixels
    int gap_height;
};

struct WGWidget *wg_grid_layout_new(int columns, int rows);

// clears the list of children and resizes the grid
// "destroy" specifies if the existing children are also destroyed
void wg_grid_layout_reset_children(struct WGGridLayout *self, int columns, int rows, int destroy);

static inline void wg_grid_layout_add(struct WGGridLayout *self, struct WGWidget *child)
{ wg_layout_add(WG_LAYOUT(self), child); }

// compute how many rows are needed for count of items and columns
static inline int wg_grid_layout_rows_for_columns(int item_count, int columns)
{ return columns > 0 ? (item_count + columns - 1) / columns : 0; }

#define WG_GRID_LAYOUT(x) ((struct WGGridLayout *)x)

// protected
void wg__grid_layout_init(struct WGGridLayout *self, int columns, int rows);
void wg__grid_layout_do_layout(struct WGLayout *self);
static inline void wg__grid_layout_free(struct WGWidget *self)
{ wg__layout_free(self); }

#endif // FB_HEADER_WIDGIE_GRIDLAYOUT
