# Installing dependencies

## Install build dependencies

You'll need these to be able to build widgie and the apps.

### Archlinux / Artixlinux

    pacman -S --needed make gcc pkgconf libxkbcommon freetype2

    # for wayland backend:
    pacman -S wayland-protocols wayland 

    # for x11 backend: (xorgproto is not needed, but is only a workaround for pkg-config cflag bugs)
    pacman -S --needed libxkbcommon-x11 libxcb xcb-util-image xorgproto

    # for framebuffer backend:
    pacman -S --needed --noconfirm libinput

    # for building packages
    pacman -S --needed base-devel git

### Alpine / PostmarketOS

    apk add make gcc libc-dev libxkbcommon-dev freetype-dev pkgconfig fontconfig-dev

    # for wayland backend:
    apk add wayland-protocols wayland-dev 

    # for X11 backend:
    apk add xcb-util-image-dev

    # for framebuffer backend:
    apk add libinput-dev

    # for building packages
    apk add alpine-sdk

### Debian / Devuan

    apt-get install make gcc libc-dev libxkbcommon-dev libfreetype-dev pkg-config libfontconfig-dev fontconfig

    # for wayland support:
    apt-get install wayland-protocols libwayland-dev 

    # for X11 support:
    apt-get install libxcb-xkb-dev libxkbcommon-x11-dev libxcb-image0-dev libxcb-util-dev

    # for framebuffer support:
    apt-get install libinput-dev

    # for building packages
    apt-get install build-essential fakeroot devscripts

## Sailfish OS

    # Note: Widgie does not run on sailfish os yet, because it requires XDG shell protocol
    # extension, which Sailfish's display server does not support yet.

    # Build dependencies needed:
    zypper install freetype-devel wayland-protocols-devel wayland-devel \
        fontconfig-devel libxkbcommon-devel pkgconfig make

## Installing some fonts

Nerd fonts are for application icons. You'll only need them for widgie-menu to display
nice icons.

### Archlinux / Artixlinux

    pacman -S --needed ttf-dejavu ttf-dejavu-nerd
    # or use ttf-nerd-fonts-symbols if you are not using repository "extra"

### Alpine / PostmarketOS

    apk add font-dejavu font-dejavu-sans-mono-nerd

### Debian / Devuan

Unfortunately there does not seem to be a package for nerd fonts, see manual install
steps below

### Manual

If the distribution does not provide package for any nerd fonts, you can install
one manually. Eg. to install dejavu-sans-mono-nerd, do something like this:

    # copy the font to its place
    mkdir -p /usr/share/fonts/dejavu-sans-mono-nerd
    cd /usr/share/fonts/dejavu-sans-mono-nerd
    wget "https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/DejavuSansMono.zip"
    unzip "DejavuSansMono.zip"

    # update the font cache
    fc-cache -fv
