# Widgie-stopper

A simple stopwatch that measures how time passes: minutes, seconds, miliseconds.

![widgie-stopper](doc/widgie-stopper.png)

## Integrate with Simple X Mobile

To add an icon for widgie-stopper, you can eg. create a symlink under userscripts:

    ln -sf "$PWD/build/widgie-stopper" "$HOME/.config/sxmo/userscripts/"
