# widgie-menu

A dmenu/bemenu like menu application designed to be friendly for the finger (touch screens).
Primarily written for Simple X Mobile, but might be useful generally. Its goal is to keep widgie's startup time quick.

![Portrait](doc/widgie-menu-portrait.png)

## Usage

    Usage: widgie-menu [options]
      -p title                Add a title for the window. (The title will be shown before
                              the input field in the title bar.)

      --resizable             Instead of a fix sized "dialog", open as a "normal" resizable window.
                              The button-width config option will not have an effect, and buttons
                              will get as much space as they can according to size of the window.

      --special-menus         Overrides special menus specified in the config file.
      --separator-menus       Overrides separator menus specified in the config file.

    Common widgie options:
      --store-default-config
      --print-default-config
         Stores all configuration defaults to the filesystem (or prints them to standard output)
         to help a quick start of customization.

      [--version | -V]
      [--help | -h]
        Show version or help and exit.

      --config "key=value"
      --config "[section] key=value"
        Override configurations. This has the highest priority, overrides the config even if
        it gets set in a config file.
        Section defaults to 'default', and can be omitted.
        Escaping is supported the same way as in the config file, but beware that the shell
        also processes its own escape sequences first.

      --config-file PATH      Instead of the default config file places, load the configuration
                              from the specified file. Can be specified multiple times, configs will
                              be loaded in the specified order (last override wins).

## Features

Some extra features compared to bemenu:

- big buttons friendlier for the fingers
- scrolling instead of paging (but volume buttons also work)
- some special buttons (eg. close, back) stucks on the title line,
  so they are accessible no matter of scrolling
- separate layout for landscape mode to use the available space more efficiently
- support for adding "button group" separators between buttons: eg. in SxMO's audio
  menu there is a button group for output and input

![Landscape mode](doc/widgie-menu-landscape.png)

![Grouped mode with filtering](doc/widgie-menu-grouped.png)

## Customize

Widgie-menu can be customized through config files under the widgie-menu subdirectory of
 $XDG_CONFIG_HOME, or $HOME/.config if no such variable exists.

What can be customized:

- colors of buttons and areas
- sizes of text and buttons depending on orientation
- special buttons which will snap to the top title line
- input lines that will be considered as separators

To get an idea, you can find an example menu configuration [here](../tests/data/default_widgie_menu.conf) and a widgie configuration [here](../tests/data/default_widgie.conf).

But easiest to get started with customizing is to store the defaults yourself:

    widgie-menu --store-default-config

## Try it

To try widgie-menu out, [compile the project](../README.md#install) and supply some
example inputs:

    # An example run for widgie-menu selecting some applications:
    ./build/widgie-menu -p "Prompt:" <apps/widgie-menu/example_input.txt

    # Another example to demonstrate separator lines:
    ./build/widgie-menu <apps/widgie-menu/example_input_tabbed.txt

You can simulate orientation change by pressing "F2".

## Integrate with Simple X Mobile

Widgie-menu can be used as a drop in replacement for dmenu and bemenu which is used
by Simple X Mobile on X11 and wayland. To replace dmenu / bemenu, install the package
"widgie-menu-compat" for your distribution. See [compile the project](../README.md#install)
on how to build this package.

If everything looks too small, you can set a scaling factor for widgie apps per backend:

    $ cat ~/.config/widgie/widgie.conf

    [widgie]
    backend.wayland_config.scale = 1.0
    backend.x11_config.scale = 2.0

Note that widgie-menu is not yet able to replace bemenu in the terminal (eg. ssh) in itself.
It is however able to replace itself to a specified command if it is not able to init any
of its engines (see config "terminal_app"). The default is "fzf", so if you want to use it
in a terminal, install it!
