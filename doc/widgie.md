# widgie

Widgie aims to be a lightweight, minimal widget library that can be used to
create applications not needing a splash screen.
I have created it because I have got jealous of the speed of dmenu / bemenu,
foot, etc on my pinephone (low end device) and got the idea to create such
speedy apps.

The goal is not to recreate GTK, but to have a lightweight abstraction that
hides the complexities of writing on top of wayland / X11 (or other display
protocols in future) without adding noticable overhead. So the 2-3 widgets a simple
application needs can be written without too much effort. For more complex UIs,
use a more complex widget library.

It is heavily under development, I promise to break API/ABI/behaviour whenever
I have some free time :) So it is *not good enough yet for general use*.

I am by no means expert on the topic, so suggestions, pull requests are welcome.


## Supported display protocols

Currently widgie only supports wayland, X11 and linux framebuffer.
In future I also plan android support, unless it turns out to be very hard.

Compilation of backends are configured with --wayland=value, --x11=value, --framebuffer=value
configure options. Valid values:
 - yes: the backend will be compiled into the widgie library
 - shared: it will be a separate shared object which widgie will load dynamically if needed
 - no: the backend will be disabled (its build dependencies are not needed)

If enabled, widgie will search for shared backends by default in the directory of
the executable and under "/usr/lib" in this order. This can be customized with the
"backend.search_paths" configuration option.

### wayland

I mostly only test on sway, but should be working on other wayland compositors as well.

If compiled as a module, libwidgie-wayland.so will provide the wayland support. The
backend is enabled by default but only tried if WAYLAND_DISPLAY environment
variable is non empty.

### X11

For X11 it requires xkeyboard extension to be present, and admires shm (shared
memory) extension. The latter should automatically turn off when not present, eg.
when X11 runs on a different machine. It can also be disabled if needed.

If compiled as a module, libwidgie-x11.so will provide the X11 support. The
backend is enabled by default but only tried if DISPLAY environment variable is
non empty.

### framebuffer

Linux framebuffer is a lot less mature way to use GUI programs, so depending on
the platform and use case, you might need some configurations.

First important is that the framebuffer backend is disabled by default. This
is because I could not find a good way to detect whether the user would want it, and
I guess it is more common that one does not. To enable use config option

    [widgie] backend.framebuffer = yes

Note that framebuffer device is a plain dumb thing, if multiple applications are
writing to it (eg. the terminal), it might mess up rendering, so you might want
to ensure to disable the output of the application by redirecting to file or /dev/null.

    widgie-stopper --config "[widgie] backend.framebuffer = yes" 2>/dev/null

#### Framebuffer common configurations

Keyboard settings can be altered through "backend.framebuffer_config.xkb_config..."
options. Example to switch to Hungarian keyboard layout:

    backend.framebuffer_config.xkb_config.layout = hu

You can also modify the window placement, alignment, orientation if needed:

    ; The alignment of the window if it is smaller than the framebuffer. Valid values: 'top', 'bottom', 'left', 'right', 'center' and 'free' (default is 'center') [alignment]
    backend.framebuffer_config.window.align = top

    ; The number of rows/columns the gap will contain between the window and the screen border. [number]
    backend.framebuffer_config.window.margin_width = 0
    backend.framebuffer_config.window.margin_height = 200

    ; The rotation count of 90 degrees to apply counterclockwise (default is 0) [number]
    backend.framebuffer_config.window.rotate = 0

If you have a mouse, widgie will emulate a mouse pointer by default. You can disable this with

    backend.framebuffer_config.emulate_mouse = no

Widgie will also try to modify the current tty (hide cursor, disable key echoing etc) by default.
To disable this, or modify which terminal to mess with, use:

    backend.framebuffer_config.tty = /dev/tty

(Leave it empty to disable.)

#### Permissions required for running on the framebuffer

(If you use PostmarketOS, you probably do not need to change anything, as these
seems to be already preconfigured.)

For the display to work, you'll need access to the framebuffer device, it is
commonly at /dev/fb0.

    $ ls -lh /dev/fb0
    crw-rw---- 1 root video /dev/fb0

See its ownerships on your system, you probably have to put your the user which
runs the application into the "video" group.

Input devices (keyboard, mouse, touchscreen) are handled by libinput's udev
backend, therefor you'll need to have access to the input devices themselves:

    $ ls -lh /dev/input/
    total 0
    drwxr-xr-x 2 root root  by-id
    drwxr-xr-x 2 root root  by-path
    crw-rw---- 1 root input event0
    crw-rw---- 1 root input event1
    ...

So you'll probably need to be part of the "input" group.

Additionally, for detecting the devices with udev, you'll need to have udevd
running and access to /run/udev. This is probably only interesting if you
run the application inside a container or a chroot: you'll need to mount this
path.

Docker will also require "--privileged" flag to enable access to raw devices.
