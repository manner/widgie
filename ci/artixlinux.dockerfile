FROM artixlinux/artixlinux:latest

# for build
RUN pacman -Syu --noconfirm --needed make gcc pkgconf libxkbcommon freetype2 fontconfig
# for wayland backend:
RUN pacman -S --needed --noconfirm wayland-protocols wayland
# for x11 backend: (xorgproto is not needed, but only for pkg-config cflag bugs)
RUN pacman -S --needed --noconfirm libxkbcommon-x11 libxcb xcb-util-image xorgproto
# for framebuffer backend:
RUN pacman -S --needed --noconfirm libinput

# fonts
RUN pacman -S --needed --noconfirm ttf-dejavu ttf-nerd-fonts-symbols-mono

# for tests
RUN pacman -S --needed --noconfirm diffutils

# for package build
RUN pacman -S --needed --noconfirm sudo base-devel git
RUN groupadd -g 100 user && useradd -m -u 1000 -g 100 user && \
  echo "user ALL=(ALL) NOPASSWD: ALL" >>/etc/sudoers
USER user
