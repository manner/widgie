#! /bin/bash

set -euo pipefail

case "${1:-}" in
  remove)
    sudo apk del widgie-apps widgie-menu-compat widgie widgie-dev
    ;;
  *)
    sudo apk add build/pkg/*/*.apk
    ;;
esac
