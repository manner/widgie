#! /bin/bash

set -euo pipefail

case "${1:-}" in
  remove)
    sudo pacman -Rc --noconfirm widgie-git
    ;;
  *)
    sudo pacman --needed --noconfirm -U build/pkg/*.pkg.*
    ;;
esac
