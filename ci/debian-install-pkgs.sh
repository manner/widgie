#! /bin/bash

set -euo pipefail

case "${1:-}" in
  remove)
    sudo apt-get -y purge libwidgie
    ;;
  *)
    sudo dpkg -i build/pkg/*.deb
    ;;
esac
