FROM debian:stable

RUN apt-get -y update && apt-get -y dist-upgrade

# build dependencies:
RUN apt-get -y install make gcc libc-dev libxkbcommon-dev libfreetype-dev pkg-config libfontconfig-dev fontconfig

# for wayland:
RUN apt-get -y install wayland-protocols libwayland-dev

# for x11:
RUN apt-get -y install libxcb-xkb-dev libxkbcommon-x11-dev libxcb-image0-dev libxcb-util-dev

# for framebuffer:
RUN apt-get -y install libinput-dev

# Unfortunately there does not seem to be a package for nerd fonts
RUN apt-get -y install unzip curl && \
  curl -fL "https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/DejavuSansMono.zip" >DejavuSansMono.zip && \
  mkdir -p /usr/share/fonts/dejavu-sans-mono-nerd && \
  cd /usr/share/fonts/dejavu-sans-mono-nerd && \
  unzip /DejavuSansMono.zip "DejaVuSansMNerdFontMono-Regular.ttf" && \
  fc-cache -fv && \
  apt-get -y purge unzip curl && \
  apt-get -y autoremove && \
  rm -f /DejavuSansMono.zip

# for package build
RUN apt-get -y install sudo build-essential fakeroot devscripts
RUN useradd -m -u 1000 -g 100 user && \
  echo "user ALL=(ALL) NOPASSWD: ALL" >>/etc/sudoers
USER user

