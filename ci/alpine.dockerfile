FROM alpine:latest

RUN apk update

# build dependencies:
RUN apk add make gcc libc-dev libxkbcommon-dev freetype-dev pkgconfig fontconfig-dev

# for wayland backend:
RUN apk add wayland-protocols wayland-dev 

# for x11 backend:
RUN apk add xcb-util-image-dev

# for framebuffer backend:
RUN apk add libinput-dev

# fonts:
RUN apk add font-dejavu font-dejavu-sans-mono-nerd

# for copyright and other checks
RUN apk add bash

# for package build
RUN apk add alpine-sdk sudo
RUN adduser -D -G abuild -u 1000 user && \
  echo "user ALL=(ALL) NOPASSWD: ALL" >>/etc/sudoers
USER user
RUN mkdir -p ~/.abuild && \
  echo 'PACKAGER_PRIVKEY="/mnt/home/.abuild/rmanni@gmail.com-646e1f90.rsa"' >>/home/user/.abuild/abuild.conf
