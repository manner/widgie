#! /bin/bash

set -euo pipefail

scriptdir="$(dirname "$0")"  # the directory of the script
self="$(basename "$0")"      # eg. alpine-test.sh, archlinux-test.sh
name=${self%-*}              # eg. alpine/archlinux

if [ "${DOCKER:-}" != "y" ]; then
  name=$name exec "$scriptdir"/env.sh "ci/$self"
  exit $?
fi

has_symbol_for() {
  nm build/libwidgie.so | grep "${1}_application_new"
}

check_symbols() {
  backend=$1
  enabled=$2
  case $enabled in
    yes)
      has_so=1
      has_sym=0
      ;;
    no)
      has_so=1
      has_sym=1
      ;;
    shared)
      has_so=0
      has_sym=1
      ;;
    *)
      echo check_symbols: error
      return 1
      ;;
  esac
  result=0
  [ -f build/libwidgie-$backend.so ] || result=$?
  if [ "$result" != "$has_so" ]; then
    echo "File check failed for build/libwidgie-$backend.so: result=$result, expected=$has_so"
    return 1
  fi
  result=0
  has_symbol_for ${backend} || result=$?
  if [ "$result" != "$has_sym" ]; then
      echo "Symbol check failed for $backend: result=$result, expected=$has_sym"
      return 1
  fi
  return 0
}

test_build() {
  make distclean
  if ! ./configure --x11=$1 --framebuffer=$2 --wayland=$3 --enable-devel ||
     ! make ||
     ! check_symbols x11 $1 ||
     ! check_symbols fb $2 ||
     ! check_symbols wayland $3 ||
     ! make check FEATURE_X11=$1 FEATURE_FRAMEBUFFER=$2 FEATURE_WAYLAND=$3
  then
    echo Failed feature set: FEATURE_X11=$1 FEATURE_FRAMEBUFFER=$2 FEATURE_WAYLAND=$3
    return 1
  fi
}

if [ -x ci/$name-install-pkgs.sh ]; then
  make distclean

  # mark our public keys trusted on alpine as package building needs it
  ! [ -d /etc/apk/keys ] || ! stat -t /mnt/home/.abuild/*.pub >/dev/null 2>&1 || sudo cp -a /mnt/home/.abuild/*.pub /etc/apk/keys/

  make package
  ci/$name-install-pkgs.sh
  files=(/usr/lib/libwidgie-x11.so /usr/lib/libwidgie-wayland.so /usr/lib/libwidgie.so \
    /usr/bin/widgie-stopper /usr/bin/widgie-counter /usr/bin/widgie-menu /usr/include/widgie/widget.h \
    /usr/lib/libwidgie.a /usr/bin/dmenu_run)
  for i in "${files[@]}" ; do
    if ! [ -f "$i" ]; then
      echo "Missing "$i" from packages!"
      exit 1
    fi
  done
  widgie-menu --store-default-config
  diff ~/.config/widgie-menu/menu.conf tests/data/default_widgie_menu.conf
  ci/$name-install-pkgs.sh remove
  for i in "${files[@]}" ; do
    if [ -f "$i" ]; then
      echo "Uninstall packages did not remove "$i"!"
      exit 1
    fi
  done
fi

test_build yes shared no
test_build shared no yes
test_build no yes shared
test_build yes yes yes
sudo make install

hash -r
apps/widgie-counter/test.sh 0.2s
widgie-stopper
widgie-menu <apps/widgie-menu/example_input.txt
