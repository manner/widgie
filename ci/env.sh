#! /bin/bash

set -euo pipefail

scriptdir="$(dirname "$0")"  # the directory of the script
self="$(basename "$0")"      # eg. alpine-test.sh, archlinux-test.sh
name=${name:-${self%-*}}     # eg. alpine/archlinux

set -x

REQ_WAYLAND="-e WAYLAND_DISPLAY=${WAYLAND_DISPLAY:-}"
REQ_X11="-v ${HOME}/.Xauthority:/root/.Xauthority -v $XDG_RUNTIME_DIR:$XDG_RUNTIME_DIR -e XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR -e DISPLAY=${DISPLAY:-}"
REQ_FRAMEBUFFER="-v /dev/fb0:/dev/fb0 -v /dev/input:/dev/input -v /run/udev:/run/udev --privileged"

docker build --pull --network=host -f "$scriptdir/${name}.dockerfile" -t widgie-${name} "$scriptdir"
docker run -w /home/user/source -v $PWD:/home/user/source \
  -v "${HOME}:/mnt/home" $REQ_WAYLAND $REQ_X11 $REQ_FRAMEBUFFER \
  -v /tmp:/tmp -e DOCKER=y -it "widgie-${name}" bash "$@"
