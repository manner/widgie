#! /bin/bash

{
  echo "## Usage"
  echo
  build/widgie-menu --help | sed -e 's/^\(.\)/    \1/'
  echo "## Features"
} | sed -i -e '/## Usage/,/## Features/!b' -e '/## Usage/!d;r /dev/stdin' -e 'd' doc/widgie-menu.md

XDG_CONFIG_HOME=tests/data build/widgie-menu --store-default-config
mv tests/data/widgie-menu/menu.conf tests/data/default_widgie_menu.conf
rm -rf tests/data/widgie-menu
