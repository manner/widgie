#! /bin/bash

set -euo pipefail

detection_preamble="// License:"

update=0
if [ "${1:-}" = "update" ]; then
  update=1
fi

check_preamble_present() {
  grep -q "$detection_preamble" "$1"
}

add_preamble() {
  echo >&2 "Adding preamble for $1"
  {
    year=$(date +'%Y')
    name=$(git config user.name)
    mail=$(git config user.email)
    echo "// License: GPL-3.0 or later"
    echo "// (c) $year $name <$mail>"
    echo
    cat "$1"
  } >/tmp/preamble.tmp
  mv /tmp/preamble.tmp "$1"
}

ret=0
find apps ci doc scripts tests widgie -name '*.h' -o -name '*.c' | while true; do
  if ! read path; then
    exit "${ret:-0}"
  fi
  if ! check_preamble_present "$path"; then
    if [ "$update" = 1 ]; then
      add_preamble "$path"
    else
      echo >&2 "Missing preamble: $path"
      ret=1
    fi
  fi
done

exit "$ret"
