[widgie]

; The path where backend plugins will be searched (default is the path of the application and /usr/local/lib) [stringlist]
backend.search_paths = 

; Enable trying to use the wayland backend (default is true) [bool]
backend.wayland = yes

; Scale up/down every size related configuration values when using wayland engine (default is 1.0) [double]
backend.wayland_config.scale = 1.00

; Enable trying to use the x11 backend (default is true) [bool]
backend.x11 = yes

; Enable trying to use the x11-shm extension if available (default is true) [bool]
backend.x11_config.shm = yes

; Scale up/down every size related configuration values when using x11 engine (default is 1.0) [double]
backend.x11_config.scale = 1.80

; Enable trying to use the framebuffer backend (default is no) [bool]
backend.framebuffer = no

; Scale up/down every size related configuration values when using framebuffer engine (default is 1.0) [double]
backend.framebuffer_config.scale = 1.80

; The device representing the framebuffer, usually /dev/fb0 [string]
backend.framebuffer_config.device = /dev/fb0

; The framebuffer backend can alter a tty for example to hide cursor when it runs (default is /dev/tty, set it to empty to avoid doing so) [string]
backend.framebuffer_config.tty = /dev/tty

; Files of rules to be used for keyboard mapping composition, see xkeyboard (default is XKB_DEFAULT_RULES env var) [string]
backend.framebuffer_config.xkb_config.rules = 

; The model of the keyboard, see xkeyboard (default is XKB_DEFAULT_MODEL env var) [string]
backend.framebuffer_config.xkb_config.model = 

; The layout of the keyboard (example: 'us', 'hu', 'de' ... (default is XKB_DEFAULT_LAYOUT env var) [string]
backend.framebuffer_config.xkb_config.layout = 

; Sets the variant of the keyboard like dvorak or colemak (default is XKB_DEFAULT_VARIANT env var) [string]
backend.framebuffer_config.xkb_config.variant = 

; Sets extra xkb configuration options for the keyboard (default is XKB_DEFAULT_OPTIONS env var) [string]
backend.framebuffer_config.xkb_config.options = 

; A delay in milliseconds after which a key long press be considered a repeat (default is 600) [number]
backend.framebuffer_config.xkb_config.repeat_delay = 600

; Sets the rate for the key repeat: how many times the key should be repeated per second (default is 25) [number]
backend.framebuffer_config.xkb_config.repeat_rate = 25

; The alignment of the window if it is smaller than the framebuffer. Valid values: 'top', 'bottom', 'left', 'right', 'center' and 'free' (default is 'center') [alignment]
backend.framebuffer_config.window.align = center

; The number of columns the gap will contain between the window and the screen border. [number]
backend.framebuffer_config.window.margin_width = 0

; The number of rows the gap will contain between the window and the screen border. [number]
backend.framebuffer_config.window.margin_height = 0

; The rotation count of 90 degrees to apply counterclockwise (default is 0) [number]
backend.framebuffer_config.window.rotate = 0

; If yes, widgie will show a pointer arrow in case mouse moves (default is true) [bool]
backend.framebuffer_config.emulate_mouse = yes

; Default background color of buttons [color]
button.color = 0x303030

; Default background color when a button is pressed [color]
button.pressed_color = 0x0077aa

; Default background color when a button is selected [color]
button.selected_color = 0x005577

; Specifies whether button groups select to the first item after the last [bool]
button.group.select_rotates = yes

; A character showing up at the cursor position in inputfields [string]
inputfield.cursor_shape = _

; Default font of labels. Must be fontconfig lookup (multiple fonts can be separated by commas) [string]
text.font = 

; Fallback font of labels used when a character is not present in the default font [string]
text.fallback_font = 

; Optional direct paths where the default font might be found to avoid the fontconfig lookup [stringlist]
text.font_paths = 

; Optional direct paths for the fallback font [stringlist]
text.fallback_font_paths = 

; Default text height of labels [number]
text.height = 12

; Default text height for icons [number]
text.icon_height = 20

; Specifies how width/height affect the size of the window: 'unspecified', 'preferred', 'minimum', 'maximum', 'fixed'. Default is unspecified == controlled by the application. [constraint]
window.constraint = unspecified

; Override the sizehint (height) of the window (default is application specific) [number]
window.width = 0

; Override the sizehint (height) of the window (default is application specific) [number]
window.height = 0

; How much overscrolling to permit relative to the scrollarea size (default is 0.3) [double]
scroll.overscroll = 0.30

debug.show_widget_bounding_rects = no

; 0 = error, 1 = warning, 2 = info, 3 = debug, 4 = trace [number]
debug.log_level = 2

; If true, the source code line where the log happens will be prepended to each log [bool]
debug.log_display_source = yes

; If specified, logging will output into the specified file instead of stderr [string]
debug.log_output = 

