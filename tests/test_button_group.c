// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "widgie/button_group.h"
#include "widgie/button.h"
#include "widgie/label.h"
#include "testcommon.h"

#define BUTTON_LABEL(button) \
    wg_label_get_text(WG_LABEL(wg_button_get_content(WG_BUTTON(button))))
#define SELECTED_LABEL(btngroup) \
    BUTTON_LABEL(group->selected->button)

static struct WGButton *btn1 = NULL;
static struct WGButton *btn2 = NULL;
static struct WGButton *btn3 = NULL;
static struct WGButton *btn4 = NULL;

static const char *clicked_title = NULL;

static int on_click(struct WGWidget *self)
{
    clicked_title = BUTTON_LABEL(self);
    return 1;
}

static int
test_select_next_prev_empty(void)
{
    struct WGButtonGroup *group = wg_button_group_new();

    group->select_rotates = 0;
    wg_button_group_select_next(group);
    VERIFY_PTR_NULL(group->selected);
    wg_button_group_select_prev(group);
    VERIFY_PTR_NULL(group->selected);

    group->select_rotates = 1;
    wg_button_group_select_prev(group);
    VERIFY_PTR_NULL(group->selected);

    wg_button_group_free(group);
    return 0;
}

static int
test_select_next_prev_one_item(void)
{
    struct WGButtonGroup *group = wg_button_group_new();

    wg_button_group_append(group, btn1);
    VERIFY_PTR_NULL(group->selected);

    // for one item, next/prev must always select the one existing regardless of select rotate setting
    for (group->select_rotates = 0; group->select_rotates < 2; ++group->select_rotates)
    {
        group->selected = NULL;
        wg_button_group_select_next(group);
        VERIFY_PTR_NOT_NULL(group->selected);
        VERIFY_STR_EQ(SELECTED_LABEL(group), "1");
        VERIFY_INT_EQ(btn1->is_selected, 1);

        wg_button_group_select_next(group);
        VERIFY_PTR_NOT_NULL(group->selected);
        VERIFY_STR_EQ(SELECTED_LABEL(group), "1");
        VERIFY_INT_EQ(btn1->is_selected, 1);

        group->selected = NULL;
        wg_button_group_select_prev(group);
        VERIFY_PTR_NOT_NULL(group->selected);
        VERIFY_STR_EQ(SELECTED_LABEL(group), "1");

        wg_button_group_select_prev(group);
        VERIFY_PTR_NOT_NULL(group->selected);
        VERIFY_STR_EQ(SELECTED_LABEL(group), "1");
    }

    wg_button_group_free(group);
    return 0;
}

static int
test_select_next_prev_rotation(void)
{
    struct WGButtonGroup *group = wg_button_group_new();

    group->select_rotates = 0;
    wg_button_group_append(group, btn1);
    wg_button_group_append(group, btn2);
    VERIFY_PTR_NULL(group->selected);

    wg_button_group_select_next(group);
    VERIFY_PTR_NOT_NULL(group->selected);
    VERIFY_STR_EQ(SELECTED_LABEL(group), "1");
    VERIFY_INT_EQ(btn1->is_selected, 1);
    VERIFY_INT_EQ(btn2->is_selected, 0);

    wg_button_group_select_next(group);
    VERIFY_PTR_NOT_NULL(group->selected);
    VERIFY_STR_EQ(SELECTED_LABEL(group), "2");
    VERIFY_INT_EQ(btn1->is_selected, 0);
    VERIFY_INT_EQ(btn2->is_selected, 1);

    wg_button_group_select_next(group);
    VERIFY_PTR_NOT_NULL(group->selected);
    VERIFY_STR_EQ(SELECTED_LABEL(group), "2");
    VERIFY_INT_EQ(btn1->is_selected, 0);
    VERIFY_INT_EQ(btn2->is_selected, 1);

    group->select_rotates = 1;
    wg_button_group_select_next(group);
    VERIFY_PTR_NOT_NULL(group->selected);
    VERIFY_STR_EQ(SELECTED_LABEL(group), "1");
    VERIFY_INT_EQ(btn1->is_selected, 1);
    VERIFY_INT_EQ(btn2->is_selected, 0);

    group->select_rotates = 0;
    wg_button_group_select_prev(group);
    VERIFY_PTR_NOT_NULL(group->selected);
    VERIFY_STR_EQ(SELECTED_LABEL(group), "1");
    VERIFY_INT_EQ(btn1->is_selected, 1);
    VERIFY_INT_EQ(btn2->is_selected, 0);

    group->select_rotates = 1;
    wg_button_group_select_prev(group);
    VERIFY_PTR_NOT_NULL(group->selected);
    VERIFY_STR_EQ(SELECTED_LABEL(group), "2");
    VERIFY_INT_EQ(btn1->is_selected, 0);
    VERIFY_INT_EQ(btn2->is_selected, 1);

    group->select_rotates = 0;
    wg_button_group_select_prev(group);
    VERIFY_PTR_NOT_NULL(group->selected);
    VERIFY_STR_EQ(SELECTED_LABEL(group), "1");
    VERIFY_INT_EQ(btn1->is_selected, 1);
    VERIFY_INT_EQ(btn2->is_selected, 0);

    wg_button_group_free(group);
    return 0;
}

static int
test_select_next_prev_skips_hidden(void)
{
    struct WGButtonGroup *group = wg_button_group_new();
    group->select_rotates = 1;

    wg_button_group_append(group, btn1);
    wg_button_group_append(group, btn2);
    wg_button_group_append(group, btn3);
    wg_button_group_append(group, btn4);

    wg_widget_set_hidden(WG_WIDGET(btn1), 1);

    wg_button_group_select_next(group);  // skips 1
    VERIFY_PTR_NOT_NULL(group->selected);
    VERIFY_STR_EQ(SELECTED_LABEL(group), "2");

    wg_widget_set_hidden(WG_WIDGET(btn1), 0);
    wg_widget_set_hidden(WG_WIDGET(btn3), 1);
    wg_widget_set_hidden(WG_WIDGET(btn4), 1);

    wg_button_group_select_next(group);  // skips 3,4 and rotates
    VERIFY_PTR_NOT_NULL(group->selected);
    VERIFY_STR_EQ(SELECTED_LABEL(group), "1");

    wg_button_group_select_prev(group);  // rotates, skips 4 and 3
    VERIFY_PTR_NOT_NULL(group->selected);
    VERIFY_STR_EQ(SELECTED_LABEL(group), "2");

    wg_widget_set_hidden(WG_WIDGET(btn1), 1);
    wg_widget_set_hidden(WG_WIDGET(btn2), 1);
    wg_button_group_select_prev(group);  // fails
    VERIFY_PTR_NULL(group->selected);

    VERIFY_INT_EQ(btn1->is_selected, 0);
    VERIFY_INT_EQ(btn2->is_selected, 0);
    VERIFY_INT_EQ(btn3->is_selected, 0);
    VERIFY_INT_EQ(btn4->is_selected, 0);

    wg_button_group_free(group);
    return 0;
}

static int
test_click_selected(void)
{
    int clicked_count = 0;
    struct WGButtonGroup *group = wg_button_group_new();
    group->select_rotates = 1;
    wg_widget_set_hidden(WG_WIDGET(btn1), 0);
    wg_widget_set_hidden(WG_WIDGET(btn2), 0);
    wg_widget_set_hidden(WG_WIDGET(btn3), 0);

    wg_button_group_append(group, btn1);
    wg_button_group_append(group, btn2);
    wg_button_group_append(group, btn3);

    wg_button_group_select_next(group);
    VERIFY_PTR_NOT_NULL(group->selected);
    VERIFY_STR_EQ(SELECTED_LABEL(group), "1");

    // click when a visible button is selected
    clicked_count = wg_button_group_click_selected(group);
    VERIFY_INT_EQ(clicked_count, 1);
    VERIFY_STR_EQ(clicked_title, "1");

    // click when a hidden button is selected
    wg_widget_set_hidden(WG_WIDGET(btn1), 1);
    wg_widget_set_hidden(WG_WIDGET(btn2), 1);
    clicked_count = wg_button_group_click_selected(group);  // skips 1, 2, clicks 3
    VERIFY_INT_EQ(clicked_count, 1);
    VERIFY_STR_EQ(clicked_title, "3");

    // click when nothing is selected
    group->selected = NULL;
    wg_widget_set_hidden(WG_WIDGET(btn2), 0);
    clicked_count = wg_button_group_click_selected(group);  // skips 1, clicks 2
    VERIFY_INT_EQ(clicked_count, 1);
    VERIFY_STR_EQ(clicked_title, "2");

    // click when nothing can be selected
    clicked_title = NULL;
    wg_widget_set_hidden(WG_WIDGET(btn1), 1);
    wg_widget_set_hidden(WG_WIDGET(btn2), 1);
    wg_widget_set_hidden(WG_WIDGET(btn3), 1);
    wg_widget_set_hidden(WG_WIDGET(btn4), 1);
    clicked_count = wg_button_group_click_selected(group);
    VERIFY_INT_EQ(clicked_count, 0);
    VERIFY_STR_NULL(clicked_title);

    wg_button_group_free(group);
    return 0;
}

int
main()
{
    btn1 = WG_BUTTON(wg_button_new_with_label("1"));
    btn2 = WG_BUTTON(wg_button_new_with_label("2"));
    btn3 = WG_BUTTON(wg_button_new_with_label("3"));
    btn4 = WG_BUTTON(wg_button_new_with_label("4"));
    WG_WIDGET(btn1)->on_mouse_click = on_click;
    WG_WIDGET(btn2)->on_mouse_click = on_click;
    WG_WIDGET(btn3)->on_mouse_click = on_click;
    WG_WIDGET(btn4)->on_mouse_click = on_click;

    RUN_TESTCASE(test_select_next_prev_empty);
    RUN_TESTCASE(test_select_next_prev_one_item);
    RUN_TESTCASE(test_select_next_prev_rotation);
    RUN_TESTCASE(test_select_next_prev_skips_hidden);
    RUN_TESTCASE(test_click_selected);

    // buttons are not freed by the group, but their layout/parent widget
    wg_widget_free(WG_WIDGET(btn1));
    wg_widget_free(WG_WIDGET(btn2));
    wg_widget_free(WG_WIDGET(btn3));
    wg_widget_free(WG_WIDGET(btn4));
    wg_log_deinit();
    return 0;
}
