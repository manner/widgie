// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "widgie/text_rendering.h"
#include "widgie/frame.h"
#include "testcommon.h"

#include <stdlib.h>

static int
test_render_text_out_of_bounds(void)
{
  struct WGRenderFrame *frame = wg_render_frame_new(1, 1, NULL);
/*
  render_text(frame, 0, 0, "Test", 12);
  render_text(frame, -10, 0, "Test", 12);
  render_text(frame, -10, -10, "Test", 12);
  render_text(frame, 0, -10, "Test", 12);
  render_text(frame, 10, 0, "Test", 12);
  render_text(frame, 10, 10, "Test", 12);
  render_text(frame, 0, 10, "Test", 12);
*/
  wg_render_frame_free(frame);
  return 0;
}

int
main()
{
  RUN_TESTCASE(test_render_text_out_of_bounds);
  wg_log_deinit();
  return 0;
}
