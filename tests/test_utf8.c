// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "widgie/utf8.h"
#include "testcommon.h"

#include <stdlib.h>

static int
test_u8_strcmpstr(void)
{
  const char *haystack = "\xef\x9b\xad Aerc";
  VERIFY_STR_EQ("Aerc", wg_u8_strcasestr(haystack, "a"));
  VERIFY_STR_EQ("Aerc", wg_u8_strcasestr(haystack, "ae"));
  VERIFY_STR_EQ("Aerc", wg_u8_strcasestr(haystack, "aer"));
  VERIFY_STR_EQ("Aerc", wg_u8_strcasestr(haystack, "aerc"));
  VERIFY_STR_EQ(haystack, wg_u8_strcasestr(haystack, "\xef\x9b\xad"));
  VERIFY_STR_NULL(wg_u8_strcasestr(haystack, "\xef"));

  VERIFY_STR_EQ("tűrő tükörfúrógép", wg_u8_strcasestr("árvíztűrő tükörfúrógép", "tűrő"));
  VERIFY_STR_NULL(wg_u8_strcasestr("árvíztűrő tükörfúrógép", "tűrö"));
  return 0;
}

static int
test_u8_strinsert(void)
{
  const char *st = "\xef\x9b\xad Aerc";
  struct WGStrInsertTestData {
    int pos;
    const char *expected;
  } data[] = {
    { 0, "|-\xef\x9b\xad Aerc" },
    { 1, "\xef\x9b\xad|- Aerc" },
    { 2, "\xef\x9b\xad |-Aerc" },
    { 3, "\xef\x9b\xad A|-erc" },
    { 4, "\xef\x9b\xad Ae|-rc" },
    { 5, "\xef\x9b\xad Aer|-c" },
    { 6, "\xef\x9b\xad Aerc|-" },
    { 999, "\xef\x9b\xad Aerc|-" },
  };

  int count = sizeof(data) / sizeof(data[0]);
  for (int i = 0; i < count; ++i)
  {
    char *result = wg_u8_strinsert(st, data[i].pos, "|-");
    VERIFY_STR_EQ(result, data[i].expected);
    VERIFY_INT_EQ(wg_u8_strlen(result), 8);
    free(result);
  }
  char *result = wg_u8_strinsert("", 0, "a");
  VERIFY_STR_EQ(result, "a");
  free(result);

  return 0;
}

static int
test_u8_strlen(void)
{
  VERIFY_INT_EQ(wg_u8_strlen("\xef\x9b\xad Aerc"), 6);
  VERIFY_INT_EQ(wg_u8_strlen("a"), 1);
  VERIFY_INT_EQ(wg_u8_strlen(""), 0);
  return 0;
}

static int
test_strarray(void)
{
  char *arr[] = { "one", "two", "three", NULL };
  VERIFY_INT_EQ(wg_strarray_len(arr), 3);

  char **arr_copy = wg_strarray_dup(arr);
  VERIFY_PTR_NOT_NULL(arr_copy);
  VERIFY_INT_EQ(wg_strarray_len(arr_copy), 3);
  VERIFY_STR_EQ(arr_copy[0], arr[0]);
  VERIFY_STR_EQ(arr_copy[1], arr[1]);
  VERIFY_STR_EQ(arr_copy[2], arr[2]);
  VERIFY_PTR_NULL(arr_copy[3]);

  wg_strarray_free(arr_copy);
  return 0;
}

static int
test_strarray_cmp(void)
{
  char *arr[] = { "one", "two", "three", NULL };
  char **arr_copy = wg_strarray_dup(arr);
  VERIFY_INT_EQ(wg_strarray_cmp(arr, arr_copy), 0);

  free(arr_copy[2]);  // second array's last item is "less"
  arr_copy[2] = strdup("th");
  VERIFY_INT_GT(wg_strarray_cmp(arr, arr_copy), 0);

  free(arr_copy[2]);  // second array has fewer elements
  arr_copy[2] = NULL;
  VERIFY_INT_GT(wg_strarray_cmp(arr, arr_copy), 0);

  arr[1] = "tw";  // first array second item is "less"
  VERIFY_INT_LT(wg_strarray_cmp(arr, arr_copy), 0);

  arr[1] = NULL;  // first array has fewer elements
  VERIFY_INT_LT(wg_strarray_cmp(arr, arr_copy), 0);

  wg_strarray_free(arr_copy);
  return 0;
}

int
main()
{
  RUN_TESTCASE(test_u8_strcmpstr);
  RUN_TESTCASE(test_u8_strinsert);
  RUN_TESTCASE(test_u8_strlen);
  RUN_TESTCASE(test_strarray);
  RUN_TESTCASE(test_strarray_cmp);
  wg_log_deinit();
  return 0;
}
