#! /bin/bash

input_file="${2:-apps/widgie-menu/example_input.txt}"
test_round_count="${1:-10}"

"${0//widgie-menu/window-open}" "${test_round_count}" "build/widgie-menu <\"$input_file\""
