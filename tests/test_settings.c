// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "widgie/settings.h"
#include "widgie/configstore.h"
#include "testcommon.h"
#include "widgie/utf8.h"

#include <limits.h>

// TODO: utf-8 testcase

static int
test_config_path(void)
{
  setenv("HOME", "/home/user", 1);
  setenv("XDG_CONFIG_HOME", "/etc/xdg_config", 1);
  char *path = wg_config_path("widgie", "conf");
  VERIFY_STR_EQ(path, "/etc/xdg_config/widgie/conf");
  free(path);

  unsetenv("XDG_CONFIG_HOME");
  path = wg_config_path("widgie", "conf");
  VERIFY_STR_EQ(path, "/home/user/.config/widgie/conf");
  free(path);

  path = wg_config_path(NULL, "conf");
  VERIFY_STR_EQ(path, "/home/user/.config/widgie/conf");
  free(path);

  path = wg_config_path(NULL, NULL);
  VERIFY_STR_EQ(path, "/home/user/.config/widgie/widgie.conf");
  free(path);

  path = wg_config_path("myapp", NULL);
  VERIFY_STR_EQ(path, "/home/user/.config/myapp/widgie.conf");
  free(path);

  setenv("HOME", "/tmp", 1);
  return 0;
}

static int
test_settings_set_defaults(void)
{
  wg_settings()->text.height = 100;
  wg_settings_set_defaults();
  VERIFY_INT_EQ(wg_settings()->text.height, 12);
  return 0;
}

static int
test_settings_load_save(void)
{
  wg_configstore_deinit();  // remove registered settings if any
  wg_settings_register();

  int rc = wg_config_save_path("/tmp/widgie.conf");  // TODO mkstemp
  VERIFY_INT_EQ(rc, 0);

  const char *expected_conf = TESTDATA "/default_widgie.conf";
  if (strcmp(LIBDIR, "/usr/local/lib") != 0) {
    #define EXPECTED_CONF "/tmp/expected_widgie.conf"
    rc = system("sed 's,/usr/local/lib," LIBDIR ",g' " TESTDATA "/default_widgie.conf >" EXPECTED_CONF);
    VERIFY_INT_EQ(rc, 0);
    expected_conf = EXPECTED_CONF;
  }
  rc = verify_file("/tmp/widgie.conf", expected_conf);
  VERIFY_INT_EQ(rc, 0);

  rc = wg_config_load_path("/tmp/widgie.conf");
  VERIFY_INT_EQ(rc, 0);

  if (unlink("/tmp/widgie.conf") != 0) {
    testlog_warning("Failed to remove temporary file /tmp/widgie.conf: %d - %s\n", errno, strerror(errno));
  }
  return 0;
}

static int
test_settings_load_save_escaping(void)
{
  wg_configstore_deinit();  // remove registered settings if any
  wg_settings_register();

  const char *str_value = "escaping;test\\";
  char *list[] = {
    "escaping,test",
    "escaping;test",
    NULL
  };

  char *empty_list[] = {
    NULL
  };

  char *one_empty_item_list[] = {
    "",
    NULL
  };

  free(wg_settings()->text.font);
  wg_settings()->text.font = strdup(str_value);
  wg_strarray_free(wg_settings()->text.font_paths);
  wg_settings()->text.font_paths = wg_strarray_dup(list);
  wg_strarray_free(wg_settings()->text.fallback_font_paths);
  wg_settings()->text.fallback_font_paths = wg_strarray_dup(one_empty_item_list);
  wg_strarray_free(wg_settings()->backend.search_paths);
  wg_settings()->backend.search_paths = wg_strarray_dup(empty_list);

  int rc = wg_config_save_path("/tmp/widgie.conf");  // TODO mkstemp
  VERIFY_INT_EQ(rc, 0);

  // We just do these to ensure they contain something different to verify that
  // loading really does something.
  wg_settings_set_defaults();
  VERIFY_STR_NEQ(wg_settings()->text.font, str_value);
  VERIFY_INT_NEQ(wg_strarray_len(wg_settings()->text.font_paths), wg_strarray_len(list));
  // backend search paths get filled with defaults:
  VERIFY_PTR_NULL(wg_settings()->backend.search_paths);

  rc = wg_config_load_path("/tmp/widgie.conf");
  VERIFY_INT_EQ(rc, 0);

  VERIFY_STR_EQ(wg_settings()->text.font, str_value);
  VERIFY_STR_EQ(wg_settings()->text.font_paths[0], list[0]);
  VERIFY_STR_EQ(wg_settings()->text.font_paths[1], list[1]);
  VERIFY_STR_NULL(wg_settings()->text.font_paths[2]);

  VERIFY_STR_EQ(wg_settings()->text.fallback_font_paths[0], one_empty_item_list[0]);
  VERIFY_STR_NULL(wg_settings()->text.fallback_font_paths[1]);
  VERIFY_STR_NULL(wg_settings()->backend.search_paths[0]);

  if (unlink("/tmp/widgie.conf") != 0) {
    testlog_warning("Failed to remove temporary file /tmp/widgie.conf: %d - %s\n", errno, strerror(errno));
  }

  return 0;
}

static int test_ini_parse_load_success = 0;
struct WGIniParseTestCase
{
  const char *exp_section;
  const char *exp_key;
  const char *exp_value;
};

static int
test_ini_parse_plain(const char *section, const char *key, const char *value, void *userdata)
{
  static int test_num = 0;
  struct WGIniParseTestCase *test = &((struct WGIniParseTestCase *)userdata)[test_num];
  VERIFY_STR_NOT_NULL(test->exp_key);
  ++test_num;

  VERIFY_STR_EQ(section, test->exp_section);
  VERIFY_STR_EQ(key, test->exp_key);
  VERIFY_STR_EQ(value, test->exp_value);

  ++test_ini_parse_load_success;
  return 0;
}

static int
test_ini_parse_load_plain(void)
{
  struct WGIniParseTestCase tests[] = {
    { "", "color", "0x222222" },
    { "", "boolean", "no" },
    { "", "string1", "value" },
    { "", "stringlist1", "First, Next, Last" },
    { "", "stringlist2", "First,Next,Last" },
    { "", "stringlist3", "F i r s t, N e x t, L a s t" },
    { "", "stringlist4", "First" },
    { "", "stringlist5", "First,\\ Next,\\ Last" },
    { "", "number1", "42" },
    { "", "number2", "-42" },
    { "", "string2", "\\\\value\\\\" },
    { "", "string3", "\\valu\\e" },
    { "", "\\key\\", "value" },
    { "", "value_contains_equality", "===" },
    { "", "key=", "key_with_equality" },
    { "", "string_comment1", "value" },
    { "", "string_comment2", "value" },
    { "", "string_comment_escaped", "\\;\\#comment" },
    { NULL, NULL, NULL },
  };

  test_ini_parse_load_success = 0;
  int rc = wg_ini_parser_load(TESTDATA "/ini_parser_plain.conf", test_ini_parse_plain, tests);
  if (rc != 0) {
    testlog_warning("ini_parser_load has failed with: %d - %s\n", rc, strerror(rc));
  }
  VERIFY_INT_EQ(rc, 0);
  VERIFY_INT_EQ(test_ini_parse_load_success, sizeof(tests) / sizeof(tests[0]) - 1);  // got all results correctly
  return 0;
}

static int
test_ini_parse_bool(void)
{
  log_verify_init();
  VERIFY_INT_EQ(wg_ini_parser_parse_bool("true"), 1);
  VERIFY_INT_EQ(wg_ini_parser_parse_bool("yes"), 1);
  VERIFY_INT_EQ(wg_ini_parser_parse_bool("1"), 1);
  VERIFY_INT_EQ(wg_ini_parser_parse_bool("false"), 0);
  VERIFY_INT_EQ(wg_ini_parser_parse_bool("no"), 0);
  VERIFY_INT_EQ(wg_ini_parser_parse_bool("0"), 0);
  VERIFY_INT_EQ(wg_ini_parser_parse_bool("unknown"), 0);
  log_verify_check("parse_bool_errors");
  return 0;
}

static int
test_ini_parse_stringlist(void)
{
  char **result = NULL;

  result = wg_ini_parser_parse_stringlist("one, two,three");
  VERIFY_INT_EQ(wg_strarray_len(result), 3);
  VERIFY_STR_EQ(result[0], "one");
  VERIFY_STR_EQ(result[1], "two");
  VERIFY_STR_EQ(result[2], "three");
  VERIFY_STR_NULL(result[3]);
  wg_strarray_free(result);

  // escaping the ',' symbol
  result = wg_ini_parser_parse_stringlist("comma\\,, comma\\,comma,\\,");
  VERIFY_INT_EQ(wg_strarray_len(result), 3);
  VERIFY_STR_EQ(result[0], "comma,");
  VERIFY_STR_EQ(result[1], "comma,comma");
  VERIFY_STR_EQ(result[2], ",");
  VERIFY_STR_NULL(result[3]);
  wg_strarray_free(result);

  // escaping the whitespace at beginning - end of value
  result = wg_ini_parser_parse_stringlist("\\ begin_end_space\\ ,\\\\");
  VERIFY_INT_EQ(wg_strarray_len(result), 2);
  VERIFY_STR_EQ(result[0], " begin_end_space ");
  VERIFY_STR_EQ(result[1], "\\");
  VERIFY_STR_NULL(result[2]);
  wg_strarray_free(result);

  // stringlist ends with ',' (last is empty)
  result = wg_ini_parser_parse_stringlist(", ,");
  VERIFY_INT_EQ(wg_strarray_len(result), 2);
  VERIFY_STR_EQ(result[0], "");
  VERIFY_STR_EQ(result[1], "");
  VERIFY_STR_NULL(result[2]);
  wg_strarray_free(result);

  // empty string means zero element count
  result = wg_ini_parser_parse_stringlist("");
  VERIFY_INT_EQ(wg_strarray_len(result), 0);
  VERIFY_STR_NULL(result[0]);
  wg_strarray_free(result);

  // one empty element
  result = wg_ini_parser_parse_stringlist(",");
  VERIFY_STR_EQ(result[0], "");
  VERIFY_STR_NULL(result[1]);
  VERIFY_INT_EQ(wg_strarray_len(result), 1);
  wg_strarray_free(result);

  return 0;
}

static int
test_ini_parse_string(void)
{
  char *result = NULL;
  result = wg_ini_parser_parse_string("simple");
  VERIFY_STR_EQ(result, "simple");
  free(result);

  result = wg_ini_parser_parse_string("\\\\");
  VERIFY_STR_EQ(result, "\\");
  free(result);

  result = wg_ini_parser_parse_string("\\\\val\\\\ue\\\\");
  VERIFY_STR_EQ(result, "\\val\\ue\\");
  free(result);

  result = wg_ini_parser_parse_string("\\v\\a\\l\\u\\e");
  VERIFY_STR_EQ(result, "value");
  free(result);
  return 0;
}

static int
test_ini_parse_numbers(void)
{
  log_verify_init();

  VERIFY_INT_EQ(wg_ini_parser_parse_int("42"), 42);
  VERIFY_INT_EQ(wg_ini_parser_parse_int("0"), 0);
  VERIFY_INT_EQ(wg_ini_parser_parse_int("-42"), -42);
  VERIFY_INT_EQ(wg_ini_parser_parse_int("invalid"), 0);
  VERIFY_INT_EQ(wg_ini_parser_parse_int("0x7fffffff"), 0x7fffffff);
  VERIFY_INT_EQ(wg_ini_parser_parse_int("-0x7fffffff"), -0x7fffffff);
  VERIFY_INT_EQ(wg_ini_parser_parse_int("0x8fffffff"), INT_MAX); // out of range
  VERIFY_INT_EQ(wg_ini_parser_parse_int("-0x8fffffff"), INT_MIN); // out of range
  VERIFY_INT_EQ(wg_ini_parser_parse_int("0765"), 0765);

  VERIFY_INT_EQ(wg_ini_parser_parse_long("0xffffffff"), 0xffffffff);
  VERIFY_INT_EQ(wg_ini_parser_parse_long("-0xffffffff"), -0xffffffff);

  log_verify_check("parse_numbers_errors");
  return 0;
}

static int
test_ini_parse_color(void)
{
  VERIFY_INT_EQ(wg_ini_parser_parse_color("0xffffff"), 0xffffff);
  return 0;
}

static struct WGConfigStore * mock_config_store_new_subgroup(struct WGConfigStore *self, const char *subgroup_name);
static int mock_config_store_set(struct WGConfigStore *self, const char *key, const char *value);
static int mock_config_store_print_example(struct WGConfigStore *self, int fd);

static char teststore_calls[1024];

struct WGConfigStore teststore = {
  .set = mock_config_store_set,
  .print_example = mock_config_store_print_example,
  .new_subgroup = mock_config_store_new_subgroup,
};

static int
mock_config_store_set(struct WGConfigStore *self, const char *key, const char *value)
{
  strcat(teststore_calls, "set('");
  strcat(teststore_calls, key == NULL ? "(null)" : key);
  strcat(teststore_calls, "','");
  strcat(teststore_calls, key == NULL ? "(null)" : value);
  strcat(teststore_calls, "')\n");
  return 0;
};

static struct WGConfigStore *
mock_config_store_new_subgroup(struct WGConfigStore *self, const char *subgroup_name)
{
  strcat(teststore_calls, "new_subgroup('");
  strcat(teststore_calls, subgroup_name);
  strcat(teststore_calls, "')\n");
  return &teststore;
};

static int
mock_config_store_print_example(struct WGConfigStore *self, int fd)
{
  return 0;  // we do not store anything
}

static int
test_config_store_subgroup(void)
{
  const char *path = "/tmp/widgie.conf";
  save_file(path,
    "[main]\n\n"
    "[[sub1]]\n\n"
    "key1=value1\n"
    "key2=value2\n\n"
    "[[sub2]]\n"
    "key3=value3\n\n"
  );
  wg_configstore_deinit();  // remove registered settings if any
  wg_configstore_register("main", &teststore);
  wg_configstore_register("other", &teststore);
  int rc = wg_config_load_path(path);
  VERIFY_STR_EQ(teststore_calls,
    "new_subgroup('sub1')\n"
    "set('key1','value1')\n"
    "set('key2','value2')\n"
    "new_subgroup('sub2')\n"
    "set('key3','value3')\n");
  VERIFY_INT_EQ(rc, 0);

  // this verifies converting the subgroup names to the more readable form:
  rc = wg_config_save_path(path);
  VERIFY_INT_EQ(rc, 0);
  save_file("/tmp/widgie_expected.conf",
    "[main]\n\n"
    "[[sub1]]\n\n"
    "[[sub2]]\n\n"
    "[other]\n\n"
  );
  rc = verify_file("/tmp/widgie_expected.conf", path);
  VERIFY_INT_EQ(rc, 0);

  return 0;
}

int
test_default_plugin_search_path(void)
{
  // with no known application path
  free(app_path);
  app_path = NULL;
  char **search_path = wg_settings_get_default_plugin_search_paths();
  VERIFY_STR_EQ(search_path[0], LIBDIR);
  VERIFY_STR_NULL(search_path[1]);
  wg_strarray_free(search_path);

  // with known application path
  app_path = strdup("/path/application");
  search_path = wg_settings_get_default_plugin_search_paths();
  VERIFY_STR_EQ(search_path[0], "/path");
  VERIFY_STR_EQ(search_path[1], LIBDIR);
  wg_strarray_free(search_path);

  return 0;
}

int
test_fallback_to_default_store()
{
  const char *path = "/tmp/widgie.conf";
  save_file(path,
    "[unknown]\n"
    "key1=value1\n\n"
  );
  wg_configstore_deinit();  // remove registered settings if any
  teststore_calls[0] = '\0';

  wg_configstore_register("", &teststore);
  int rc = wg_config_load_path(path);
  VERIFY_STR_EQ(teststore_calls,
    "set('key1','value1')\n");
  VERIFY_INT_EQ(rc, 0);

  // section gets saved with "default" label
  rc = wg_config_save_path(path);
  VERIFY_INT_EQ(rc, 0);
  save_file("/tmp/widgie_expected.conf", "[default]\n\n");
  rc = verify_file("/tmp/widgie_expected.conf", path);
  VERIFY_INT_EQ(rc, 0);

  return 0;
}

int
main()
{
  RUN_TESTCASE(test_config_path);

  RUN_TESTCASE(test_settings_load_save);
  RUN_TESTCASE(test_settings_load_save_escaping);
  RUN_TESTCASE(test_settings_set_defaults);

  RUN_TESTCASE(test_ini_parse_load_plain);
  RUN_TESTCASE(test_ini_parse_bool);
  RUN_TESTCASE(test_ini_parse_stringlist);
  RUN_TESTCASE(test_ini_parse_string);
  RUN_TESTCASE(test_ini_parse_numbers);
  RUN_TESTCASE(test_ini_parse_color);

  RUN_TESTCASE(test_config_store_subgroup);
  RUN_TESTCASE(test_default_plugin_search_path);
  RUN_TESTCASE(test_fallback_to_default_store);

  wg_configstore_deinit();
  wg_log_deinit();
  return 0;
}
