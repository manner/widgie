#! /bin/bash

set -euo pipefail

test_round_count="${1:-10}"
cmd="$2"

send_window_close() {
  swaymsg -q -t subscribe '[ "window" ]'
  wtype -M shift -M win -k c
}

test_round() {
  send_window_close &
  eval "${cmd}" &>/dev/null
  wait
}

all_test_rounds() {
  for j in $(seq "${test_round_count}"); do
    test_round
  done
}

echo "First round takes:"
time test_round
echo

echo "Executing ${test_round_count} count test rounds:"
time all_test_rounds
