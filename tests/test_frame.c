// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#include "widgie/priv/fb_frame.h"
#include "testcommon.h"

static int
test_rotate_coord(void)
{
    struct WGRenderFrame frame = {10, 20};
    int x1 = 1, x = 1, y1 = 5, y = 5;
    wg_render_frame_rotate_xy(&frame, &x, &y, 1);
    VERIFY_INT_EQ(x, 4);
    VERIFY_INT_EQ(y, 1);
    wg_render_frame_rotate_xy(&frame, &x, &y, 0);
    VERIFY_INT_EQ(x, 4);
    VERIFY_INT_EQ(y, 1);

    // unapply rotation in the reverse direction:
    x = x1;
    y = y1;
    for (int rotation = 1; rotation < 10; ++rotation) {
        wg_render_frame_rotate_xy(&frame, &x, &y, rotation);
        wg_render_frame_rotate_xy(&frame, &x, &y, - rotation);
        VERIFY_INT_EQ(x, x1);
        VERIFY_INT_EQ(y, y1);
    }

    return 0;
}

int
main()
{
    RUN_TESTCASE(test_rotate_coord);
    return 0;
}