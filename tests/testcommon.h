// License: GPL-3.0 or later
// (c) 2024 Robert Manner <rmanni@gmail.com>

#ifndef WG_HEADER_TESTCOMMON_H
#define WG_HEADER_TESTCOMMON_H

#include <widgie/log.h>

#include <string.h>
#include <stddef.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#define testlog_error WG_DEBUG_SHOW_STDERR
#define testlog_warning WG_DEBUG_SHOW_STDERR
#define testlog_info WG_DEBUG_SHOW_STDERR

#define MOVETO_PREVLINE "\033[1A"

#define CONVERT_STR_NULL(x) (x ? x : "(nil)")
#define CONVERT_NONE(x) x

#define VERIFY_SG2(x, y, op, typ1, typ2, format1, format2, print_convert1) \
  do { \
    const typ1 xx = (x); \
    const typ2 yy = (y); \
    if (!(xx op yy)) { \
      testlog_error("Expectation '" #x " " #op " " #y "' has failed: " \
                format1 " " #op " " format2 "\n", print_convert1(xx), yy); \
      return 1; \
    } \
  } while (0)

#define VERIFY_SG(x, y, op, typ, format) VERIFY_SG2(x, y, op, typ, typ, format, format, CONVERT_NONE)

#define VERIFY_STR_NULL(x) VERIFY_SG2(x, NULL, ==, char *, char *, "%s", "%p", CONVERT_STR_NULL)
#define VERIFY_PTR_NULL(x) VERIFY_SG(x, NULL, ==, void *, "%p")
#define VERIFY_STR_NOT_NULL(x) VERIFY_SG2(x, NULL, !=, char *, char *, "%s", "%p", CONVERT_STR_NULL)
#define VERIFY_PTR_NOT_NULL(x) VERIFY_SG(x, NULL, !=, void *, "%p")
#define VERIFY_INT_EQ(x, y) VERIFY_SG(x, y, ==, int, "%d")
#define VERIFY_INT_NEQ(x, y) VERIFY_SG(x, y, !=, int, "%d")
#define VERIFY_INT_GT(x, y) VERIFY_SG(x, y, >, int, "%d")
#define VERIFY_INT_LT(x, y) VERIFY_SG(x, y, <, int, "%d")

#define VERIFY_STR_SG(x, y, op) \
  do { \
    const char *xx = (x); \
    const char *yy = (y); \
    if (((xx == NULL) || (yy == NULL)) || !(strcmp(xx, yy) op 0)) { \
      testlog_error("Expectation '" #x " " #op " " #y "' has failed: " \
                "'%s' != '%s'\n", CONVERT_STR_NULL(xx), CONVERT_STR_NULL(yy)); \
      return 1; \
    } \
  } while (0)

#define VERIFY_STR_EQ(x, y) VERIFY_STR_SG(x, y, ==)
#define VERIFY_STR_NEQ(x, y) VERIFY_STR_SG(x, y, !=)

#define RUN_TESTCASE(testcase) \
  do { \
    int rc = 0; \
    testlog_info(#testcase " ... \n"); \
    if ((rc = testcase()) != 0) return rc; \
    fprintf(stderr, MOVETO_PREVLINE); \
    testlog_info(#testcase " ... passed\n"); \
  } while (0)

#define TMP_LOG_PATH "/tmp/test_log.txt"

static inline void
log_verify_init(void)
{
  wg_log_to_path(TMP_LOG_PATH, 0);
  wg_log_set_display_source(0);
}

static inline int
verify_file(const char *path, const char *expected_path)
{
  char *cmd = NULL;

  asprintf(&cmd, "diff '%s' '%s'", path, expected_path);
  int rc = system(cmd);

  if (rc != 0) {
    const char *update_env = getenv("UPDATE_TESTDATA");
    if (update_env && strcmp(update_env, "1") == 0) {
      testlog_info("Updating testdata '%s'\n", expected_path);
      free(cmd);
      asprintf(&cmd, "cp '%s' '%s'", path, expected_path);
      rc = system(cmd);
    }
  }

  VERIFY_INT_EQ(rc, 0);

  free(cmd);
  return 0;
}

static inline int
log_verify_check_(const char *path, const char *expected_path)
{
  wg_log_to_stderr();
  wg_log_set_display_source(1);

  VERIFY_INT_EQ(verify_file(path, expected_path), 0);

  if (unlink(path) != 0) {
    testlog_warning("Failed to remove temporary file " TMP_LOG_PATH ": %d - %s\n",
                    errno, strerror(errno));
  }
  return 0;
}

#define log_verify_check(refpath) \
  VERIFY_INT_EQ(log_verify_check_(TMP_LOG_PATH, TESTDATA "/" refpath), 0)

static inline int
save_file_(const char *path, const char *content)
{
  FILE *f = fopen(path, "w");
  if (f == NULL)
    return errno;
  if (fputs(content, f) == EOF)
    return errno;
  return fclose(f) == EOF ? errno : 0;
}

#define save_file(path, content) \
  VERIFY_INT_EQ(save_file_(path, content), 0)

#endif  // HEADER_TESTCOMMON_H

